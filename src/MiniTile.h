#ifndef CLASS_MINI_TILE
#define CLASS_MINI_TILE

#include "Object.h"

/// A tile of isometric minimap
class MiniTile: public Object
{
	private:

		/// pointer to the Image stored into the vector of tiles of the map
		graphic::Image * _img;

	public:
		/// constructor, just load the image and set default values
		MiniTile(graphic::Image * img) { _img = img; };
		/// destroyer
		~MiniTile() { };

		/// get image pointer of the tile
		graphic::Image * GetImage() { return _img; };
		/// set image pointer of the tile
		void SetImage(graphic::Image * img) { _img = img; };
};

#endif

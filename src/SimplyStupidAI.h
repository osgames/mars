#ifndef SIMPLYSTUPIDAI_H_
#define SIMPLYSTUPIDAI_H_

#include "AIBase.h"

/// SimplyStupid AI, attacks if unit is within range else moves towards enemies. Cheats - does not follow FoW
class SimplyStupidAI: public AIBase
{
	private:
		/// Checks if there are any enemies with in range for any unit
		bool AttackWithInRange()
		{
			#ifdef DEBUG_MODE
				printf("<SimplyStupidAI::AttackWithInRange()>\n");
			#endif
			_selected = NULL;
			while( (_selected = GetNextIdle()) != NULL)
			{
				//Find enemy player, checks from _selected
				Element * defender = GetDefender();
				if(defender) // found a victim 
				{
					while( _act_cost <= _selected->GetCurStat("actionpoints") && defender)
					{
						_m->DoAction(ACT_ATTACK);
						defender = GetDefender();
					}
					return true;
				}
			}
			// No unit within fire range select one and move it instead
			_selected = GetFirstIdle();
			if(_selected)
			{
				#ifdef DEBUG_MODE
					printf("%d\n", _selected->GetId());
				#endif
				MoveTowardsClosest();
				Unit * mover = dynamic_cast < Unit * > (_selected);
				if(mover == NULL)
					return false;
					
				while(mover->IsMoving())
					_m->DoAction(ACT_MOVE);
				
				#ifdef DEBUG_MODE
					printf("Output: True\n</SimplyStupidAI::AttackWithInRange()>\n");
				#endif
				return true;
			}
			#ifdef DEBUG_MODE
				printf("Output: False\n</SimplyStupidAI::AttackWithInRange()>\n");
			#endif
			return false;
		};

		

		
	public:
		///Defualt constructor
		SimplyStupidAI(Mission *m) : AIBase(m) { };
							
		/*
		 * Inherited from AIBase
		 * */
		virtual bool Run() 
		{ 
			#ifdef DEBUG_MODE
				printf("<SimplyStupidAI::Run()>");
			#endif
			_act_cost = 0;
			
			_selected = NULL;
			
			if(AttackWithInRange())
				LockOwnElement(_selected);
			else
				LockOwnElement(GetFirstIdle());
					
			if(GetFirstIdle()) // AI want end move, check that there are idle units
				_m->DoAction(ACT_ENDMOVE);
			else // No more idle units force end turn
				_m->DoAction(ACT_FORCEENDTURN);
				
			#ifdef DEBUG_MODE
				printf("</SimplyStupidAI::Run()>");
			#endif
			return false; 
		};
	
};

#endif /*SIMPLYSTUPIDAI_H_*/

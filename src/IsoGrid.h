#ifndef CLASS_ISO_GRID
#define CLASS_ISO_GRID

#include "graphic/Image.h"

#include "IsoTilesMap.h"
#include "Layer.h"
#include "Tile.h"

#define GRID_V_DEFAULT_OPACITY 30
#define GRID_PV_DEFAULT_OPACITY 20
#define GRID_NV_DEFAULT_OPACITY 50

/// A grid of tiles
class IsoGrid : public Layer
{
	private:
	 /// image associated to a visible tile
	 graphic::Image * _v_cell;
	 /// image associated to a partially visible tile
	 graphic::Image * _pv_cell;
	 /// image associated to a NOT visible tile
	 graphic::Image * _nv_cell;

	 /// pointer to the Isometric Tiles Map
	 IsoTilesMap * _tm;

	 /// width of the tile
	 Uint16 _tile_width;
	 /// height of the tile
	 Uint16 _tile_height;
	 /// number of rows
	 int _num_rows;
	 /// number of columns
	 int _num_cols;
	 /// side of an orthogonal tile
	 float _square_side;

	 /// origin of the map
	 IntPoint _origin;

	public:
	 /// base constructor, use a map to choose tiles
	 IsoGrid(IsoTilesMap * tm, graphic::Image * v_image, graphic::Image * pv_image, graphic::Image * nv_image);
	 /// destroyer
	 ~IsoGrid() { };

	 /// blit part of the layer on a surface, if the surface is NULL blit on screen
	 void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);

	 /// set opacity of the grid on visible tiles
	 void SetVOpacity(Uint8 opacity)	{ _v_cell->SetOpacity(opacity); };
	 /// set opacity of the grid on not visible tiles
	 void SetPVOpacity(Uint8 opacity) { _pv_cell->SetOpacity(opacity); };
	 /// set opacity of the grid on not visible tiles
	 void SetNVOpacity(Uint8 opacity) { _nv_cell->SetOpacity(opacity); };
};

#endif

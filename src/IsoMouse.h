#ifndef CLASS_ISO_MOUSE
#define CLASS_ISO_MOUSE

#include "DataTypes.h"
#include "Functions.h"
#include "IsoTilesMap.h"
#include "Mouse.h"

/// extends Mouse class adding methods and atributes related to iso management
class IsoMouse : public Mouse
{
	private:
	 /// indexes of the current cell pointed by the mouse
	 CellInd * _cur_cell;
	 /// indexes of the previous cell pointed by the mouse
	 CellInd * _prev_cell;
	 /// flag that is true when the current cell change
	 bool _changed_cell;

	 /// a side of an isometric square transformed in an orthogonal one
	 float _square_side;
	 /// number of rows of the tiles map
	 int _num_rows;
	 /// number of cols of the tiles map
	 int _num_cols;
	 /// origin of the tiles map
	 IntPoint * _origin;

	 /// orthogoanl position obtained by the screen (isometric) one
	 FloatPoint * _ortho_pos;

	 /// set current indexes of the cell under the mouse
	 void SetIndexes();

	public:
	 /// constructor
	 IsoMouse(std::vector< graphic::MovableImage * > pointers, IsoTilesMap * tm, Sint16 x = 0, Sint16 y = 0);
	 /// destroyer
	 virtual ~IsoMouse();

	 /// set the current view values
	 void SetView(Sint16 view_x, Sint16 view_y)
	 {
		Mouse::SetView(view_x, view_y);
		SetIndexes();
	 };
	 /// set the current view values passing a point
	 void SetView(IntPoint view) { SetView(view.x, view.y); };

	 /// set the position passing just the screen position
	 void SetPosition(Sint16 x, Sint16 y)
	 {
		Mouse::SetPosition(x, y);
		SetIndexes();
	 };
	 /// set the position passing just the point of screen position
	 void SetPosition(IntPoint pos) { SetPosition(pos.x, pos.y); };
	 /// set the position passing screen position and view position
	 void SetPosition(Sint16 x, Sint16 y, Sint16 view_x, Sint16 view_y)
	 {
		SetView(view_x, view_y);
		SetPosition(x, y);
	 };
	 /// set the position passing screen position as point and view position as point
	 void SetPosition(IntPoint pos, IntPoint view) { SetPosition(pos.x, pos.y, view.x, view.y); };

	 /// return true if the current change is just changed
	 bool IsCellChanged() { return _changed_cell; };

	 /// return true if the mouse is inside the isometric map
	 bool IsInsideMap();

	 /// return the current row
	 int GetRow() { return _cur_cell->row; };
	 /// return the current col
	 int GetCol() { return _cur_cell->col; };
	 /// return the current cell indexes
	 CellInd * GetCell();

	 /// return the previous row
	 int GetPrevRow() { return _prev_cell->row; };
	 /// return the previous col
	 int GetPrevCol() { return _prev_cell->col; };
	 /// return the previous cell indexes
	 CellInd * GetPrevCell();

	 /// return the absolute X iso2scr position
	 float GetAbsOrthoX() { return _ortho_pos->x; };
	 /// return the absolute Y iso2scr position
	 float GetAbsOrthoY() { return _ortho_pos->y; };
};

#endif

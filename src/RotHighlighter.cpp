#include "RotHighlighter.h"

// constructor
RotHighlighter::RotHighlighter(std::vector < graphic::Image * > images, IsoTilesMap * map)
{
	int size = images.size();

	// store pointers to images
	for(int i= 0; i < size; i++)
		_images.push_back(images[i]);

	_unit = NULL;
	
	_map = map;

	_direction = NO_DIRECTION;
	_rot_cost = 0;
}

// blit part of the layer on a surface, if the surface is NULL blit on screen
void RotHighlighter::BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf)
{
	// no unit selected or no selection to highlight
	if(_unit == NULL || _direction == NO_DIRECTION)
		return ;

	Sint16 l_limit = limits_rect.x;
	Sint16 u_limit = limits_rect.y;

	int row = _unit->GetRow();
	int col = _unit->GetCol();

	int new_ap = _unit->GetCurStat("actionpoints") - _rot_cost;

	// unit has enough AP => green highlighter
	// unit has not enough AP => red highlighter
	int offset = (new_ap < 0) ? BAD_OFFSET : GOOD_OFFSET;

	// no change in direction => yellow highlighter
	if(_direction == _unit->GetDirection())
		offset = FRONT_OFFSET;

	// temporary IntPoint
	IntPoint p;

	// position computed according to center of unit tile
	p.x = _map->GetTileX(row, col) + ((_map->GetTileW() - _images[offset + _direction]->GetW()) / 2);
	p.y = _map->GetTileY(row, col) + ((_map->GetTileH() - _images[offset + _direction]->GetH()) / 2);

	_images[offset + _direction]->SetPosition(p.x - l_limit, p.y - u_limit);
	// it's useless to set a src_rect because quoting from SDL_BlitSurface page on SDL wiki:
	// "negative dstrect coordinates will be clipped properly."

	graphic::blitter->Blit(_images[offset + _direction], dst_surf);
}

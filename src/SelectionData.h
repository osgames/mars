#ifndef CLASS_SELECTION_DATA
#define CLASS_SELECTION_DATA

#include "graphic/Image.h"
#include "graphic/Text.h"

#include "Element.h"
#include "Layer.h"

#define BAR_BORDER_IMG 0
#define BAR_EN_GOOD_IMG 1
#define BAR_EN_BAD_IMG 2
#define BAR_AP_GOOD_IMG 3
#define BAR_AP_BAD_IMG 4

#define BORDER_V_OFFSET 30

#define BARS_OFFSET 5

#define BAR_H_OFFSET 1
#define BAR_V_OFFSET 1

#define AP_OFFSET 65

#define AP_FONT "font/tahomabd.ttf"
#define AP_FONT_SIZE 13

/// this layer is used to show element stats, like energy or rank
class SelectionData : public Layer
{
	private:
		/// current selected object
		Element * _sel_elem;
		/// images used to show data of selected element
		std::vector < graphic::Image * > _images;

		/// remaining Action Points to show
		graphic::Text * _txt;

		int _remaining_ap;

		bool _show_ap;

	public:
		/// constructor
		SelectionData(std::vector < graphic::Image * > images, graphic::Text * txt);
		/// destroyer
		~SelectionData() { delete _txt; };

		/// store a pointer to the selected object
		void SetSelected(Element * e)
		{
			_sel_elem = e;
			Active();
		};
		/// set the  pointer to the selected object to NULL
		void ClearSelected()
		{
			_sel_elem = NULL;
			_show_ap = false;
			Active(false);
		};

		/// return true if an object is selected
		bool IsSelected() { return (_sel_elem != NULL); };

		/// blit part of the layer on a surface, if the surface is NULL blit on screen
		void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);

		void SetRemainingAP(int ap) { _remaining_ap = ap; };
		void ShowAP(bool s = true) { _show_ap = s; };
};

#endif

#ifndef CLASS_ISO_OBJECTS_MAP
#define CLASS_ISO_OBJECTS_MAP

#include <vector>

#include "graphic/Image.h"

#include "Map.h"
#include "Layer.h"
#include "IsoObject.h"
#include "IsoTilesMap.h"

#include "Exception.h"

// coefficient used to compute the partially visible radius
#define PV_COEFFICIENT 3

/// this class is a map for all the objects on the field
class IsoObjectsMap : public Layer
{
	private:
	 /// pointer to the Tiles Map
	 IsoTilesMap * _tm;
	 /// map of pointer to the objects
	 std::vector < IsoObject * > _objects_map;
	 /// vector containing Iso objects
	 std::vector < IsoObject * > _objects;
	 /// objects present in the current view
	 std::vector < IsoObject * > _cur_objects;
	 /// number of objects contained
	 int _num_objects;
	 /// number of rows of the map
	 int _num_rows;
	 /// number of cols of the map
	 int _num_cols;

	 /// sort objects so to blit front objects after rear objects
	 void SortObjects();

	public:
	 /// constructor
	 IsoObjectsMap(IsoTilesMap * tm);
	 /// destroyer
	 ~IsoObjectsMap() { };

	 /// blit part of the layer on a surface, if the surface is NULL blit on screen
	 void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);

	 /// add an object to the Map at the [row, col] position stored in it
	 void AddObject(IsoObject * obj) { AddObject(obj, obj->GetRow(), obj->GetCol()); };
	 /// add an object to the Map at the [row, col] position
	 void AddObject(IsoObject * obj, int row, int col);

	 /// remove an object from the map, this method does NOT delete the object
	 void RemoveObject(IsoObject * obj);

	 /// select the object at the x,y position, return the pointer if an object
	 /// has been selected, NULL otherwise
	 IsoObject * SelectObject(Sint16 x, Sint16 y);
	 /// return a pointer to the object at [row. col] cell
	 IsoObject * GetObject(int row, int col) { return _objects_map[(row * _num_cols) + col];};

	 /// move an object at the [row, col] position
	 void MoveObject(IsoObject * obj, int row, int col);
	 /// move an object at the dest_cell position
	 void MoveObject(IsoObject * obj, CellInd * dest_cell) { MoveObject(obj, dest_cell->row, dest_cell->col);};

	 /// set the visibility range of an object in accord with the view direction and its
	 /// visibility radius.
	 void ComputeObjVisibility(IsoObject * obj, int radius, int direction = ANY);

	 /// reset visibility of the IsoTilesMap
	 void ResetMapVisibility() { _tm->SetVisibility(NOT_VISIBLE); };

	 /// return the absolute X position of the image of the object on the map
	 Sint16 GetObjX(IsoObject * obj) 
	 {
		return _tm->GetTileX(obj->GetRow(), obj->GetCol());
	 };

	 /// return the absolute Y position of the image of the object on the map
	 Sint16 GetObjY(IsoObject * obj)
	 {
		return _tm->GetTileY(obj->GetRow(), obj->GetCol()) +
			   ((_tm->GetTileH() / 2) * (obj->GetNumCols() + 1)) - obj->GetH();
	 };

	 /// vertical flip of the layer
	 void VFlip();
};

#endif

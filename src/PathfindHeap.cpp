#include "PathfindHeap.h"

// add a node to the heap
void PathfindHeap::AddNode(PathfindNode * node)
{
	// push the node back to the vector
	_heap.push_back(node);
	// sort the element into the heap
	push_heap(_heap.begin(), _heap.end(), GreaterPathfindNode());

}

// return the head element and remove it from the heap
PathfindNode * PathfindHeap::GetHead()
{
	// no nodes
	if(_heap.empty())
		return NULL;

	// move the current head to the back of the vector
	pop_heap(_heap.begin(), _heap.end(), GreaterPathfindNode());

	// copy the head
	PathfindNode * head = _heap[(_heap.size() - 1)];

	// remove it from the vector
	_heap.pop_back();

	return head;
}

// return the element identified by passed id
PathfindNode * PathfindHeap::SearchNode(int id)
{
	int heap_size = _heap.size();

	// look for the node
	for(int i = 0; i < heap_size; i++)
		if(_heap[i]->GetId() == id)
			return _heap[i];

	// no right node found
	return NULL;
}

// destroy and removes all the elements from the heap
void PathfindHeap::Clear()
{
	int num_elem =  _heap.size();

	// delete all nodes
	for(int i = 0; i < num_elem; i++)
		delete _heap[i];

	_heap.clear();
}



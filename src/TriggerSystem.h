#ifndef TRIGGERSYSTEM_H_
#define TRIGGERSYSTEM_H_

#include <iostream>
#include "Actions.h"

 
template<typename T> class Singleton
{
  public:
    static T& Instance()
    {
        static T theSingleInstance;  
        return theSingleInstance;
    }
    virtual ~Singleton();                                 
};

/// Singleton class trigger system
class TriggerSystem : public Singleton<TriggerSystem>
{
	friend class Singleton<TriggerSystem>;
	private:
	
	public:
		/// Constructor
		TriggerSystem();
		/// Destructor
		~TriggerSystem();
};

#endif /*TRIGGERSYSTEM_H_*/

#include "Map.h"

// constructor
Map::Map(int num_rows, int num_cols)
{
	// setting numbers of rows and cols
	_num_rows = num_rows;
	_num_cols = num_cols;

	// setting number of cells
	_num_cells = num_rows * num_cols;

	// no objects yet
	_num_objects = 0;

	// default origin point is (0,0)
	_origin.x = 0;
	_origin.y = 0;

	// no image by default
	_graphic_origin.x = 0;
	_graphic_origin.y = 0;

	// in the beginning 0 width and height
	_width = 0;
	_height = 0;

}

// destroyer
Map::~Map()
{
	// free all created objects
	for(int i = 0; i < _num_objects; i++)
		delete _objects[i];
}

// set X coordinate of the origin point, then update all objects X value
void Map::SetOriginX(Sint16 new_x)
{
	int diff = _origin.x - new_x;

	_origin.x -= diff;
	_graphic_origin.x -= diff;

	// update all objects position
	for(int i = 0; i < _num_objects; i++)
		_objects[i]->SetXPosition(_objects[i]->GetX() - diff);
}

// set Y coordinate of the origin point, then update all objects Y value
void Map::SetOriginY(Sint16 new_y)
{
	int diff = _origin.y - new_y;

	_origin.y -= diff;
	_graphic_origin.y -= diff;

	// update all objects position
	for(int i = 0; i < _num_objects; i++)
		_objects[i]->SetYPosition(_objects[i]->GetY() - diff);
}

#include "Mouse.h"

using namespace graphic;

 // Constructor
Mouse::Mouse(std::vector< MovableImage * > pointers, Sint16 x, Sint16 y)
{
	// absolute position on the map
	_abs_position = new IntPoint;
	_abs_position->x = x;
	_abs_position->y = y;

	// position on the screen
	_screen_position = new IntPoint;
	_screen_position->x = x;
	_screen_position->y = y;

	// origin of the screen view
	_view = new IntPoint;
	_view->x = 0;
	_view->y = 0;

	// store pointers images
	int num_imgs = pointers.size();

	for(int i= 0; i < num_imgs; i++)
		_pointers.push_back(pointers[i]);

	_pointer_id = 0;

	_cur_pointer = pointers[_pointer_id];

	_cur_pointer->SetInitialPosition(x, y);

	// update mouse position in the event queue
	SDL_WarpMouse(x, y);
}

// Destroyer
Mouse::~Mouse()
{
	delete _abs_position;
	delete _screen_position;
	delete _view;
}

// Set the current view values and update position
void Mouse::SetView(Sint16 view_x, Sint16 view_y)
{
	_view->x = view_x;
	_view->y = view_y;

	// set absolute position on the map
	_abs_position->x = _screen_position->x + _view->x;
	_abs_position->y = _screen_position->y + _view->y;
}

// Set the position passing just the screen position
void Mouse::SetPosition(Sint16 x, Sint16 y)
{
	// set screen position
	_screen_position->x = x;
	_screen_position->y = y;

	// set absolute position on the map
	_abs_position->x = x + _view->x;
	_abs_position->y = y + _view->y;
}


// set the pointer image
void Mouse::SetPointer(int p)
{
	// same pointer, do nothing
	if(p == _pointer_id)
		return ;

	_pointer_id = p;

	// restore screen
	_cur_pointer->RestoreBackground();
	_cur_pointer->UpdateScreen();

	Sint16 x, y;
	x = _cur_pointer->GetX();
	y = _cur_pointer->GetY();

	// change pointer
	_cur_pointer = _pointers[p];

	// set position and save backg
	_cur_pointer->SetInitialPosition(x, y);
	_cur_pointer->SaveBackground();
}

// move and blit without restoring the background
void Mouse::MoveAndBlit()
{
	_cur_pointer->SetPosition(_screen_position->x, _screen_position->y);
	_cur_pointer->SaveBackground();
	graphic::blitter->Blit(_cur_pointer);
	_cur_pointer->UpdateScreen();
}

// restore the background, move and blit
void Mouse::UpdateAndBlit()
{
	_cur_pointer->RestoreBackground();
	_cur_pointer->SetPosition(_screen_position->x, _screen_position->y);
	_cur_pointer->SaveBackground();
	graphic::blitter->Blit(_cur_pointer);
	_cur_pointer->UpdateScreen();
}

// update the mouse position after a VFlip of the screen
void Mouse::VFlip(int screen_w, int screen_h)
{
	// set screen position
	_screen_position->x = screen_w - _screen_position->x;
	_screen_position->y = screen_h - _screen_position->y;

	// update mouse position in the event queue
	SDL_WarpMouse(_screen_position->x, _screen_position->y);
}

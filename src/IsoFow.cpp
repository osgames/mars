#include "IsoFow.h"

using namespace graphic;

// base constructor, use a map to choose tiles
IsoFow::IsoFow(IsoTilesMap * tm, Image * image)
{
	// store the new Image
	_image = image;

	_image->SetOpacity(FOW_DEFAULT_OPACITY);

	// storing IsoTilesMap pointer
	_tm = tm;

	_tile_width = _image->GetW();
	_tile_height = _image->GetH();
	_square_side = _tile_height * SQRT2;

	_num_rows = _tm->GetNumRows();
	_num_cols = _tm->GetNumCols();

	_origin.x = _tm->GetOriginX();
	_origin.y = _tm->GetOriginY();

	// dimensions of the map
	_width = _tm->GetW();
	_height = _tm->GetH();
}

// blit part of the layer on a surface, if the surface is NULL blit on screen
void IsoFow::BlitLayer(SDL_Rect limits_rect, Surface * dst_surf)
{
	Sint16 l_limit = limits_rect.x ;
	Sint16 r_limit = limits_rect.x + limits_rect.w;
	Sint16 u_limit = limits_rect.y;
	Sint16 b_limit = limits_rect.y + limits_rect.h;

	// temporary IntPoint
	IntPoint p;

	FloatPoint * start, * end;
	int s_row, s_col, e_row, e_col;

	// compute start row =>  top, right corner
	start = iso2scr(r_limit, u_limit, _origin.x, _origin.y);
	s_row = (int)(start->y / _square_side) - 1;
	if(s_row < 0)
		s_row = 0;
	delete start;

	// compute start col => top, left corner
	start = iso2scr(l_limit, u_limit, _origin.x, _origin.y);
	s_col = (int)(start->x / _square_side) - 1;
	if(s_col < 0)
		s_col = 0;
	delete start;

	// compute end row => bottom, left corner
	end = iso2scr(l_limit, b_limit, _origin.x, _origin.y);
	// row and col of the end IntPoint
	e_row = (int)(end->y / _square_side) + 1;
	if(e_row > _num_rows)
		e_row = _num_rows;
	delete end;

	// compute end col => bottom, right corner
	end = iso2scr(r_limit, b_limit, _origin.x, _origin.y);
	e_col = (int)(end->x / _square_side) + 1;
	if(e_col > _num_cols)
		e_col = _num_cols;
	delete end;

	for(int r = s_row; r < e_row; r++)
	{
		for(int c = s_col; c < e_col; c++)
		{
			p.x = _tm->GetObjectX(r,c);
			p.y = _tm->GetObjectY(r,c);

			if( (_tm->IsVisible(r, c) == PARTIALLY_VISIBLE) &&
				(p.x + _tile_width >= l_limit) && (p.x <= r_limit) &&
				(p.y + _tile_height >= u_limit) && (p.y <= b_limit) )
			{
				//set blit position of the image
				_image->SetPosition(p.x - l_limit, p.y - u_limit);

				blitter->Blit(_image, dst_surf);
			}
		}
	}	
}


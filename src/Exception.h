#ifndef CLASS_EXCEPTION
#define CLASS_EXCEPTION

#include <string>
#include <iostream>

/// This is a class used for a simple exception management
class Exception
{
	private:
	 /// name of the class that has generated the exception
	 std::string _src;

	 /// description of the error occurred
	 std:: string _error;

	public:
	 /// constructor, init attributes
	 Exception(std::string src, std::string error)
	 {
		 _src = src;
		 _error = error;
	 }

	 /// destroyer
	 ~Exception() { };

	 /// print info about the errror
	 void PrintError()
	 {
		 std::cout << "[ERROR] " << _src << " [ERROR]" << std::endl;
		 std::cout << _error << std::endl;
	 }
};

#endif

#include "AIBase.h"

Element * AIBase::GetDefender(int optimal)
{
	Unit * attacker = dynamic_cast < Unit * > (_selected);
	if(attacker)
	{
		//Find enemy player,
		for( unsigned int p = 0; p < _m->_players.size(); p++)
		{
			if(_m->_players[_m->_active_player]->GetFaction() != _m->_players[p]->GetFaction()) // found one
			{
				Element * el = NULL;
				//Check if any of enemy players units are within selected range
				while( (el = _m->_players[p]->GetNextIdleUnit(el)) != NULL )
				{
					int component = attacker->LocateString("cannonname");
					// Check cannons
					if(component != -10 && attacker->HasEnoughCurStat("ammo", 0, component))
					{
						int optdist = _m->_attacksystem->GetTilesOptimalRange(attacker, el->GetRow(), el->GetCol());
						// Return defender if weapon is within optimal range	
						if( optimal <= optdist )
						{
							// Lock unit
							LockOwnElement(attacker);
							// Set defender
							_m->_defender = el;
							// place mouse above defender, used to calculate cost
							_m->_mouse->SetPosition(el->GetX() - _m->_renderer->GetViewX(), el->GetY() - _m->_renderer->GetViewY());
							// Set attackhighlighter, used to calc cost
							_m->DoAction(ACT_SELECTCANNON);
							_act_cost = _m->_scene->GetAttCost();
							return el;
						}
					} 
					component = attacker->LocateString("rocketname");
					// Check rockets
					if(component != -10 && attacker->HasEnoughCurStat("ammo", 0, component))
					{
						int optdist = _m->_attacksystem->GetTilesOptimalRange(attacker, el->GetRow(), el->GetCol());
						// Return defender if weapon is within optimal range	
						if( optimal <= optdist )
						{
							// Lock unit
							LockOwnElement(attacker);
							// Set defender
							_m->_defender = el;
							// place mouse above defender, used to calculate cost
							_m->_mouse->SetPosition(el->GetX() - _m->_renderer->GetViewX(), el->GetY() - _m->_renderer->GetViewY());
							// Set attackhighlighter, used to calc cost
							_m->DoAction(ACT_SELECTROCKET);
							_act_cost = _m->_scene->GetAttCost();
							return el;
						}
					} 
				}						
				
			}
		}
	}
	return NULL;
}

Element * AIBase::MoveTowardsClosest()
{
	#ifdef DEBUG_MODE
		printf("<AIBase::MoveTowardsClosest()>\n");
	#endif
	Unit * attacker = dynamic_cast < Unit * > (_selected);
	if(attacker)
	{
		Element * closest = NULL;
		int col = 9999;
		int row = 9999;
		//Find enemy player
		for( unsigned int p = 0; p < _m->_players.size(); p++)
		{
			if(_m->_players[_m->_active_player]->GetFaction() != _m->_players[p]->GetFaction()) // found one
			{
				Element * el = NULL;
				//Find the closest unit
				while( (el = _m->_players[p]->GetNextIdleUnit(el)) != NULL )
				{
					if( abs(attacker->GetCol() - el->GetCol()) < col && abs(attacker->GetRow() - el->GetRow()) < row)
						closest = el;
				}
			}
		}
		//Move towards closest
		if(closest)
		{
			int distance = 1;
			int comp = attacker->LocateString("rocketname");
			if( comp != -10 && attacker->HasEnoughCurStat("ammo", 0, comp))
				distance = attacker->GetCurStat("range", comp);
			else
			{
				comp = attacker->LocateString("cannonname");
				if( comp != -10 && attacker->HasEnoughCurStat("ammo", 0, comp))
					distance = attacker->GetCurStat("range", comp);
			}
			
			
			
			_m->_path = _m->_pathfinder->MakePath(attacker->GetRow(), attacker->GetCol(),
										closest->GetRow() - distance , closest->GetCol() - distance,
										attacker->GetDirection());
			if(_m->_path)
			{
				#ifdef DEBUG_MODE
					printf("Set move %d,%d %d,%d\n", attacker->GetRow(), attacker->GetCol(), closest->GetRow() - distance , closest->GetCol() - distance);
				#endif
				attacker->StartMoving();
				_m->_selected = attacker;
				_m->_path->PopNode();
			}
		}
	}
	#ifdef DEBUG_MODE
		printf("</AIBase::MoveTowardsClosest()>\n");
	#endif
	return NULL; 
}


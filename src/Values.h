#ifndef VALUES_H_
#define VALUES_H_

#include <string>
#include <map>
#include <iostream>

#include "StatisticalValue.h"

typedef std::map<std::string, StatisticalValue * > Statsmap;
typedef std::map<std::string, std::string > Stringmap;

class Values
{
	private:
		/// Check if Stat exists and return
		bool GetStat(std::string id)
		{
			Statsmap::iterator it = _stats.find(id);
			if( it != _stats.end() )
				return true;
			return false;
		};
		
		bool GetStringStat(std::string id)
		{
			Stringmap::iterator it = _strings.find(id);
			if( it != _strings.end() )
				return true;
			return false;
		};
	
	protected:
		/// Map containing all string values (ex, descriptions)
		Stringmap _strings;
		/// Map containing all Statistical values (ex, attack values)
		Statsmap _stats;
		
	public:
		/* STATS MANAGEMENT */
		/// Return the max of a statistical value
		int GetMaxStat(std::string id) { if(GetStat(id)) return _stats[id]->GetMax(); return 0; };
		/// Set the max of a statistical value
		void SetMaxStat(std::string id, int val) { if(GetStat(id)) _stats[id]->SetMax(val); };
		/// Add a value to the max statistical value
		void AddMaxStat(std::string id, int val) { if(GetStat(id)) _stats[id]->AddMax(val); };
		/// Subtract a value from the max statistical value
		void SubMaxStat(std::string id, int val) { if(GetStat(id)) _stats[id]->SubMax(val); };
		/// Return the current  value of a statistical value
		int GetCurStat(std::string id) { if(GetStat(id)) return _stats[id]->GetCur(); return 0; };
		/// Set the current value of a statistical value
		void SetCurStat(std::string id, int val) { if(GetStat(id)) _stats[id]->SetCur(val); };
		/// Add a value to the current statistical value
		void AddCurStat(std::string id, int val) { if(GetStat(id)) _stats[id]->AddCur(val); };
		/// Subtract a value from the current statistical value
		void SubCurStat(std::string id, int val) { if(GetStat(id)) _stats[id]->SubCur(val); };
		/// Restore the current value to the max
		void RestoreCurStat(std::string id) { if(GetStat(id)) _stats[id]->RestoreCur(); };
		/// Set a string value
		void SetString(std::string id, std::string text) { if(GetStat(id)) _strings[id] = text;};
		/// Get a string value
		std::string GetString(std::string id) { if(GetStringStat(id)) return _strings[id]; return ""; };
		/// Return if the stats is shared
		bool IsStatShared(std::string id) {	if(GetStat(id))	return _stats[id]->IsShared(); return false; };
		/// Toggels a stats shared state false/true
		void ToggleStatShared(std::string id) { if(GetStat(id)) _stats[id]->ToggleShared(); };
	
};

#endif /*VALUES_H_*/

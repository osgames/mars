#ifndef CLASS_UNIT
#define CLASS_UNIT

#include "Exception.h"
#include "FactionElement.h"
#include "MovableElement.h"
#include "AnimatedElement.h"
#include "Actions.h"


/// an unit (like a mech or a tank)
class Unit : public FactionElement, public MovableElement, public AnimatedElement
{
	private:
		///Current action
		std::string _action;

	public:
		/// Constructor
		Unit(int r, int c, std::vector < graphic::Image * > images,
			std::vector <Component *> components) :
			Element(r, c, images, components),
			FactionElement(r, c, images)
		{
			_type = UNIT;
			/// Default unit action
			_action.assign(ACT_SELECTMODE);
		};

		/// Destroyer
		~Unit() { };

		/// Returns units current action
		std::string GetAction() { return _action; };
		/// Compares units action
		bool IsAction(std::string act) { return (!_action.compare(act)); };
		/// Sets a unit action
		void SetAction(std::string act) { _action.assign(act); };
		/// Switches to next std action, Move -> Rotate -> Attack -> Move...
		void NextAction()
		{ 
			if(IsAction(ACT_SELECTMODE))
				SetAction(ACT_MOVEMODE);
			else if(IsAction(ACT_MOVEMODE))
				SetAction(ACT_ROTATEMODE);
			else if(IsAction(ACT_ROTATEMODE))
				SetAction(ACT_ATTACKMODE);
			else if(IsAction(ACT_ATTACKMODE))
				SetAction(ACT_MOVEMODE);
		};
};

#endif

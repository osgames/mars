#include "Player.h"

Player::Player(int id) : PlayerContainer(id)
{
	_faction = id;

	_selected = _locked = NULL;
	
	_turn = 0;
	
	_aiplayer = NULL;
}

void Player::SetSelected(Element * e)
{
	FactionElement * fe = dynamic_cast < FactionElement * > (e);

	if((fe != NULL) && (fe->GetFaction() == _faction))
		_selected = e;
	else
		_selected = NULL;
}

void Player::SetLocked(Element * e)
{
	FactionElement * fe = dynamic_cast < FactionElement * > (e);

	if((fe != NULL) && (fe->GetFaction() == _faction))
	{
		e->Lock();
		_locked = e;
	}
}

void Player::ClearLocked()
{
	if(_locked)
		_locked->Unlock();

	_locked = NULL;
}

void Player::EndMove()
{
	if(_locked)
		_locked->SetTurn(_turn);	
}

int Player::GetIdleUnits()
{
	int nr = 0;
	for( std::vector<Element *>::iterator it = _elements.begin(); it != _elements.end(); it++ )
	{
		if((*it)->GetType() == UNIT)
		{
			if( _turn != (*it)->GetTurn())
				nr++;
		}
	}
	return nr;
}

Element * Player::GetNextIdleUnit()
{
	for( std::vector<Element *>::iterator it = _elements.begin(); it != _elements.end(); it++ )
	{
		if((*it)->GetType() == UNIT)
		{
			if( _turn != (*it)->GetTurn())
				return (*it);
		}
	}
	return NULL;
}

Element * Player::GetNextIdleUnit(Element *el)
{
	if(el == NULL)
		return GetNextIdleUnit();
		
	std::vector<Element *>::iterator it = _elements.begin();
	if(el)
		for( ; it != _elements.end(); it++ )
			if(el == (*it))
				break;
	
	if(it != _elements.end())
		it++;
	else
		return NULL;
		
	for( ; it != _elements.end(); it++ )
	{
		if((*it)->GetType() == UNIT)
		{
			if( _turn != (*it)->GetTurn())
				return (*it);
		}
	}
	return NULL;
}

void Player::SetPlayerStartView(int x, int y)
{
	// use first unit as view
	if( x == -1 || y == -1)
	{
		std::vector<Element *>::iterator it = _elements.begin();
		if( it != _elements.end() )
		{
			_view.x = (*it)->GetX() - (graphic::screen->GetW()/2);
			_view.y = (*it)->GetY() - (graphic::screen->GetH()/2);
		}
		else //No units loaded
		{
			_view.x = 1;
			_view.y = 1;
		}
	}
	else
	{
		_view.x = x;
		_view.y = y;	
	}
}




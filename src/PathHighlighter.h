#ifndef CLASS_PATH_HIGHLIGHTER
#define CLASS_PATH_HIGHLIGHTER

#include <string>
#include <vector>

#include "graphic/Image.h"
#include "graphic/Text.h"

#include "IsoTilesMap.h"
#include "DataTypes.h"
#include "Layer.h"
#include "Path.h"
#include "PathNode.h"

#define PATH_TRANS 50
// default font used to show remaining Action Points
#define RAP_FONT "font/tahomabd.ttf"
#define RAP_FONT_SIZE 13

/// Manage the graphical path highlighting, this class needs to work on a tiles map
class PathHighlighter : public Layer
{
	private:
	 /// number of rows of the map
	 int _num_rows;
	 /// number of cols of the map
	 int _num_cols;
	 /// height of a tile of the map
	 int _tile_height;
	 /// width of a tile of the map
	 int _tile_width;
	 /// graphical origin of the [0,0] tile of the map
	 IntPoint _graphic_origin;
	 /// path cells
	 Path * _path;
	 /// reamining AP to print at the end of the higlighted path
	 int _ap;

	 /// good cell higlighter image
	 graphic::Image * _good_highlighter;
	 /// bad cell higlighter image
	 graphic::Image * _bad_highlighter;
	 /// final cell higlighter image
	 graphic::Image * _final_highlighter;
	 /// remaining Action Points to show in the last cell of the path (good path)
	 graphic::Text * _txt_good;
	 /// remaining Action Points to show in the last cell of the path (bad path)
	 graphic::Text * _txt_bad;

	public:
	 /// constructor
	 PathHighlighter(IsoTilesMap * tm, graphic::Image * good, graphic::Image * bad,
							graphic::Image * final, graphic::Text * txt_good, graphic::Text * txt_bad);
	 /// destroyer
	 ~PathHighlighter() { delete _txt_good; delete _txt_bad; };

	 /// set the current path
	 void SetPath(Path * path) { _path = path; };

	 /// set the remaining Action Points to show in the last cell of the path
	 void SetRemainingAP(int ap) { _ap = ap; };

	 /// set the Text for print remaining AP in good path
	 void SetTextGood(graphic::Text * txt) { _txt_good = txt; };
	 /// set the Text for print remaining AP in good path
	 void SetTextBad(graphic::Text * txt) { _txt_bad = txt;};

	 /// blit part of the layer on a surface, if the surface is NULL blit on screen
	 void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);
};

#endif

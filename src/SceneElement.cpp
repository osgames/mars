#include "SceneElement.h"

using namespace graphic;

// Constructor
SceneElement::SceneElement(int r, int c, std::vector < Image * > views,
						   std::vector <Component *> components) : Element(r, c, components)
{
	// set all the views pointes to 0
	for(int i = 0; i < NUM_VIEWS; i++)
		_views.push_back(NULL);

	// set north and south views
	_views[NORTH] = views[0];
	_views[SOUTH] = views[1];

	// set default view
	_img = _views[SOUTH];

	 _direction = SOUTH;

	// set the type
	_type = SCENE_ELEMENT;
}

// set the image that should be blitted
void SceneElement::SetDirection(int direction)
{
	if(direction != NORTH && direction != SOUTH)
		return;

	_direction = direction;
	_img = _views[direction];
}

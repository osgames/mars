#ifndef CLASS_MOVABLE_ELEMENT
#define CLASS_MOVABLE_ELEMENT

#include "Element.h"

#define MOVE_SPEED 0.20f

/// this class introduces methods that make an element movable
class MovableElement : public virtual Element
{
	private:
	 /// this flag is set to true if the element is moving, false if not
	 bool _moving;
	 ///Last move distance, used for movable defence
	 int _moved_distance;

	public:
	 /// constructor
	 MovableElement() { _moving = false; _moved_distance = 0; };

	 /// destroyer
	 virtual ~MovableElement() { };

	 /// move the element to the [dest_x, dest_y] point
	 /// return true when the destination is reached
	 bool MoveTo(Sint16 dest_x, Sint16 dest_y, int mov);

	 /// return true if the element is moving
	 bool IsMoving() { return _moving; };
	 /// start moving, set the flag to true
	 void StartMoving() { _moving = true; _moved_distance = 0; };
	 /// stop moving, set the flag to false
	 void StopMoving() { _moving = false; };

	 /// Increase moved distance
	 void MovedOneTile() { _moved_distance++; };
	 /// Get number of moved tiles
	 int GetMoved() { return _moved_distance; };
};

#endif

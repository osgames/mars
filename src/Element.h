#ifndef CLASS_ELEMENT
#define CLASS_ELEMENT

#include <vector>
#include <map>

#include "graphic/Image.h"
#include "Attributes.h"
#include "IsoObject.h"

/* views */
#define NORTH 0
#define NORTH_EAST 1
#define EAST 2
#define SOUTH_EAST 3
#define SOUTH 4
#define SOUTH_WEST 5
#define WEST 6
#define NORTH_WEST 7
#define NO_DIRECTION 8

#define SELF -1
#define ANY 8

/* types of element */
#define SCENE_ELEMENT 0
#define BUILDING 1
#define UNIT 2

/* Pilot ranks */
#define PRIVATE		8
#define CORPORAL 	20
#define SERGEANT	36
#define LIEUTENANT	66
#define	CAPTAIN		90
#define MAJOR		116
#define ELITE		144

#define MAX_ELEMENTS 1000

/// A base class for elements that can be present on the landscape
/// DO NOT USE this class for create objects, USE its subclasses (Unit, SceneElement, Building) 
class Element : public IsoObject, public Attributes
{
	protected:
		/// images of the element
		std::vector < graphic::Image * > _views;
		/// type of the element
		int _type;
		/// Elements ID value
		int _id;
		/// flag that is true if the element is selected
		bool _selected;
		/// The latest turn the element performed an action
		int _turn;
		/// Latest turn the element got attacked
		int _attacked;
		/// True if the element is locked
		bool _locked;

	public: 
		/// Base constructor
		Element () {  };
		/// Constructor
		Element(int r, int c, Statsmap stats) : IsoObject(r, c), Attributes(stats) { };
		Element(int r, int c, std::vector <Component *> components) : IsoObject(r, c), Attributes(components) 
		{
			_selected = false;
			_locked = false;
		};
		/// Full constructor
		Element(int r, int c, std::vector < graphic::Image * > views, Statsmap stats);
		Element(int r, int c, std::vector < graphic::Image * > views, Statsmap stats,  Stringmap strings);
		Element(int r, int c, std::vector < graphic::Image * > views, std::vector <Component *> components);
		/// Destroyer
		virtual ~Element();

		/// set the image that should be blitted
		virtual void SetDirection(int direction)
		{
			#ifdef DEBUG_MODE
				printf( "<Element::SetDirection(int direction)>\nInput: %d %d\n", direction, _direction);
			#endif
			
			_direction = direction;
			if(_img->IsVirtual() )
				_img->SetVirtualView(direction);
			else
				if(direction != NO_DIRECTION)
					_img = _views[direction];
				else
					_img = _views[SOUTH];
		};
		
		/// return direction value
		int GetDirection() { return _direction; };

		/// return the type of the element
		int GetType() { return _type;};
	 
		/// Return elements ID
		int GetId() { return _id; };
		/// Sets elements ID
		void SetId(int id) { _id = id; };
		/// Return elements Player ID
		int GetOwnerId() { return (int)round(_id/MAX_ELEMENTS); };
		
		/// Return latest action turn
		int GetTurn() { return _turn; };
		/// Set latest action turn
		void SetTurn(int turn) { _turn = turn; };

		/// Return latest attacked turn
		int GetAttackedTurn() { return _attacked; };
		/// Set latest attacked turn
		void SetAttackedTurn(int attacked) { _attacked = attacked; };

		/// flip image and direction
		void Flip()
		{
			// flip direction
			_direction = (_direction + (NUM_VIEWS / 2)) % NUM_VIEWS;
			// set view
			if(_img->IsVirtual())
				_img->SetVirtualView(_direction);
			else
				_img = _views[_direction];
		};

		graphic::Image * GetImg(int ind) { return _views[ind]; };

		/// set selected flag to true
		void Select() { _selected = true; };
		/// set selected flag to false
		void Deselect() { _selected = false; };
		/// return true if the element is selected
		bool IsSelected() { return _selected; };

		/// set locked flag to true
		void Lock() { _locked = true; };
		/// set locked flag to false
		void Unlock() { _locked = false; };
		/// return true if the element is locked
		bool IsLocked() { return _locked; };

};

#endif

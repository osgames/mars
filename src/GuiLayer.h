#ifndef CLASS_GUI_LAYER
#define CLASS_GUI_LAYER

#include "Layer.h"
#include "event_system/MouseEvent.h"

#include "guichan/GuiManager.h"

/// a layer used to print the gui
class GuiLayer : public Layer
{
	private:
		/// Guimanager pointer
		gui::GuiManager *_gui;

	public:
		GuiLayer(gui::GuiManager * gui, bool graphic_mode = SDL_GRAPHIC);
		~GuiLayer() { delete _gui;};
		
		gui::GuiManager * GetGui() { return _gui; };
		
		void SetSelected(Element * el) { _gui->SetSelected(el); };
		
		/// blit part of the layer on a surface, if the surface is NULL blit on screen
		void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);
};

#endif

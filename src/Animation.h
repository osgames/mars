#ifndef ANIMATION_H
#define ANIMATION_H

#include "graphic/Image.h"

#include <string>

#define ANIM_REPLACE 0
#define ANIM_OVERLAY 1

/// Animation holder class
class Animation
{
	private:
        /// Animation type, how its blitted
        int _type;
        /// Frames per second
        int _fps;
	/// How many times the animation should play, 0 are infinit loops
	int _loops;
        /// Animation name
        std::string _name;
        /// Animation sprite
        graphic::Image * _animation;

	public:
		/// Base constructor
		Animation (std::string name, graphic::Image *anim, int type, int fps, int loops)
		{
		    _fps = fps;
		    _type = type;
		    _animation = anim;
		    _loops = loops;
		    _name = name;
        };

        /// Set image view to next frame
        int NextFrame() { return _animation->NextVirtualFrame(); };
        /// Set image view to frame
        void SetFrame(int frame) { _animation->SetVirtualFrame(frame); };
        /// Return animation type
        int GetAnimType() { return _type; };
        /// Return frames per second
        int GetFPS() { return _fps; };
        /// Return number of loops
        int GetLoops() { return _loops; };
        /// Return true if animation is called $name
        bool IsCalled(std::string name) { return !_name.compare(name); };
        /// Return animation sprite
        graphic::Image * GetAnim() { return _animation; };

};

#endif // ANIMATION_H

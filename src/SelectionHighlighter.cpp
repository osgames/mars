#include "SelectionHighlighter.h"

using namespace graphic;

// constructor
SelectionHighlighter::SelectionHighlighter(std::vector < Image * > images)
{
	int num_imgs = images.size();

	// store images pointers
	for(int i = 0; i < num_imgs; i++)
		_images.push_back(images[i]);

	// no obj selected by default
	_sel_elem = NULL;

	_usable = false;
}

// blit part of the layer on a surface, if the surface is NULL blit on screen
void SelectionHighlighter::BlitLayer(SDL_Rect limits_rect, Surface * dst_surf)
{
	// no elem selected
	if(_sel_elem == NULL)
		return ;

	Sint16 l_limit = limits_rect.x;
	Sint16 r_limit = limits_rect.x + limits_rect.w;
	Sint16 u_limit = limits_rect.y;
	Sint16 b_limit = limits_rect.y + limits_rect.h;

	Image * obj_img = _sel_elem->GetImage();
	Image * corner;

	// temporary IntPoint
	IntPoint p;

	int img_base = 0;

	if(_sel_elem->IsLocked())
		img_base = LOCKED_IMGS_GAP;
	else
		img_base = 0;

	// top left corner
	corner = _images[img_base + 0];

	if(_usable)
		corner->SetOpacity(SEL_USABLE_ALPHA);
	else
		corner->SetOpacity(SEL_NOT_USABLE_ALPHA);

	p.x = _sel_elem->GetX() + obj_img->GetVisibleX() - H_SPACE;
	p.y = _sel_elem->GetY() + obj_img->GetVisibleY() - V_SPACE;

	if((p.x + corner->GetW() >= l_limit) && (p.y + corner->GetH() >= u_limit))
	{
		corner->SetPosition(p.x - l_limit, p.y - u_limit);
		blitter->Blit(corner, dst_surf);
	}

	// top right corner
	corner = _images[img_base + 1];

	if(_usable)
		corner->SetOpacity(SEL_USABLE_ALPHA);
	else
		corner->SetOpacity(SEL_NOT_USABLE_ALPHA);

	p.x = _sel_elem->GetX() + obj_img->GetVisibleW() - corner->GetW() + H_SPACE;

	if((p.x <= r_limit) && (p.y + corner->GetH() >= u_limit))
	{
		corner->SetPosition(p.x - l_limit, p.y - u_limit);
		blitter->Blit(corner, dst_surf);
	}

	// bottom left corner
	corner = _images[img_base + 2];

	if(_usable)
		corner->SetOpacity(SEL_USABLE_ALPHA);
	else
		corner->SetOpacity(SEL_NOT_USABLE_ALPHA);

	p.x = _sel_elem->GetX() + obj_img->GetVisibleX() - H_SPACE;
	p.y = _sel_elem->GetY() + obj_img->GetVisibleH() - corner->GetH() + V_SPACE;

	if((p.x + corner->GetW() >= l_limit) && (p.y <= b_limit))
	{
		corner->SetPosition(p.x - l_limit, p.y - u_limit);
		blitter->Blit(corner, dst_surf);
	}

	// bottom right corner
	corner = _images[img_base + 3];

	if(_usable)
		corner->SetOpacity(SEL_USABLE_ALPHA);
	else
		corner->SetOpacity(SEL_NOT_USABLE_ALPHA);

	p.x = _sel_elem->GetX() + obj_img->GetVisibleW() - corner->GetW() + H_SPACE;

	if((p.x <= r_limit) && (p.y <= b_limit))
	{
		corner->SetPosition(p.x - l_limit, p.y - u_limit);
		blitter->Blit(corner, dst_surf);
	}
}


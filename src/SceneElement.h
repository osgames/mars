#ifndef CLASS_SCENE_ELEMENT
#define CLASS_SCENE_ELEMENT

#include "Element.h"

/// A scene element (like a rock or a ruin)
class SceneElement : public Element
{
	public:
	 /// Constructor
	 SceneElement(int r, int c, std::vector < graphic::Image * > views,
				  std::vector <Component *> components);

	 /// Destroyer
	 ~SceneElement() { };

	 /// set the image that should be blitted
	 void SetDirection(int direction);
};

#endif

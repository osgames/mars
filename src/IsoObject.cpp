#include "IsoObject.h"

using namespace graphic;

// base object constructor, just set row and col
IsoObject::IsoObject(int r, int c)
{
	_num_rows = r;
	_num_cols = c;
}

// IsoObject constructor, set row and col and the object image
IsoObject::IsoObject(int r, int c, Image * img)
{
	_num_rows = r;
	_num_cols = c;

	_img = img;
}

// check if ob1 is ahead to ob2
bool operator>(IsoObject & ob1, IsoObject & ob2)
{
	// back row
	int ob1_row_b = ob1.GetRow() - ob1.GetNumRows() + 1;
	// ahead row
	int ob1_row_a = ob1.GetRow();
	// left col
	int ob1_col_l = ob1.GetCol();

	// back row
	int ob2_row_b = ob2.GetRow() - ob2.GetNumRows() + 1;
	// ahead row
	int ob2_row_a = ob2.GetRow();
	// left col
	int ob2_col_l = ob2.GetCol();

	// case I: one row collides
	if(	(ob1_row_b >= ob2_row_b && ob1_row_b <= ob2_row_a) ||
		(ob1_row_a >= ob2_row_b && ob1_row_a <= ob2_row_a) ||
		(ob2_row_b >= ob1_row_b && ob2_row_b <= ob1_row_a) ||
		(ob2_row_a >= ob1_row_b && ob2_row_a <= ob1_row_a)	)
			return ob1_col_l > ob2_col_l;

	// case II: no rows collide
	return ob1_row_a > ob2_row_a;
}

bool operator<(IsoObject & ob1, IsoObject & ob2)
{
		// back row
	int ob1_row_b = ob1.GetRow() - ob1.GetNumRows() + 1;
	// ahead row
	int ob1_row_a = ob1.GetRow();
	// left col
	int ob1_col_l = ob1.GetCol();

	// back row
	int ob2_row_b = ob2.GetRow() - ob2.GetNumRows() + 1;
	// ahead row
	int ob2_row_a = ob2.GetRow();
	// left col
	int ob2_col_l = ob2.GetCol();

	// case I: one row collides
	if(	(ob1_row_b >= ob2_row_b && ob1_row_b <= ob2_row_a) ||
			(ob1_row_a >= ob2_row_b && ob1_row_a <= ob2_row_a) ||
			(ob2_row_b >= ob1_row_b && ob2_row_b <= ob1_row_a) ||
			(ob2_row_a >= ob1_row_b && ob2_row_a <= ob1_row_a)	)
		return ob1_col_l < ob2_col_l;

	// case II: no rows collide
	return ob1_row_a < ob2_row_a;
}

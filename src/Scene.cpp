#include "Scene.h"

using namespace graphic;

// Scene contructor needs maps dimensions [rows x cols] and the terrain kind
Scene::Scene(int rows, int cols, int terrain, gui::GuiManager *gui, AttackSystem *attsys)
{
	SDL_Color magenta = {0xFF, 0, 0xFF, 0};
	SDL_Color yellow = {0xFF, 0xFF, 0, 0};
	SDL_Color orange = {0xFF, 0xCC, 0, 0};

	Image * img, * img2, * img3;
	Text * txt, * txt2;

	// the current graphic mode (SDL or GL)
	bool g_mode = screen->GetGraphicMode();

	// -- ISO TILES MAP --
	// select tileset to load
	std::string dir;
	if(terrain == TERR_1)
		dir = data_dir + "img/tiles/128x64/terrain_01/";
	else if(terrain == TERR_2)
		dir = data_dir + "img/tiles/128x64/terrain_02/";
	else if(terrain == TERR_3)
		dir = data_dir + "img/tiles/128x64/terrain_03/";
	
	std::string dir_mm = data_dir + "img/tiles/minimap/";

	// load the tileset of the map
	TileSet * ts = new TileSet(dir, &magenta, g_mode);
	TileSet * ts_mm = new TileSet(dir_mm, &magenta, g_mode);

	// random generated tiles map
	IsoTilesMap * tm = new IsoTilesMap(rows, cols, ts);

	_layers.push_back(tm);
	// -- ISO TILES MAP --

	// -- Tile Highlighter Layer --
	try
	{
		img = gtracker->GetImage((data_dir + "img/tiles/128x64/black.png").c_str(), magenta, g_mode);
		img->SetOpacity(TILE_H_TRANS);
		img2 = gtracker->GetImage((data_dir + "img/tiles/128x64/red.png").c_str(), magenta, g_mode);
		img2->SetOpacity(TILE_H_TRANS);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}

	TileHighlighter * tileh = new TileHighlighter(tm, img, img2);

	_layers.push_back(tileh);
	// -- Tile Highlighter Layer --

	// -- ISO FOW --
	try
	{
		img = gtracker->GetImage((data_dir + "img/tiles/128x64/black.png").c_str(), magenta, g_mode);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}

	IsoFow * fow = new IsoFow(tm, img);

	_layers.push_back(fow);
	// -- ISO FOW --

	// -- ROTATION HIGHLIGHTER --
	std::vector< Image * > roth_images;
	try
	{
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/01.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/02.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/03.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/04.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/05.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/06.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/07.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/08.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/11.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/12.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/13.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/14.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/15.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/16.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/17.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/18.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/21.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/22.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/23.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/24.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/25.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/26.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/27.png").c_str(), g_mode);
		roth_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/rotation/28.png").c_str(), g_mode);
		roth_images.push_back(img);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}
	RotHighlighter * rot_h = new RotHighlighter(roth_images, tm);

	_layers.push_back(rot_h);
	// -- ROTATION HIGHLIGHTER --

// -- ATTACK HIGHLIGHTER --
	std::vector< Image * > attack_images;
	try
	{
		img = gtracker->GetImage((data_dir + "img/tiles/128x64/green.png").c_str(), magenta, g_mode);
		attack_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/tiles/128x64/greenyellow.png").c_str(), magenta, g_mode);
		img->SetOpacity(PATH_TRANS);
		attack_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/tiles/128x64/yellow.png").c_str(), magenta, g_mode);
		attack_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/tiles/128x64/orange.png").c_str(), magenta, g_mode);
		img->SetOpacity(PATH_TRANS);
		attack_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/tiles/128x64/red.png").c_str(), magenta, g_mode);
		attack_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/tiles/128x64/black.png").c_str(), magenta, g_mode);
		img->SetOpacity(PATH_TRANS);
		attack_images.push_back(img);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}
	AttHighlighter * att_h = new AttHighlighter(attack_images, tm);
	att_h->SetAttackSystem(attsys);
	_layers.push_back(att_h);
	// -- ATTACK HIGHLIGHTER --

	// -- ISO GRID --
	try
	{
		img = gtracker->GetImage((data_dir + "img/grids/128x64/black_grid.png").c_str(), magenta, g_mode);
		img2 = gtracker->GetImage((data_dir + "img/grids/128x64/white_grid.png").c_str(), magenta, g_mode);
		img3 = gtracker->GetImage((data_dir + "img/grids/128x64/white_grid.png").c_str(), magenta, g_mode);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}

	IsoGrid * grid = new IsoGrid(tm, img, img2, img3);

	_layers.push_back(grid);
	// -- ISO GRID --

	// -- PATH HIGHLIGHTER --
	try
	{
		img = gtracker->GetImage((data_dir + "img/tiles/128x64/green.png").c_str(), magenta, g_mode);
		img2 = gtracker->GetImage((data_dir + "img/tiles/128x64/red.png").c_str(), magenta, g_mode);
		img3 = gtracker->GetImage((data_dir + "img/tiles/128x64/yellow.png").c_str(), magenta, g_mode);
		txt = new Text((data_dir + RAP_FONT).c_str(), yellow, RAP_FONT_SIZE, g_mode);
		txt2 = new Text((data_dir + RAP_FONT).c_str(), orange, RAP_FONT_SIZE, g_mode);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}
	PathHighlighter * path_h = new PathHighlighter(tm, img, img2, img3, txt, txt2);

	_layers.push_back(path_h);
	// -- PATH HIGHLIGHTER --

	// -- OBJECTS MAP --
	IsoObjectsMap * om = new IsoObjectsMap(tm);

	_layers.push_back(om);
	// -- OBJECTS MAP --

	// -- SELECTION HIGHLIGHTER --
	std::vector< Image * > sel_h_images;
	try
	{
		img = gtracker->GetImage((data_dir + "img/highlighter/selection/01.png").c_str(), magenta, g_mode);
		sel_h_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/selection/02.png").c_str(), magenta, g_mode);
		sel_h_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/selection/03.png").c_str(), magenta, g_mode);
		sel_h_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/selection/04.png").c_str(), magenta, g_mode);
		sel_h_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/selection/11.png").c_str(), magenta, g_mode);
		sel_h_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/selection/12.png").c_str(), magenta, g_mode);
		sel_h_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/selection/13.png").c_str(), magenta, g_mode);
		sel_h_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/highlighter/selection/14.png").c_str(), magenta, g_mode);
		sel_h_images.push_back(img);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}
	SelectionHighlighter * sel_h = new SelectionHighlighter(sel_h_images);

	_layers.push_back(sel_h);
	// -- SELECTION HIGHLIGHTER --

	// -- SELECTION DATA --
	std::vector< Image * > sdata_images;
	try
	{
		img = gtracker->GetImage((data_dir + "img/sel_data/ebar_border.png").c_str(), magenta, g_mode);
		sdata_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/sel_data/ebar_good.png").c_str(), g_mode);
		sdata_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/sel_data/ebar_bad.png").c_str(), g_mode);
		sdata_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/sel_data/apbar_good.png").c_str(), g_mode);
		sdata_images.push_back(img);
		img = gtracker->GetImage((data_dir + "img/sel_data/ebar_bad.png").c_str(), g_mode);
		sdata_images.push_back(img);

		txt = new Text((data_dir + AP_FONT).c_str(), yellow, AP_FONT_SIZE, g_mode);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}
	SelectionData * sel_data = new SelectionData(sdata_images, txt);

	_layers.push_back(sel_data);
	// -- SELECTION DATA --

	// -- INFO TXT --
	SDL_Color txt_color = {0xFF, 0XFF, 0xFF, 0};

	InfoText * info = new InfoText(INFO_FONT, txt_color, tm, INFO_DIM_FONT, g_mode);

	_layers.push_back(info);
	// -- INFO TXT --
	
	// -- Gui Layer --
	GuiLayer * guilayer = new GuiLayer(gui);
	_layers.push_back(guilayer);
	
	gui->SetIsoMiniMap(new IsoMiniMap(tm, om, ts_mm));
	// -- Gui Layer --

	 _orientation = ORIENTATION_NS;
}

Scene::~Scene()
{
	// restore N->S orientation
	if(_orientation == ORIENTATION_SN)
		VFlip();

	for(int i = 0; i < (int) _layers.size(); i++)
		delete _layers[i];

	_layers.clear();
}

void Scene::BindRenderer(SceneRenderer * renderer)
{
	// add the tiles map
	_layers[LAYER_TM]->UseBuffer();
	renderer->AddLayer(_layers[LAYER_TM]);
	// add tile highlight layer
	renderer->AddLayer(_layers[LAYER_TILE_H]);
	// add the fow 
	_layers[LAYER_FOW]->UseBuffer();
	renderer->AddLayer(_layers[LAYER_FOW]);
	// add the rotation higlighter
	_layers[LAYER_ROT_H]->Active(false);
	renderer->AddLayer(_layers[LAYER_ROT_H]);
	// add the attack higlighter
	_layers[LAYER_ATT_H]->Active(false);
	renderer->AddLayer(_layers[LAYER_ATT_H]);
	// add the grid
	_layers[LAYER_GRID]->UseBuffer();
	renderer->AddLayer(_layers[LAYER_GRID]);
	// add path highlighter
	_layers[LAYER_PATH_H]->Active(false);
	renderer->AddLayer(_layers[LAYER_PATH_H]);
	// add objet map
	renderer->AddLayer(_layers[LAYER_OM]);
	// add selection highlighter
	_layers[LAYER_SEL_H]->Active(false);
	renderer->AddLayer(_layers[LAYER_SEL_H]);
	// add selection data
	_layers[LAYER_SEL_DATA]->Active(false);
	renderer->AddLayer(_layers[LAYER_SEL_DATA]);
	// add info txt
	_layers[LAYER_INFO]->Active(false);
	renderer->AddLayer(_layers[LAYER_INFO]);
	// add Gui Layer
	renderer->AddLayer(_layers[LAYER_GUI]);

	// init the buffer -> first blit
	renderer->RequestBufferRefresh();
}

void Scene::VFlip()
{
	(static_cast < IsoTilesMap * > (_layers[LAYER_TM]))->VFlip();
	(static_cast < IsoObjectsMap * > (_layers[LAYER_OM]))->VFlip();

	// switch orientation value
	_orientation = !(_orientation);
}

void Scene::SetSelected(Element * elem,  bool usable)
{
	(static_cast < SelectionHighlighter * > (_layers[LAYER_SEL_H]))->SetSelected(elem, usable);
	(static_cast < SelectionData * > (_layers[LAYER_SEL_DATA]))->SetSelected(elem);
	(static_cast < AttHighlighter * > (_layers[LAYER_ATT_H]))->SetSelected(elem);
	(static_cast < GuiLayer * > (_layers[LAYER_GUI]))->SetSelected(elem);
}

void Scene::ClearSelected()
{
	(static_cast < SelectionHighlighter * > (_layers[LAYER_SEL_H]))->ClearSelected();
	(static_cast < SelectionData * > (_layers[LAYER_SEL_DATA]))->ClearSelected();

	DisableRotHighlighter();
	DisableAttHighlighter();
}

void Scene::SetRotHighlighter(Element * e, Sint16 mouse_x, Sint16 mouse_y)
{
	Unit * unit = dynamic_cast < Unit * > (e);
	IsoTilesMap * tm = static_cast < IsoTilesMap * > (_layers[LAYER_TM]);

	if(!unit)
		return;

	FloatPoint * f_origin = iso2scr(tm->GetTileX(unit->GetRow(), unit->GetCol()) + (tm->GetTileW() / 2),
						tm->GetTileY(unit->GetRow(), unit->GetCol()) + (tm->GetTileH() / 2),
						tm->GetOriginX(),  tm->GetOriginY());

	// get orientation
	int dir = RelDirection((int)round(f_origin->x), (int)round(f_origin->y),
									mouse_x, mouse_y);

	int rot_cost;
	int rot_diff = abs(dir - unit->GetDirection());

	// compute rotation cost
	if(rot_diff <= (NUM_VIEWS / 2))
		rot_cost = ROTATION_COST * rot_diff;
	else
		rot_cost = ROTATION_COST * (NUM_VIEWS - rot_diff);

	_layers[LAYER_ROT_H]->Active();

	(static_cast < RotHighlighter * > (_layers[LAYER_ROT_H]))->SetSelected(unit);
	(static_cast < RotHighlighter * > (_layers[LAYER_ROT_H]))->SetDirection(dir);
	
	(static_cast < RotHighlighter * > (_layers[LAYER_ROT_H]))->SetRotCost(rot_cost);

	int rem_ap = unit->GetCurStat("actionpoints") - rot_cost;
	(static_cast < SelectionData * > (_layers[LAYER_SEL_DATA]))->SetRemainingAP(rem_ap);
	(static_cast < SelectionData * > (_layers[LAYER_SEL_DATA]))->ShowAP();

	delete f_origin;
}

void Scene::DisableRotHighlighter()
{
	(static_cast < RotHighlighter * > (_layers[LAYER_ROT_H]))->ClearSelected();
	(static_cast < SelectionData * > (_layers[LAYER_SEL_DATA]))->ShowAP(false);
}

void Scene::SetAttHighlighter(Element * e, IsoMouse * mouse)
{
	mouse->SetPointer(POINTER_ATTACK_1);
	
	Unit * unit = dynamic_cast < Unit * > (e);
	IsoTilesMap * tm = static_cast < IsoTilesMap * > (_layers[LAYER_TM]);
	int mouse_x = (int)round(mouse->GetAbsOrthoX());
	int mouse_y = (int)round(mouse->GetAbsOrthoY());

	if(!unit)
		return;

	FloatPoint * f_origin = iso2scr(tm->GetTileX(unit->GetRow(), unit->GetCol()) + (tm->GetTileW() / 2),
						tm->GetTileY(unit->GetRow(), unit->GetCol()) + (tm->GetTileH() / 2),
						tm->GetOriginX(),  tm->GetOriginY());

	// get orientation
	int dir = RelDirection((int)round(f_origin->x), (int)round(f_origin->y),
									mouse_x, mouse_y);
						
	int rot_diff = abs(dir - unit->GetDirection());
	int rot_cost = 0;
	
	// compute rotation cost
	if(rot_diff <= (NUM_VIEWS / 2))
		rot_cost = ROTATION_COST * rot_diff;
	else
		rot_cost = ROTATION_COST * (NUM_VIEWS - rot_diff);

	int att_cost = ((unit->GetCurStat("actioncost") + rot_cost) * ((static_cast < AttHighlighter * > (_layers[LAYER_ATT_H]))->GetAttackSystem())->GetAttackCostMultiplier()) / 100 ;

	_layers[LAYER_ATT_H]->Active();
	
	(static_cast < AttHighlighter * > (_layers[LAYER_ATT_H]))->SetMouse(mouse);

	(static_cast < AttHighlighter * > (_layers[LAYER_ATT_H]))->SetSelected(unit);
	if(mouse_x || mouse_y)
		(static_cast < AttHighlighter * > (_layers[LAYER_ATT_H]))->SetDirection(dir);
	else
		(static_cast < AttHighlighter * > (_layers[LAYER_ATT_H]))->SetDirection(unit->GetDirection());
	
	(static_cast < AttHighlighter * > (_layers[LAYER_ATT_H]))->SetAttCost(att_cost);

	int rem_ap = unit->GetCurStat("actionpoints") - att_cost;
	(static_cast < SelectionData * > (_layers[LAYER_SEL_DATA]))->SetRemainingAP(rem_ap);
	(static_cast < SelectionData * > (_layers[LAYER_SEL_DATA]))->ShowAP();
	
	//Change mouse pointer depening on what target it hover on
	Element * hover_target = SelectObject(mouse->GetAbsX(), mouse->GetAbsY());
	if(hover_target)
	{
		//friendly target
		if( hover_target->GetOwnerId() == e->GetOwnerId())
			mouse->SetPointer(POINTER_ATTACK_3);
		//enemy target
		else
			mouse->SetPointer(POINTER_ATTACK_2);
	}
	// No target
	else
		mouse->SetPointer(POINTER_ATTACK_1);
		
	delete f_origin;
}

void Scene::DisableAttHighlighter()
{
	(static_cast < RotHighlighter * > (_layers[LAYER_ATT_H]))->ClearSelected();
	(static_cast < SelectionData * > (_layers[LAYER_SEL_DATA]))->ShowAP(false);
	_layers[LAYER_ATT_H]->Active(false); 
}

void Scene::SetPathHighlighter(Path * path, int ap)
{
	(static_cast < PathHighlighter * > (_layers[LAYER_PATH_H]))->SetPath(path);
	(static_cast < PathHighlighter * > (_layers[LAYER_PATH_H]))->SetRemainingAP(ap);
	
	_layers[LAYER_PATH_H]->Active();
}

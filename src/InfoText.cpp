#include "InfoText.h"

using namespace graphic;
using namespace std;

InfoText::InfoText(string font, SDL_Color color, IsoTilesMap * map, int size, bool graphic_mode)
{
	try
	{
		_txt = new Text((data_dir + font).c_str(), color, size, graphic_mode);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}
	_timer = -1;
	_map = map;
}


// blit part of the layer on a surface, if the surface is NULL blit on screen
void InfoText::BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf)
{
	Sint16 l_limit = limits_rect.x;
	Sint16 u_limit = limits_rect.y;
	
	int row = _unit->GetRow();
	int col = _unit->GetCol();
	
		// temporary IntPoint
	IntPoint p;
	Image * obj_img = _unit->GetImage();

	// position computed according to center of unit tile
	p.x = _map->GetTileX(row, col) + (_map->GetTileW())/2 - l_limit;
	p.y = _unit->GetY() + obj_img->GetVisibleY() - u_limit - INFO_SPACE;
	
	_txt->WriteText(_message, p.x, p.y, TEXT_CENTER, dst_surf);
}


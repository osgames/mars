/* Mars, Land of No Mercy 0.3.0-svn - http://www.marsnomercy.org */

    // The following comment will appear in the doxygen main page.
    /**
     * @mainpage
     * @section Introduction
     * This game is released under GPL license, you can find a copy of this license here: http://www.opensource.org/licenses/gpl-license.php 
     * @n
     * @n
     * This documentation is updated to latest svn every night.
     */

#include <sys/stat.h>
#include <errno.h>

#include <SDL_mixer.h>

#include "event_system/InputManager.h"

#include "graphic/Blitter.h"
#include "graphic/GraphicTracker.h"
#include "graphic/Screen.h"

#include "Mission.h"
#include "Player.h"
#include "PlayerContainer.h"
#include "SceneContainer.h"
#include "Timer.h"
#include "SoundTracker.h"
#include "TriggerSystem.h"

#include "xml/XmlMission.h"

#include "AIBase.h"
#include "SimplyStupidAI.h"

// default data directory
#ifndef DATA_DIR
	#define DATA_DIR "data/"
#endif

// map dimensions
#define ROWS 50
#define COLS 50

using namespace std;
using namespace graphic;
using namespace xml;

Screen * graphic::screen;
GraphicTracker * graphic::gtracker;
Blitter * graphic::blitter;
SoundTracker * sounds;

// timer used for frame control
Timer * timer;

// global directory strings
string data_dir;
string home_dir;
string screens_dir;

int main(int argc, char **argv)
{
	#ifdef DEBUG_MODE
		printf( "<Mars main>\nInput: \n");
	#endif
	
	// init SDL
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO);
	SDL_EnableUNICODE(1);
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
	
	// init SDL_ttf
	TTF_Init();
	// when exit, execute SDL_Quit and TTF_Quit to restore everything
	atexit(TTF_Quit);
	atexit(SDL_Quit);

	// initializing a data directory string
	data_dir = DATA_DIR;
	cout << "data directory is: " << data_dir << endl;
	// creating a home directory if it does not exist already
	#ifndef WIN32
	home_dir = getenv("HOME") + string("/.mars-lonm");
	#else
	home_dir = DATA_DIR;
	#endif
	cout << "home directory is: " << home_dir << endl;

	#ifndef WIN32
	if(!mkdir(home_dir.c_str(), 00755))
		cout << "Created " << home_dir << endl;
	else
		perror(string("mkdir " + home_dir).c_str());
	#endif

	// creating the home directory tree
	screens_dir = home_dir + "/screens";

	#ifndef WIN32
	if (!mkdir(screens_dir.c_str(), 00755))
		cout << "Created " << screens_dir << endl;
	else
		perror(string("mkdir " + screens_dir).c_str());
	#endif

	try
	{
		// init screen
		screen = new Screen(RIS1_W, RIS1_H, SDL_SWSURFACE | SDL_RESIZABLE);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}

	// turn the system cursor off
	SDL_ShowCursor(SDL_DISABLE);

	// a new blitter
	blitter = new Blitter();

	// a new grahic tracker
	gtracker = new GraphicTracker();
	
	// a new sound tracker
	sounds = new SoundTracker();
	
	// options (resolution & tileset)
	Options opt;
	// default resolution is 800x600
	opt.ris.w = RIS1_W;
	opt.ris.h = RIS1_H;

	// input manager for events management
	InputManager im;

	// load data
	SceneContainer * elements;
	Player * p1;
	Player * p2;

	// An AI
	AIBase * ai;

	// set up a mission
	Mission * m;

	// timer used for frame control
	timer = new Timer();

	bool game = true;

	while(game)
	{
		sounds->PlayMusic(data_dir + "music/TheBattleMarch_I_mixdown_01.ogg");
		// SFX and Music are autoloaded the first time they are played, preloading might be needed on slow systems
		// -- example of preloading -- //
		// sounds->AddSfx(data_dir + "sfx/cannon.ogg");

		try
		{
			// start menu
			menu(opt);
			// exit
			if(!(opt.choice))
				break;
		}
		catch(Exception e)
		{
			e.PrintError();
			exit(-1);
		}
			
		sounds->StopMusic();

		elements = new SceneContainer(F_NO_FACTION);
		p1 = new Player(F_NGG);
		p2 = new Player(F_SWA);

		//TODO tmp solution until we have more the just missions
		std::string missionpath;
		if(opt.choice == 1)
			missionpath = "xml/Twoplayer.xml";
		else if(opt.choice == 2)
			missionpath = "xml/PlayervsAI.xml";
		else if(opt.choice == 3)
			missionpath = "xml/Tutorial.xml";
			
		XmlMission * xmlm;
		try
		{
			xmlm = new XmlMission(data_dir + missionpath);
			
			elements->CreateElements(data_dir + xmlm->GetStringSetting("elements"));
			p1->LoadMissionElements(data_dir + xmlm->GetStringSetting("player1"));
			p2->LoadMissionElements(data_dir + xmlm->GetStringSetting("player2"));
	
			m = new Mission(xmlm->GetIntSetting("col"), xmlm->GetIntSetting("row"), xmlm->GetIntSetting("tile"));
			m->AddElements(elements);
			m->AddPlayer(p1);
			m->AddPlayer(p2);
			p1->SetPlayerStartView();
			p2->SetPlayerStartView();
			
			try
			{
				if(xmlm->HasSetting("gui"))
					m->LoadGui(data_dir + xmlm->GetStringSetting("gui"));
				else
					m->LoadGui();
			}
			catch(gcn::Exception e)
			{
				e.PrintError();
				exit(-1);
			}
			
			// Create and set player two as AI	
			if(xmlm->HasSetting("player2ai"))
			{	
				//ai = new AIBase(m);
				ai = new SimplyStupidAI(m);
				m->SetAI(p2);
				//m->SetAI(p1);
			}
			
			if(xmlm->HasSetting("side_p1"))
				p1->SetFaction(xmlm->GetIntSetting("side_p1"));
			if(xmlm->HasSetting("side_p2"))
				p2->SetFaction(xmlm->GetIntSetting("side_p2"));
	
			// The mission should be first and primary listener
			im.AddKeyListener(m);
			im.AddMouseListener(m);
			im.AddWindowListener(m);
			im.AddWindowListener(m->GetGui());
			// The gui needs raw SDL_events
			im.AddRawListener(m->GetGui());
		}
		catch(Exception e)
		{
			e.PrintError();
			exit(-1);
		}
			
		delete xmlm;
		
		try
		{
			m->Start();
		}
		catch(gcn::Exception e)
		{
			e.PrintError();
			exit(-1);
		}
		timer->Start();

		// mission loop
		while(m->IsRunning())
		{
			// collect input
			im.Run();

			try
			{
				// Run one game frame, let AI do action if return true
				if(m->Run(timer->GetFrameLength()))
				{
					//AI's turn
					if(ai != NULL)
						ai->Run();
				}
			}
			catch(gcn::Exception e)
			{
				e.PrintError();
				exit(-1);
			}

			try
			{
				if(m->Draw())
					screen->Update();
			}
			catch(Exception e)
			{
				e.PrintError();
				exit(-1);
			}

			// compute elapsed time and have some delay in order to keep a constante frame rate
			timer->GoOnAndDelay();
		}

		if(m->GetExitStatus() == EXIT_AND_QUIT)
			game = false;

		elements->ClearElements();
		p1->ClearMissionElements();
		p2->ClearMissionElements();

		im.ClearListeners();
		delete m;
		delete p1;
		delete p2;
		delete elements;
	}

	delete screen;
	delete blitter;
	delete gtracker;
	delete sounds;

	#ifdef DEBUG_MODE
		printf( "Output: 0\n</Mars main>\n");
	#endif

	return 0;
}

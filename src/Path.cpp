#include "Path.h"

// Destroyer
Path::~Path()
{
	int path_len = _path.size();

	// delete all nodes
	for(int i = 0; i < path_len; i++)
		delete _path[i];

	_path.clear();
}

// add a node passing row, col and cost
void Path::AddNode(int row, int col, int cost, int dir)
{
	PathNode * node = new PathNode(row, col, cost, dir);

	_total_cost += cost;

	_path.push_back(node);
}

// return the first node and remove it from the path
PathNode * Path::PopNode()
{
	// path empty
	if(_path.empty())
		return NULL;

	PathNode * node = _path.front();

	// remove cost of the node from the total cost
	_total_cost -=  node->GetCost();

	// remove first node from the vector
	_path.erase(_path.begin());

	return node;
}

// vertical flip of the path
void Path::VFlip(int map_rows, int map_cols)
{
	int num_nodes = _path.size();
	PathNode * node;
	
	for(int i = 0; i < num_nodes; i++)
	{
		node = _path[i];

		node->SetRow(map_rows - 1 - node->GetRow());
		node->SetCol(map_cols - 1 - node->GetCol());
		node->SetDirection(((node->GetDirection() + HNUM_DIRS) % NUM_DIRS));
	}
}

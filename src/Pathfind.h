#ifndef CLASS_PATHFIND
#define CLASS_PATHFIND

#include <bitset>
#include <vector>
#include <stack>

#include "DataTypes.h"
#include "Functions.h"
#include "IsoTilesMap.h"
#include "Path.h"
#include "PathfindHeap.h"
#include "PathfindNode.h"

#define OPEN 0
#define CLOSED 1

#define ORTHO_COST 10
#define DIAGONAL_COST 14
#define ROTATION_COST 2

/* directions */
#define DIR_NORTH 0
#define DIR_NORTH_EAST 1
#define DIR_EAST 2
#define DIR_SOUTH_EAST 3
#define DIR_SOUTH 4
#define DIR_SOUTH_WEST 5
#define DIR_WEST 6
#define DIR_NORTH_WEST 7

/// this class implements A* pathfind
class Pathfind
{
	private:
	 /// IsoTilesMap where the path is search
	 IsoTilesMap * _tm;
	 /// OPEN LIST
	 PathfindHeap _open;
	 /// CLOSED LIST
	 std::vector < PathfindNode * > _closed;
	 /// membership list, holds the membership of a node
	 std::vector < std::bitset<2> * > _membership;
	 /// end node of the path
	 CellInd _end;

	 /// generate an unique ID for a node
	 int GenNodeId(int row, int col) { return ((row * _tm->GetNumCols()) + col); }

	 /// expands near nodes from one centered
	 /// checks that nodes are walkable and not yet into the closed list
	 void ExpandNodes(PathfindNode * center);

	 /// generate a node from the passed one, in according with the passed direction
	 /// for optimization reason, are passed also new node coordinates
	 void GenNode(PathfindNode * from, int new_row, int new_col,  int direction);

	 /// compute the heuristics value of the remaining path from a passed node
	 /// in this case DIAGONAL DISTANCE
	 int ComputeHeuristics(PathfindNode * node);

	 /// erase unused resource
	 void CleanPathfindData();

	public:
	 /// Constructor
	 Pathfind(IsoTilesMap * tm);
	 /// Destroyer
	 ~Pathfind();

	 /// Try to make a path from start to end
	 Path * MakePath(CellInd * start, CellInd * end, int start_dir)
	 {
		 return MakePath(start->row, start->col, end->row, end->col, start_dir);
	 };
	 /// Try to make a path from start[row,col] to end[row,col]
	 Path * MakePath(int start_row, int start_col, int end_row, int end_col, int start_dir);
};

#endif

#include "AttHighlighter.h"

// constructor
AttHighlighter::AttHighlighter(std::vector < graphic::Image * > images, IsoTilesMap * map)
{
	int size = images.size();

	// store pointers to images
	for(int i= 0; i < size; i++)
		_images.push_back(images[i]);

	_unit = NULL;
	
	_map = map;

	_direction = NO_DIRECTION;
	_att_cost = 0;
}

// blit part of the layer on a surface, if the surface is NULL blit on screen
void AttHighlighter::BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf)
{
	// no unit selected or no selection to highlight
	if(_unit == NULL || _direction == NO_DIRECTION)
		return ;
	
	//BlitFullRange(limits_rect, dst_surf);
	BlittMouseHover(limits_rect, dst_surf);
}

// TODO blit full range data into a surface WORK IN PROGRESS
void AttHighlighter::BlitFullRange(SDL_Rect limits_rect, graphic::Surface * dst_surf)
{
	Sint16 l_limit = limits_rect.x;
	Sint16 u_limit = limits_rect.y;

	int row = _unit->GetRow();
	int col = _unit->GetCol();
	
	// temporary IntPoint
	IntPoint p;
//	FloatPoint * f_origin;
	int step = 0;
	int color = 8;

	//First drawn tile should be a optimal range green tile
	MoveInDirection(&row,&col);

	//draw forward tiles
	for(step = 1; _attacksystem->GetTilesOptimalRange(_unit, row, col); MoveInDirection(&row,&col, step) )
	{
		p.x = _map->GetTileX(row, col) + ((_map->GetTileW() - _images[8]->GetW()) / 2);
		p.y = _map->GetTileY(row, col) + ((_map->GetTileH() - _images[8]->GetH()) / 2);

/*		f_origin = iso2scr(_map->GetTileX(_unit->GetRow(), _unit->GetCol()) + (_map->GetTileW() / 2),
							_map->GetTileY(_unit->GetRow(), _unit->GetCol()) + (_map->GetTileH() / 2),
							_map->GetOriginX(),  _map->GetOriginY());*/
							
		color = GetColor(_attacksystem->GetTilesOptimalRange(_unit, row, col));
	
		_images[color]->SetPosition(p.x - l_limit, p.y - u_limit);
		graphic::blitter->Blit(_images[color], dst_surf);
	}
	
	// get orientation
	//int dir = RelDirection((int)round(f_origin->x), (int)round(f_origin->y),
	//								p.x - l_limit, p.y - u_limit);
	//printf("Optimal: %d dir: %d %d\n", _attacksystem->GetTilesOptimalRange(_unit, row, col), dir, _direction);
}

void AttHighlighter::BlittMouseHover(SDL_Rect limits_rect, graphic::Surface * dst_surf)
{
	Sint16 l_limit = limits_rect.x;
	Sint16 u_limit = limits_rect.y;
	
	// temporary IntPoint
	IntPoint p;
	int color = 8;
	
	p.x = _map->GetTileX(_mouse->GetRow(), _mouse->GetCol()) + ((_map->GetTileW() - _images[0]->GetW()) / 2);
	p.y = _map->GetTileY(_mouse->GetRow(), _mouse->GetCol()) + ((_map->GetTileH() - _images[0]->GetH()) / 2);
						
	color = GetColor(_attacksystem->GetTilesOptimalRange(_unit, _mouse->GetRow(), _mouse->GetCol()));

	_images[color]->SetPosition(p.x - l_limit, p.y - u_limit);
	graphic::blitter->Blit(_images[color], dst_surf);
	
}

void AttHighlighter::MoveInDirection(int * row, int * col, int steps)
{
	int range = _unit->GetCurStat("range");
	int diag_range = (int)round(range * 10 / DIAGONAL_COST);
	
	//If steps are present do not start from weapon range
	if(steps)
	{
		range = steps;
		diag_range = steps;	
	}
	
	// Move depending on direction
	switch (_direction)
	{
		case 0:
			*row -= range;
			break;
		case 1:
			*col += diag_range;
			*row -= diag_range;
			break;
		case 2:
			*col += range;
			break;
		case 3:
			*col += diag_range;
			*row += diag_range;
			break;
		case 4:
			*row += range;
			break;
		case 5:
			*col -= diag_range;
			*row += diag_range;
			break;
		case 6:
			col -= range;
			break;
		case 7:
			*col -= diag_range;
			*row -= diag_range;
			break;
		default:
			break;
	}	
	
}

int AttHighlighter::GetColor(int optimal)
{
	// 0 == Green, 1 == GreenYellow, 2 == Yellow, 3 == Orange, 4 == Red, 5  == Black
	if(optimal > GREEN_YELLOW)
		return 0;
	if(optimal > YELLOW)
		return 1;
	if(optimal > ORANGE)
		return 2;
	if(optimal > RED)
		return 3;
	if(optimal > BLACK)
		return 4;
		
	return 5;		
}

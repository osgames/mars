#include "SelectionData.h"

using namespace graphic;
using namespace std;

// constructor
SelectionData::SelectionData(std::vector < Image * > images, Text * txt)
{
	int num_imgs = images.size();

	// store images pointers
	for(int i = 0; i < num_imgs; i++)
		_images.push_back(images[i]);

	// no obj selected by default
	_sel_elem = NULL;

	_txt = txt;

	_show_ap = false;
}

// blit part of the layer on a surface, if the surface is NULL blit on screen
void SelectionData::BlitLayer(SDL_Rect limits_rect, Surface * dst_surf)
{
	if(_sel_elem == NULL)
		return ;

	Sint16 l_limit = limits_rect.x;
	Sint16 u_limit = limits_rect.y;

	Image * obj_img = _sel_elem->GetImage();

	SDL_Rect fill_r, dest_r;

	// temporary IntPoint
	IntPoint p;

	p.x = _sel_elem->GetX() +
			 ((obj_img->GetVisibleX() + obj_img->GetVisibleW() - _images[BAR_BORDER_IMG]->GetW())/2) ;
	p.y = _sel_elem->GetY() + obj_img->GetVisibleY() - BORDER_V_OFFSET;

	// check if the bar is inside the scene
	if(p.x + _images[BAR_BORDER_IMG]->GetW() >= l_limit && p.y + _images[BAR_BORDER_IMG]->GetH()>= u_limit)
	{
		_images[BAR_BORDER_IMG]->SetPosition(p.x - l_limit, p.y - u_limit);

		//blit border
		blitter->Blit(_images[BAR_BORDER_IMG], dst_surf);

		// blit all bad bar under the good one
		_images[BAR_EN_BAD_IMG]->SetPosition(p.x + BAR_H_OFFSET - l_limit, p.y + BAR_V_OFFSET - u_limit);

		blitter->Blit(_images[BAR_EN_BAD_IMG], dst_surf);

		int perc = (100 * _sel_elem->GetCurStat("hitpoints")) / _sel_elem->GetMaxStat("hitpoints");

		// blit part of the good bar according to element energy
		fill_r.x = 0;
		fill_r.y = 0;
		fill_r.w = perc / 2;
		fill_r.h = _images[BAR_EN_GOOD_IMG]->GetH();

		dest_r.x = p.x + BAR_H_OFFSET - l_limit;
		dest_r.y = p.y + BAR_V_OFFSET - u_limit;
		dest_r.w = fill_r.w;
		dest_r.h = fill_r.h;

		if(fill_r.w > 0)
			blitter->BlitPart(_images[BAR_EN_GOOD_IMG], fill_r, dest_r, dst_surf);
	}

	// scene elements have just the energy bar
	if(_sel_elem->GetType() == SCENE_ELEMENT)
		return ;

	p.y = p.y - _images[BAR_BORDER_IMG]->GetH() - BARS_OFFSET;

	// check if the bar is inside the scene
	if(p.x + _images[BAR_BORDER_IMG]->GetW() >= l_limit && p.y + _images[BAR_BORDER_IMG]->GetH()>= u_limit)
	{
		_images[BAR_BORDER_IMG]->SetPosition(p.x - l_limit, p.y - u_limit);

		//blit border
		blitter->Blit(_images[BAR_BORDER_IMG], dst_surf);

		// blit all bad bar under the good one
		_images[BAR_AP_BAD_IMG]->SetPosition(p.x + BAR_H_OFFSET - l_limit, p.y + BAR_V_OFFSET - u_limit);

		blitter->Blit(_images[BAR_AP_BAD_IMG], dst_surf);

		int perc = (100 * _sel_elem->GetCurStat("actionpoints")) / _sel_elem->GetMaxStat("actionpoints");

		SDL_Rect fill_r, dest_r;

		// blit part of the good bar according to element energy
		fill_r.x = 0;
		fill_r.y = 0;
		fill_r.w = perc / 2;
		fill_r.h = _images[BAR_AP_GOOD_IMG]->GetH();

		dest_r.x = p.x + BAR_H_OFFSET - l_limit;
		dest_r.y = p.y + BAR_V_OFFSET - u_limit;
		dest_r.w = fill_r.w;
		dest_r.h = fill_r.h;

		if(fill_r.w > 0)
			blitter->BlitPart(_images[BAR_AP_GOOD_IMG], fill_r, dest_r, dst_surf);
	}

	if(_show_ap)
	{
		p.x = _sel_elem->GetX() +  ((obj_img->GetVisibleX() + obj_img->GetVisibleW() - _txt->GetNumberW(10110))/2) ;
		p.y = _sel_elem->GetY() + obj_img->GetVisibleY() - AP_OFFSET;

		if(p.x + _images[BAR_BORDER_IMG]->GetW() >= l_limit && p.y + _images[BAR_BORDER_IMG]->GetH()>= u_limit)
		{
			char buff[32];
			snprintf(buff,32,"%d/%d",_remaining_ap, _sel_elem->GetMaxStat("actionpoints"));
			//_txt->WriteNumber(_remaining_ap, p.x -l_limit, p.y - u_limit);
			_txt->WriteText(buff,p.x -l_limit, p.y - u_limit);
		}
	}
}


#ifndef ATTRIBUTES_H_
#define ATTRIBUTES_H_

#include <vector>

#include "Component.h"

class Attributes
{
	private:
		/// All components belonging to this Element
		std::vector <Component *> _components;
		/// Check is id exists as an component
		bool CheckId(int id);
		/// Elements currently active/primary component. -1 all components are active/primary
		int _active_component;

	public:
		/// Base constructor
		Attributes() {_active_component = -1;};
		/// Stats constructor
		Attributes(Statsmap stats);
		/// Full constructor
		Attributes(Statsmap stats, Stringmap strings);
		
		Attributes(std::vector <Component *> components) {_components = components; };
		/// Destructor
		~Attributes();
		
		/// Adds a Component to this elements attributes
		void AddComponent(Component * component) {_components.push_back(component); };
		void AddComponents(std::vector <Component *> components);

		/// set component as active
		void SetActiveComponentId(unsigned int comp)
		{ 
			if(_components.size() < comp)
				_active_component = -1;
			_active_component = comp; 
		};
		
		/// return active component
		int GetActiveComponentId() { return _active_component; };

		/* STATS MANAGEMENT */
		/// Return the max of a statistical value
		int GetMaxStat(std::string id, int component = -1);
		/// Set the max of a statistical value
		void SetMaxStat(std::string id, int val, int component = -1);
		/// Add a value to the max statistical value
		void AddMaxStat(std::string id, int val, int component = -1);
		/// Subtract a value from the max statistical value
		void SubMaxStat(std::string id, int val, int component = -1);
		/// Return the current  value of a statistical value
		int GetCurStat(std::string id, int component = -1);
		/// Set the current value of a statistical value
		void SetCurStat(std::string id, int val, int component = -1);
		/// Add a value to the current statistical value
		void AddCurStat(std::string id, int val, int component = -1);
		/// Subtract a value from the current statistical value
		void SubCurStat(std::string id, int val, int component = -1);
		/// Restore the current value to the max
		void RestoreCurStat(std::string id, int component = -1);
		// return true if the element has a value larger the the passed one for the current stat
		bool HasEnoughCurStat(std::string id, int val, int component = -1);
		/// Set a string value
		void SetString(std::string id, std::string text, int component = -1);
		/// Get a string value
		std::string GetString(std::string id, int component = -1);
		/// Return first component with string
		int LocateString(std::string id);
		
		/// Return if the stats is shared
		bool IsStatShared(std::string id, int component ) { return _components[component]->IsStatShared(id); };
		/// Toggels a stats shared state false/true
		void ToggleStatShared(std::string id, int component = -1);
		/// Force toggles are Stat(id) shared state
		void ToggleStatSharedForced(std::string id);
		/// Return if the Component is active
		bool IsComponentActive(int id) { if(CheckId(id)) return _components[id]->IsActive(); return false; };
		/// Toggles a Component active state false/true
		void ToggleComponentActive(int id) { if(CheckId(id)) _components[id]->ToggleActive(); };
		/// Find first component of said type, return vector number
		int FindComponentType(std::string type);
		/// Find first component with Stat
		int FindStatComponent(std::string id);
};

#endif /*ATTRIBUTES_H_*/

#include "SoundTracker.h"

SoundTracker::SoundTracker()
{
	int audio_rate = 22050;
	Uint16 audio_format = AUDIO_S16SYS;
	int audio_channels = 2;
	int audio_buffers = 4096;
	 
	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0)
	{
		fprintf(stderr, "Unable to initialize audio: %s\n", Mix_GetError());
		_active = false;
	}
	else
	{
		Mix_VolumeMusic(VOLUME_MUS);
		_active = true;
	}
}

SoundTracker::~SoundTracker()
{
	for(MusicMap::iterator it = _music.begin(); it != _music.end(); it++)
				Mix_FreeMusic((*it).second);
	for(SfxMap::iterator it = _sfx.begin(); it != _sfx.end(); it++)
				Mix_FreeChunk((*it).second);
	Mix_CloseAudio();
}

bool SoundTracker::AddMusic(std::string file)
{
	if(_active == false)
		return false;
	
	if(file.empty())
		return false;
		
	// look for the music file
	MusicMap::iterator it = _music.find(file);

	// not found, create and store it
	if(it == _music.end())
	{
		Mix_Music * music = Mix_LoadMUS(file.c_str());
			if(!music)
				return false;
		_music.insert(make_pair(file, music));
	}

	return true;
}

bool SoundTracker::PlayMusic(std::string file, int repeat)
{
	if(_active == false)
		return false;
		
	if(file.empty())
		return false;
		
	// look for the music file
	MusicMap::iterator it = _music.find(file);

	// not found
	if(it == _music.end())
	{
		//try loading the file
		if(!AddMusic(file))
			return false;
		else
			it = _music.find(file);
	}
	
	Mix_PlayMusic((*it).second, repeat);
//	Mix_HookMusicFinished(musicDone);
	return true;
}

bool SoundTracker::AddSfx(std::string file)
{
	if(_active == false)
		return false;
		
	if(file.empty())
		return false;
		
	// look for the sfx file
	SfxMap::iterator it = _sfx.find(file);

	// not found, create and store it
	if(it == _sfx.end())
	{
		Mix_Chunk * sfx = Mix_LoadWAV(file.c_str());
			if(!sfx)
				return false;
		_sfx.insert(make_pair(file, sfx));
	}

	return true;
}

bool SoundTracker::PlaySfx(std::string file, int repeat)
{
	if(_active == false)
		return false;
		
	if(file.empty() || !file.compare(" N/A "))
		return false;

	// look for the sfx file
	SfxMap::iterator it = _sfx.find(file);

	// not found
	if(it == _sfx.end())
	{
		//try loading the file
		if(!AddSfx(file))	
			return false;
		else
			it = _sfx.find(file);	
	}
			
	Mix_PlayChannel(-1, (*it).second, repeat);
//	Mix_HookMusicFinished(musicDone);
	return true;
}


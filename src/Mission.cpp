#include "Mission.h"

using namespace graphic;
using namespace std;

Mission::Mission(int rows, int cols, int terrain)
{
	#ifdef DEBUG_MODE
		printf("<Mission::Mission(int rows, int cols, int terrain)>\nInput: %d %d %d\n", rows, cols, terrain);
	#endif
	
	//Attack system used during mission
	_attacksystem = new AttackSystem();

	// the current graphic mode (SDL or GL)
	bool g_mode = screen->GetGraphicMode();

	_gui = new gui::GuiManager(this);

	_scene = new Scene(rows, cols, terrain, _gui, _attacksystem);

	// -- SCENE RENDERER --
	_renderer = new SceneRenderer(((_scene->GetMapW() - screen->GetW()) / 2 ), 0,
									screen->GetW(), screen->GetH(), g_mode);
	_renderer->SetLimits(-(screen->GetH() / 2), _scene->GetMapH() - (screen->GetH()  / 2),
							-(screen->GetW() / 2), _scene->GetMapW() - (screen->GetW() / 2));

	_scene->BindRenderer(_renderer);

	_paused = _running = _exit_status = _shift = _guilock = false;

	std::vector < MovableImage * > pointers;
	// isometric mouse
	MovableImage * mimg;
	try
	{
		mimg = gtracker->GetMovableImage((data_dir + "img/mouse/pointer.png").c_str(), g_mode);
		pointers.push_back(mimg);
		mimg = gtracker->GetMovableImage((data_dir + "img/mouse/attack_1.png").c_str(), g_mode);
		pointers.push_back(mimg);
		mimg = gtracker->GetMovableImage((data_dir + "img/mouse/attack_2.png").c_str(), g_mode);
		pointers.push_back(mimg);
		mimg = gtracker->GetMovableImage((data_dir + "img/mouse/attack_3.png").c_str(), g_mode);
		pointers.push_back(mimg);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}
	_mouse = new IsoMouse(pointers, (static_cast <IsoTilesMap * > (_scene->GetLayer(LAYER_TM))),
							screen->GetW() / 2, screen->GetH() / 2);
	_mouse->SetView(_renderer->GetViewX(), _renderer->GetViewY());

	_keyboard = new Keyboard;

	_arrows.reset();

	_current_turn = 1;
	_active_player = 0;

	_selected = NULL;

	_pathfinder = new Pathfind((static_cast <IsoTilesMap * > (_scene->GetLayer(LAYER_TM))));
	_path = NULL;
	_cur_pnode = NULL;

	_gamestate = NOTHING;

	#ifdef DEBUG_MODE
		printf("Output: Void\n</Mission::Mission(int rows, int cols, int terrain)>\n");
	#endif

}

void Mission::NewMove()
{
	#ifdef DEBUG_MODE
		printf("<Mission::NewMove()>\n");
	#endif
	// default select mode
	DoAction(ACT_SELECTMODE);
	CheckMissionConditions();

	if(_active_player != -1)
	{
		_players[_active_player]->EndMove();
		_players[_active_player]->ClearLocked();
		_players[_active_player]->ClearSelected();
		_players[_active_player]->SetView(_renderer->GetViewX(), _renderer->GetViewY());
	}

	_active_player++;

	if(_active_player == (int)_players.size())
		_active_player = 0;

	// Select new player if current already has ended large turn
	while( _players[_active_player]->GetTurn() == _current_turn )
	{
		_active_player++;
		if(_active_player == (int)_players.size())
			_active_player = 0;
	}
	// Update and set view for human players
	if(!_players[_active_player]->IsAI())
	{
		_players[_active_player]->ComputeVisibility();
		_renderer->UpdateViewPosition(_players[_active_player]->GetViewX(), _players[_active_player]->GetViewY());
		// Gui clear selected must be called before deploy window, or deploy window will not show
		_gui->ClearSelected();
		// Show hide deploy window
		if(_current_turn == 1 && _players[_active_player]->GetNumHiddenElements() != 0)
			_gui->SetVisible("Deploy",true);
		else
			_gui->SetVisible("Deploy",false);
		_gui->SetPlayer(_players[_active_player]);
		_scene->SetPlayer(_players[_active_player]);
	}

	if(_selected)
	{
		_selected->Deselect();
		_selected = NULL;
		_scene->ClearSelected();
	}

	_gui->SetDisabled("endmove",true);
	_gui->SetDisabled("select",false);	
	_gui->UpdateMiniMap();
	DoAction(ACT_HIDEACTION);
	DoAction(ACT_SELECTMODE);

	_renderer->RequestBufferRefresh();
	#ifdef DEBUG_MODE
		printf("</Mission::NewMove()>\n");
	#endif
}

void Mission::NewTurn()
{
	#ifdef DEBUG_MODE
		printf("<Mission::NewTurn()>\n");
	#endif
	int walkplayers = _active_player;
	// End large turn for current player
	_players[_active_player]->SetTurn(_current_turn);

	// Search for new next player in turn
	do{
		walkplayers++;
		if(walkplayers >= (int)_players.size())
			walkplayers = 0;
		// Found player that has not ended large end turn
		if( _players[walkplayers]->GetTurn() != _current_turn )
		{
			NewMove();
			return;
		}		
	}while(walkplayers != _active_player);

	// All players have done Large end turn
	_current_turn++;
	_active_player = -1;
	for(walkplayers = 0; walkplayers < (signed)_players.size(); walkplayers++)
		_players[walkplayers]->RestoreStat("actionpoints");
	_attacksystem->SetTurn(_current_turn);
	NewMove();
	#ifdef DEBUG_MODE
		printf("</Mission::NewTurn()>\n");
	#endif
}

bool Mission::Draw()
{
	if(_paused)
		return false;

	if(_renderer->NeedBlit())
	{
		_renderer->BlitScene();
		_mouse->Blit();

		return true;
	}
	else
		return false;
}

void Mission::NewResolution(int x, int y)
{
	if(screen->GetW() == x && screen->GetH() == x )
		return;

	screen->SetResolution(x, y);

	#ifdef WIN32
	gtracker->UpdateAllTextures();
	#endif

	_renderer->UpdateViewDimensions(x, y);
	_gui->WindowResize();	
	_renderer->RequestBlit();		
}

void Mission::AddPlayer(Player * p)
{
	_players.push_back(p);
	p->BindObjectsMap((static_cast <IsoObjectsMap * > (_scene->GetLayer(LAYER_OM))));
}

void Mission::SetAI(Player * p)
{
	p->SetAI();
}

bool Mission::Run(Uint32 time_elapsed)
{
	_time_elapsed = time_elapsed;
	if(_paused == false)
	{
		_arrows.set(ARW_UP, _keyboard->IsKeyPressed(SDLK_UP));
		_arrows.set(ARW_DOWN, _keyboard->IsKeyPressed(SDLK_DOWN));
		_arrows.set(ARW_LEFT, _keyboard->IsKeyPressed(SDLK_LEFT));
		_arrows.set(ARW_RIGHT, _keyboard->IsKeyPressed(SDLK_RIGHT));

		// Do not mouse scroll screen if mouse is above a widget
		if(_gui->HitWidget(_mouse->GetX(), _mouse->GetY()) || _guilock)
			_renderer->Scrolling(_arrows, screen->GetW()/2, screen->GetH()/2, time_elapsed);
		else
			_renderer->Scrolling(_arrows, _mouse->GetX(), _mouse->GetY(), time_elapsed);
		_gui->UpdateMiniMap(MM_SCROLLING);


		//Info text timer
		InfoText * itit = (static_cast <InfoText * > (_scene->GetLayer(LAYER_INFO)));
		if( !itit->TickInfoText())
		{
			_scene->DisableInfo();
			_renderer->RequestBlit(BUFFERED_RENDERING);
		}

		//Animations
		std::vector <AnimatedElement *>::iterator aeit = _anims.begin();
        for( unsigned int x = 1; x <= _anims.size() ; x++)
        {
        	//Next frame in animation, if final frame stop and erase animation
            if(!(*aeit)->NextFrame())
            {
                (*aeit)->StopAnimation();
                _anims.erase(aeit);
            }
            aeit++;
            _renderer->RequestBlit();
		}

		//Unit movement
		Unit * unit = dynamic_cast < Unit * > (_selected);
		if(unit != NULL && unit->IsMoving())
			DoAction(ACT_MOVE);
	}
	// Return true if current players is  an AI, used to tell the AI to do actions
	if(_players[_active_player]->IsAI())
		return true;
	return false;
}

void Mission::KeyPressed(KeyEvent & e)
{
	_keyboard->SetKeyPressed(e.GetKey());

	switch(e.GetKey())
	{
		// shift
		case SDLK_LSHIFT:
		case SDLK_RSHIFT:
			_shift = true;
		break;

		default:
		break;
	}
}

void Mission::KeyReleased(KeyEvent & e)
{
	_keyboard->SetKeyReleased(e.GetKey());

	switch(e.GetKey())
	{
		// ESC -> selectmode, shift ESC -> quit
		case SDLK_ESCAPE:
			if(_shift)
				DoAction(ACT_QUIT);
			else
				DoAction(ACT_SELECTMODE);
		break;

		// q -> quit
		case SDLK_q:
			DoAction(ACT_QUIT);
		break;

		// a -> deselect
		case SDLK_a:
			DoAction(ACT_DESELECT);
		break;
		
		// p -> test a function
		case SDLK_p:
			_players[_active_player]->UnHideElementAt(_players[_active_player]->GetContainerId() * MAX_ELEMENTS + 500);
			_gui->UpdateMiniMap();
			_renderer->RequestBlit();
		break;

		// r -> rotatemode
		case SDLK_r:
			DoAction(ACT_ROTATEMODE);
		break;

		// t -> movemode
		case SDLK_t:
			DoAction(ACT_MOVEMODE);
		break;

		//f -> attackmode, shift f -> fullscreen
		case SDLK_f:
			if(_shift)
				DoAction(ACT_FULLSCREEN);
			else
				DoAction(ACT_ATTACKMODE);
		break;


		// '+' -> increment gamma
		case SDLK_PLUS:
			DoAction(ACT_GAMMAUP);
		break;

		// '-' -> decrement gamma
		case SDLK_MINUS:
			DoAction(ACT_GAMMADOWN);
		break;

		// 'g' -> switch the grid
		case SDLK_g:
			DoAction(ACT_GRIDSWITCH);
		break;

		// 'v' -> vertical flip
		case SDLK_v:
			DoAction(ACT_VFLIP);
		break;

		// 'w' -> windowed
		case SDLK_w:
			DoAction(ACT_WINDOWED);
		break;

		// F1 -> 800x600
		case SDLK_F1:
			if(screen->GetW() == RIS1_W)
				break;

			screen->SetResolution(RIS1_W, RIS1_H);

			#ifdef WIN32
			gtracker->UpdateAllTextures();
			#endif

			_renderer->UpdateViewDimensions(RIS1_W, RIS1_H);
			_gui->WindowResize();
		break;

		// F2 -> 1024x768
		case SDLK_F2:
			if(screen->GetW() == RIS2_W)
				break;

			screen->SetResolution(RIS2_W, RIS2_H);

			#ifdef WIN32
			gtracker->UpdateAllTextures();
			#endif

			_renderer->UpdateViewDimensions(RIS2_W, RIS2_H);
			_gui->WindowResize();
		break;

		// F3 -> change current graphic mode
		#ifdef WITH_OPENGL
		case SDLK_F3:
			DoAction(ACT_GLSWSWITCH);
		break;
		#endif

		// F4 -> take a screenshot
		case SDLK_F4:
			DoAction(ACT_SCREENSHOT);
		break;

		case SDLK_RETURN:
			if(_shift)
				DoAction(ACT_ENDTURN);
			else
				DoAction(ACT_ENDMOVE);
		break;

		case SDLK_END:
			if(_shift)
				DoAction(ACT_ENDTURN);
			else
				DoAction(ACT_ENDMOVE);
		break;

		// remove shift
		case SDLK_LSHIFT:
		case SDLK_RSHIFT:
			_shift = false;
		break;

		default:
		break;
	}
}

void Mission::MouseMoved(MouseEvent & e)
{
	_mouse->SetPosition(e.GetX(), e.GetY(), _renderer->GetViewX(), _renderer->GetViewY());

	// if mouse is within the gui then let it handle all actions, _guilock is needed if mouse moves outside of widget
	if(_gui->HitWidget(_mouse->GetX(), _mouse->GetY()) || _guilock)
	{
		//rerender screen, if the mouse is moving and all action is on the gui (e.g draging a window)
		if(_guilock)
			_renderer->RequestBlit(BUFFERED_RENDERING);
		_mouse->UpdateAndBlit();
		// do not perform any usual mouse operation if inside an gui widget
		return;
	}

	Unit * unit = dynamic_cast < Unit * > (_selected);

	if(_usable && unit != NULL && unit->IsAction(ACT_ROTATEMODE))
	{
		_scene->SetRotHighlighter(_selected,
									(int)round(_mouse->GetAbsOrthoX()),
									(int)round(_mouse->GetAbsOrthoY()));
		_renderer->RequestBlit(BUFFERED_RENDERING);
	}
	else if(unit != NULL && unit->IsAction(ACT_ATTACKMODE))
	{
		_scene->SetAttHighlighter(_selected, _mouse);
		_renderer->RequestBlit(BUFFERED_RENDERING);
	}
	else if(_usable && unit != NULL && !unit->IsMoving() && unit->IsAction(ACT_MOVEMODE) && _mouse->IsCellChanged())
	{
		IsoTilesMap * tm = (static_cast <IsoTilesMap * > (_scene->GetLayer(LAYER_TM)));

		if(_path)
		{
			delete _path;
			_path = NULL;
		}

		if(_mouse->IsInsideMap() &&
		(tm->IsWalkable(_mouse->GetRow(), _mouse->GetCol()) ||
		tm->IsVisible(_mouse->GetRow(), _mouse->GetCol()) != VISIBLE))
		{
			_path = _pathfinder->MakePath(unit->GetRow(), unit->GetCol(),
										_mouse->GetRow(), _mouse->GetCol(),
										unit->GetDirection());

			_scene->SetPathHighlighter(_path, unit->GetCurStat("actionpoints") - _path->GetTotalCost());
		}
		else
			_scene->DisablePathHighlighter();

		_renderer->RequestBlit(BUFFERED_RENDERING);
	}

	// restore the screen and blit
	_mouse->UpdateAndBlit();
}

void Mission::MousePressed(MouseEvent & e)
{
	// if mouse is within the gui then let it handle all actions
	if(_gui->HitWidget(_mouse->GetX(), _mouse->GetY()))
	{
		// Forces Blits during mouse movements, prevents windows from getting dropped by fast mouse movement
		_guilock = true;
		_renderer->RequestBlit(BUFFERED_RENDERING);
		return;
	}

	_guilock = false;
}

void Mission::MouseReleased(MouseEvent & e)
{
	_guilock = false;
	Element * sel = _scene->SelectObject(_mouse->GetAbsX(), _mouse->GetAbsY());
	Unit * unit = dynamic_cast < Unit * > (sel);
	Building * building = dynamic_cast < Building * > (sel);

	int sel_faction = F_NO_FACTION;

	if(unit)
		sel_faction = unit->GetFaction();
	else if(building)
		sel_faction = building->GetFaction();

	Element * p_sel = _players[_active_player]->GetSelected();
	Unit * p_unit = dynamic_cast < Unit * > (p_sel);

	Element * p_lock = _players[_active_player]->GetLocked();
	//Unit * p_lock_unit = dynamic_cast < Unit * > (p_lock);

	_usable = ((sel == NULL) || ((sel_faction == _players[_active_player]->GetFaction()) && ((p_lock == NULL) || (p_lock == sel)) && sel->GetTurn() != _players[_active_player]->GetTurn()));

	// left button
	if(e.GetButton() == SDL_BUTTON_LEFT)
	{
		// If left mouse is release inside a widget then ignore and let gui handle it
		if(_gui->HitWidget(_mouse->GetX(), _mouse->GetY()))
		{
			_renderer->RequestBlit(BUFFERED_RENDERING);
			return;
		}
		// an element has been clicked
		if(sel != NULL && _gamestate != DEPLOYING)
		{
			// the clicked element is just selected -> deselect it
			if(sel->IsSelected())
			{
				DoAction(ACT_DESELECT);
			}
			// the selected unit if performing attack action
			else if(_selected && p_sel && p_unit && p_unit->IsAction(ACT_ATTACKMODE)
						&& p_unit->HasEnoughCurStat("actionpoints",_scene->GetAttCost())
						&& p_unit->GetTurn() != _players[_active_player]->GetTurn())
			{
				_players[_active_player]->SetLocked(_selected);
				_defender = sel;
				DoAction(ACT_ATTACK);
			}
			// the clicked element is not selected yet -> select it
			else
			{
				if(_selected != NULL)
				{
					_selected->Deselect();
					_scene->ClearSelected();
				}

				_selected = sel;
				_selected->Select();

				_scene->SetSelected(sel, _usable);
				_gui->SetVisible("Info", true);
				// Show actions if own unit is selected and it has not performed an action this turn and no unit is locked
				if( dynamic_cast < Unit * > (sel)
					&&_players[_active_player]->GetFaction() == (dynamic_cast < Unit * > (sel))->GetFaction()
					&& sel->GetTurn() != _players[_active_player]->GetTurn())
				{
					if(_players[_active_player]->GetLocked())
					{	//Only show actions if the locked unit is selected
						if(sel == _players[_active_player]->GetLocked())
							_gui->SetVisible("action", true);
						else
							DoAction("hideaction");
					}
					else
						_gui->SetVisible("action", true);
					sounds->PlaySfx(data_dir + sel->GetString("select_sfx"));
				}
				else
				{
					DoAction("hideaction");
				}
				_players[_active_player]->SetSelected(sel);

				//Set display to default action, default is set in Unit.h. Also remembers units last action
				if(_usable && unit != NULL)
					DoAction(ACT_SELECTMODE);

				_renderer->RequestBlit(BUFFERED_RENDERING);
			}
		}
		// no element has been clicked
		else
		{
			// Perform rotation action
			if(p_unit && p_unit->IsAction(ACT_ROTATEMODE)
			&& p_unit->HasEnoughCurStat("actionpoints", _scene->GetRotCost())
			&& p_unit->GetDirection() != _scene->GetRotDirection()
			&& p_unit->GetTurn() != _players[_active_player]->GetTurn())
			{
				if(p_lock == NULL)
				{
					_players[_active_player]->SetLocked(_selected);
					p_lock = _players[_active_player]->GetLocked();
					_gui->SetDisabled("endmove",false);
					_gui->SetDisabled("select",true);
				}

				if(p_lock == _selected)
				{
					p_unit->SetDirection(_scene->GetRotDirection());
					p_unit->SubCurStat("actionpoints", _scene->GetRotCost());

					_scene->SetRotHighlighter(_selected, (int)round(_mouse->GetAbsOrthoX()),
												(int)round(_mouse->GetAbsOrthoY()));

					// compute map visibility
 					_players[_active_player]->ComputeVisibility();

					_renderer->RequestBufferRefresh();
				}
			}

			// Perform walk action
			if(p_unit && p_unit->IsAction(ACT_MOVEMODE)
			&& p_unit->HasEnoughCurStat("actionpoints", _path->GetTotalCost())
			&& p_unit->GetTurn() != _players[_active_player]->GetTurn())
			{
				if(p_lock == NULL)
				{
					_players[_active_player]->SetLocked(_selected);
					p_lock = _players[_active_player]->GetLocked();
					_gui->SetDisabled("endmove",false);
					_gui->SetDisabled("select",true);
				}

				if(p_lock == _selected)
				{
					p_unit->StartMoving();
					p_unit->StartAnimation("move");

					_scene->DisableLayer(LAYER_SEL_H);
					_scene->DisableLayer(LAYER_SEL_DATA);

 					_path->PopNode();
				}
			}
			
			// Perform deploy action
			if(_gamestate == DEPLOYING)
			{
				_players[_active_player]->UnHideElementAt( _gui->GetSelectedId(), _mouse->GetRow(), _mouse->GetCol());
				_gui->NewHiddenSelected();
				_gamestate = NOTHING;
				char buf[128];
				snprintf(buf, 128, "Turn: %d    Idle Units: %d", _current_turn, _players[_active_player]->GetIdleUnits());
				_gui->SetText("turninfo", buf);
				_players[_active_player]->ComputeVisibility();
				_gui->UpdateMiniMap();
				_gui->SetVisible("Deploy", true);
				_renderer->RequestBlit();
			}
		}

		//_renderer->RequestBlit(BUFFERED_RENDERING);
	}
	else if(e.GetButton() == SDL_BUTTON_RIGHT)
	{
		if(_usable && _selected != NULL  && sel == p_sel && unit != NULL)
		{
			// restore the base pointer if it's been changed before
			_mouse->SetPointer(POINTER_BASE_1);

			unit->NextAction();

			DoAction(unit->GetAction());

			_renderer->RequestBlit(BUFFERED_RENDERING);
		}
		else if(_shift) // Move across screen
		{
			_renderer->UpdateViewPosition(_mouse->GetAbsX() - (screen->GetW()/2), _mouse->GetAbsY() - (screen->GetH()/2));
			_renderer->RequestBufferRefresh();
		}
	}
}

void Mission::FocusLost(WindowEvent & e)
{
	_paused = true;

	MouseListener::StopListening();
	KeyListener::StopListening();
}

void Mission::FocusGained(WindowEvent & e)
{
	if(e.GetType() == SDL_APPMOUSEFOCUS)
	{
		_paused = false;

		MouseListener::StartListening();
		KeyListener::StartListening();
	}
}

void Mission::WindowResize(WindowEvent & e)
{
	screen->SetResolution(e.GetWidth(), e.GetHeight());

	#ifdef WIN32
	gtracker->UpdateAllTextures();
	#endif

	_renderer->UpdateViewDimensions(e.GetWidth(), e.GetHeight());
}

void Mission::CheckMissionConditions()
{
	if(_current_turn == 1)
		return;
		
	for(int unsigned i=0; i != _players.size(); i++)
	{
		// no more Unit type elements
		if(!(_players[i])->GetNumElements(UNIT))
			Stop();
	}
}

// Gui listener
void Mission::action(const gcn::ActionEvent& actionEvent)
{
	std::string action = actionEvent.getId();

	// Menu should used do action with the selected menu item
	gcn::MarsMenu * menu = dynamic_cast<gcn::MarsMenu *>(actionEvent.getSource());
	if (menu != NULL)	        		
		action = menu->GetSelectedMenu();

	if( action.empty() )
		return;
	DoAction(action);
}

void Mission::DoAction(std::string action)
{
	if( action.empty() )
		return;

	#ifdef DEBUG_MODE
		printf("<Mission::DoAction(std::string action)>\nInput: %s\n", action.c_str());
	#endif

	//Quit mission
	if(!action.compare(ACT_QUIT))
		Stop();

	//Increase gamma
	else if(!action.compare(ACT_GAMMAUP))
		screen->GammaUp();

	//Decrease gamma
	else if(!action.compare(ACT_GAMMADOWN))
		screen->GammaDown();

	//Change to Fullscreen
	else if(!action.compare(ACT_FULLSCREEN))
	{
		screen->GoFullScreen();
		#ifdef WIN32
		gtracker->UpdateAllTextures();
		#endif
		_renderer->RequestBufferRefresh();
	}

	//Change resultion
	else if(!action.compare(ACT_800x600))
		NewResolution(800,600);
	//Change resultion
	else if(!action.compare(ACT_1024x768))
		NewResolution(1024,768);
	//Change resultion
	else if(!action.compare(ACT_1280x960))
		NewResolution(1280,960);
	//Change resultion
	else if(!action.compare(ACT_1024x640))
		NewResolution(1024,640);
	//Change resultion
	else if(!action.compare(ACT_1280x800))
		NewResolution(1280,800);
	//Change resultion
	else if(!action.compare(ACT_1680x1050))
		NewResolution(1680,1050);

	//Toggle gird on/off
	else if(!action.compare(ACT_GRIDSWITCH))
	{
		_scene->SwitchGrid();
		_renderer->RequestBufferRefresh();
		_renderer->RequestBlit();
	}

	//Vertically flip screen
	else if(!action.compare(ACT_VFLIP))
	{
		_renderer->UpdateViewPosition(_scene->GetMapW() - (_renderer->GetViewX() + _renderer->GetViewW()),
									 _scene->GetMapH() - (_renderer->GetViewY() + _renderer->GetViewH()));
		_scene->VFlip();
		// flip an existing path
		if(_path != NULL)
			_path->VFlip((static_cast <IsoTilesMap * > (_scene->GetLayer(LAYER_TM)))->GetNumRows(),
						(static_cast <IsoTilesMap * > (_scene->GetLayer(LAYER_TM)))->GetNumCols());

		_mouse->VFlip(screen->GetW(), screen->GetH());
		_renderer->RequestBufferRefresh();
	}

	//Chaneg to windowed
	else if(!action.compare(ACT_WINDOWED))
	{
		screen->GoWindowed();
		#ifdef WIN32
		gtracker->UpdateAllTextures();
		#endif
		_renderer->RequestBufferRefresh();
	}

	//Toggle gl render / software render
	#ifdef WITH_OPENGL
	else if(!action.compare(ACT_GLSWSWITCH))
	{
		screen->SwitchGraphicMode();
		gtracker->SwitchGraphicMode();
		_renderer->SwitchGraphicMode();
	}
	#endif

	//Set software render
	#ifdef WITH_OPENGL
	else if(!action.compare(ACT_SDLRENDER))
	{
		if( SDL_GRAPHIC == screen->GetGraphicMode())
			return;
		DoAction(ACT_GLSWSWITCH);
	}
	#endif

	//Set OpenGl render
	#ifdef WITH_OPENGL
	else if(!action.compare(ACT_GLRENDER))
	{
		if( GL_GRAPHIC == screen->GetGraphicMode())
			return;
		DoAction(ACT_GLSWSWITCH);
	}
	#endif
	

	//Take screenshot
	else if(!action.compare(ACT_SCREENSHOT))
	{
		try
		{
			screen->TakeScreenshot();
		}
		catch(Exception e)
		{
			e.PrintError();
		}
	}

	//End unit move
	else if(!action.compare(ACT_ENDMOVE))
	{
		//do not allow end move if no unit has performed an action this turn
		if( !_players[_active_player]->GetLocked())
			return;
		NewMove();
		_scene->DisableRotHighlighter();
		_scene->DisableAttHighlighter();
		_scene->DisablePathHighlighter();
		char buf[128];
		snprintf(buf, 128, "Turn: %d    Idle Units: %d", _current_turn, _players[_active_player]->GetIdleUnits());
		_gui->SetText("turninfo", buf);
		_renderer->RequestBlit();
	}

	//End player turn
	else if(!action.compare(ACT_ENDTURN))
	{
		//before ending turn check if there are idle units left, display warning if there are
		if( 1 < _players[_active_player]->GetIdleUnits())
		{
			char buf[128];
			snprintf(buf, 128, "Are you sure you want to end turn? #There are %d idle units.", _players[_active_player]->GetIdleUnits());
			_gui->SetText("endturnmessage", buf);
			_gui->SetVisible("confirmendturn", true);
		}
		else
		{
			NewTurn();
			_scene->DisableRotHighlighter();
			_scene->DisableAttHighlighter();
			_scene->DisablePathHighlighter();
			char buf[128];
			snprintf(buf, 128, "Turn: %d    Idle Units: %d", _current_turn, _players[_active_player]->GetIdleUnits());
			_gui->SetText("turninfo", buf);
			_renderer->RequestBlit();
		}
	}

	//Force End player turn, no idle check
	else if(!action.compare(ACT_FORCEENDTURN))
	{
		_gui->SetVisible("confirmendturn", false);
		NewTurn();
		_scene->DisableRotHighlighter();
		_scene->DisableAttHighlighter();
		_scene->DisablePathHighlighter();
		char buf[128];
		snprintf(buf, 128, "Turn: %d    Idle Units: %d", _current_turn, _players[_active_player]->GetIdleUnits());
		_gui->SetText("turninfo", buf);
		_renderer->RequestBlit();
	}

	else if(!action.compare(ACT_SELECTMODE))
	{
		Unit * unit = dynamic_cast < Unit * > (_selected);
		if(_usable && _selected != NULL  && unit != NULL)
		{
			_gamestate = SELECTED;
			// restore the base pointer if it's been changed before
			_mouse->SetPointer(POINTER_BASE_1);

			_scene->DisableRotHighlighter();
			_scene->DisableAttHighlighter();
			_scene->DisablePathHighlighter();

			unit->SetAction(ACT_SELECTMODE);

			_gui->SetVisible("rocketattack", false);
			_gui->SetVisible("cannonattack", false);
			_gui->SetVisible("selectattack", false);

			_renderer->RequestBlit();
		}

	}

	//Move mode, active PathHighligher
	else if(!action.compare(ACT_MOVEMODE))
	{
		Unit * unit = dynamic_cast < Unit * > (_selected);
		if(_usable && _selected != NULL  && unit != NULL)
		{
			_gamestate = MOVEMODE;
			// restore the base pointer if it's been changed before
			_mouse->SetPointer(POINTER_BASE_1);

			_scene->DisableRotHighlighter();
			_scene->DisableAttHighlighter();

			#ifdef DEBUG_MODE
				printf("%d:%d | %d:%d | dir:%d\n",unit->GetRow(), unit->GetCol(),
										_mouse->GetRow(), _mouse->GetCol(),
										unit->GetDirection());
			#endif

			if(!_path)
				_path = _pathfinder->MakePath(unit->GetRow(), unit->GetCol(),
										_mouse->GetRow(), _mouse->GetCol(),
										unit->GetDirection());

			unit->SetAction(ACT_MOVEMODE);
			_scene->SetPathHighlighter(_path, unit->GetCurStat("actionpoints") - _path->GetTotalCost());

			_gui->SetVisible("rocketattack", false);
			_gui->SetVisible("cannonattack", false);
			_gui->SetVisible("selectattack", false);

			_renderer->RequestBlit();
		}

	}

	// Rotation mode, active RotationHighlighter
	else if(!action.compare(ACT_ROTATEMODE))
	{
		Unit * unit = dynamic_cast < Unit * > (_selected);
		if(_usable && _selected != NULL  && unit != NULL)
		{
			_gamestate = ROTATEMODE;
			// restore the base pointer if it's been changed before
			_mouse->SetPointer(POINTER_BASE_1);

			_scene->DisablePathHighlighter();
			_scene->DisableAttHighlighter();

			unit->SetAction(ACT_ROTATEMODE);
			_scene->SetRotHighlighter(_selected, (int)round(_mouse->GetAbsOrthoX()),
											(int)round(_mouse->GetAbsOrthoY()));

			_gui->SetVisible("rocketattack", false);
			_gui->SetVisible("cannonattack", false);
			_gui->SetVisible("selectattack", false);

			_renderer->RequestBlit();
		}

	}

	// Attack mode, active AttackHighlighter, display gui depending on what weapons are present, reset attacksystem
	else if(!action.compare(ACT_ATTACKMODE))
	{
		Unit * unit = dynamic_cast < Unit * > (_selected);
		if(_usable && _selected != NULL  && unit != NULL)
		{
			_gamestate = ATTACKMODE;
			// restore the base pointer if it's been changed before
			_mouse->SetPointer(POINTER_BASE_1);

			_scene->DisableRotHighlighter();
			_scene->DisablePathHighlighter();

			unit->SetAction(ACT_ATTACKMODE);
			_scene->SetAttHighlighter(_selected, _mouse);

			//hide select weapons, default setting and reset attacksystem to default
			_gui->SetVisible("selectrocket", false);
			_gui->SetVisible("selectcannon", false);
			_gui->SetMarked("selectnormal", true);
			//Default Normal
			_attacksystem->SetAttackType();

			// The first component with damage stat is the default weapon for this unit, select and active it
			int comp = unit->FindStatComponent("damage");
			unit->SetActiveComponentId(comp);

			if( (unit->GetString("cannonname", comp)).compare(" N/A "))
				_gui->SetMarked("selectcannon", true);
			else if( (unit->GetString("rocketname", comp)).compare(" N/A "))
				_gui->SetMarked("selectrocket", true);

			// Show weapon select gui if the weapon exists
			if((unit->GetString("rocketname")).compare(" N/A "))
			{
				_gui->SetVisible("rocketattack", true);
				_gui->SetVisible("selectattack", true);
				_gui->SetVisible("selectrocket", true);
			}
			if((unit->GetString("cannonname")).compare(" N/A "))
			{
				_gui->SetVisible("cannonattack", true);
				_gui->SetVisible("selectattack", true);
				_gui->SetVisible("selectcannon", true);
			}

			_renderer->RequestBlit();
		}

	}

	// Set cannons as active for the AttackSystem
	else if(!action.compare(ACT_SELECTCANNON))
	{
		Element * sel = _players[_active_player]->GetLocked();
		if(!sel)
			sel = _selected;
		sel->SetActiveComponentId(sel->LocateString("cannonname"));
		_scene->SetAttHighlighter(sel, _mouse);
		_renderer->RequestBlit(BUFFERED_RENDERING);
	}

	// Set rockets as active for the AttackSystem
	else if(!action.compare(ACT_SELECTROCKET))
	{
		Element * sel = _players[_active_player]->GetLocked();
		if(!sel)
			sel = _selected;
		sel->SetActiveComponentId(sel->LocateString("rocketname"));
		_scene->SetAttHighlighter(sel, _mouse);
		_renderer->RequestBlit(BUFFERED_RENDERING);
	}

	// Set quick attack mode in AttackSystem
	else if(!action.compare(ACT_SELECTQUICK))
	{
		Element * sel = _players[_active_player]->GetLocked();
		if(!sel)
			sel = _selected;
		_attacksystem->SetAttackType(ATT_QUICK);
		_scene->SetAttHighlighter(sel, _mouse);
		_renderer->RequestBlit(BUFFERED_RENDERING);
	}

	// Set normal attack mode in AttackSystem
	else if(!action.compare(ACT_SELECTNORMAL))
	{
		Element * sel = _players[_active_player]->GetLocked();
		if(!sel)
			sel = _selected;
		_attacksystem->SetAttackType(ATT_NORMAL);
		_scene->SetAttHighlighter(sel, _mouse);
		_renderer->RequestBlit(BUFFERED_RENDERING);
	}

	// Set aimed attack mode in AttackSystem
	else if(!action.compare(ACT_SELECTAIMED))
	{
		Element * sel = _players[_active_player]->GetLocked();
		if(!sel)
			sel = _selected;
		_attacksystem->SetAttackType(ATT_AIMED);
		_scene->SetAttHighlighter(sel, _mouse);
		_renderer->RequestBlit(BUFFERED_RENDERING);
	}

	// Deselect a element and restore values and gui
	else if(!action.compare(ACT_DESELECT))
	{
		DoAction(ACT_SELECTMODE);
		_gamestate = NOTHING;
		if(_selected != NULL)
			_selected->Deselect();

		_selected = NULL;

		_scene->ClearSelected();
		_gui->ClearSelected();
		DoAction("hideaction");

		// restore the base pointer if it's been changed before
		_mouse->SetPointer(POINTER_BASE_1);
	}

	// Set action window and similar windows hidden
	else if(!action.compare(ACT_HIDEACTION))
	{
		_mouse->SetPointer(POINTER_BASE_1);
		_gui->SetVisible("action", false);
		_gui->SetVisible("cannonattack", false);
		_gui->SetVisible("selectattack", false);
		_gui->SetVisible("rocketattack", false);
	}

	// Perform an attack
	else if(!action.compare(ACT_ATTACK))
	{
		//The defender has to be present
		if(_defender == NULL)
			return;
		//The attacker
		Unit * attacker = dynamic_cast<Unit *>(_players[_active_player]->GetLocked());
		if(attacker == NULL)
			return;

		_gui->SetDisabled("endmove",false);
		_gui->SetDisabled("select",true);
		attacker->SetDirection(_scene->GetAttDirection());
		attacker->SubCurStat("actionpoints", _scene->GetAttCost());
		bool killed = _attacksystem->Attack(attacker, _defender);
		_scene->SetAttHighlighter(_selected, _mouse);
		_gui->ElementUpdate();
		sounds->PlaySfx(data_dir + attacker->GetString("attack_sfx", attacker->GetActiveComponentId()));
		///Play the right animation depending what weapon is used
		if(attacker->FindComponentType("cannon") == attacker->GetActiveComponentId())
            attacker->StartAnimation("cannon");
        else if(attacker->FindComponentType("rocket") == attacker->GetActiveComponentId())
            attacker->StartAnimation("rocket");
        _anims.push_back(attacker);
		if(!killed)
		{
			int dmg = _attacksystem->LastDmg();
			char tmp[32];
			if(dmg == 0)
				snprintf(tmp, 32, "Attack failed!");
			else if(dmg != -1 &&  _defender)
				snprintf(tmp, 32, "Damage: -%dhp (%d/%d)", dmg, _defender->GetCurStat("hitpoints"), _defender->GetMaxStat("hitpoints"));

			_scene->WriteInfo(tmp, _defender, INFO_TIM);
		}
		else
		{
			int id = _defender->GetId();
			for(unsigned int player=0; player < _players.size(); player++)
				_players[player]->DeleteElement(id);
			_scene->DeleteElement(id);

		 	; /// Start explosion animation & sfx
		 	_gui->UpdateMiniMap();
		}
		_defender = NULL;
	}


	// unit moving -> manage unit movement
	else if(!action.compare(ACT_MOVE))
	{
		Unit * unit = dynamic_cast < Unit * > (_selected);
		if(unit != NULL && unit->IsMoving())
		{

			IsoTilesMap * tm = (static_cast <IsoTilesMap * > (_scene->GetLayer(LAYER_TM)));
			IsoObjectsMap * om = (static_cast <IsoObjectsMap * > (_scene->GetLayer(LAYER_OM)));

			// no node yet
			if(_cur_pnode == NULL)
			{
				_cur_pnode = _path->PopNode();

				if(_path->GetPathLen() == 0)
					_scene->DisableLayer(LAYER_PATH_H);

				// set new direction
				if(unit->GetDirection() != _cur_pnode->GetDirection())
					unit->SetDirection(_cur_pnode->GetDirection());

				// next cell is not walkable
				if(!tm->IsWalkable(_cur_pnode->GetRow(), _cur_pnode->GetCol()))
				{
					delete _cur_pnode;
					_cur_pnode = NULL;

					delete _path;
					_path = NULL;

					_scene->DisablePathHighlighter();

					unit->StopMoving();
				}
				// next cell is walkable
				else
				{
					// remove needed action points
					if( unit->HasEnoughCurStat("actionpoints", _cur_pnode->GetCost()))
						unit->SubCurStat("actionpoints", _cur_pnode->GetCost());
					else //stop
					{
						_cur_pnode = NULL;
						unit->StopMoving();
						unit->StopAnimation();
						_gui->UpdateMiniMap();
						_scene->ActiveLayer(LAYER_SEL_H);
						_scene->ActiveLayer(LAYER_SEL_DATA);
						return;
					}

					// play move sound
					sounds->PlaySfx(data_dir + unit->GetString("move_sfx"));
					
					//increase move distance by one, reset to zero in StartMoving()
					unit->MovedOneTile();
					#ifdef DEBUG_MODE
						printf("New Cell\n");
					#endif

					//new cell update minimap possition
					_gui->UpdateMiniMap();

					// move unit on the ObjectMap
					om->MoveObject(unit, _cur_pnode->GetRow(), _cur_pnode->GetCol());
					// recalculate animation timing, has to be after MoveObject
					unit->CalcMinMovBeforeFrame(om->GetObjX(unit), om->GetObjY(unit));
				}

				// compute map visibility
 				_players[_active_player]->ComputeVisibility();

				_renderer->RequestBufferRefresh();
			}
			// a node is active
			else
			{

				bool movedone = unit->MoveTo(om->GetObjX(unit), om->GetObjY(unit), (int)ceil(MOVE_SPEED * _time_elapsed));
				unit->TickWalkAnim();

				if(movedone)
				{
					_cur_pnode = NULL;

					if(_path->GetPathLen() == 0)
					{
						unit->StopMoving();
						unit->StopAnimation();
						_gui->UpdateMiniMap();

						_scene->ActiveLayer(LAYER_SEL_H);
						_scene->ActiveLayer(LAYER_SEL_DATA);
					}
				}

				_renderer->RequestBlit(BUFFERED_RENDERING);
			}
		}
	}
	
	// A new Hidden unit has been selected
	else if(!action.compare(ACT_DEPLOYSELECT))
	{
		_gui->NewHiddenSelected();
		_gui->SetVisible("Info", true);
	}

	// A new Hidden unit is ready to be deployed
	else if(!action.compare(ACT_DEPLOYELEMENT))
	{
		_gamestate = DEPLOYING;
		_gui->SetVisible("Deploy", false);
		
	}
		
	#ifdef DEBUG_MODE
		printf("</Mission::DoAction(std::string action)>\n");
	#endif
}

#ifndef CLASS_ROT_HIGHLIGHTER
#define CLASS_ROT_HIGHLIGHTER

#include "IsoTilesMap.h"
#include "Layer.h"
#include "Element.h"

#include "graphic/Image.h"
#include "graphic/Blitter.h"

#define GOOD_OFFSET 0
#define BAD_OFFSET 8
#define FRONT_OFFSET 16

/// this is the layer that shows the highlighter for unit rotation
class RotHighlighter : public Layer
{
	private:
		/// images showed as highlighter
		std::vector < graphic::Image * > _images;
		/// a pointer to the selected unit
		Element * _unit;
		/// a pointer to the tiles map
		IsoTilesMap * _map;

		/// next direction to show
		int _direction;
		/// cost of the next rotation
		int _rot_cost;

	public:
		/// constructor
		RotHighlighter(std::vector < graphic::Image * > images, IsoTilesMap * map);
		/// destroyer
		~RotHighlighter() { };

		/// store a pointer to the selected unit
		void SetSelected(Element * unit) { _unit = unit; };
		/// set the  pointer to the selected unit to NULL
		void ClearSelected() { _unit = NULL; };

		/// set the direction of the rotation to highlight
		void SetDirection(int dir) { _direction = dir; };

		/// set the cost required by the rotation
		void SetRotCost(int cost) { _rot_cost = cost; };

		/// blit part of the layer on a surface, if the surface is NULL blit on screen
		void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);

		int GetDirection() { return _direction; };
		int GetCost() { return _rot_cost; };
};

#endif

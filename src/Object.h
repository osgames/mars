#ifndef CLASS_OBJECT
#define CLASS_OBJECT

#include "DataTypes.h"

/// An elementar object, have a position and a couple of indexes
class Object
{
	protected:
	 /// IntPoint that store the top-left IntPoint of the object
	 IntPoint _position;
	 /// row and column indexes
	 CellInd _indexes;

	public:
	 /// constructor
	 Object()
	 {
		// set defaul values
	 	_position.x = 0;
		_position.y = 0;
	
		_indexes.row = 0;
		_indexes.col = 0;
	 }
	 /// destroyer
	virtual ~Object() {  };

	 /// set the indexes of the object using CellInd
	 void SetIndexes(CellInd ind)
	 {
		_indexes.row = ind.row;
		_indexes.col = ind.col;
	 };
	 /// set the indexes of the object using 2 int
	 void SetIndexes(int row, int col)
	 {
		_indexes.row = row;
		_indexes.col = col;
	 };

	 /// get row index
	 int GetRow() { return _indexes.row; };
	 /// get column index
	 int GetCol() { return _indexes.col; };

	 /// set the position of the object passing a IntPoint
	 void SetPosition(IntPoint pos)
	 {
		_position.x = pos.x;
		_position.y = pos.y;
	 };
	 /// set the position of the object passing 2 int
	 void SetPosition(Sint16 x, Sint16 y)
	 {
		_position.x = x;
		_position.y = y;
	 };
	 /// set the X coordinate of the object
	 void SetXPosition(Sint16 x) { _position.x = x; };
	 /// set the Y coordinate of the object
	 void SetYPosition(Sint16 y) { _position.y = y; };

	 /// get X coordinate of the object
	 Sint16 GetX() { return _position.x; };
	 /// get Y coordinate of the object
	 Sint16 GetY() { return _position.y; };
};

#endif

#ifndef ANIMATEDELEMENT_H_
#define ANIMATEDELEMENT_H_

#include "Element.h"
#include "Animation.h"

// An map containing animation name as key, point to a 2-dimensional vector, [direction][animframe]
typedef std::map<std::string, graphic::Image * > Animationsmap;

/// The animation class stores any animations the Element might have. It also changes the IsoObject img to display the correct frame
class AnimatedElement : public virtual Element
{
	private:
		/// Storing all the vectors with animations frames
		std::vector <Animation *> _animations;
		/// Current animation
		std::vector <Animation *>::iterator _curanim;
		/// Minimum movement element has to do before new frame
		int _minmov;
		/// Last position we performed a new frame
		IntPoint _lastposmov;
		/// no animation present flag, needed for old file format
		bool _noanim;
		/// Number of played loops
		int _loops;
		int tmp;

	public:
		AnimatedElement() { _noanim = true;};
		~AnimatedElement();
		/// Adds a new animation to the Object
		void AddAnimation(Animation * anim);
		/// Adds a new animations to the Object
		void AddAnimations(std::vector <Animation *> anims);
		/// Starts an animation
		void StartAnimation(std::string name);
		/// Calculates the minimal distance the unit has to move before showing next frame
		void CalcMinMovBeforeFrame(int x_dest, int y_dest);
		/// Playes animation forward one frame if unit has moved enough
		void TickWalkAnim();
		/// Playes animation forward one frame
		bool NextFrame();
		/// Resets Object to original Image and stop animation
		void StopAnimation();
		/// Return fps of current animation
		int GetFPS() { return (*_curanim)->GetFPS(); };
};

#endif /*ANIMATEDELEMENT_H_*/

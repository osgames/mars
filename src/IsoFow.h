#ifndef CLASS_ISO_FOW
#define CLASS_ISO_FOW

#include "graphic/Image.h"

#include "IsoTilesMap.h"
#include "Layer.h"
#include "Tile.h"

#define FOW_DEFAULT_OPACITY 120

/// This layer that represents the FOG OF WAR
class IsoFow : public Layer
{
	private:
	 /// black tile
	 graphic::Image * _image;
	 /// width of the tile
	 Uint16 _tile_width;
	 /// height of the tile
	 Uint16 _tile_height;
	 /// number of rows
	 int _num_rows;
	 /// number of columns
	 int _num_cols;
	 /// side of an orthogonal tile
	 float _square_side;
	 /// pointer to the Isometric Tiles Map
	 IsoTilesMap * _tm;

	 /// origin of the map
	 IntPoint _origin;

	public:
	 /// base constructor, use a map to choose tiles
	 IsoFow(IsoTilesMap * tm, graphic::Image * image);
	 /// destroyer
	 ~IsoFow() { };

	 /// blit part of the layer on a surface, if the surface is NULL blit on screen
	 void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);

	 /// Set opacity of the grid
	 void SetOpacity(Uint8 opacity) { _image->SetOpacity(opacity); };
};

#endif

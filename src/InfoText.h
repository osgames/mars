#ifndef CLASS_INFO_TEXT
#define CLASS_INFO_TEXT

#include "graphic/Text.h"
#include "IsoTilesMap.h"

#include "Layer.h"

#define INFO_FONT "font/tahomabd.ttf"
#define INFO_DIM_FONT 11
#define INFO_TIM 75
#define INFO_SPACE 15

extern std::string data_dir;

/// a layer used to print some useful text on the screen (waiting for a GUI...)
class InfoText : public Layer
{
	private:
		/// the Text object that writes on the screen
		graphic::Text * _txt;
		/// the message that has to be printed
		std::string _message;
		/// the text timer
		Sint16 _timer;
		/// selected element
		Element * _unit;
		/// a pointer to the tiles map
		IsoTilesMap * _map;

	public:
		InfoText(std::string font, SDL_Color color, IsoTilesMap * map, int size = INFO_DIM_FONT, bool graphic_mode = SDL_GRAPHIC);
		~InfoText() { delete _txt; };

		void WriteText(std::string txt, Element * unit, Sint16 timer = -1)
		{
			_message = txt;
			_unit = unit;
			_timer = timer;
		};
		
		bool TickInfoText()
		{
			if( _timer != -1)
				_timer--;
			return _timer;
		}

		/// blit part of the layer on a surface, if the surface is NULL blit on screen
		void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);
};

#endif

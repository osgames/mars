#ifndef CLASS_PATHFIND_NODE
#define CLASS_PATHFIND_NODE

#include <functional>

#include "PathNode.h"

/// a node generated and used by the pathfind to make the path
class PathfindNode : public PathNode
{
	private:
	 /// unique id of the node
	 int _id;
	 /// prediceted full path cost passing for this node ( _f = _g + _h )
	 int _f;
	 /// path cost until this node
	 int _g;
	 /// predicted path cost from this node to the end
	 int _h;
	 /// pointer to the father
	 PathfindNode * _father;

	public:
	 /// constructor, inits attributes
	 PathfindNode(int row, int col, int id) : PathNode(row, col)
	 {
		 _id = id;
		 _f = _g = _h = 0;
		 _father = NULL;
	 }
	 /// Destroyer
	 ~PathfindNode() { };

	 /// set father pointer
	 void SetFather(PathfindNode * father) { _father = father; };
	 /// set f, g, h scores (f = g + h)
	 void SetScores(int g, int h)
	 {
		 _f = g + h;
		 _g = g;
		 _h = h;
	 }

	 /// return F value, predicted full path cost passing for this node (F = G + H)
	 int GetF() { return _f; };
	 /// return G value, path cost until this node
	 int GetG() { return _g; };
	 /// return H value, predicted path cost from this node to the end
	 int GetH() { return _h; };

	 /// return the node id
	 int GetId() { return _id; };

	 /// return the father of the node
	 PathfindNode * GetFather() { return _father; };
};

/// binary predicate, used to compare the F value of two nodes
struct GreaterPathfindNode : public std::binary_function<PathfindNode *, PathfindNode *, bool>
{
	bool operator()(PathfindNode * n1, PathfindNode * n2)
	{
		return ((n1->GetF()) > (n2->GetF()));
	}
};

#endif

#include "TileSet.h"

using namespace graphic;
using namespace std;

// this constructor use a vector of strings to create the tileset
// set trans as transparent color
TileSet::TileSet(vector < string > files, SDL_Color * trans, bool graph)
{
	int num_img = files.size();

	for(int i = 0; i < num_img; i++)
		AddImg(files[i], trans, graph);

	_tile_width = _images[0]->GetW();
	_tile_height = _images[0]->GetH();
}

// this constructor read all the images from a passed directory
// set trans as transparent color
TileSet::TileSet(string dir, SDL_Color * trans, bool graph)
{
	AddDirImg(dir, trans, graph);

	_tile_width = _images[0]->GetW();
	_tile_height = _images[0]->GetH();
}

// delete all images
TileSet::~TileSet()
{
	_images.clear();
	for(int i = _files.size(); i != 0; i--)
		delete _files[i-1];
	_files.clear();
}

// add just an image to the tile set
// set trans as transparent color
void TileSet::AddImg(string file, SDL_Color * trans, bool graph)
{
	Image * img;
	string * str;

	try
	{
			if(trans)
				img = gtracker->GetImage(file, *trans, graph);
			else
				img = gtracker->GetImage(file, graph);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}

	str = new string(file);

	_images.push_back(img);
	_files.push_back(str);
}

// add all images contained in a passed directory
// set trans as transparent color
void TileSet::AddDirImg(string dir, SDL_Color * trans, bool graph)
{
	DIR * dir_p;
 	struct dirent * dir_file;
	string * file;
	string * ext;
	Image * img;

	if(dir[dir.length() - 1] != '/')
		dir.append("/");

	// open the directory
	dir_p = opendir(dir.c_str());

	// get files .png from it
	while((dir_file = readdir(dir_p)))
	{
		// file path
		file = new string(dir_file->d_name);

		if((*file) == "." || (*file) == "..")
		{
			delete file;
			continue;
		}

		// check file extension
		if(file->length() > EXT_LEN)
		{
			// get file extension
			ext = new string(file->substr(file->length() - EXT_LEN));

			// allow .PNG
			if((*ext) != "png")
			{
				delete ext;
				delete file;

				continue;
			}
			delete ext;
		}
		else
		{
			delete file;
			continue;
		}

		_files.push_back(file);
	}

	// sort files
	sort(_files.begin(), _files.end(), LessStrPointer());

	// add the path
	for(unsigned int i = 0; i < _files.size(); i++)
		(*_files[i]) = dir + (*_files[i]);

	// create and store images
	for(unsigned int i = 0; i < _files.size(); i++)
	{
		try
		{
			if(trans)
				img = gtracker->GetImage(*_files[i], *trans, graph);
			else
				img = gtracker->GetImage(*_files[i], graph);
		}
		catch(Exception e)
		{
			e.PrintError();
			exit(-1);
		}

		_images.push_back(img);
	}

	// close dir
	closedir(dir_p);
}

// remove the index image
void TileSet::RemImg(int index)
{
	// take titerators
	vector < Image * >::iterator img_to_del;
	vector < string * >::iterator file_to_del;

	// iterators point to index element
	img_to_del = _images.begin() + index;
	file_to_del = _files.begin() + index;

	// erase elements from the vector
	_images.erase(img_to_del);
	_files.erase(file_to_del);
}

// remove an image range from start (included) to end (excluded)
void TileSet::RemImgRange(int start, int end)
{
	// take iterators
	vector < Image * >::iterator img_to_del;
	vector < string * >::iterator file_to_del;

	// iterators point to first element to erase
	img_to_del = _images.begin() + start;
	file_to_del = _files.begin() + start;

	for(int i = start; i < end; i++)
	{
		// erease elements from the vector
		_images.erase(img_to_del);
		_files.erase(file_to_del);
		img_to_del++;
		file_to_del++;
	}
}


#ifndef ATTACKCALC_H_
#define ATTACKCALC_H_

#include <cmath>

#include "Pathfind.h"
#include "Unit.h"

#define PI 3.14159265
#define MINIMUM_OPTIMAL_COST 20

// Weight values
#define W_ATT 15
#define W_DEF 13
#define W_ROT 11
#define W_OPT 11
#define W_MOV 11
#define W_EXP 10
#define W_RAND 18
#define W_DEFHIT 20
#define W_ATTMULTI 10

#define ATT_QUICK 0
#define ATT_NORMAL 1
#define ATT_AIMED 2

#define ATT_QUICK_DMG 	75
#define ATT_QUICK_CST	75
#define ATT_AIMED_DMG	120
#define ATT_AIMED_CST	120

class AttackCalc
{
	protected:
		/// What kind of attack is done
		int _attacktype;
		/// Current game turn
		int _turn;
	
		/// Calculates and returns the attacker multiplies value
		int AttackMultipliers(Element * attacker, Element * defender);
		/// Calculates and returns the defender multiplies value
		int DefendMultipliers(Element * attacker, Element * defender);
	
	private:
		/// Calculates and return the angle of attack, 0 = head on -> 180 = from behind
		int CalcAttackAngle(Element * attacker, Element * defender);
		/// Calculates the distance between attacker and defender
		int CalcDistance(Element * attacker, Element * defender);
		int CalcDistance(Element * attacker, int defrow, int defcol);
		/// Compares OptimalRange and Distance to defender
		int CalcOptimalRange(Element * attacker, int distance);

	public:
		AttackCalc() { _attacktype = ATT_NORMAL; _turn = 0; };
		/// Returns tiles optimal range value
		int GetTilesOptimalRange(Element * attacker, int row, int col);
		/// Set attack type, ATT_NORMAL is default
		void SetAttackType(int type = ATT_NORMAL) { _attacktype = type; };
		/// Get attack cost multiplier depending on attack type
		int GetAttackCostMultiplier();
		/// Set current game turn
		void SetTurn(int turn) { _turn = turn; };
};

#endif /*ATTACKCALC_H_*/

#include "Element.h"

using namespace graphic;

// Full constructor
Element::Element(int r, int c, std::vector < Image * > views, Statsmap stats) :
				IsoObject(r, c), Attributes(stats)
{
	if(views.size() < NUM_VIEWS && !views[0]->IsVirtual())
		throw Exception("Element::Element", "an Element misses some images!");

	// store views
	_views = views;

	// set default view
	_img = _views[NORTH];

	// default orientation
	_direction = SOUTH;

	// not selected by default
	_selected = false;
	
	// Start at turn -1 = no action taken yet
	_turn = -1;
	_attacked = -1;

	// not locked by default
	_locked = false;
}

Element::Element(int r, int c, std::vector < Image * > views, Statsmap stats, Stringmap strings) :
				IsoObject(r, c), Attributes(stats, strings)
{
	if(views.size() < NUM_VIEWS && !views[0]->IsVirtual())
		throw Exception("Element::Element", "an Element misses some images!");

	// store views
	_views = views;

	// set default view
	_img = _views[NORTH];

	// default orientation
	_direction = SOUTH;

	// not selected by default
	_selected = false;
	
	// Start at turn -1 = no action taken yet
	_turn = -1;
	_attacked = -1;

	// not locked by default
	_locked = false;

	// not locked by default
	_locked = false;
}

Element::Element(int r, int c, std::vector < Image * > views, std::vector <Component *> components) :
				IsoObject(r, c), Attributes(components)
{
	if(views.size() < NUM_VIEWS && !views[0]->IsVirtual())
		throw Exception("Element::Element", "an Element misses some images!");

	// store views
	_views = views;

	// set default view
	_img = _views[NORTH];

	// default orientation
	_direction = SOUTH;

	// not selected by default
	_selected = false;
	
	// Start at turn -1 = no action taken yet
	_turn = -1;
	_attacked = -1;

	// not locked by default
	_locked = false;
}

// Destroyer
Element::~Element()
{
	_views.clear();
}


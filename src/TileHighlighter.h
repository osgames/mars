#ifndef CLASS_TILE_HIGHLIGHTER
#define CLASS_TILE_HIGHLIGHTER

#include "graphic/Image.h"
#include "Player.h"
#include "IsoTilesMap.h"

#include "Layer.h"


#define TILE_H_TRANS 160

/// Manage the graphical Icon highlighting
class TileHighlighter : public Layer
{
	private:
	 /// disabled unit, has already moved this turn
	 graphic::Image * _disabled;
	 /// attacked unit, this unit has lost health this turn
	 graphic::Image * _attacked;
	 /// Selected player
	 Player * _player;
	 /// IsoTilesMap
	 IsoTilesMap * _tm;

	public:
	 /// constructor
	 TileHighlighter(IsoTilesMap * tm, graphic::Image * disabled, graphic::Image * attacked);
	 /// destroyer
	 ~TileHighlighter() { };

	 /// Sets selected element
	 void SetPlayer(Player * p) { _player = p; };

	 /// blit part of the layer on a surface, if the surface is NULL blit on screen
	 void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);
};

#endif

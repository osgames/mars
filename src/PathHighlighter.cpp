#include "PathHighlighter.h"

using namespace graphic;

// constructor
PathHighlighter::PathHighlighter(IsoTilesMap * tm, Image * good, Image * bad, Image * final, 
													Text * txt_good, Text * txt_bad)
{
	// store rows, cols
	_num_rows = tm->GetNumRows();
	_num_cols = tm->GetNumCols();
	// store tile dimensions
	_tile_width = tm->GetTileW();
	_tile_height = tm->GetTileH();

	// store the graphic origin
	_graphic_origin.x =  tm->GetGraphicOriginX();
	_graphic_origin.y =  tm->GetGraphicOriginY();

	// store higlighters
	_good_highlighter = good;
	_bad_highlighter = bad;
	_final_highlighter = final;

	// set opacity
	_good_highlighter->SetOpacity(PATH_TRANS);
	_bad_highlighter->SetOpacity(PATH_TRANS);
	_final_highlighter->SetOpacity(PATH_TRANS);

	// store Text renderer
	_txt_good = txt_good;
	_txt_bad = txt_bad;
}

// blit part of the layer on a surface, if the surface is NULL blit on screen
void PathHighlighter::BlitLayer(SDL_Rect limits_rect, Surface * dst_surf)
{
	Sint16 l_limit = limits_rect.x ;
	Sint16 r_limit = limits_rect.x + limits_rect.w;
	Sint16 u_limit = limits_rect.y;
	Sint16 b_limit = limits_rect.y + limits_rect.h;

	Uint16 ap_w, ap_h;

	IntPoint p = { 0, 0 };

	// total cells of the path
	int num_cells = _path->GetPathLen();
	// cells that use the same image
	int num_equal_cells;

	int half_height = _tile_height / 2;

	Image * cell_img;

	// good path => final cell is different
	if(_ap >= 0)
	{
		cell_img = _good_highlighter;
		num_equal_cells = num_cells - 1;
	}
	// bad path => all cells are equal
	else
	{
		cell_img = _bad_highlighter;
		num_equal_cells = num_cells;
	}

	PathNode * node;

	// blit (n-1) cells
	for(int i = 0; i < num_equal_cells; i++)
	{
		node = _path->GetNode(i);

		p.x = (node->GetCol() - node->GetRow()) * _tile_height + _graphic_origin.x;
		p.y = (node->GetCol() + node->GetRow()) * half_height + _graphic_origin.y;

		if( (p.x + _tile_width >= l_limit) && (p.x <= r_limit) &&
			(p.y + _tile_height >= u_limit) && (p.y <= b_limit))
		{
			//set blit position of the image
			cell_img->SetPosition(p.x - l_limit, p.y - u_limit );
			//blit
			blitter->Blit(cell_img, dst_surf);
		}
	}

	// if good path, blit the final cell
	if(_ap >= 0 && num_cells != 0)
	{
		node = _path->GetNode(num_cells - 1);

		// final cell
		p.x = (node->GetCol() - node->GetRow()) * _tile_height + _graphic_origin.x;
		p.y = (node->GetCol() + node->GetRow()) * half_height + _graphic_origin.y;

		if( (p.x + _tile_width >= l_limit) && (p.x <= r_limit) &&
			(p.y + _tile_height >= u_limit) && (p.y <= b_limit))
		{
			//set blit position of the image
			_final_highlighter->SetPosition(p.x - l_limit, p.y - u_limit );
			//blit
			blitter->Blit( _final_highlighter, dst_surf);
		}
	}

	// blit remaining AP on the final cell
	if(_ap >= 0)
	{
		ap_w = _txt_good->GetNumberW(_ap);
		ap_h = _txt_good->GetCharH();
	}
	else
	{
		ap_w = _txt_bad->GetNumberW(_ap);
		ap_h = _txt_bad->GetCharH();
	}

	p.x += (_tile_width - ap_w) / 2;
	p.y += (_tile_height - ap_h) / 2;

	if((p.x + ap_w >= l_limit) && (p.x <= r_limit) &&
		(p.y + ap_h >= u_limit) && (p.y <= b_limit))
	{
		if(_ap >= 0)
			_txt_good->WriteNumber(_ap, p.x -l_limit, p.y - u_limit);
		else
			_txt_bad->WriteNumber(_ap, p.x -l_limit, p.y - u_limit);
	}
}

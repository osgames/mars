#include "SceneContainer.h"

using namespace std;
using namespace graphic;
using namespace xml;

SceneContainer::SceneContainer(int id)
{
	#ifdef DEBUG_MODE
		printf("<SceneContainer::SceneContainer(int id)>\nInput: %d\n", id);
	#endif
 	_om = NULL;
 	_id = id * MAX_ELEMENTS;
	// Init Xml handler
	_xml = new XmlLoader((data_dir + "xml/xmlconfig.xml").c_str());
	#ifdef DEBUG_MODE
		printf("</SceneContainer::SceneContainer(int id)>\n");
	#endif
}

// loads xml files and creates elements (unit/buildings and sceneelements)
bool SceneContainer::CreateElements(const char *xmlpath)
{
	#ifdef DEBUG_MODE
		printf("<SceneContainer::CreateElements(const char *xmlpath)>\nInput: %s\n", xmlpath);
	#endif
	Element * e = NULL;
	try
	{
		_xml->XmlSetDoc(xmlpath);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}
	
	do
	{
		// Get Element pointer
		e = _xml->GetElement();
		if(_xml->IsHidden()) // Hidden elements are not in the game just stored here
			_hidden_elements.push_back(e);
		else // Put elements in storage
			_elements.push_back(e);
		// Add an ID to the Element so it can later be identified (including hidden)
		if(_xml->GetSpecialId())
			e->SetId(GetContainerId() * MAX_ELEMENTS + _xml->GetSpecialId());
		else
		{
			e->SetId(_id);
			_id++;
		}
		#ifdef DEBUG_MODE
			printf("Elements id: %d\n", e->GetId());
		#endif		
	}while(_xml->NextTag()); //Keep reading while there are more xml tags
	// Not needed anymore
	delete _xml;

	#ifdef DEBUG_MODE
		printf("Last _id: %d\n</SceneContainer::CreateElements(const char *xmlpath)>\n", _id);
	#endif
	return true;
}

void SceneContainer::ClearElements()
{
	int size = _elements.size();
	vector<Element *>::iterator it;

	// delete all Elements from the vector
	for( int i=0; i < size; i++ )
	{
		it = _elements.begin();
		Delete(it);
	}

	size = _hidden_elements.size();
	// delete all Elements from the vector
	for( int i=0; i < size; i++ )
	{
		it = _hidden_elements.begin();
		DeleteHidden(it);
	}

	_om = NULL;
}

// fill the objects map with elements
void SceneContainer::FillObjectsMap()
{
	int num_e = _elements.size();

	for(int i= 0; i < num_e ; i++)
		_om->AddObject(_elements[i]);
}

// find and delete a element at row,col if it belongs to playercontainer
void SceneContainer::DeleteElement(int row, int col)
{
	Element * aElement = (Element *)_om->GetObject(row, col);

	if(aElement)
		DeleteElement(aElement->GetId());
}

// delete selected element
void SceneContainer::DeleteElement(Element * element)
{
	if(element)
		DeleteElement(element->GetId());
}

// find and delete a element with ID if it belongs to playercontainer
void SceneContainer::DeleteElement(int id)
{
	vector<Element *>::iterator aElementIt = LocateElement(id);

	if(  aElementIt != _elements.end())
	{
		if(_om != NULL)
			_om->GetObject((*aElementIt)->GetRow(), (*aElementIt)->GetCol());
		Delete(aElementIt);
	}
}

// return element present at row,col if it belongs to playercontainer
Element * SceneContainer::GetElement(int row, int col)
{
	Element * aElement = (Element *)_om->GetObject(row, col);
	return GetElement(aElement->GetCurStat(STAT_ID));
}

// return element with ID only own elements are checked
Element * SceneContainer::GetElement(int id)
{
	vector<Element *>::iterator aElementIt = LocateElement(id);

	if(  aElementIt != _elements.end())
		return (*aElementIt);
	else
		return NULL;
}

// returns an element and removes it from own list
Element * SceneContainer::GiveElement(int row, int col)
{
	Element * element = GetElement(row,col);
	if(element)
		return GiveElement(element->GetCurStat(STAT_ID));
	return NULL;
}

// returns an element and removes it from own list
Element * SceneContainer::GiveElement(int id)
{
	Element * element = GetElement(id);
	if(element)
		_elements.erase(LocateElement(id));
	return element;
}

bool SceneContainer::UnHideElementAt(int id, int row, int col)
{
	vector<Element *>::iterator it = _hidden_elements.begin();
	
	#ifdef DEBUG_MODE
		printf("<SceneContainer::UnHideElement(int id)>\nInput: %d\n", id);
		printf("_id: %d : _hidden_elements: %d\n", _id, _hidden_elements.size());
	#endif
	
	for(; it != _hidden_elements.end(); it++)
	{
		if( (*it)->GetId() == id)
		{
			//Set new col/row if specified
			if( col != -1 && row != -1)
				(*it)->SetIndexes(row, col);
			// Add to SceneContainer
			AddExistingElement(*it);
			// Add to IsoObjectMap
			_om->AddObject(*it);
			// remove from hidden vector
			_hidden_elements.erase(it);
			#ifdef DEBUG_MODE
				printf("Output: true\n</SceneContainer::UnHideElement(int id)>\n");
			#endif
			//Success
			return true;
		}
	}
	#ifdef DEBUG_MODE
		printf("Output: false\n</SceneContainer::UnHideElement(int id)>\n");
	#endif
	// No element found and unhidden
	return false;
}

// adds an already created Element to this container
void SceneContainer::AddExistingElement(Element * element)
{
	if(element)
		_elements.push_back(element);
}

int SceneContainer::GetNumElements(int type)
{
	int count = 0;

	for(unsigned int i = 0; i < _elements.size(); i++)
	{
		if(type == _elements[i]->GetType())
			count++;
	}

	return count;
}

/*---- Private ----*/

// delete any element type
void SceneContainer::Delete(vector<Element *>::iterator it)
{
	if(it != _elements.end())
	{	
		IsoObject * isoobject = (*it);
		Element * element = (*it);
		
		// Remove element from objectmap
		if(_om != NULL)
			_om->RemoveObject(isoobject);

		SceneElement * scene_elem;
		Unit * unit;
		Building * build;

		if((scene_elem = dynamic_cast<SceneElement *> (element)))
			delete scene_elem;
		else if((unit = dynamic_cast<Unit *> (element)))
			delete unit;
		else if((build = dynamic_cast<Building *> (element)))
			delete build;

		// Delete from container
		_elements.erase(it);
	}
}

// delete hidden element type
void SceneContainer::DeleteHidden(vector<Element *>::iterator it)
{
	if(it != _hidden_elements.end())
	{	
		Element * element = (*it);

		SceneElement * scene_elem;
		Unit * unit;
		Building * build;

		if((scene_elem = dynamic_cast<SceneElement *> (element)))
			delete scene_elem;
		else if((unit = dynamic_cast<Unit *> (element)))
			delete unit;
		else if((build = dynamic_cast<Building *> (element)))
			delete build;

		// Delete from container
		_hidden_elements.erase(it);
	}
}

// search in storage if element with same id is there
vector<Element *>::iterator SceneContainer::LocateElement(int id)
{
	vector<Element *>::iterator it = _elements.begin();
	
	for(; it != _elements.end(); it++)
	{
		if( (*it)->GetId() == id)
			return it;
	}
	// it == _elements.end()
	return it;
}

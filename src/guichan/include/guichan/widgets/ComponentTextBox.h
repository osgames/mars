#ifndef COMPONENTTEXTBOX_H_
#define COMPONENTTEXTBOX_H_

#include "guichan/widgets/textbox.hpp"
#include "../../Element.h"

namespace gcn
{
	class GCN_CORE_DECLSPEC ComponentTextBox: public TextBox
	{
		public:
			ComponentTextBox() { _component = ""; };
			///Sets what component should be used to retrieve the data, must be set before every UpdateMessage.
			void SetComponent(std::string id) { _component = id; };
			
			///Update text message
			void UpdateMessage(Element *e)
			{
				#ifdef DEBUG_MODE
					printf("<ComponentTextBox::UpdateMessage(Element *e)>\n Component: %s\n", _component.c_str());
				#endif
				std::string text;
				char buf[64];
				
				int comp = e->FindComponentType(_component);
				//Did not find any component with that name
				if(comp == -1)
				{
					setText("");
					adjustSize();
					return;
				}
				#ifdef DEBUG_MODE
					printf("comp: %d, type: %s \n", comp, _component.c_str());
				#endif
				
				if(!_component.compare("equipment"))
				{
					text.append("Equipment: " + e->GetString("equipmentname", comp) + "#");
					if(e->GetMaxStat("vision", comp) > 0)
					{
						snprintf(buf, 64, "Vision: %d#", e->GetCurStat("vision", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("hitpoints", comp) > 0)
					{
						snprintf(buf, 64, "Hitpoints: %d#", e->GetCurStat("hitpoints", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("defence", comp) > 0)
					{
						snprintf(buf, 64, "Defence: %d#", e->GetCurStat("defence", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("cost", comp) > 0)
					{
						snprintf(buf, 64, "Cost: %d#", e->GetCurStat("cost", comp));
						text.append(buf);
					}
				}
				else if(!_component.compare("chassis"))
				{
					text.append("Chassi: " + e->GetString("chassisname", comp) + "#");
					if(e->GetMaxStat("vision", comp) > 0)
					{
						snprintf(buf, 64, "Vision: %d#", e->GetCurStat("vision", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("hitpoints", comp) > 0)
					{
						snprintf(buf, 64, "Hitpoints: %d#", e->GetCurStat("hitpoints", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("defence", comp) > 0)
					{
						snprintf(buf, 64, "Defence: %d#", e->GetCurStat("defence", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("movementcost", comp) > 0)
					{
						snprintf(buf, 64, "Movement Cost: %d#", e->GetCurStat("movementcost", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("cost", comp) > 0)
					{
						snprintf(buf, 64, "Cost: %d#", e->GetCurStat("cost", comp));
						text.append(buf);
					}
				}
				else if(!_component.compare("pilot"))
				{
					text.append("Pilot: " + e->GetString("pilotname", comp) + "#");
					text.append("Rank: " + GetRank(e->GetCurStat("experience", comp)) + "#");
					if(e->GetMaxStat("actionpoints", comp) > 0)
					{
						snprintf(buf, 64, "Action Points: %d#", e->GetCurStat("actionpoints", comp));
						text.append(buf);
					}
				}
				else
				{
					if(!_component.compare("rocket"))
						text.append("Rocket: " + e->GetString("rocketname", comp) + "#");
					else if(!_component.compare("cannon"))
						text.append("Cannon: " + e->GetString("cannonname", comp) + "#");
					else
					{	//Found nothing
						setText("");
						adjustSize();
						return;
					}
						
					if(e->GetMaxStat("damage", comp) > 0)
					{
						snprintf(buf, 64, "Damage: %d#", e->GetCurStat("damage", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("range", comp) > 0)
					{
						snprintf(buf, 64, "Range: %d#", e->GetCurStat("range", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("ammo", comp) > 0)
					{
						snprintf(buf, 64, "Ammo: %d/%d#", e->GetCurStat("ammo", comp), e->GetMaxStat("ammo", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("actioncost", comp) > 0)
					{
						snprintf(buf, 64, "Action Cost: %d#", e->GetCurStat("actioncost", comp));
						text.append(buf);
					}
					if(e->GetMaxStat("cost", comp) > 0)
					{
						snprintf(buf, 64, "Cost: %d#", e->GetCurStat("cost", comp));
						text.append(buf);
					}					
				} 
				
				#ifdef DEBUG_MODE
					printf("std::string text:\n %s\n", text.c_str());
				#endif
				
				setText(text);
				adjustSize();
				#ifdef DEBUG_MODE
					printf("</ComponentTextBox::UpdateMessage(Element *e)>\n");
				#endif
			};
			
		private:
			///Component name type
			std::string _component;
			
			std::string GetRank(int exp)
			{
				if(exp < PRIVATE)
					return " Roockie ";
				if(exp < CORPORAL)
					return " Private ";
				if(exp < SERGEANT)
					return " Corporal ";
				if(exp < LIEUTENANT)
					return " Seargeant ";
				if(exp < CAPTAIN)
					return " Lieutenant ";
				if(exp < MAJOR)
					return " Captain ";
				if(exp < ELITE)
					return " Major ";
			
				return " Elite ";
			};
		
		
	};
	
}

#endif /*COMPONENTTEXTBOX_H_*/

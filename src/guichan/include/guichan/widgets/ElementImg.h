#ifndef ELEMENTIMG_H_
#define ELEMENTIMG_H_

#include "guichan/widgets/icon.hpp"

namespace gcn
{
	/// Widget that displays the image of the current selected Element
	class GCN_CORE_DECLSPEC ElementImg: public Icon
	{
		public:
			ElementImg(gcn::Image *im) {_im = im; };

			virtual ~ElementImg() 
			{ 
				if(_im != NULL)
					delete _im;
			}
			
			///Update image
			void UpdateImage(gcn::Image *im)
			{
				if(im != NULL)
				{
					if(_im != NULL)
						delete _im;
					setImage(im);
				}
				else
					setImage(_im);
			};
			
		private:
			/// Default image
			gcn::Image *_im;
	};
	
}

#endif /*ELEMENTIMG_H_*/

#ifndef ELEMENTPILOTIMG_H_
#define ELEMENTPILOTIMG_H_

#include "guichan/widgets/icon.hpp"

namespace gcn
{
	class GCN_CORE_DECLSPEC ElementPilotImg: public Icon
	{
		public:
			ElementPilotImg(gcn::Image *im) {_im = im; };
			
			virtual ~ElementPilotImg() 
			{ 
				if(_im != NULL)
					delete _im;
			}
			
			///Update image
			void UpdateImage(gcn::Image *im)
			{
				if(im != NULL)
				{
					if(_im != NULL)
						;//delete _im;
					setImage(im);
				}
				else
					setImage(_im);
			};
			
		private:
			/// Default image
			gcn::Image *_im;
	};
	
}

#endif /*ELEMENTPILOTIMG_H_*/

#ifndef GCN_MINMAPPANEL_H
#define GCN_MINMAPPANEL_H

#include "guichan/image.hpp"
#include "guichan/platform.hpp"
#include "guichan/graphics.hpp"
#include "guichan/image.hpp"
#include "guichan/rectangle.hpp"
#include "guichan/widgets/scrollarea.hpp"
#include "guichan/widgets/icon.hpp"
#include "../../../../IsoMiniMap.h"
#include "../../../../SceneRenderer.h"


#define MM_SCROLLING 0
#define MM_UPDATEMAP 1

namespace gcn
{
	#define MMW 200
	#define MMH 150
	
    /**
     * Implements an MiniMap.
     */
    class GCN_CORE_DECLSPEC MiniMapPanel: public ScrollArea
    {
    public:
        /**
         * Default constructor.
         */
        MiniMapPanel(IsoMiniMap * mm, SceneRenderer * renderer);

        /**
         * Descructor.
         */
        virtual ~MiniMapPanel();
        
        /// Updates the minimap and blits new minimap to the minimappanel
        void UpdatePanel(int choice);

		// Inherited from mouse listener
		virtual void mousePressed(MouseEvent& mouseEvent);
        // Inherited from Widget
        virtual void draw(Graphics* graphics);

    private:
        /// The MiniMap to display.
        IsoMiniMap *_mm;
        /// Scenerendered pointer, needed for scrolling
        SceneRenderer * _renderer;
        /// Movable surface
        graphic::Surface * _surf;
		/// Gui target image, movable surface is blitted to here
        const Image* _image;
        /// limits for drawing, entire map is drawn so this could be avoided if minimap code is altered
        SDL_Rect _limits_rect;
        /// Image widget
        gcn::Icon * _icon;
        /// Scene view X
        int _viewx;
        /// Scene view Y
        int _viewy;
        /// Scene view W
        int _vieww;
        /// Scene view H
        int _viewh;

    };
}

#endif

#ifndef ELEMENTTEXTBOX_H_
#define ELEMENTTEXTBOX_H_

#include "guichan/widgets/textbox.hpp"
#include "../../Element.h"

namespace gcn
{
	class GCN_CORE_DECLSPEC ElementTextBox: public TextBox
	{
		public:
			ElementTextBox() { _component = -1; };
			ElementTextBox(const std::string& xml, const std::string& type) { _xml = xml; _type = type; _component = -1;};
			
			///Sets the xml tag the textbox will use
			void SetXmlTag(const std::string& xml) { _xml = xml;};
			///Sets the xml type the textbox will use
			void SetXmlType(const std::string& type) { _type = type;};
			///Sets the pre text
			void SetPreText(const std::string& text) { _text = text;};
			///Sets what component should be used to retrieve the data, must be set before every UpdateMessage. Component is reseted to default after each UpdateMessage
			void SetComponent(int id) { _component = id; };
			
			///Update text message
			void UpdateMessage(Element *e)
			{			
				if( !_type.compare("string"))
				{
					setText(_text + e->GetString(_xml, _component));
				}
				else if ( !_type.compare("int"))
				{
					char buf[32];
					snprintf(buf, 32, "%s%d", _text.c_str(), e->GetCurStat(_xml, _component));
					setText(buf);
				}
				else if ( !_type.compare("intmax"))
				{
					char buf[32];
					snprintf(buf, 32, "%s%d/%d", _text.c_str(), e->GetCurStat(_xml, _component), e->GetMaxStat(_xml, _component));
					setText(buf);
				}
				else if ( !_type.compare("rank"))
				{
					setText(_text + GetRank(e->GetCurStat(_xml, _component)));
				}
				adjustSize();
				//Reset component to default
				_component = -1;
			};
			
		private:
			///Xml tag
			std::string _xml;
			///Values type
			std::string _type;
			///Old text
			std::string _text;
			///Component number, default -1 == all components
			int _component;
			
			std::string GetRank(int exp)
			{
				if(exp < PRIVATE)
					return " Roockie ";
				if(exp < CORPORAL)
					return " Private ";
				if(exp < SERGEANT)
					return " Corporal ";
				if(exp < LIEUTENANT)
					return " Seargeant ";
				if(exp < CAPTAIN)
					return " Lieutenant ";
				if(exp < MAJOR)
					return " Captain ";
				if(exp < ELITE)
					return " Major ";
			
				return " Elite ";
			};
		
		
	};
	
}

#endif /*ELEMENTTEXTBOX_H_*/

#ifndef DEPLOYLIST_H_
#define DEPLOYLIST_H_

#include "guichan/listmodel.hpp"
#include "../../../../Player.h"

namespace gcn
{
	class GCN_CORE_DECLSPEC DeployList: public gcn::ListModel

	{
		private:
			/// Current active player
			Player * _player;
		
		public:
			/// Default
			DeployList() {_player = NULL;};
			
			/// Set current active player
			void SetPlayer(Player * player) { _player = player; }; 
			
			/// Return Element at row nr i
		    std::string getElementAt(int i)
		    {
		    	// Starting from zero
		    	Element * e = _player->GetHiddenElement(i);
		    	if(e == NULL)
		    		return " N/A ";
		    	return e->GetString("name");
		    };
		
			/// Return number of rows
		    int getNumberOfElements()
		    {
		        if(_player != NULL)
		        	return _player->GetNumHiddenElements();
		        return 0;
		    };
		
	};
	
}
#endif /*DEPLOYLIST_H_*/

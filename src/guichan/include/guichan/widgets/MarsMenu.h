#ifndef GCN_MARSMENU_H
#define GCN_MARSMENU_H

#include "guichan/actionlistener.hpp"
#include "guichan/basiccontainer.hpp"
#include "guichan/deathlistener.hpp"
#include "guichan/focushandler.hpp"
#include "guichan/focuslistener.hpp"
#include "guichan/keylistener.hpp"
#include "guichan/listmodel.hpp"
#include "guichan/mouselistener.hpp"
#include "guichan/platform.hpp"
#include "guichan/selectionlistener.hpp"
#include "guichan/widgets/listbox.hpp"
#include "guichan/widgets/scrollarea.hpp"

namespace gcn
{
	/// MarsMenu class, derived from GuiChan dropdown
    class GCN_CORE_DECLSPEC MarsMenu :
        public ActionListener,
        public BasicContainer,
        public KeyListener,
        public MouseListener,
        public FocusListener,
        public SelectionListener
    {
    public:
        /**
         * Contructor.
         *
         * @param listModel the ListModel to use.
         * @param scrollArea the ScrollArea to use.
         * @param listBox the listBox to use.
         * @see ListModel, ScrollArea, ListBox.
         */
        MarsMenu(ListModel *listModel = NULL,
                 ScrollArea *scrollArea = NULL,
                 ListBox *listBox = NULL);

        /**
         * Destructor.
         */
        virtual ~MarsMenu();

		/// return the name of the selected menu
		std::string GetSelectedMenu();

        /**
         * Gets the selected item as an index in the list model.
         *
         * @return the selected item as an index in the list model.
         * @see setSelected
         */
        int getSelected() const;

        /**
         * Sets the selected item. The selected item is represented by
         * an index from the list model.
         *
         * @param selected the selected item as an index from the list model.
         * @see getSelected
         */
        void setSelected(int selected);

        /**
         * Sets the list model to use when displaying the list.
         *
         * @param listModel the list model to use.
         * @see getListModel
         */
        void setListModel(ListModel *listModel);

        /**
         * Gets the list model used.
         *
         * @return the ListModel used.
         * @see setListModel
         */
        ListModel *getListModel();

        /**
         * Adjusts the height of the drop down to fit the height of the
         * drop down's parent's height. It's used to not make the drop down
         * draw itself outside of it's parent if folded down.
         */
        void adjustHeight();

        /**
         * Adds a selection listener to the drop down. When the selection
         * changes an event will be sent to all selection listeners of the
         * drop down.
         *
         * @param selectionListener the selection listener to add.
         * @since 0.8.0
         */
        void addSelectionListener(SelectionListener* selectionListener);

        /**
         * Removes a selection listener from the drop down.
         *
         * @param selectionListener the selection listener to remove.
         * @since 0.8.0
         */
        void removeSelectionListener(SelectionListener* selectionListener);


        // Inherited from Widget

        virtual void draw(Graphics* graphics);

        void setBaseColor(const Color& color);

        void setBackgroundColor(const Color& color);

        void setForegroundColor(const Color& color);

		void setFont(Font *font);

        void setSelectionColor(const Color& color);


        // Inherited from BasicContainer

        virtual Rectangle getChildrenArea();


        // Inherited from FocusListener

        virtual void focusLost(const Event& event);


        // Inherited from ActionListener

        virtual void action(const ActionEvent& actionEvent);


        // Inherited from DeathListener

        virtual void death(const Event& event);


        // Inherited from KeyListener

        virtual void keyPressed(KeyEvent& keyEvent);


        // Inherited from MouseListener

        virtual void mousePressed(MouseEvent& mouseEvent);

        virtual void mouseReleased(MouseEvent& mouseEvent);

        virtual void mouseWheelMovedUp(MouseEvent& mouseEvent);

        virtual void mouseWheelMovedDown(MouseEvent& mouseEvent);

        virtual void mouseDragged(MouseEvent& mouseEvent);


        // Inherited from SelectionListener

        virtual void valueChanged(const SelectionEvent& event);

    protected:
        /**
         * Sets the drop down to be dropped down.
         */
        virtual void dropDown();

        /**
         * Sets the drop down to be folded up.
         */
        virtual void foldUp();

        /**
         * Distributes a value changed event to all selection listeners
         * of the drop down.
         * 
         * @since 0.8.0
         */
        void distributeValueChangedEvent();

        /**
         * True if the drop down is dropped down, false otherwise.
         */
        bool mDroppedDown;

        /**
         * True if the drop down has been pushed with the mouse, false
         * otherwise.
         */ 
        bool mPushed;

        /**
         * Holds what the height is if the drop down is folded up. Used when
         * checking if the list of the drop down was clicked or if the upper part
         * of the drop down was clicked on a mouse click 
         */
        int mFoldedUpHeight;

        /**
         * The scroll area used.
         */
        ScrollArea* mScrollArea;

        /**
         * The list box used.
         */
        ListBox* mListBox;

        /**
         * The internal focus handler used to keep track of focus for the
         * internal list box.
         */
        FocusHandler mInternalFocusHandler;

        /**
         * True if an internal scroll area is used, false if a scroll area
         * has been passed to the drop down which the drop down should not
         * deleted in it's destructor.
         */
        bool mInternalScrollArea;

        /**
         * True if an internal list box is used, false if a list box
         * has been passed to the drop down which the drop down should not
         * deleted in it's destructor.
         */
        bool mInternalListBox;

        /**
         * True if the drop down is dragged.
         */
        bool mIsDragged;

        /**
         * Typedef.
         */
        typedef std::list<SelectionListener*> SelectionListenerList;

        /**
         * The selection listener's of the drop down.
         */
        SelectionListenerList mSelectionListeners;

        /**
         * Typedef.
         */
        typedef SelectionListenerList::iterator SelectionListenerIterator;
    };
}

#endif 

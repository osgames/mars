#ifndef GCN_MARSFONT_H
#define GCN_MARSFONT_H

#include <map>
#include <string>

#include "../../../graphic/Text.h"

#include "guichan/font.hpp"
#include "guichan/graphics.hpp"
#include "guichan/image.hpp"
#include "guichan/platform.hpp"

#define MARS_FONT "font/tahomabd.ttf"
#define MARS_DIM_FONT 12

namespace gcn
{
  /**
   *	Mars Font use SDL_ttf, see the implementation in graphic/Text
   */
  class GCN_EXTENSION_DECLSPEC MarsFont: public Font
  {
  public:

    /**
     * Constructor.
     *     
     * @param filename the filename of the True Type Font.
     * @param color used when drawing the font.
     * @param size the size the font should be in.
     */
    MarsFont (const std::string& filename, SDL_Color color, int size);

      /**
       * Destructor.
       */
      virtual ~MarsFont();
      
      /**
       * Sets the spacing between rows in pixels. Default is 0 pixels.
       * The spacing can be negative.
       *
       * @param spacing the spacing in pixels.
       */
      virtual void setRowSpacing (int spacing);
      
      /**
       * Gets the spacing between rows in pixels.
       *
       * @return the spacing.
       */
      virtual int getRowSpacing();
      
      /**
       * Sets the spacing between letters in pixels. Default is 0 pixels.
       * The spacing can be negative.
       *
       * @param spacing the spacing in pixels.
       */
      virtual void setGlyphSpacing(int spacing);

      /**
       * Gets the spacing between letters in pixels.
       *
       * @return the spacing.
       */
      virtual int getGlyphSpacing();
      
      /**
       * Sets the use of anti aliasing..
       *
       * @param antaAlias true for use of antia aliasing.
       */
      virtual void setAntiAlias(bool antiAlias);
      
      /**
       * Checks if anti aliasing is used.
       *
       * @return true if anti aliasing is used.
       */
      virtual bool isAntiAlias();
      
        
      // Inherited from Font
      
      virtual int getWidth(const std::string& text) const;
      
      virtual int getHeight() const;        
      
      virtual void drawString(Graphics* graphics, const std::string& text, int x, int y);
      
  protected:
      TTF_Font *mFont;
      /// Mars font reference
      graphic::Text * _marsfont;
      
      int mHeight;
      int mGlyphSpacing;
      int mRowSpacing;
      
      std::string mFilename;
      bool mAntiAlias;      
  }; 
}

#endif


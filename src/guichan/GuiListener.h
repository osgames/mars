#ifndef GUILISTENER_H_
#define GUILISTENER_H_

#include "guichan.hpp"

namespace gui
{
	class GuiListener : public gcn::ActionListener
	{
		public:
			GuiListener() { };
			
			/// Sets top container used to search for widgets by name
			void SetTop(gcn::Widget *widget) { _top = dynamic_cast<gcn::Container *>(widget); };
			
			/// According to EventId toggle widgets visibility
		    void action(const gcn::ActionEvent& actionEvent)
		    {
		        if(_top)
		        {			
					#ifdef DEBUG_MODE
						printf("<GuiListener::action(const gcn::ActionEvent& actionEvent))>\nInput: %s\n", (actionEvent.getId()).c_str());
					#endif		        	
		        	
		        	// Menus should change visibility to there list not there eventId
		        	gcn::MarsMenu * menu = dynamic_cast<gcn::MarsMenu *>(actionEvent.getSource());
		        	if (menu != NULL)
		        	{
					#ifdef DEBUG_MODE
						printf("Menu selected: %s\n", (menu->GetSelectedMenu()).c_str());
					#endif		        		
		        		gcn::Widget *widget = _top->findWidgetById(menu->GetSelectedMenu());
		        		if(widget) //toggle widget visibility
		        			widget->setVisible(!widget->isVisible());
		        		return;
		        	}
		        	gcn::Widget *widget = _top->findWidgetById(actionEvent.getId());
		        	if(widget) //toggle widget visibility
		        		widget->setVisible(!widget->isVisible());
		        }
		    }
		private:
			gcn::Container *_top;
	};
}
#endif /*GUILISTENER_H_*/

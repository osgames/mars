#ifndef GUIMANAGER_H_
#define GUMANAGERI_H_

#include "GuiListener.h"
#include "../xml/XmlGui.h"

#include "../graphic/Screen.h"
#include "../event_system/RawListener.h"
#include "../event_system/WindowListener.h"
#include "../Element.h"
#include "../Player.h"

//Guichan 
#include "guichan.hpp"
#include "guichan/sdl/sdlinput.hpp"
#include "src/mars/MarsGraphics.h"
#include "src/mars/MarsImageLoader.h"

#define FONT "font/whitefont.png"

extern std::string data_dir;

namespace gui
{
	class GuiManager : public RawListener, public WindowListener
	{
		private:
		/// GuiChan reference
		gcn::Gui *_gui;
		/// IsoMiniMap pointer
		IsoMiniMap * _imm;
	    /// Focus handler for input management
		gcn::FocusHandler* _focushandler;
		/// The top container of the GUI.
		gcn::Widget* _topcontainer;
		/// The deploylist, players hidden units
		gcn::DeployList * _deploylist;
		/// The imageloader.
		gcn::MarsImageLoader* _imageloader;
		/// The graphics engine
		gcn::MarsGraphics* _graphics;
		/// The input controller.
		gcn::SDLInput *_input;
		/// Font
		gcn::Font* _font;
		/// Selected element
		Element * _selected;
		/// Active player
		Player * _player;
		/// Element depended widgets
		std::vector<gcn::Widget*> _element_widgets;
		/// Widget visibility toggle listener
		GuiListener *_guilistener;
		/// Engine action listener
		gcn::ActionListener *_gst;
		/// Minimap widget
		gcn::MiniMapPanel * _panel;
					
		protected:
		
		public:
			/// Creates a gui manager, ParseGui should be called after
			GuiManager(gcn::ActionListener * gst);
			/// Destructor
			~GuiManager();
			/// Loads a gui from xml file
			void ParseGui(std::string path = "");
			/// Returns true if widget exist at x,y
			bool HitWidget(int x, int y);
			/// Sets selected element
			void SetSelected(Element * el) { _selected = el; ElementUpdate(); };
			/// Resets selected element to null and hides widgets that depend on elements
			void ClearSelected() { _selected = NULL; ElementUpdate(); };
			/// Update all element depended widgets
			void ElementUpdate();
			/// Sets widget (name) disable (bool) status
			void SetDisabled(std::string name, bool dis);
			/// Sets widget (name) Visible status, non visible widgets are not drawn
			void SetVisible(std::string name, bool vis);
			/// Sets new text to widget (name)
			void SetText(std::string name, std::string text);
			/// Sets what component this element widgets uses
			void SetComponent(std::string name, int id);
			/// Sets Checkboxes as marked or not
			void SetMarked(std::string name, bool vis);
			
			/// -- Raw Sdl Events --
			void RawEvent(SDL_Event &e) { _input->pushInput(e);};
			
			/// -- WINDOW EVENTS --
			/// Main window lost focus
			void FocusLost(WindowEvent & e) { };
			/// Main window gained focus
			void FocusGained(WindowEvent & e) { };
			/// Main window resized
			void WindowResize(WindowEvent & e) { _graphics->setTarget(graphic::screen->GetSurface()); _topcontainer->setDimension(gcn::Rectangle(0, 0, graphic::screen->GetW(), graphic::screen->GetH())); };		
			/// Non event main window resized
			void WindowResize() { _graphics->setTarget(graphic::screen->GetSurface()); _topcontainer->setDimension(gcn::Rectangle(0, 0, graphic::screen->GetW(), graphic::screen->GetH())); };
			
			/// Draw gui
			void Display() { Logic(); _gui->draw(); };
			/// Run gui Logic
			void Logic() {  _gui->logic(); };
			
			/// Sets isominimap pointer
			void SetIsoMiniMap(IsoMiniMap * imm) { _imm = imm; };
			/// Create minimap
			void InitMiniMap(SceneRenderer * renderer);
			/// Updates and rerenders base minimap image. Should only be called when tilemap has changed or scrolling
			void UpdateMiniMap(int choice = MM_UPDATEMAP) { if(_panel != NULL) _panel->UpdatePanel(choice); };
			
			/// Set current player for the deploy window (does nothing if deploy window is not visable or present
			void SetPlayer(Player * player);
			/// Update witch hidden unit is selected
			void NewHiddenSelected();
			
			/// Return id of selected Element
			int GetSelectedId() { return _selected->GetId(); };
	};
}

#endif /*GUIMANAGER_H_*/

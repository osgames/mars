#ifndef GCN_MARSGRAPHICS_HPP
#define GCN_MARSGRAPHICS_HPP

#include "SDL.h"
#include "../../graphic/Blitter.h"
#include "../../graphic/Screen.h"
#include "../../graphic/Text.h"

#include "guichan/color.hpp"
#include "guichan/graphics.hpp"
#include "guichan/platform.hpp"
#include "guichan/exception.hpp"
#include "guichan/font.hpp"
#include "guichan/image.hpp"
#include "guichan/sdl/sdlpixel.hpp"
#include "MarsImage.h"

namespace gcn
{
    class Image;
    class Rectangle;

    /**
     * Mars implementation of the Graphics.
     */
    class GCN_EXTENSION_DECLSPEC MarsGraphics : public Graphics
    {
    public:

        // Needed so that drawImage(gcn::Image *, int, int) is visible.
        using Graphics::drawImage;

        /**
         * Constructor.
         */
        MarsGraphics();

        /**
         * Sets the target SDL_Surface to draw to. The target can be any
         * SDL_Surface. This funtion also pushes a clip areas corresponding to
         * the dimension of the target.
         *
         * @param target the target to draw to.
         */
        virtual void setTarget(SDL_Surface* target);

        /**
         * Gets the target SDL_Surface.
         *
         * @return the target SDL_Surface.
         */
        virtual SDL_Surface* getTarget() const;

        // Inherited from Graphics

        virtual void _beginDraw();

        virtual void _endDraw();

        virtual bool pushClipArea(Rectangle area);

        virtual void popClipArea();

        virtual void drawImage(const Image* image,
                               int srcX,
                               int srcY,
                               int dstX,
                               int dstY,
                               int width,
                               int height);

		virtual void drawMarsSurface(graphic::Surface* surface,
                                 SDL_Rect source,
                                 SDL_Rect destination);

		virtual void drawMarsFont(graphic::Text * font, const std::string& text, int x, int y);

        virtual void drawPoint(int x, int y);

        virtual void drawLine(int x1, int y1, int x2, int y2);

        virtual void drawRectangle(const Rectangle& rectangle);

        virtual void fillRectangle(const Rectangle& rectangle);

        virtual void setColor(const Color& color);

        virtual const Color& getColor() const;

    protected:
        /**
         * Draws a horizontal line.
         *
         * @param x1 the start coordinate of the line.
         * @param y the y coordinate of the line.
         * @param x2 the end coordinate of the line.
         */
        virtual void drawHLine(int x1, int y, int x2);

        /**
         * Draws a vertical line.
         *
         * @param x the x coordinate of the line.
         * @param y1 the start coordinate of the line.
         * @param y2 the end coordinate of the line.
         */
        virtual void drawVLine(int x, int y1, int y2);

        SDL_Surface* mTarget;
        Color mColor;
        int mWidth;
        int mHeight;
        bool mAlpha;
    };
}

#endif // end GCN_MARSGRAPHICS_HPP

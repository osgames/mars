#ifndef GCN_MARSIMAGE_HPP
#define GCN_MARSIMAGE_HPP

#include "SDL.h"
#include "../../graphic/Surface.h"

#include <string>

#include "guichan/color.hpp"
#include "guichan/platform.hpp"
#include "guichan/image.hpp"
#include "guichan/exception.hpp"

namespace gcn
{
    /**
     * Mars implementation of Image.
     */
    class GCN_EXTENSION_DECLSPEC MarsImage : public Image
    {
    public:
        /**
         * Constructor. Load an image from an Mars surface.
         *
         * NOTE: The functions getPixel and putPixel are only guaranteed to work
         *       before an image has been converted to display format.
         *
         * @param surface the surface from which to load.
         * @param autoFree true if the surface should automatically be deleted.
         */
        MarsImage(graphic::Surface* surface, bool autoFree);

        /**
         * Destructor.
         */
        virtual ~MarsImage();

        /**
         * Gets the Mars surface for the image.
         *
         * @return the Mars surface for the image.
         */
        virtual graphic::Surface* getSurface() const;


        // Inherited from Image

		/// Graphictracker does this for us
        virtual void free() { };

		/// returns image width
        virtual int getWidth() const;
		/// return image height
        virtual int getHeight() const;
		/// returns color of pixel at x,y
        virtual Color getPixel(int x, int y);
		/// Sets pixel at x,y to Color
        virtual void putPixel(int x, int y, const Color& color);
		/// Not needed in Mars engine
        virtual void convertToDisplayFormat() { };
        /// Returns Image
        graphic::Surface* GetSurface() { return _mSurface; };

    protected:
        graphic::Surface* _mSurface;
        bool mAutoFree;
    };
}

#endif // end GCN_MARSIMAGE_HPP

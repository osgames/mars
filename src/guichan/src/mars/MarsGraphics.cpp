#include "MarsGraphics.h"

// For some reason an old version of MSVC did not like std::abs,
// so we added this macro.
#ifndef ABS
#define ABS(x) ((x)<0?-(x):(x))
#endif

using namespace gcn;


MarsGraphics::MarsGraphics()
{
    mAlpha = false;
}

void MarsGraphics::_beginDraw()
{    
	if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
		pushClipArea(Rectangle(0, 0, mWidth, mHeight));
    }
    #ifdef WITH_OPENGL
    else
    {    
		// GuiChan can not handle GL_TEXTURE_2D, MarsImage uses GL_TEXTURE_2D so reenable when rendering them
		glDisable(GL_TEXTURE_2D);
   
        pushClipArea(Rectangle(0, 0, mWidth, mHeight));
	}
    #endif
}

void MarsGraphics::_endDraw()
{
	if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
    	popClipArea();
    }
    #ifdef WITH_OPENGL
    else
    { 
		// Return GL_TEXTURE_2D, needed for graphic::Surface
		glEnable(GL_TEXTURE_2D);
        popClipArea();
	}
    #endif
}

void MarsGraphics::setTarget(SDL_Surface* target)
{
    mTarget = target;
    mWidth = target->w;
    mHeight = target->h;
}

bool MarsGraphics::pushClipArea(Rectangle area)
{
    SDL_Rect rect;
    bool result = Graphics::pushClipArea(area);

    if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
	    const ClipRectangle& carea = mClipStack.top();
	    rect.x = carea.x;
	    rect.y = carea.y;
	    rect.w = carea.width;
	    rect.h = carea.height;
	
	    SDL_SetClipRect(mTarget, &rect);
    }
    #ifdef WITH_OPENGL
    else
    {
        glScissor(mClipStack.top().x,
              mTarget->h - mClipStack.top().y - mClipStack.top().height,
              mClipStack.top().width,
              mClipStack.top().height);
    }
    #endif
    return result;
}

void MarsGraphics::popClipArea()
{
    Graphics::popClipArea();

    if (mClipStack.empty())
    {
        return;
    }
    if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
	    const ClipRectangle& carea = mClipStack.top();
	    SDL_Rect rect;
	    rect.x = carea.x;
	    rect.y = carea.y;
	    rect.w = carea.width;
	    rect.h = carea.height;
	
	    SDL_SetClipRect(mTarget, &rect);
    }
    #ifdef WITH_OPENGL
    else
    {
        glScissor(mClipStack.top().x,
	      	mTarget->h - mClipStack.top().y - mClipStack.top().height,
	      	mClipStack.top().width,
	      	mClipStack.top().height);
    }
    #endif
}

SDL_Surface* MarsGraphics::getTarget() const
{
    return mTarget;
}

void MarsGraphics::drawImage(const Image* image,
                            int srcX,
                            int srcY,
                            int dstX,
                            int dstY,
                            int width,
                            int height)
{
    if (mClipStack.empty())
    {
        throw GCN_EXCEPTION("Clip stack is empty, perhaps you called a draw funtion outside of _beginDraw() and _endDraw()?");
    }

    const ClipRectangle& top = mClipStack.top();

    SDL_Rect src;
    SDL_Rect dst;
    src.x = srcX;
    src.y = srcY;
    src.w = width;
    src.h = height;
    dst.x = dstX + top.xOffset;
    dst.y = dstY + top.yOffset;

    const MarsImage* srcImage = dynamic_cast<const MarsImage*>(image);

    if (srcImage == NULL)
    {
        throw GCN_EXCEPTION("Trying to draw an image of unknown format, must be an MarsImage.");
    }
    
    if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
    	graphic::blitter->BlitPart(srcImage->getSurface(), src, dst);
    }
    #ifdef WITH_OPENGL
    else
    {
        glEnable(GL_TEXTURE_2D);
    	graphic::blitter->BlitPart(srcImage->getSurface(), src, dst);
    	glDisable(GL_TEXTURE_2D);
    }
    #endif
    
}

void MarsGraphics::drawMarsSurface(graphic::Surface* surface,
                                 SDL_Rect source,
                                 SDL_Rect destination)
{
    if (mClipStack.empty())
    {
        throw GCN_EXCEPTION("Clip stack is empty, perhaps you called a draw funtion outside of _beginDraw() and _endDraw()?");
    }

    const ClipRectangle& top = mClipStack.top();

    destination.x += top.xOffset;
    destination.y += top.yOffset;

    graphic::blitter->BlitPart(surface, source, destination);
}

void MarsGraphics::drawMarsFont(graphic::Text * font, const std::string& text, int x, int y)
{
    if (mClipStack.empty())
    {
        throw GCN_EXCEPTION("Clip stack is empty, perhaps you called a draw funtion outside of _beginDraw() and _endDraw()?");
    }

    const ClipRectangle& top = mClipStack.top();

    #ifdef WITH_OPENGL
        glEnable(GL_TEXTURE_2D);
	#endif
	font->WriteText(text,x + top.xOffset, y + top.yOffset); 
	#ifdef WITH_OPENGL
    	glDisable(GL_TEXTURE_2D);
    #endif
}


void MarsGraphics::fillRectangle(const Rectangle& rectangle)
{
    if (mClipStack.empty())
    {
        throw GCN_EXCEPTION("Clip stack is empty, perhaps you called a draw funtion outside of _beginDraw() and _endDraw()?");
    }

    const ClipRectangle& top = mClipStack.top();
    
    if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
	    Rectangle area = rectangle;
	    area.x += top.xOffset;
	    area.y += top.yOffset;
	
	    if(!area.isIntersecting(top))
	    {
	        return;
	    }
	
	    if (mAlpha)
	    {
	        int x1 = area.x > top.x ? area.x : top.x;
	        int y1 = area.y > top.y ? area.y : top.y;
	        int x2 = area.x + area.width < top.x + top.width ? area.x + area.width : top.x + top.width;
	        int y2 = area.y + area.height < top.y + top.height ? area.y + area.height : top.y + top.height;
	        int x, y;
	
	        SDL_LockSurface(mTarget);
	        for (y = y1; y < y2; y++)
	        {
	            for (x = x1; x < x2; x++)
	            {
	                SDLputPixelAlpha(mTarget, x, y, mColor);
	            }
	        }
	        SDL_UnlockSurface(mTarget);
	
	    }
	    else
	    {
	        SDL_Rect rect;
	        rect.x = area.x;
	        rect.y = area.y;
	        rect.w = area.width;
	        rect.h = area.height;
	
	        Uint32 color = SDL_MapRGBA(mTarget->format,
	                                   mColor.r,
	                                   mColor.g,
	                                   mColor.b,
	                                   mColor.a);
	        SDL_FillRect(mTarget, &rect, color);
	    }
    }
    #ifdef WITH_OPENGL
    else
    {
	    glBegin(GL_QUADS);
	    glVertex2i(rectangle.x + top.xOffset,
	               rectangle.y + top.yOffset);
	    glVertex2i(rectangle.x + rectangle.width + top.xOffset,
	               rectangle.y + top.yOffset);
	    glVertex2i(rectangle.x + rectangle.width + top.xOffset,
	               rectangle.y + rectangle.height + top.yOffset);
	    glVertex2i(rectangle.x + top.xOffset,
	               rectangle.y + rectangle.height + top.yOffset);
	    glEnd();
    }
    #endif
}

void MarsGraphics::drawPoint(int x, int y)
{
    if (mClipStack.empty())
    {
        throw GCN_EXCEPTION("Clip stack is empty, perhaps you called a draw funtion outside of _beginDraw() and _endDraw()?");
    }

    const ClipRectangle& top = mClipStack.top();
    
    x += top.xOffset;
    y += top.yOffset;

    if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
	    if(!top.isPointInRect(x,y))
	        return;
	    if (mAlpha)
	    {
	        SDLputPixelAlpha(mTarget, x, y, mColor);
	    }
	    else
	    {
	        SDLputPixel(mTarget, x, y, mColor);
	    }
    }
    #ifdef WITH_OPENGL
    else
    {
        glBegin(GL_POINTS);
        glVertex2i(x, y);
        glEnd();
    }
    #endif
}

void MarsGraphics::drawHLine(int x1, int y, int x2)
{
    if (mClipStack.empty())
    {
        throw GCN_EXCEPTION("Clip stack is empty, perhaps you called a draw funtion outside of _beginDraw() and _endDraw()?");
    }

    const ClipRectangle& top = mClipStack.top();

    x1 += top.xOffset;
    y += top.yOffset;
    x2 += top.xOffset;

    if (y < top.y || y >= top.y + top.height)
    {
        return;
    }

    if (x1 > x2)
    {
        x1 ^= x2;
        x2 ^= x1;
        x1 ^= x2;
    }

    if (top.x > x1)
    {
        if (top.x > x2)
        {
            return;
        }

        x1 = top.x;
    }

    if (top.x + top.width <= x2)
    {
        if (top.x + top.width <= x1)
        {
            return;
        }
        
        x2 = top.x + top.width -1;
    }

    int bpp = mTarget->format->BytesPerPixel;

    SDL_LockSurface(mTarget);

    Uint8 *p = (Uint8 *)mTarget->pixels + y * mTarget->pitch + x1 * bpp;

    Uint32 pixel = SDL_MapRGB(mTarget->format,
                              mColor.r,
                              mColor.g,
                              mColor.b);
    switch(bpp)
    {
        case 1:
            for (;x1 <= x2; ++x1)
            {
                *(p++) = pixel;
            }
            break;
            
        case 2:
        {
            Uint16* q = (Uint16*)p;
            for (;x1 <= x2; ++x1)
            {
                *(q++) = pixel;
            }
            break;
        }
        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            {
                for (;x1 <= x2; ++x1)
                {
                    p[0] = (pixel >> 16) & 0xff;
                    p[1] = (pixel >> 8) & 0xff;
                    p[2] = pixel & 0xff;
                    p += 3;
                }
            }
            else
            {
                for (;x1 <= x2; ++x1)
                {
                    p[0] = pixel & 0xff;
                    p[1] = (pixel >> 8) & 0xff;
                    p[2] = (pixel >> 16) & 0xff;
                    p += 3;
                }
            }
            break;

        case 4:  
        {          
            Uint32* q = (Uint32*)p;
            for (;x1 <= x2; ++x1)
            {
                if (mAlpha)
                {
                    *q = SDLAlpha32(pixel,*q,mColor.a);
                    q++;
                }
                else
                {
                    *(q++) = pixel;
                }
            }
            break;
        }
            
    } // end switch

    SDL_UnlockSurface(mTarget);
}

void MarsGraphics::drawVLine(int x, int y1, int y2)
{
    if (mClipStack.empty())
    {
        throw GCN_EXCEPTION("Clip stack is empty, perhaps you called a draw funtion outside of _beginDraw() and _endDraw()?");
    }

    const ClipRectangle& top = mClipStack.top();

    x += top.xOffset;
    y1 += top.yOffset;
    y2 += top.yOffset;

    if (x < top.x || x >= top.x + top.width)
    {
        return;
    }
    
    if (y1 > y2)
    {
        y1 ^= y2;
        y2 ^= y1;
        y1 ^= y2;
    }

    if (top.y > y1)
    {
        if (top.y > y2)
        {
            return;
        }

        y1 = top.y;
    }

    if (top.y + top.height <= y2)
    {
        if (top.y + top.height <= y1)
        {
            return;
        }

        y2 = top.y + top.height - 1;
    }

    int bpp = mTarget->format->BytesPerPixel;

    SDL_LockSurface(mTarget);

    Uint8 *p = (Uint8 *)mTarget->pixels + y1 * mTarget->pitch + x * bpp;

    Uint32 pixel = SDL_MapRGB(mTarget->format, mColor.r, mColor.g, mColor.b);

    switch(bpp)
    {            
      case 1:
          for (;y1 <= y2; ++y1)
          {
              *p = pixel;
              p += mTarget->pitch;
          }
          break;

      case 2:
          for (;y1 <= y2; ++y1)
          {
              *(Uint16*)p = pixel;
              p += mTarget->pitch;
          }
          break;

      case 3:
          if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
          {
              for (;y1 <= y2; ++y1)
              {
                  p[0] = (pixel >> 16) & 0xff;
                  p[1] = (pixel >> 8) & 0xff;
                  p[2] = pixel & 0xff;
                  p += mTarget->pitch;
              }
          }
          else
          {
              for (;y1 <= y2; ++y1)
              {
                  p[0] = pixel & 0xff;
                  p[1] = (pixel >> 8) & 0xff;
                  p[2] = (pixel >> 16) & 0xff;
                  p += mTarget->pitch;
              }
          }
          break;

      case 4:
          for (;y1 <= y2; ++y1)
          {
              if (mAlpha)
              {
                  *(Uint32*)p = SDLAlpha32(pixel,*(Uint32*)p,mColor.a);
              }
              else
              {
                  *(Uint32*)p = pixel;
              }
              p += mTarget->pitch;
          }
          break;
          
    } // end switch
    
    SDL_UnlockSurface(mTarget);
}

void MarsGraphics::drawRectangle(const Rectangle& rectangle)
{
    if (mClipStack.empty())
    {
        throw GCN_EXCEPTION("Clip stack is empty, perhaps you called a draw funtion outside of _beginDraw() and _endDraw()?");
    }
        
	if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {    
	    int x1 = rectangle.x;
	    int x2 = rectangle.x + rectangle.width - 1;
	    int y1 = rectangle.y;
	    int y2 = rectangle.y + rectangle.height - 1;
	
	    drawHLine(x1, y1, x2);
	    drawHLine(x1, y2, x2);
	
	    drawVLine(x1, y1, y2);
	    drawVLine(x2, y1, y2);
    }
    #ifdef WITH_OPENGL
    else
    {
    	const ClipRectangle& top = mClipStack.top();
        glBegin(GL_LINE_LOOP);
        glVertex2f(rectangle.x + top.xOffset,
                   rectangle.y + top.yOffset);
        glVertex2f(rectangle.x + rectangle.width + top.xOffset - 1.0f,
                   rectangle.y + top.yOffset + 0.375f);
        glVertex2f(rectangle.x + rectangle.width + top.xOffset - 1.0f,
                   rectangle.y + rectangle.height + top.yOffset);
        glVertex2f(rectangle.x + top.xOffset,
                   rectangle.y + rectangle.height + top.yOffset);
        glEnd();
    }
    #endif
}

void MarsGraphics::drawLine(int x1, int y1, int x2, int y2)
{
	if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
	    if (x1 == x2)
	    {
	        drawVLine(x1, y1, y2);
	        return;
	    }
	    if (y1 == y2)
	    {
	        drawHLine(x1, y1, x2);
	        return;
	    }
    }

    if (mClipStack.empty())
    {
        throw GCN_EXCEPTION("Clip stack is empty, perhaps you called a draw funtion outside of _beginDraw() and _endDraw()?");
    }

    const ClipRectangle& top = mClipStack.top();

    x1 += top.xOffset;
    y1 += top.yOffset;
    x2 += top.xOffset;
    y2 += top.yOffset;

    if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
    	// Draw a line with Bresenham
	    int dx = ABS(x2 - x1);
	    int dy = ABS(y2 - y1);
	
	    if (dx > dy)
	    {
	        if (x1 > x2)
	        {
	            // swap x1, x2
	            x1 ^= x2;
	            x2 ^= x1;
	            x1 ^= x2;
	
	            // swap y1, y2
	            y1 ^= y2;
	            y2 ^= y1;
	            y1 ^= y2;
	        }
	
	        if (y1 < y2)
	        {
	            int y = y1;
	            int p = 0;
	
	            for (int x = x1; x <= x2; x++)
	            {
	                if (top.isPointInRect(x, y))
	                {
	                    if (mAlpha)
	                    {
	                        SDLputPixelAlpha(mTarget, x, y, mColor);
	                    }
	                    else
	                    {
	                        SDLputPixel(mTarget, x, y, mColor);
	                    }
	                }
	
	                p += dy;
	
	                if (p * 2 >= dx)
	                {
	                    y++;
	                    p -= dx;
	                }
	            }
	        }
	        else
	        {
	            int y = y1;
	            int p = 0;
	
	            for (int x = x1; x <= x2; x++)
	            {
	                if (top.isPointInRect(x, y))
	                {
	                    if (mAlpha)
	                    {
	                        SDLputPixelAlpha(mTarget, x, y, mColor);
	                    }
	                    else
	                    {
	                        SDLputPixel(mTarget, x, y, mColor);
	                    }
	                }
	
	                p += dy;
	
	                if (p * 2 >= dx)
	                {
	                    y--;
	                    p -= dx;
	                }
	            }
	        }
	    }
	    else
	    {
	        if (y1 > y2)
	        {
	            // swap y1, y2
	            y1 ^= y2;
	            y2 ^= y1;
	            y1 ^= y2;
	
	            // swap x1, x2
	            x1 ^= x2;
	            x2 ^= x1;
	            x1 ^= x2;
	        }
	
	        if (x1 < x2)
	        {
	            int x = x1;
	            int p = 0;
	
	            for (int y = y1; y <= y2; y++)
	            {
	                if (top.isPointInRect(x, y))
	                {
	                    if (mAlpha)
	                    {
	                        SDLputPixelAlpha(mTarget, x, y, mColor);
	                    }
	                    else
	                    {
	                        SDLputPixel(mTarget, x, y, mColor);
	                    }
	                }
	
	                p += dx;
	
	                if (p * 2 >= dy)
	                {
	                    x++;
	                    p -= dy;
	                }
	            }
	        }
	        else
	        {
	            int x = x1;
	            int p = 0;
	
	            for (int y = y1; y <= y2; y++)
	            {
	                if (top.isPointInRect(x, y))
	                {
	                    if (mAlpha)
	                    {
	                        SDLputPixelAlpha(mTarget, x, y, mColor);
	                    }
	                    else
	                    {
	                        SDLputPixel(mTarget, x, y, mColor);
	                    }
	                }
	
	                p += dx;
	
	                if (p * 2 >= dy)
	                {
	                    x--;
	                    p -= dy;
	                }
	            }
	        }
	    }
    }
    #ifdef WITH_OPENGL
    else
    {
 		glBegin(GL_LINES);
        glVertex2f(x1 + 0.375f,
                   y1 + 0.375f);
        glVertex2f(x2 + 1.0f - 0.375f,
                   y2 + 1.0f - 0.375f);
        glEnd();

        glBegin(GL_POINTS);
        glVertex2f(x2 + 1.0f - 0.375f,
                   y2 + 1.0f - 0.375f);
        glEnd();

        glBegin(GL_POINTS);
        glVertex2f(x1 + 0.375f,
                   y1 + 0.375f);
        glEnd();
    }
    #endif
}

void MarsGraphics::setColor(const Color& color)
{
	if( graphic::screen->GetGraphicMode() == SDL_GRAPHIC)
    {
	    mColor = color;
	    mAlpha = color.a != 255;
    }
    #ifdef WITH_OPENGL
    else
    {
        mColor = color;
        glColor4ub(color.r, color.g, color.b, color.a);

        mAlpha = color.a != 255;

     /*   if (mAlpha)
        {
            glEnable(GL_BLEND);
        }*/
    }
    #endif
}

const Color& MarsGraphics::getColor() const
{
    return mColor;
}



#ifndef GCN_MARSIMAGELOADER_HPP
#define GCN_MARSIMAGELOADER_HPP

#include "../../graphic/GraphicTracker.h"

#include "MarsImage.h"

#include "guichan/imageloader.hpp"

namespace gcn
{
    class Image;

    /**
     * Mars implementation of ImageLoader.
     */
    class GCN_EXTENSION_DECLSPEC MarsImageLoader : public ImageLoader
    {
    public:

        // Inherited from ImageLoader
        virtual Image* load(const std::string& filename, bool convertToDisplayFormat = true);

    };
}

#endif // end GCN_MARSIMAGELOADER_HPP

#include "MarsImage.h"

using namespace gcn;

MarsImage::MarsImage(graphic::Surface* surface, bool autoFree)
{
    mAutoFree = autoFree;
    _mSurface = surface;
}

MarsImage::~MarsImage()
{
	
}

graphic::Surface* MarsImage::getSurface() const
{
    return _mSurface;
}

int MarsImage::getWidth() const
{
    if (_mSurface == NULL)
    {
        throw GCN_EXCEPTION("Trying to get the width of a non loaded image.");
    }

    return _mSurface->GetW();
}

int MarsImage::getHeight() const
{
    if (_mSurface == NULL)
    {
        throw GCN_EXCEPTION("Trying to get the height of a non loaded image.");
    }

    return _mSurface->GetH();
}

Color MarsImage::getPixel(int x, int y)
{
    if (_mSurface == NULL)
    {
        throw GCN_EXCEPTION("Trying to get a pixel of a non loaded image.");
    }		
    
    _mSurface->Lock();
    
    Color c = _mSurface->GetPixelColor(x,y);

    _mSurface->Unlock();
    
    return c;
}

void MarsImage::putPixel(int x, int y, const Color& color)
{
    if (_mSurface == NULL)
    {
        throw GCN_EXCEPTION("Trying to set a pixel of a non loaded image.");
    }		
    _mSurface->Lock();
    _mSurface->SetPixelColor(x,y,color);
    _mSurface->Unlock();
}


#include "MarsImageLoader.h"

gcn::Image* gcn::MarsImageLoader::load(const std::string& filename, bool convertToDisplayFormat)
{
	graphic::Surface *mimg = NULL;
	try
	{
    	mimg = graphic::gtracker->GetImage(filename, graphic::screen->GetGraphicMode());
	}
	catch(Exception e)
	{
		e.PrintError();
	}
	

    if (mimg == NULL)
    {
        throw GCN_EXCEPTION(
                std::string("Unable to load image file:: ") + filename);
    }
    
    gcn::MarsImage *img = new gcn::MarsImage(mimg, true);
    
    return img;
}



#include "guichan/widgets/MiniMapPanel.h"
#include "../mars/MarsImageLoader.h"

namespace gcn
{
    MiniMapPanel::MiniMapPanel(IsoMiniMap * mm, SceneRenderer * renderer)
    {
        _mm = mm;
        _renderer = renderer;
        //get empty surface
        _surf = graphic::gtracker->GetSurface(_mm->GetTileMapWidth() / _mm->GetScale()
        										 , _mm->GetTileMapHeight() / _mm->GetScale());
        _limits_rect.x = 0;
        _limits_rect.y = 0;
        _limits_rect.w = _mm->GetTileMapWidth() / _mm->GetScale();
        _limits_rect.h = _mm->GetTileMapHeight() / _mm->GetScale();
        _mm->BlitLayer(_limits_rect, _surf);
        _image = new gcn::MarsImage(_surf, true);
        _icon = new gcn::Icon(_image);
        setSize(_limits_rect.w, _limits_rect.h);
		setWidth(MMW);
		setHeight(MMH);
		setContent(_icon);
	    setScrollPolicy(gcn::ScrollArea::SHOW_ALWAYS, gcn::ScrollArea::SHOW_ALWAYS);
	    _viewx = _viewy = _vieww = _viewh = -1;
    }


    MiniMapPanel::~MiniMapPanel()
    {
    	delete _icon;
		delete _mm;
    }

	void MiniMapPanel::UpdatePanel(int choice)
	{
		// Move minimap content, scrolling
		if( choice == MM_SCROLLING)
		{
			int x = _renderer->GetViewX();
			int y = _renderer->GetViewY();
			int w = _renderer->GetViewW();
			int h = _renderer->GetViewH();
			if( x != _viewx || y != _viewy || w != _vieww || h != _viewh)
			{
				_viewx = x;
				_viewy = y;
				_vieww = w;
				_viewh = h;
				int scale = _mm->GetScale();
				// Place center of the screen (whatever size) to the center of the minimap (whatever size)
				SetContentPosition((x/scale) + ((w/2)/scale) - getWidth()/2 , (y/scale) + ((h/2)/scale) - getHeight()/2);
			}
		}
		else // element has changed location
		{
			_mm->UpdateMap();
			_mm->BlitLayer(_limits_rect, _surf);
		}
	}

    void MiniMapPanel::draw(Graphics* graphics)
    {
        if (mVBarVisible)
        {
            drawUpButton(graphics);
            drawDownButton(graphics);
            drawVBar(graphics);
            drawVMarker(graphics);
        }

        if (mHBarVisible)
        {
            drawLeftButton(graphics);
            drawRightButton(graphics);
            drawHBar(graphics);
            drawHMarker(graphics);
        }

        if (mHBarVisible && mVBarVisible)
        {
            graphics->setColor(getBaseColor());
            graphics->fillRectangle(Rectangle(getWidth() - mScrollbarWidth,
                                              getHeight() - mScrollbarWidth,
                                              mScrollbarWidth,
                                              mScrollbarWidth));
        }

        drawChildren(graphics);
        // Draw screen indicator
        Color shadowColor = getBaseColor() - 0x303030;
        shadowColor.a = getBaseColor().a;
        graphics->setColor(shadowColor);
        int scale = _mm->GetScale();
        int x = _viewx/scale - getHorizontalScrollAmount();
        int y = _viewy/scale - getVerticalScrollAmount();
        //top line
        graphics->drawLine(x, y, x + (_vieww/scale), y);
        //bottom line
        graphics->drawLine(x, y + (_viewh/scale), x + (_vieww/scale), y  + (_viewh/scale));
        //left line
        graphics->drawLine(x, y, x, y  + (_viewh/scale));
        //right line
        graphics->drawLine(x + (_vieww/scale), y , x + (_vieww/scale), y  + (_viewh/scale));
    }
    
    void MiniMapPanel::mousePressed(MouseEvent& mouseEvent)
    {
        int x = mouseEvent.getX();
        int y = mouseEvent.getY();

        if (getUpButtonDimension().isPointInRect(x, y))
        {
            setVerticalScrollAmount(getVerticalScrollAmount()
                                    - mUpButtonScrollAmount);
            mUpButtonPressed = true;
        }
        else if (getDownButtonDimension().isPointInRect(x, y))
        {
            setVerticalScrollAmount(getVerticalScrollAmount()
                                    + mDownButtonScrollAmount);
            mDownButtonPressed = true;
        }
        else if (getLeftButtonDimension().isPointInRect(x, y))
        {
            setHorizontalScrollAmount(getHorizontalScrollAmount()
                                      - mLeftButtonScrollAmount);
            mLeftButtonPressed = true;
        }
        else if (getRightButtonDimension().isPointInRect(x, y))
        {
            setHorizontalScrollAmount(getHorizontalScrollAmount()
                                      + mRightButtonScrollAmount);
            mRightButtonPressed = true;
        }
        else if (getVerticalMarkerDimension().isPointInRect(x, y))
        {
            mIsHorizontalMarkerDragged = false;
            mIsVerticalMarkerDragged = true;

            mVerticalMarkerDragOffset = y - getVerticalMarkerDimension().y;
        }
        else if (getVerticalBarDimension().isPointInRect(x,y))
        {
            if (y < getVerticalMarkerDimension().y)
            {
                setVerticalScrollAmount(getVerticalScrollAmount()
                                        - (int)(getChildrenArea().height * 0.95));
            }
            else
            {
                setVerticalScrollAmount(getVerticalScrollAmount()
                                        + (int)(getChildrenArea().height * 0.95));
            }
        }
        else if (getHorizontalMarkerDimension().isPointInRect(x, y))
        {
            mIsHorizontalMarkerDragged = true;
            mIsVerticalMarkerDragged = false;

            mHorizontalMarkerDragOffset = x - getHorizontalMarkerDimension().x;
        }
        else if (getHorizontalBarDimension().isPointInRect(x,y))
        {
            if (x < getHorizontalMarkerDimension().x)
            {
                setHorizontalScrollAmount(getHorizontalScrollAmount()
                                          - (int)(getChildrenArea().width * 0.95));
            }
            else
            {
                setHorizontalScrollAmount(getHorizontalScrollAmount()
                                          + (int)(getChildrenArea().width * 0.95));
            }
        }
        else if(getChildrenArea().isPointInRect(x,y))
        {
        	int scale = _mm->GetScale();
        	_renderer->UpdateViewPosition((x + getHorizontalScrollAmount()) * scale - _renderer->GetViewW()/2,
        								  (y + getVerticalScrollAmount()) * scale - _renderer->GetViewH()/2);
			_renderer->RequestBufferRefresh();
        }
    }
    
}



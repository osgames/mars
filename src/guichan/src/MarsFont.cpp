#include "guichan/MarsFont.h"

#include "guichan/exception.hpp"
#include "guichan/image.hpp"

#include "mars/MarsGraphics.h"

namespace gcn
{    
    MarsFont::MarsFont (const std::string& filename, SDL_Color color, int size)
    {
        mRowSpacing = 0;
        mGlyphSpacing = 0;
        mAntiAlias = true;        
        mFilename = filename;
        mFont = NULL;
        
        try
        {
			_marsfont = new graphic::Text(filename, color, size);
        }
		catch(Exception e)
		{
			e.PrintError();
			exit(-1);
		}
    }
    
    MarsFont::~MarsFont()
    {
        if(_marsfont != NULL)
        	delete _marsfont;
    }
  
    int MarsFont::getWidth(const std::string& text) const
    {
		return _marsfont->GetTextW(text) + _marsfont->GetTextW("  ");
    }

    int MarsFont::getHeight() const
    {
        return _marsfont->GetCharH() + mRowSpacing;
    }
    
    void MarsFont::drawString(Graphics* graphics, const std::string& text, const int x, const int y)
    {
        if (text == "")
        {
            return;
        }
        
        std::string paddedtext = text;
        paddedtext.insert(paddedtext.begin(), ' ');
        paddedtext.insert(paddedtext.end(), ' ');
        
		dynamic_cast<MarsGraphics *>(graphics)->drawMarsFont(_marsfont, paddedtext, x, y);     
    }
    
    void MarsFont::setRowSpacing(int spacing)
    {
        mRowSpacing = spacing;
    }

    int MarsFont::getRowSpacing()
    {
        return mRowSpacing;
    }
    
    void MarsFont::setGlyphSpacing(int spacing)
    {
        mGlyphSpacing = spacing;
    }
    
    int MarsFont::getGlyphSpacing()
    {
        return mGlyphSpacing;
    }

    void MarsFont::setAntiAlias(bool antiAlias)
    {
        mAntiAlias = antiAlias;
    }

    bool MarsFont::isAntiAlias()
    {
        return mAntiAlias;        
    }    
}


#include "GuiManager.h"

using namespace gui;
//using namespace gcn;

GuiManager::GuiManager(gcn::ActionListener * gst)
{		
	try
	{
		//Uses mars graphictracker
		_imageloader = new gcn::MarsImageLoader();
		gcn::Image::setImageLoader(_imageloader);
		//Uses mars images
		_graphics = new gcn::MarsGraphics();
		_graphics->setTarget(graphic::screen->GetSurface());
		//Listens to raw sdl events from the event system
		_input = new gcn::SDLInput();
		SDL_Color txt_color = {0xFF, 0XFF, 0xFF, 0};
		_font = new gcn::MarsFont((data_dir + MARS_FONT), txt_color, MARS_DIM_FONT);
		gcn::Widget::setGlobalFont(_font); 
		//Instance of gui chan
		_gui = new gcn::Gui();
		_gui->setGraphics(_graphics);
		_gui->setInput(_input);
		//Gui action listener that toggles widgets visible state
		_guilistener = new GuiListener();
		_gst = gst;
		_panel = NULL;
	}
	catch(gcn::Exception e)
	{
		e.PrintError();
	}

}

GuiManager::~GuiManager()
{
	delete _imageloader;
	delete _graphics;
	delete _input;
	delete _font;
	delete _gui;
	delete _guilistener;
	delete _imm;
	//all subwidgets are deleted
	for(unsigned int x=0; x != _element_widgets.size(); x++)
		delete _element_widgets[x];
	delete _topcontainer;
}

void GuiManager::ParseGui(std::string path)
{
	//Gui xml reader, it creates all widgets
	XmlGui * xmlgui = new XmlGui();
	xmlgui->addActionListener("mars", _gst);
	xmlgui->addActionListener("gui", _guilistener);
	// Use default gui if no path is provided
	if(path.empty())
		xmlgui->parse(data_dir + "xml/gui.xml");
	else
		xmlgui->parse(path);
	//Store all widgets in member class
	_element_widgets = xmlgui->GetElementWidgets();
	//The top widget stores all subwidgets, guichan need to know with is the top widget
	_topcontainer = xmlgui->getWidget("top");
	_topcontainer->setDimension(gcn::Rectangle(0, 0, graphic::screen->GetW(), graphic::screen->GetH()));
	_gui->setTop(_topcontainer);
	_guilistener->SetTop(_topcontainer);
	_focushandler = _topcontainer->_getFocusHandler();
	// Deploy list that is used by the deploy window
	_deploylist = new gcn::DeployList();
	//Not needed anymore
	delete xmlgui;
}

bool GuiManager::HitWidget(int x, int y)
{
	return dynamic_cast<gcn::Container *>(_topcontainer)->getWidgetAt(x,y);
}

void GuiManager::SetDisabled(std::string name, bool dis)
{
	gcn::Widget *widget = dynamic_cast<gcn::Container *>(_topcontainer)->findWidgetById(name);
	gcn::ImageButton *button = dynamic_cast<gcn::ImageButton *>(widget);
	if(button)
		button->setDisabled(dis);
}

void GuiManager::SetVisible(std::string name, bool vis)
{
	gcn::Widget *widget = dynamic_cast<gcn::Container *>(_topcontainer)->findWidgetById(name);
	if(widget)
		widget->setVisible(vis);
}

void GuiManager::SetText(std::string name, std::string text)
{
	gcn::Widget *widget = dynamic_cast<gcn::Container *>(_topcontainer)->findWidgetById(name);
	if(widget)
	{
		gcn::TextBox *textbox = dynamic_cast<gcn::TextBox *>(widget);
		if(textbox)
		{
			textbox->setText(text);
			//Resize the parent window in case the textbox got larger
			gcn::Window *win = dynamic_cast<gcn::Window *>(textbox->getParent());
			if(win)
				win->resizeToContent();	
		}
	}
}

void GuiManager::SetComponent(std::string name, int id)
{
	gcn::Widget *widget = dynamic_cast<gcn::Container *>(_topcontainer)->findWidgetById(name);
	gcn::ElementTextBox *box = dynamic_cast<gcn::ElementTextBox *>(widget);
	if(box)
		box->SetComponent(id);
}

void GuiManager::SetMarked(std::string name, bool vis)
{
	gcn::Widget *widget = dynamic_cast<gcn::Container *>(_topcontainer)->findWidgetById(name);
	gcn::CheckBox *box = dynamic_cast<gcn::CheckBox *>(widget);
	if(box)
		box->setSelected(vis);
	gcn::RadioButton *button = dynamic_cast<gcn::RadioButton *>(widget);
	if(button)
		button->setSelected(vis);
}

void GuiManager::InitMiniMap(SceneRenderer * renderer)
{
	_panel = new gcn::MiniMapPanel(_imm, renderer);
	gcn::Window *window = new gcn::Window;
	window->add(_panel);
	window->setCaption("MiniMap");
	window->setX(5);
	window->setY(5);
	window->SetSnapTo(WIN_BOTTOM_LEFT);
	window->SetAlignTo(WIN_BOTTOM_LEFT);
	window->resizeToContent();
	static_cast<gcn::Container *>(_topcontainer)->add(window);
}

// ***** Private ***** //
void GuiManager::ElementUpdate()
{
	if(_selected == NULL)
		return;
	
	int comp = -1;
	// Update witch component holds the widgets data
	comp = _selected->FindComponentType("rocket");
	if(_selected && comp != -1)
	{	//set widget "name" to use component 'comp'. Does nothing if widget does not exist
		SetComponent("rdamage",comp);
		SetComponent("rrange",comp);
		SetComponent("rammo",comp);
	}
	comp = _selected->FindComponentType("cannon");
	if(_selected && comp != -1)
	{
		SetComponent("cdamage",comp);
		SetComponent("crange",comp);
		SetComponent("cammo",comp);
	}
/*	comp = _selected->FindComponentType("equpiment");
	if(_selected && comp != -1)
	{
		SetComponent("evision",comp);
		SetComponent("ehitpoints",comp);
		SetComponent("edefence",comp);
	}
	comp = _selected->FindComponentType("chassis");
	if(_selected && comp != -1)
	{
		SetComponent("cmovecost",comp);
		SetComponent("cvision",comp);
		SetComponent("chitpoints",comp);
		SetComponent("cdefence",comp);
	}	*/
		
	for(unsigned int i = 0; i < _element_widgets.size(); i++)
	{
		gcn::ElementTextBox *box = dynamic_cast<gcn::ElementTextBox *>(_element_widgets[i]);
		if(box)
		{
			if(_selected)
			{	//Update widget if new selected
				box->UpdateMessage(_selected);
				gcn::Window *win = dynamic_cast<gcn::Window *>(box->getParent());
				if(win)
					win->resizeToContent();
			}
			else
			{	//There is no selected element, hide widgets parent window
				gcn::Window *win = dynamic_cast<gcn::Window *>(box->getParent());
				if(win)
					win->setVisible(false);				
			}
		}
		gcn::ElementPilotImg *image = dynamic_cast<gcn::ElementPilotImg *>(_element_widgets[i]);
		if(image)
		{
			if(_selected)
			{	//Update widget if new selected
				std::string path = _selected->GetString("pilotimg");
				if(path.compare(" N/A "))
				{
					// There is an image path load image, old image deleted in ElementPilotImg
					image->UpdateImage(_imageloader->load(data_dir + path));
				}
				else
				{
					//image loading failed there is no image for this element or wrong path
					//Using default image instead
					image->UpdateImage(NULL);	
				}
				gcn::Window *win = dynamic_cast<gcn::Window *>(image->getParent());
				if(win)
					win->resizeToContent();
				}
			else
			{	//There is no selected element, hide widgets parent window
				gcn::Window *win = dynamic_cast<gcn::Window *>(image->getParent());
				if(win)
					win->setVisible(false);				
			}
		}
		gcn::ElementImg *elimage = dynamic_cast<gcn::ElementImg *>(_element_widgets[i]);
		if(elimage)
		{
			if(_selected)
			{
				// Set already loaded element image as gui image
				graphic::Surface * surf = _selected->GetImage();
				// Old image is deleted in ElementImg::UpdateImage
				elimage->UpdateImage(new gcn::MarsImage(surf, true));
				gcn::Window *win = dynamic_cast<gcn::Window *>(elimage->getParent());
				if(win)
					win->resizeToContent();
				}
			else
			{	//There is no selected element, hide widgets parent window
				gcn::Window *win = dynamic_cast<gcn::Window *>(elimage->getParent());
				if(win)
					win->setVisible(false);				
			}
		}
		gcn::ComponentTextBox *combox = dynamic_cast<gcn::ComponentTextBox *>(_element_widgets[i]);
		if(combox)
		{
			if(_selected)
			{	//Update widget if new selected
				combox->UpdateMessage(_selected);
				gcn::Window *win = dynamic_cast<gcn::Window *>(combox->getParent());
				if(win)
					win->resizeToContent();
			}
			else
			{	//There is no selected element, hide widgets parent window
				gcn::Window *win = dynamic_cast<gcn::Window *>(combox->getParent());
				if(win)
					win->setVisible(false);				
			}
		}
	}
}

void GuiManager::SetPlayer(Player * player)
{
	#ifdef DEBUG_MODE
		printf("<GuiManager::SetPlayer(Player * player))>\nInput: %d\n", (int)player);
	#endif
	gcn::Widget * deploy = dynamic_cast<gcn::Container *>(_topcontainer)->findWidgetById("Deploy");
	if(deploy == NULL || !deploy->isVisible() || !player->GetNumHiddenElements())
	{
		#ifdef DEBUG_MODE
			printf("Output: Failed\n</GuiManager::SetPlayer(Player * player))>\n");
		#endif			
		return;
	}
	gcn::ListBox * deploybox = dynamic_cast<gcn::ListBox *>(dynamic_cast<gcn::Container *>(_topcontainer)->findWidgetById("deploybox"));
	if(deploybox == NULL)
	{
		#ifdef DEBUG_MODE
			printf("Output: Failed\n</GuiManager::SetPlayer(Player * player))>\n");
		#endif			
		return;
	}
	_player = player;
	_deploylist->SetPlayer(_player);
	deploybox->setListModel(_deploylist);
	deploybox->setSelected(0);
	NewHiddenSelected();
}

void GuiManager::NewHiddenSelected()
{
	// Find deploybox
	gcn::ListBox * deploybox = dynamic_cast<gcn::ListBox *>(dynamic_cast<gcn::Container *>(_topcontainer)->findWidgetById("deploybox"));
	if(deploybox == NULL)
		return;
	// Find the selected unit	
	Element * el = _player->GetHiddenElement(deploybox->getSelected());
	if(el == NULL)
		return;
	// Set as selected and updated element widgets	
	_selected = el;
	ElementUpdate();
	if(_player->GetNumHiddenElements() == 0)
	{
		SetVisible("Info", false);
		SetVisible("Deploy", false);	
	}
	else
	{
		SetVisible("Info", true);
		SetVisible("Deploy", true);	
	}
}

#ifndef ATTACKSYSTEM_H_
#define ATTACKSYSTEM_H_

#include "AttackCalc.h"

class AttackSystem: public AttackCalc
{
	private:
		/// Stores last made dmg
		int _dmg;
		
	public:
		AttackSystem();
		/// Performs an attack, returns true if defender died and deletes element
		bool Attack(Element * attacker, Element * defender);
		/// Return value of last made damage
		int LastDmg() { return _dmg; };
		
};

#endif /*ATTACKSYSTEM_H_*/

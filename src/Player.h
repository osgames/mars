#ifndef CLASS_PLAYER
#define CLASS_PLAYER

#include "Element.h"
#include "FactionElement.h"
#include "PlayerContainer.h"

/// a faction of the game
class Player : public PlayerContainer
{
	private:
		/// latest selected element of own faction
		Element * _selected;
		/// locked element
		Element * _locked;
		/// Players last turn, if equal to current mission turn then this player has ended his large turn
		int _turn;
		/// The players faction
		int _faction;
		/// players view x,y location
		IntPoint _view;
		/// AI player
		bool _aiplayer;

	public:
		/// Creates player with id, every unit will contain id plus unit nr
		Player(int id);
		/// Destructor does nothing
		~Player() { };
		
		/// Sets players start view, uses first units position as backup. Should be called after LoadMissionelements
		void SetPlayerStartView(int x=-1, int y=-1);
		/// Loads mission elemenents from XML file, same as CreateElements
		void LoadMissionElements(const char * file) { CreateElements(file); };
		/// Loads mission elemenents from XML file, same as CreateElements
		void LoadMissionElements(std::string file) { CreateElements(file.c_str()); };
		/// Deletes all Mission elements, resets selected and locked elements
 		void ClearMissionElements()
		{
			ClearElements();
			_selected = NULL;
			_locked = NULL;
		};
		/// returns players faction
		int GetFaction() { return _faction; };
		/// set players faction
		void SetFaction(int fac) { _faction = fac; };
		/// Sets element as selected
		void SetSelected(Element * e);
		/// Clears selected element
		void ClearSelected() { _selected = NULL; };
		/// Returns selected element
		Element * GetSelected() { return _selected; };
		/// Locks to element, unlocks at end move
		void SetLocked(Element * e);
		/// Clears locked element
		void ClearLocked();
		/// Returns locked element
		Element * GetLocked() { return _locked; };
		///Sets players locked unit to current turn
		void EndMove();
		
		/// Return players last turn
		int GetTurn() { return _turn; };
		/// Set players latest turn
		void SetTurn(int turn) { _turn=turn; };
		
		///Return the number of idle units
		int GetIdleUnits();
		///Return next idle unit
		Element * GetNextIdleUnit();
		///Return next idle unit after this unit
		Element * GetNextIdleUnit(Element *el);
		
		///Sets players view
		void SetView(int x, int y) { _view.x =x; _view.y=y; };
		/// Returns players x view
		int GetViewX() { return _view.x; };
		/// Returns players y view
		int GetViewY() { return _view.y; };
		
		/// Sets player as AI controled
		void SetAI() { _aiplayer = true;};
		/// True if player is AI controlled
		bool IsAI() { return _aiplayer; };
};

#endif

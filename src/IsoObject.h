#ifndef CLASS_ISOMETRIC_OBJECT
#define CLASS_ISOMETRIC_OBJECT

#include "graphic/Image.h"
#include "Object.h"

/// A tile of a landscape
class IsoObject: public Object
{
	private:
		/// number of rows that made the object
		int _num_rows;
		/// number od colons that made the object
		int _num_cols;

	protected:
		/// image that represent the object
		graphic::Image * _img;
		/// direction of the element, need as isoobject by virtual surfaces
		int _direction;

	public:
		/// base IsoObject constructor, just set row and col
		IsoObject(int r = 0, int c = 0);
		/// isoObject constructor, set row and col and the object image
		IsoObject(int r, int c, graphic::Image * img);
		/// isoObject destroyer
		~IsoObject() { };

		/// return the number of rows of the object
		int GetNumRows() { return _num_rows; };
		/// return the number of cols of the object
		int GetNumCols() { return _num_cols; };

		/// update virtual image to correct direction, else do nothing
		void UpdateVirtualDirection()
		{
			if(_img->IsVirtual() )
				_img->SetVirtualView(_direction);
		};

		/// return image pointer
		graphic::Image * GetImage() { return _img;};

		/// return the width of the current image
		Uint16 GetW() { return _img->GetW(); };
		/// return the height of the current image
		Uint16 GetH() { return _img->GetH(); };

		/// true if ob1 is ahead ob2
		friend bool operator>(IsoObject & ob1, IsoObject & ob2);
		/// true if ob1 is behind ob2
		friend bool operator<(IsoObject & ob1, IsoObject & ob2);
};

#endif

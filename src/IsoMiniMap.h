#ifndef CLASS_ISO_MINI_MAP
#define CLASS_ISO_MINI_MAP

#include "graphic/Image.h"

#include "DataTypes.h"
#include "Element.h"
#include "FactionElement.h"
#include "IsoTilesMap.h"
#include "IsoObjectsMap.h"
#include "Map.h"
#include "MiniTile.h"
#include "TileSet.h"

#define IMG_NOT_VISIBLE 0
#define IMG_PARTIALLY_VISIBLE 1
#define IMG_VISIBLE 2
#define IMG_ELEMENT 3
#define IMG_NO_FACTION 4
#define IMG_MERCENARIES 5
#define IMG_SWA 6
#define IMG_NGG 7
#define IMG_TG 8
#define IMG_MARTIANS 9
#define IMG_ALIENS 10

/// MiniMap of the scene
class IsoMiniMap : public Map
{
	private:
	 /// vector containing all the used images
	 TileSet * _tile_set;
	 /// number of images used by the map
	 int _num_images;
	 /// width of a tile
	 Uint16 _tile_width;
	 /// height of a tile
	 Uint16 _tile_height;
	 /// pointer to the Isometric Tiles Map
	 IsoTilesMap * _tm;
	 /// pointer to the Isometric Objects Map
	 IsoObjectsMap * _om;
	 /// scale of the mini map (iso tiles width / minimap tiles width)
	 int _scale;
	 /// side of an orthogonal tile
	 float _square_side;

	public:
	 /// base constructor, use a map to choose tiles
	 IsoMiniMap(IsoTilesMap * tm, IsoObjectsMap * om, TileSet * ts);

	 /// destructor
	 ~IsoMiniMap() { };

	 /// blit part of the layer on a surface, if the surface is NULL blit on screen
	 void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);

	 /// blit part of the layer on a surface using dest_rect for the position, if the surface is NULL blit on screen
	 void BlitLayer(SDL_Rect limits_rect, SDL_Rect dest_rect, graphic::Surface * dst_surf = NULL);

	 /// update minimap tiles
	 void UpdateMap();

	 /// return the scale value
	 int GetScale() { return _scale; };
	 
	 /// Get tile map width
	 int GetTileMapWidth() { return _tm->GetW(); };
	 /// Get tile map height
	 int GetTileMapHeight() { return _tm->GetH(); };

	 /// return the cell indexes of the selected point if the selection is inside the
	 /// map, NULL otherwise
	 CellInd * GetCell(Sint16 x, Sint16 y) { return NULL; };
};

#endif

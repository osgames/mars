#include "TileHighlighter.h"

using namespace graphic;

// constructor
TileHighlighter::TileHighlighter(IsoTilesMap * tm, graphic::Image * disabled, graphic::Image * attacked)
{
	_tm = tm;
	_disabled = disabled;
	_attacked = attacked;
	_player = NULL;
}

// blit part of the layer on a surface, if the surface is NULL blit on screen
void TileHighlighter::BlitLayer(SDL_Rect limits_rect, Surface * dst_surf)
{
	if(_player != NULL && !_player->IsAI())
	{	
		Sint16 l_limit = limits_rect.x;
		Sint16 u_limit = limits_rect.y;
		int turn = _player->GetTurn();
		// temporary IntPoint
		IntPoint p;
		
		std::vector<Element *> els = _player->GetAllElements();
		for(unsigned int x = 0; x < els.size(); x++)
		{
			int row = els[x]->GetRow();
			int col = els[x]->GetCol();

			p.x = _tm->GetTileX(row, col) + ((_tm->GetTileW() - _disabled->GetW()) / 2);
			p.y = _tm->GetTileY(row, col) + ((_tm->GetTileH() - _disabled->GetH()) / 2);
			
			// Unit already moved, show disabled icon
			if(els[x]->GetTurn() >=turn)
			{
				_disabled->SetPosition(p.x - l_limit, p.y - u_limit);
				blitter->Blit(_disabled, dst_surf);				
			}
			
			// Unit has been attacked this turn, show attacked icon
			if(els[x]->GetAttackedTurn() >= turn)
			{
				_attacked->SetPosition(p.x - l_limit, p.y - u_limit);		
				graphic::blitter->Blit(_attacked, dst_surf);	
			}
		}
	}
}

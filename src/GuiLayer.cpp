#include "GuiLayer.h"

using namespace graphic;
using namespace std;
using namespace gui;

GuiLayer::GuiLayer(GuiManager * gui, bool graphic_mode)
{
	_gui = gui;
	
}

// blit part of the layer on a surface, if the surface is NULL blit on screen
void GuiLayer::BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf)
{
	_gui->Display();
}


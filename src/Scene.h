#ifndef CLASS_SCENE
#define CLASS_SCENE

#include <string>
#include <vector>

#include "graphic/GraphicTracker.h"
#include "graphic/Image.h"
#include "graphic/Screen.h"
#include "graphic/Text.h"

#include "AttHighlighter.h"
#include "Exception.h"
#include "GuiLayer.h"
#include "TileHighlighter.h"
#include "InfoText.h"
#include "IsoFow.h"
#include "IsoGrid.h"
#include "IsoObjectsMap.h"
#include "IsoTilesMap.h"
#include "IsoMiniMap.h"
#include "IsoMouse.h"
#include "Player.h"
#include "Pathfind.h"
#include "PathHighlighter.h"
#include "RotHighlighter.h"
#include "SceneContainer.h"
#include "SceneRenderer.h"
#include "SelectionHighlighter.h"
#include "SelectionData.h"
#include "TileSet.h"

#define TERR_1 1
#define TERR_2 2
#define TERR_3 3

#define LAYER_TM 0
#define LAYER_TILE_H 1
#define LAYER_FOW 2
#define LAYER_ROT_H 3
#define LAYER_ATT_H 4
#define LAYER_GRID 5
#define LAYER_PATH_H 6
#define LAYER_OM 7
//#define LAYER_DEPLOY 8
#define LAYER_SEL_H 8
#define LAYER_SEL_DATA 9
#define LAYER_INFO 10
#define LAYER_GUI 11

#define ORIENTATION_NS true
#define ORIENTATION_SN false

extern std::string data_dir;

/// all the graphic layers that represents a mission
class Scene
{
	private:
		/// all the layers contained in the Scene
		std::vector < Layer * > _layers;
		/// Current scenes scene elements
		SceneContainer * _sc;

		/// keep track of the scene orientation for reverting Vflips on exit
		bool _orientation;

	public:
		/// Scene constructor needs maps dimensions [rows x cols] and the terrain kind, plus the scenes gui and attacksystem
		Scene(int rows, int cols, int terrain, gui::GuiManager *gui, AttackSystem *attsys);
		/// Scene destructor
		~Scene();

		void BindRenderer(SceneRenderer * renderer);

		/// connect the objects map to the container and load all the objects in the former
		void AddElements(SceneContainer * sc)
		{
			sc->BindObjectsMap((static_cast < IsoObjectsMap * > (_layers[LAYER_OM])));
			sc->FillObjectsMap();
			_sc = sc;
		};
		
		void DeleteElement(int id) { _sc->DeleteElement(id); };

		Layer * GetLayer(int id) { return _layers[id]; };

		void SwitchGrid()
		{
			if(_layers[LAYER_GRID]->IsActive())
				_layers[LAYER_GRID]->Active(false);
			else
				_layers[LAYER_GRID]->Active();
		};

		void VFlip();

		Uint16 GetMapW() { return (static_cast < IsoTilesMap * > (_layers[LAYER_TM])->GetW()); };
		Uint16 GetMapH() { return (static_cast < IsoTilesMap * > (_layers[LAYER_TM])->GetH()); };

		/// Set current selected element
		void SetSelected(Element * elem, bool usable);
		/// Clear selected element
		void ClearSelected();
		/// Set current player
		void SetPlayer(Player * p) { (static_cast < TileHighlighter * > (_layers[LAYER_TILE_H]))->SetPlayer(p); };

		Element * SelectObject(Sint16 x, Sint16 y)
		{ return (Element *)(static_cast < IsoObjectsMap * > (_layers[LAYER_OM]))->SelectObject(x, y); }

		void SetRotHighlighter(Element * e, Sint16 mouse_x, Sint16 mouse_y);
		void DisableRotHighlighter();
		int GetRotDirection()
		{ return static_cast < RotHighlighter * > (_layers[LAYER_ROT_H])->GetDirection(); }
		int GetRotCost()
		{ return static_cast < RotHighlighter * > (_layers[LAYER_ROT_H])->GetCost(); }
		int GetAttDirection()
		{ return static_cast < RotHighlighter * > (_layers[LAYER_ATT_H])->GetDirection(); }
		int GetAttCost()
		{ return static_cast < RotHighlighter * > (_layers[LAYER_ATT_H])->GetCost(); }
		void SetAttHighlighter(Element * e, IsoMouse * mouse);
		void DisableAttHighlighter();

		void SetPathHighlighter(Path * path, int ap);
		void DisablePathHighlighter() { _layers[LAYER_PATH_H]->Active(false); };

		void DisableLayer(int l) { _layers[l]->Active(false); };
		void ActiveLayer(int l) { _layers[l]->Active(); };

		void WriteInfo(std::string txt, Element* elem, Sint16 timer = -1)
		{
			(static_cast < InfoText * > (_layers[LAYER_INFO]))->WriteText(txt, elem, timer);
			_layers[LAYER_INFO]->Active();
		};

		void DisableInfo() { _layers[LAYER_INFO]->Active(false); };

};

#endif

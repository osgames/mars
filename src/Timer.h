#ifndef CLASS_TIMER
#define CLASS_TIMER

#include <SDL.h>

#define MAX_FPS 100
#define MIN_FPS 30
#define DEFAULT_FPS 50
#define MS_X_SEC 1000

/// This class is used to control FPS of the game
class Timer
{
	private:
		/// time elapsed in the game
		Uint32 _game_time;
		/// loop start time
		Uint32 _t1;
		/// loop end time
		Uint32 _t2;
		/// time elapsed during a loop
		Uint32 _t_diff;
		/// a variable used to store a moment
		Uint32 _saved;
		/// how much a frame should be
		int _frame_time;
		/// wanted frame rate
		int  _frame_rate;
		/// frames blitted
		int _num_frames;
		/// time elapsed, used to check if a second has passed
		Uint32 _t_sec;
		/// current FPS
		int _fps;

	public:
		/// constructor, set the wanted Frame Rate
		Timer(int fr = DEFAULT_FPS)
		{
			_frame_rate = fr;
			_frame_time = MS_X_SEC / fr;
		};

		/// destructor
		~Timer() { };

		/// start the timer, it's an init required before the game loop
		void Start()
		{
			_game_time = _t1 = SDL_GetTicks();
			_t_diff = _num_frames =  _t_sec = _fps = 0;
		};

		/// compute time elapsed on each loop
		void GoOnAndDelay();

		/// set wanted Frame Rate
		void SetFrameRate(int fr)
		{
			_frame_rate = fr;
			_frame_time = MS_X_SEC / fr; 
		};
		/// return wanted Frame Rate
		int GetFrameRate() { return _frame_rate; }

		/// return time elapsed during last loop
		Uint32 GetFrameLength() { return _t_diff; };

		/// return current FPS
		int GetFps() { return _fps; };

		Uint32 GetLastTime() { return _t2; };

		void SaveTime() { _saved = _t2; };
		Uint32 GetSavedTime() { return _saved; };
};

extern Timer * timer;

#endif

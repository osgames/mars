#ifndef CLASS_TILES_MAP
#define CLASS_TILES_MAP

#include <vector>

#include "Layer.h"
#include "Object.h"

/// A map containing objects
class Map: public Layer
{
	protected:
	 /// vector containing objects
	 std::vector < Object * > _objects;
	 /// number of rows
	 int _num_rows;
	 /// number of columns
	 int _num_cols;
	 /// total number of cells of the map (row x col), useful to avoid
	 /// further multiplication
	 int _num_cells;
	 /// number of objects of the map (_objects.size())
	 int _num_objects;
	 /// origin of the map
	 IntPoint _origin;
	 /// origin of the [0,0] image, top-left corner
	 IntPoint _graphic_origin;

	public:
	 /// constructor
	 Map(int num_rows, int num_cols);
	 /// destroyer
	 virtual ~Map();

	 /// blit part of the layer on a surface, if the surface is NULL blit on screen
	 virtual void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL) = 0;

	 /// get X coordinate of the origin IntPoint
	 Sint16 GetOriginX() { return _origin.x; };
	 /// get Y coordinate of the origin IntPoint
	 Sint16 GetOriginY() { return _origin.y; };

	 /// get X coordinate of the graphic origin IntPoint (top-left vertex of the [0, 0] tile)
	 Sint16 GetGraphicOriginX() { return _graphic_origin.x; };
	 /// get Y coordinate of the graphic origin IntPoint (top-left vertex of the [0, 0] tile)
	 Sint16 GetGraphicOriginY() { return _graphic_origin.y; };

	 /// set X coordinate of the origin IntPoint, then update all objects X value
	 void SetOriginX(Sint16 new_x);
	 /// set Y coordinate of the origin IntPoint, then update all objects Y value
	 void SetOriginY(Sint16 new_y);

	 /// get X coordinate of the object at [row,col]
	 Sint16 GetObjectX(int r, int c) { return _objects[(r * _num_cols) + c]->GetX(); };
	 /// get Y coordinate of the object at [row,col]
	 Sint16 GetObjectY(int r, int c) { return _objects[(r * _num_cols) + c]->GetY(); };

	 /// get number of rows of the map
	 int GetNumRows() { return _num_rows; };
	 /// get number of columns of the map
	 int GetNumCols() { return _num_cols; };
	 /// get number of cells of the map
	 int GetNumCells() { return _num_cells; };
	 /// get the number of objects
	 int GetNumObjects() { return _num_objects; };

	 /// get width of the map
	 Uint16 GetW() { return _width; };
	 /// get height of map
	 Uint16 GetH() { return _height; };

	 /// return the cell indexes of the selected point if the selection is inside the
	 /// map, NULL otherwise
	 virtual CellInd * GetCell(Sint16 x, Sint16 y) = 0;
};

#endif

#include "Timer.h"

// compute time elapsed on each loop
void Timer::GoOnAndDelay()
{
	_t2 = SDL_GetTicks();
	_t_diff = _t2 - _t1;
	_t1 = _t2;

	_t_sec +=_t_diff;
	_num_frames++;

	// a second has elapsed
	if(_t_sec >= MS_X_SEC)
	{
		// store FPS
		_fps = _num_frames;
		// reset counters
		_num_frames = 0;
		_t_sec = 0;
	}

	_game_time += _frame_time;

	int delay = _game_time - _t2;

	if(delay > 0)
		SDL_Delay(delay);
}



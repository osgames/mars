#include "AttackSystem.h"

AttackSystem::AttackSystem()
{
	_dmg = 0;
	srand ( time(NULL) );
	_turn = 1;
}

bool AttackSystem::Attack(Element * attacker, Element * defender)
{
	#ifdef DEBUG_MODE
		printf("<AttackSystem::Attack(Element * attacker, Element * defender)>\nOutput: %d %d\n", (int)attacker, (int)defender);
	#endif
	if(attacker == NULL || defender == NULL )
		return false;
	
	//attacker->
	
	// 0 - ~200 (%)
	int attmulti = AttackMultipliers(attacker, defender);
	int defmulti = DefendMultipliers(attacker, defender);

	#ifdef DEBUG_MODE	
		printf("attmulti:%d defmulti:%d\n", attmulti, defmulti);
	#endif
		
	int att = (attacker->GetCurStat("damage", attacker->GetActiveComponentId()) * W_ATT) + attmulti + (rand() % W_RAND);
	int def = (defender->GetCurStat("defence") * W_DEF) + defmulti  + (rand() % W_RAND);
	
	#ifdef DEBUG_MODE	
		printf("att:%d def:%d\n", att, def);
	#endif
	_dmg = (att * W_ATTMULTI) / def;
	//missed, (defence/W_DEFHIT) higher then attack or attmulti is zero == out of range
	if(att < (def*10/W_DEFHIT) || !attmulti)
	{	
		_dmg = 0;
		#ifdef DEBUG_MODE	
			printf("miss\n");
			printf("</AttackSystem::Attack(Element * attacker, Element * defender)>\n");
		#endif
		return false;
	}
	// Hit
	defender->SubCurStat("hitpoints",_dmg);
	defender->SetAttackedTurn(_turn);
	#ifdef DEBUG_MODE
		printf("damage:%d\n", _dmg);
		printf("</AttackSystem::Attack(Element * attacker, Element * defender)>\n");
	#endif
	
	//use ammo
	attacker->SubCurStat("ammo", 1, attacker->GetActiveComponentId());
		
	// return true if defender has lost all hitpoints
	if(!defender->HasEnoughCurStat("hitpoints", 1))
		return true;

	return false;
}

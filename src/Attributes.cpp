#include "Attributes.h"

Attributes::Attributes(Statsmap stats)
{
	_active_component = -1;
	Component * aComponent = new Component(stats);
	AddComponent(aComponent);	
}

Attributes::Attributes(Statsmap stats, Stringmap strings)
{
	_active_component = -1;
	Component * aComponent = new Component(stats, strings, "base");
	AddComponent(aComponent);	
}

Attributes::~Attributes()
{
	for(unsigned int i=0; i != _components.size(); i++)
		delete _components[i];
		
	_components.clear();
}

void Attributes::AddComponents(std::vector <Component *> components)
{
	for(unsigned int i=0; i != components.size(); i++)
		_components.push_back(components[i]);
}

// Return the max of a statistical value
int Attributes::GetMaxStat(std::string id, int component) 
{ 
	// If no component is selected (-1) then all shared are include in the value
	if(component == -1)
	{
		int tmpValue = 0;
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					tmpValue += _components[i]->GetMaxStat(id);
		return tmpValue;
	}
	else
		return _components[component]->GetMaxStat(id); 
}

// Set the max of a statistical value
void Attributes::SetMaxStat(std::string id, int val, int component) 
{ 
	// If no component is selected (-1) then all shared values are set
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					_components[i]->SetMaxStat(id, val);
	}
	else
		_components[component]->SetMaxStat(id, val);
}

// Add a value to the max statistical value
void Attributes::AddMaxStat(std::string id, int val, int component)
{
	// If no component is selected (-1) then all shared values are added
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					_components[i]->AddMaxStat(id, val);
	}
	else
		_components[component]->AddMaxStat(id, val);
}

// Subtract a value from the max statistical value
void Attributes::SubMaxStat(std::string id, int val, int component)
{
	// If no component is selected (-1) then all shared values are subtracted
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					_components[i]->SubMaxStat(id, val);
	}
	else
		_components[component]->SubMaxStat(id, val);
}

// Return the current  value of a statistical value
int Attributes::GetCurStat(std::string id, int component)
{ 
	// If no component is selected (-1) then all shared are include in the value
	if(component == -1)
	{
		int tmpValue = 0;
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					tmpValue += _components[i]->GetCurStat(id);

		return tmpValue;
	}
	else if( (unsigned)component < _components.size())
		return _components[component]->GetCurStat(id);
		
	return -1;
}

// return true if the element has a value larger the the passed one for the current stat
bool Attributes::HasEnoughCurStat(std::string id, int val, int component)
{
	int tmp = 0;

	// If no component is selected (-1) then all shared values are subtracted
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					tmp += _components[i]->GetCurStat(id);
	}
	else
		tmp = _components[component]->GetCurStat(id);

	return (tmp >= val);
}

// Set the current value of a statistical value
void Attributes::SetCurStat(std::string id, int val, int component)
{
	// If no component is selected (-1) then all shared values are set
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					_components[i]->SetCurStat(id, val);
	}
	else
		_components[component]->SetCurStat(id, val);
}

// Add a value to the current statistical value
void Attributes::AddCurStat(std::string id, int val, int component)
{
	// If no component is selected (-1) then all shared values are added
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					_components[i]->AddCurStat(id, val);
	}
	else
		_components[component]->AddCurStat(id, val);
}

// Subtract a value from the current statistical value
void Attributes::SubCurStat(std::string id, int val, int component)
{
	// If no component is selected (-1) then all shared values are subtracted
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					_components[i]->SubCurStat(id, val);
	}
	else
		_components[component]->SubCurStat(id, val);
}

// Restore the current value to the max
void Attributes::RestoreCurStat(std::string id, int component)
{
	// If no component is selected (-1) then all shared values are set
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					_components[i]->RestoreCurStat(id);
	}
	else
		_components[component]->RestoreCurStat(id);
}

// Set a string value
void Attributes::SetString(std::string id, std::string text, int component) 
{
	// If no component is selected (-1) then all shared values are set
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					_components[i]->SetString(id, text);
	}
	else
		_components[component]->SetString(id, text);
}

// Get a string value
std::string Attributes::GetString(std::string id, int component)
{
	// If no component is selected (-1) then return first string found
	if(component == -1)
	{
		std::string answer;
		for(unsigned int i=0; i != _components.size(); i++)
		{
			answer = _components[i]->GetString(id);
			if(!answer.empty())
				return answer;
		}
	}
	else
	{
		std::string answer;
		answer = _components[component]->GetString(id);
		if(!answer.empty())
			return answer;
	}
	return " N/A ";
}

// Get a string value
int Attributes::LocateString(std::string id)
{
	std::string answer;
	for(unsigned int i=0; i != _components.size(); i++)
	{
		answer = _components[i]->GetString(id);
		if(!answer.empty())
			return i;
	}

	return -10;
}

// Toggels a stats shared state false/true
void Attributes::ToggleStatShared(std::string id, int component)
{
	// If no component is selected (-1) then all shared values are toggled
	if(component == -1)
	{
		for(unsigned int i=0; i != _components.size(); i++)
			if(_components[i]->IsActive())
				if(_components[i]->IsStatShared(id))
					_components[i]->ToggleStatShared(id);
	}
	else
		_components[component]->ToggleStatShared(id);
}

// Force toggles are Stat(id) shared state
void Attributes::ToggleStatSharedForced(std::string id)
{
		for(unsigned int i=0; i != _components.size(); i++)
			_components[i]->ToggleStatShared(id);
}

int Attributes::FindComponentType(std::string type)
{
	for(unsigned int i=0; i != _components.size(); i++)
		if(_components[i]->IsActive())
			if(_components[i]->IsComponent(type))
				return i;
	return -1;
}

int Attributes::FindStatComponent(std::string id)
{
	for(unsigned int i=0; i != _components.size(); i++)
		if(_components[i]->IsActive())
			if(_components[i]->GetCurStat(id))
				return i;
	return -1;
}

bool Attributes::CheckId(int id)
{	
	if((unsigned)id < _components.size())
		return true;
	return false;
}

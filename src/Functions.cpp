#include <string>
#include "Functions.h"

using namespace std;
using namespace graphic;

// return screen coordinates from iso ones origin represents the origin of
// the isometric plane
FloatPoint * iso2scr(IntPoint iso, IntPoint origin)
{
	return iso2scr(iso.x, iso.y, origin.x, origin.y);
}

// return screen coordinates from iso ones origin represents the origin of
// the isometric plane
FloatPoint * iso2scr(Sint16 ix, Sint16 iy, Sint16 ox, Sint16 oy)
{
	FloatPoint * scr = new FloatPoint;

	//initial translation
	ix -= ox;
	iy -= oy;

	//back to 45°
	iy *= 2;

	//back to orthogonal
	scr->x = ix * COS_MIN_45 - iy * SIN_MIN_45;
	scr->y = ix * SIN_MIN_45 + iy * COS_MIN_45;

	return scr;
}

// return the relative orientation of an origin point respect another
int RelDirection(IntPoint * origin, IntPoint * direction)
{
	return RelDirection(origin->x, origin->y, direction->x, direction->y);
}

// return the relative orientation of an origin cell respect another
int RelDirection(Sint16 ox, Sint16 oy, Sint16 dx, Sint16 dy)
{
	FloatPoint dir;
	int d;

	dir.x = (float)(dx - ox);
	dir.y = (float)(dy - oy);

	if((dir.x >= (- dir.y / TAN_67_5)) && (dir.x <= (- dir.y / TAN_22_5)) && (dir.y <= 0))
		d = NORTH_EAST;
	else if((dir.x >= (dir.y / TAN_67_5)) && (dir.x <= (- dir.y / TAN_67_5)) && (dir.y <= 0))
		d = NORTH;
	else if((dir.y >= (- dir.x / TAN_67_5)) && (dir.y <= ( dir.x / TAN_67_5)) && (dir.x >= 0))
		d = EAST;
	else if((dir.y <= (dir.x * TAN_67_5)) && (dir.y >= (dir.x / TAN_67_5)) && (dir.x >= 0))
		d = SOUTH_EAST;
	else if((dir.x <= (- dir.y / TAN_67_5)) && (dir.x >= (- dir.y / TAN_22_5)) && (dir.y > 0))
		d= SOUTH_WEST;
	else if((dir.x <= (dir.y / TAN_67_5)) && (dir.x >= (- dir.y / TAN_67_5)) && (dir.y > 0))
		d = SOUTH;
	else if((dir.y <= (- dir.x / TAN_67_5)) && (dir.y >= ( dir.x / TAN_67_5)) && (dir.x < 0))
		d =  WEST;
	else
		d = NORTH_WEST;

	return d;
}

// Returns true if file does exist
bool ImageFileExists(std::string file)
{
	struct stat t_fd; 
	
  	if ( -1 != stat( file.c_str(), &t_fd ) )
		return true;

	return false; 
}

// temporary menu
void menu(Options & op)
{
	screen->SetFlags(screen->GetFlags() & ~SDL_RESIZABLE);

	// used colors
	SDL_Color sel_color = {0xED, 0x70, 0x00, 0};
	SDL_Color unsel_color = {0xAA, 0x60, 0x00, 0};
	SDL_Color help_color = {0xAA, 0xAA, 0xAA, 0};

	// background images
	Image * bg_800, * bg_1024, * bg;
	// menu text
	Text * menu_txt_s, * menu_txt_u;
	// help text
	Text * help_txt1, * help_txt2;

	bool gmode;

	gmode = screen->GetGraphicMode();

	try
	{
			bg_800 = gtracker->GetImage((data_dir + "img/mars1_800.png").c_str(), gmode);
			bg_1024 = gtracker->GetImage((data_dir + "img/mars1_1024.png").c_str(), gmode);

			menu_txt_s = new Text((data_dir + "font/Distres2.ttf").c_str(), sel_color , DIM_FONT_MENU, gmode);
			menu_txt_u = new Text((data_dir + "font/Distres2.ttf").c_str(), unsel_color , DIM_FONT_MENU, gmode);

			help_txt1 = new Text((data_dir + "font/tahomabd.ttf").c_str(), help_color , DIM_FONT_HELP1, gmode);
			help_txt2 = new Text((data_dir + "font/tahomabd.ttf").c_str(), help_color , DIM_FONT_HELP2, gmode);
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}


	if(screen->GetW() >= RIS2_W)
		screen->SetResolution(RIS2_W, RIS2_H);
	else
		screen->SetResolution(RIS1_W, RIS1_H);
	
	if(screen->GetW() == RIS1_W)
	{
		op.ris.w = RIS1_W;
		op.ris.h = RIS1_H;

		bg = bg_800;
	}
	else
	{
		op.ris.w = RIS2_W;
		op.ris.h = RIS2_H;

		bg = bg_1024;
	}

	bool done = false;
	bool need_update = true;
	SDL_Event event;

	op.choice = 1;

	// update mouse position in the event queue
 	SDL_WarpMouse(screen->GetW() / 2, screen->GetH() / 2);

	//game loop
	while(!done)
	{
		while(SDL_PollEvent(&event))
		{
			// exit if someone press the x simbol of the window with the mouse
			if( event.type == SDL_QUIT )
			{
				op.choice = 0;
				done = true;
			}
			else if(event.type == SDL_KEYDOWN)
			{
				switch(event.key.keysym.sym)
				{
					// ESC ==> exit
					case SDLK_ESCAPE:
						op.choice = 0;

						done = true;
						break;

					// go in fullscreen mode
					case SDLK_f:
						screen->GoFullScreen();

						#ifdef WIN32
						gtracker->UpdateAllTextures();
						#endif

						need_update = true;
						break;

					// go in window mode
					case SDLK_w:
						screen->GoWindowed();

						#ifdef WIN32
						gtracker->UpdateAllTextures();
						#endif

						need_update = true;
						break;

					// end
					case SDLK_RETURN:
						done = true;
						break;

					// 800x600
					case SDLK_F1:
						screen->SetResolution(RIS1_W, RIS1_H);

						bg = bg_800;

						#ifdef WIN32
						gtracker->UpdateAllTextures();
						#endif

						op.ris.w = RIS1_W;
						op.ris.h = RIS1_H;

						need_update = true;
						break;

					// 1024x768
					case SDLK_F2:
						screen->SetResolution(RIS2_W, RIS2_H);

						bg = bg_1024;

						#ifdef WIN32
						gtracker->UpdateAllTextures();
						#endif

						op.ris.w = RIS2_W;
						op.ris.h = RIS2_H;

						need_update = true;
						break;

					#ifdef WITH_OPENGL
					// change graphic mode
					case SDLK_F3:
						if(screen->GetGraphicMode() == SDL_GRAPHIC)
						{
							screen->ChangeGraphicMode(GL_GRAPHIC);

							gtracker->ChangeGraphicMode(GL_GRAPHIC);
						}
						else
						{
							screen->ChangeGraphicMode(SDL_GRAPHIC);

							gtracker->ChangeGraphicMode(SDL_GRAPHIC);
						}

						need_update = true;
						gmode = screen->GetGraphicMode();
						break;
					#endif

					case SDLK_UP:
						op.choice = op.choice > 1 ? (op.choice - 1) : 3;

						need_update = true;
						break;

					case SDLK_DOWN:
						op.choice =  op.choice < 3 ? (op.choice + 1)  : 1;

						need_update = true;
						break;

					default:
						break;
				}
			}
		}

		// update the scene
		if(need_update || gmode == GL_GRAPHIC)
		{
			screen->Clear();

			blitter->Blit(bg);

			if(op.choice == 1)
				menu_txt_s->WriteText("Two Player Game", MENU_X_OFFSET, op.ris.h / 3);
			else
				menu_txt_u->WriteText("Two Player Game", MENU_X_OFFSET, op.ris.h / 3);
			if(op.choice == 2)
				menu_txt_s->WriteText("Player vs SSAI (SimplyStupid AI)", MENU_X_OFFSET, op.ris.h / 3 + 40);
			else
				menu_txt_u->WriteText("Player vs SSAI", MENU_X_OFFSET, op.ris.h / 3 + 40);
			if(op.choice == 3)
				menu_txt_s->WriteText("Tutorial", MENU_X_OFFSET, op.ris.h / 3 + 80);
			else
				menu_txt_u->WriteText("Tutorial", MENU_X_OFFSET, op.ris.h / 3 + 80);

			help_txt1->WriteText("Commands:", HELP_X_OFFSET, op.ris.h - 175);
			help_txt2->WriteText("F1 / F2 => 800x600 / 1024x768\nF3 => switch graphic mode SDL/GL\n\
								F4 => take a screenshot\n\
								w / F => window / fullscreen\ng => grid ON/OFF\n\
								v => VFLIP\n+ / - => adjusts gamma\narrows => scrolling\n\
								a/ESC => deselect unit/cancel action\n\
								r/t/f => rotate/move/attack\n\
								RETURN/SHIFT + RETURN => end move/end turn\nmouse left click => select unit/ perform an action\n\
								mouse right click => change action", HELP_X_OFFSET2, op.ris.h - 155);

			screen->Update();

			need_update = false;
		}

		SDL_Delay(20);
	}

	// free used resources
	delete menu_txt_s;
	delete menu_txt_u;
	delete help_txt1;
	delete help_txt2;

	screen->SetFlags(screen->GetFlags() | SDL_RESIZABLE);
}


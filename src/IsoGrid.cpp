#include "IsoGrid.h"

using namespace graphic;

// base constructor, use a map to choose tiles
IsoGrid::IsoGrid(IsoTilesMap * tm, Image * v_image, Image * pv_image, Image * nv_image)
{
	// store Image
	_v_cell = v_image;
	_pv_cell = pv_image;
	_nv_cell = nv_image;

	_v_cell->SetOpacity(GRID_V_DEFAULT_OPACITY);
	_pv_cell->SetOpacity(GRID_PV_DEFAULT_OPACITY);
	_nv_cell->SetOpacity(GRID_NV_DEFAULT_OPACITY);

	// storing IsoTilesMap pointer
	_tm = tm;

	_tile_width = _v_cell->GetW();
	_tile_height = _v_cell->GetH();
	_square_side = _tile_height * SQRT2;

	_num_rows = _tm->GetNumRows();
	_num_cols = _tm->GetNumCols();

	_origin.x = _tm->GetOriginX();
	_origin.y = _tm->GetOriginY();

	// dimensions of the map
	_width = _tm->GetW();
	_height = _tm->GetH();
}

// blit part of the layer on a surface, if the surface is NULL blit on screen
void IsoGrid::BlitLayer(SDL_Rect limits_rect, Surface * dst_surf)
{
	Sint16 l_limit = limits_rect.x ;
	Sint16 r_limit = limits_rect.x + limits_rect.w;
	Sint16 u_limit = limits_rect.y;
	Sint16 b_limit = limits_rect.y + limits_rect.h;

	// temporary IntPoint
	IntPoint p;

	FloatPoint * start, * end;
	int s_row, s_col, e_row, e_col;

	// compute start row =>  top, right corner
	start = iso2scr(r_limit, u_limit, _origin.x, _origin.y);
	s_row = (int)(start->y / _square_side) - 1;
	if(s_row < 0)
		s_row = 0;
	delete start;

	// compute start col => top, left corner
	start = iso2scr(l_limit, u_limit, _origin.x, _origin.y);
	s_col = (int)(start->x / _square_side) - 1;
	if(s_col < 0)
		s_col = 0;
	delete start;

	// compute end row => bottom, left corner
	end = iso2scr(l_limit, b_limit, _origin.x, _origin.y);
	// row and col of the end IntPoint
	e_row = (int)(end->y / _square_side) + 1;
	if(e_row > _num_rows)
		e_row = _num_rows;
	delete end;

	// compute end col => bottom, right corner
	end = iso2scr(r_limit, b_limit, _origin.x, _origin.y);
	e_col = (int)(end->x / _square_side) + 1;
	if(e_col > _num_cols)
		 e_col = _num_cols;
	delete end;

	Image * cur_img = NULL;

	for(int r = s_row; r < e_row; r++)
	{
		for(int c = s_col; c < e_col; c++)
		{
			p.x = _tm->GetObjectX(r,c);
			p.y = _tm->GetObjectY(r,c);

			if( (p.x + _tile_width >= l_limit) && (p.x <= r_limit) &&
		   	   (p.y + _tile_height >= u_limit) && (p.y <= b_limit) )
			{
				// obtain tile position
				p.x -= l_limit;
				p.y -= u_limit;

				if(_tm->IsVisible(r,c) == VISIBLE)
					cur_img = _v_cell;
				else if(_tm->IsVisible(r,c) == NOT_VISIBLE)
					cur_img = _nv_cell;
				else if(_tm->IsVisible(r,c) == PARTIALLY_VISIBLE)
					cur_img = _pv_cell;

				cur_img->SetPosition(p.x, p.y);

				blitter->Blit(cur_img, dst_surf);
			}
		}
	}
}


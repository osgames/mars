#include "AttackCalc.h"

int AttackCalc::CalcAttackAngle(Element * attacker, Element * defender)
{
	// Only units care about angle of attack
	if(defender->GetType() != UNIT)
		return 0;
		
	int defx = defender->GetX();
	int defy = defender->GetY();
	
	int attx = attacker->GetX();
	int atty = attacker->GetY();
	
	int dirx = defx;
	int diry = defy;
	
	int direction = defender->GetDirection();
	
	// Create a virtual point in the direction of the defender, used to calculate angle
	switch (direction)
	{
		case 0:
			dirx += 10;
			diry -= 10;
			break;
		case 1:
			dirx += 10;
			break;
		case 2:
			dirx += 10;
			diry += 10;
			break;
		case 3:
			diry += 10;
			break;
		case 4:
			dirx -= 10;
			diry += 10;
			break;
		case 5:
			dirx -= 10;
			break;
		case 6:
			dirx -= 10;
			diry -= 10;
			break;
		case 7:
			diry -= 10;
			break;
		default:
			break;
	}
	
	int tempx, tempy;
	
	// (a) attacker -> direction of defender
	tempx = abs(attx - dirx);
	tempy = abs(atty - diry);
	double a = sqrt( tempx * tempx + tempy * tempy);
	
	// (b) attacker -> defender
	tempx = abs(attx - defx);
	tempy = abs(atty - defy);
	double b = sqrt( tempx * tempx + tempy * tempy);
	
	// (c) direction of defender -> defender
	tempx = abs(dirx - defx);
	tempy = abs(diry - defy);
	double c = sqrt( tempx * tempx + tempy * tempy);
	
	//Cosin angle calculation
	double angle = acos( (b*b + c*c - a*a)/(2*b*c)) * 180.0 / PI;
	
	return (int)angle;	
}

int AttackCalc::CalcDistance(Element * attacker, Element * defender)
{
	return CalcDistance(attacker, defender->GetRow(), defender->GetCol());
}

int AttackCalc::CalcDistance(Element * attacker, int defrow, int defcol)
{
	int row = abs(attacker->GetRow() - defrow);
	int col = abs(attacker->GetCol() - defcol);

	if( row == col)
		return (row * DIAGONAL_COST) / 10;
	else
	{
		int diagonal;
		int ortho;
		if( row < col)
		{
			diagonal = row;
			ortho = col - diagonal;
		}
		else
		{
			diagonal = col;
			ortho = row - diagonal;
		}
		return (ortho * ORTHO_COST + diagonal * DIAGONAL_COST) / 10;
	}
}

int AttackCalc::CalcOptimalRange(Element * attacker, int distance)
{
	int range = attacker->GetCurStat("range", attacker->GetActiveComponentId());
	if(range == 0)
		return 0;
	int dist = (range - distance) * 10;
	int optimal_cost = (int)round(100/range);
	if( optimal_cost < MINIMUM_OPTIMAL_COST)
		optimal_cost = MINIMUM_OPTIMAL_COST;

	//Negative value == beyond optimal range
	if(dist < 0)
	{
		dist = abs(dist) * optimal_cost / 10;
		if( dist < 100)
			return 100 - dist;
		return 0;
	}
	// Within optimal range 100% damage
	else if( dist == 0 || dist == 10)
	{	
		return 100;
	}
	// Closer then optimal range
	else
	{
		dist = (dist / 2) * optimal_cost / 10;
		if( dist < 100)
			return 100 - dist;
		return 0;
	}
}

int AttackCalc::AttackMultipliers(Element * attacker, Element * defender)
{

	// 0 - 180
	int angle = CalcAttackAngle(attacker, defender);
	// 0 - 100
	int optimal = CalcOptimalRange(attacker, CalcDistance(attacker, defender));

	// Return 0 if optimal is 0, target out of range
	if(!optimal)
		return 0;
	// alter multiplier depending on attack type
	int atttype = 100;
	if(_attacktype == ATT_QUICK)
		atttype = ATT_QUICK_DMG;
	else if(_attacktype == ATT_AIMED)
		atttype = ATT_AIMED_DMG;

	return ((optimal * W_OPT + angle * W_ROT / 2 + attacker->GetCurStat("experince") * W_EXP) * atttype) / 1000;
}

int AttackCalc::DefendMultipliers(Element * attacker, Element * defender)
{
	Unit * unit = dynamic_cast < Unit * > (defender);
	//Only units have movable defence and only if they moved this turn (Game turn -1)
	if(unit && _turn-1 == unit->GetTurn())
	{
		// 0 - 150
		int moved = unit->GetMoved() * 10;
		if( moved > 150)
			moved = 150;
		return ( moved * W_MOV + defender->GetCurStat("experince") * W_EXP) / 10;
	}

	return (defender->GetCurStat("experince") * W_EXP) / 10;
}

int AttackCalc::GetTilesOptimalRange(Element * attacker, int row, int col)
{
	int optimal = CalcOptimalRange(attacker, CalcDistance(attacker, row, col));
	return optimal;
}

int AttackCalc::GetAttackCostMultiplier()
{
	// alter multiplier depending on attack type
	int atttype = 100;
	if(_attacktype == ATT_QUICK)
		atttype = ATT_QUICK_CST;
	else if(_attacktype == ATT_AIMED)
		atttype = ATT_AIMED_CST;
	return atttype;
}

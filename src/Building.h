#ifndef CLASS_BUILDING
#define CLASS_BUILDING

#include "FactionElement.h"

#define NUM_BUILD_VIEWS 2

/// a building that could belong to a faction
class Building : public FactionElement
{
	public:
	 /// Full Constructor
	 Building(int r, int c, std::vector < graphic::Image * > images,
			  Statsmap stats, int faction = F_NO_FACTION);
	 Building(int r, int c, std::vector < graphic::Image * > images,
			  std::vector <Component *> components, int faction = F_NO_FACTION);
	 /// Destroyer
	 ~Building() { };

	 /// set the image that should be blitted
	 void SetDirection(int direction);
};

#endif

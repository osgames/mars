#ifndef SOUNDTRACKER_H_
#define SOUNDTRACKER_H_

#include <map>
#include <string>

#include <SDL_mixer.h>

#define VOLUME_SFX 128
#define VOLUME_MUS 75

typedef std::map<std::string,  Mix_Music * > MusicMap;
typedef std::map<std::string,  Mix_Chunk * > SfxMap;

class SoundTracker
{
	private:
		/// Music player
		MusicMap _music;
		/// SFX player
		SfxMap _sfx;
		/// Audio active
		bool _active;
	protected:
	
	public:
		SoundTracker();
		~SoundTracker();
		
		/// Adds a new music file, duplicate files are not loaded
		bool AddMusic(std::string file);
		/// Plays a music file ones, -1 infinte loop
		bool PlayMusic(std::string file, int repeat = 0);
		/// Stops playing music
		void StopMusic() { Mix_FadeOutMusic(2000); };
		
		/// Adds a new sfx file, duplicate files are not loaded
		bool AddSfx(std::string file);
		/// Plays a sfx file ones, -1 infinte loop
		bool PlaySfx(std::string file, int repeat = 0);
		/// Stops playing channel or all sfx
		void StopSfx(int ch = -1) { Mix_HaltChannel(ch); };
	
};

extern SoundTracker * sounds;

#endif /*SOUNDTRACKER_H_*/

#ifndef CLASS_KEYBOARD
#define CLASS_KEYBOARD

#include <SDL.h>

#include <bitset>

/// this class keep the current status of the keabord according to input events
class Keyboard
{
	private:
		/// keys status, it's possible to access to the bitset using SDLKey values
		/// 0 -> NOT PRESSED
		/// 1 -> PRESSED
		std::bitset<SDLK_LAST> _keys;

	public:
		/// constructor
		Keyboard() { _keys.reset(); };
		/// denstructor
		~Keyboard() { };

		/// set a key as pressed
		void SetKeyPressed(SDLKey k) { _keys.set(k); };
		/// set a key as released -> not pressed
		void SetKeyReleased(SDLKey k) { _keys.set(k, 0); };

		/// return 1 if the key is pressed and 0 if it's not
		bool IsKeyPressed(SDLKey k) { return _keys.test(k); };
};

#endif

#ifndef CLASS_BLITTER
#define CLASS_BLITTER

#include "Surface.h"

namespace graphic
{

/// graphic blitter
///
/// with the current code you CAN'T blit a RGBA surface on a RGB surface with colorkey
/// this problem won't be fixed until we need it working, this for 2 reasons:
/// 1- I'm a lazy coder and I hate to do useless things
/// 2- fix it requires work (see point 1) and can give worse performance
///
/// m3xican
class Blitter
{
	private:
	 #ifdef WITH_OPENGL
	 /// main GL blit on screen instructions
	 void GLBlit(Surface * src, Sint16 x_trans, Sint16 y_trans);
	 #endif

	public:
	 /// base contructor
	 Blitter() { };
	 /// destroyer
	 ~Blitter() { };

	 // NULL -> Rect
	 /// blit an entire surface on another one using surface position
	 void Blit(Surface * src, Surface * dest = NULL);

	 // NULL -> Rect
	 /// blit an entire surface on another one NOT using surface position, but dest_rect one
	 void Blit(Surface * src, SDL_Rect & dest_rect, Surface * dest = NULL);

	 // Rect -> Rect
	 /// blit part of a surface on another one  using surface position for the destination
	 void BlitPart(Surface * src, SDL_Rect & src_rect, Surface * dest = NULL);

	 // Rect -> Rect
	 /// blit part of a surface on part of another one NOT using surface position
	 /// for the destination, but dest_rect.x and dest_rect.y
	 void BlitPart(Surface * src, SDL_Rect & src_rect,
					   SDL_Rect & dest_rect, Surface * dest = NULL);

	 /// blit the part of screen limited by src_rect on the dest surface
	 void SaveScreen(SDL_Rect & src_rect, Surface * dest);
	 /// blit part of screen limited by src_rect on part of the dest surface
	 /// limited by dest_rect
	 void SaveScreen(SDL_Rect & src_rect, SDL_Rect & dest_rect,
							 Surface * dest);
};

/// global pointer
extern Blitter * blitter;

}

#endif

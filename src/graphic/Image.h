#ifndef CLASS_IMAGE
#define CLASS_IMAGE

#include <SDL_image.h>
#include <vector>

#include "Surface.h"

#define NUM_VIEWS 8

#define MIN_TRANS 50

namespace graphic
{
/// A surface loaded from an image file
class Image : public virtual Surface
{
	protected:
	 /// mapped transparent color
	 Uint32 _trans_color;
	 /// this flag is true if the image uses a transparent color (sets  the color key)
	 bool _has_trans_color;

	 /// this flag is true if the image uses the alpha channel (for transparent background
	 /// and pixels partially transparent)
	 bool _uses_alpha;

	 /// this rectangle contains the not transparent / full visible part of the image
	 SDL_Rect * _vis_rect;
	 /// all vis rects for virtual images
	 std::vector<SDL_Rect *> _vis_rects;

	 /// set visible limits of the image
	 void SetVisibleLimits();

	public:
	 /// base constructor, Create a new image from a file name, by default no alpha
	 Image(std::string file, bool graphic_mode = SDL_GRAPHIC);
	 /// create a new image from a file name setting a color as transparent
	 Image(std::string file, SDL_Color trans_color, bool graphic_mode = SDL_GRAPHIC);
	 /// destroyer
	 ~Image()
	 { 
	 	if(_vis_rects.size() != 0)
	 	{
	 		for(unsigned int x=0; x != _vis_rects.size(); x++)
	 			delete _vis_rects[x];
	 	}
	 	else
	 		delete _vis_rect; 
	 };

	 /// return true if image use transparent color
	 bool HasTransColor() { return _has_trans_color; };
	 /// return the transparent color
	 Uint32 GetTransColor() { return _trans_color; };

	 /// return true if the image uses the alpha channel
	 bool UsesAlpha() { return _uses_alpha; };

	 /// get Surface X position (top left corner)
	 Uint16 GetVisibleX() { return _vis_rect->x; };
	 /// get Surface Y position (top left corner)
	 Uint16 GetVisibleY() { return _vis_rect->y; };
	 /// get Surface width
	 Uint16 GetVisibleW() { return _vis_rect->w; };
	 /// get Surface height
	 Uint16 GetVisibleH() { return _vis_rect->h; };

	 /// return true if the pixel at (x,y) is transparent, false otherwise
	 bool IsTrans(Uint16 x, Uint16 y);

	 /**
	  * set number of views and frames in the image, calculates virtual surface size.
	  * These surfaces have multi image SDL_surfaces that should be rendered separately
	  **/
	 void SetVirtual(int views, int frames);
	 /// Changed rendered image to frame in surface corresponding to view. Vertical changes
	 void SetVirtualView(int view);
	 /// Changed rendered image to frame in surface corresponding to frame. Horizontal changes
	 void SetVirtualFrame(int frame);
	 /// Change to next virtual frame. Horizontal changes, returns set frame
	 int NextVirtualFrame();
};

}
#endif

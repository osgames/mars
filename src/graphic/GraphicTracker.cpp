#include "GraphicTracker.h"

using namespace std;

// a new surface
graphic::Surface * graphic::GraphicTracker::GetSurface(Uint16 w, Uint16 h, bool graphic_mode, Uint32 flags, bool trans, Uint16 bpp)
{
	Surface * surf = new Surface(w, h, graphic_mode, flags, trans, bpp);
	
	_surfaces.push_back(surf);

	return surf;
}

// create a surface from a SDL_Surface
graphic::Surface * graphic::GraphicTracker::GetSurface(SDL_Surface * surface, bool graphic_mode)
{
	Surface * surf = new Surface(surface, graphic_mode);

	_surfaces.push_back(surf);

	return surf;
}

// create a new image from a file name, by default no alpha
graphic::Image * graphic::GraphicTracker::GetImage(std::string file, bool graphic_mode)
{
	Image * img;

	string key;
	key = CODE_IMG + file;

	// look for the image
	ImageMap::iterator it = _images.find(key);

	// image not found
	if(it == _images.end())
	{
		img = new Image(file, graphic_mode);
		GraphicResource * res = new GraphicResource(img);
		_images.insert(make_pair(key, res));
	}
	else
	{
		(*it).second->AddInstance();
		img = dynamic_cast <Image *>((*it).second->GetSurface());
	}

	return img;
}

// create a new image from a file name setting a color as transparent
graphic::Image * graphic::GraphicTracker::GetImage(std::string file, SDL_Color trans_color, bool graphic_mode)
{
	Image * img;

	string key;
	key = CODE_CK_IMG + file;

	// look for the image
	ImageMap::iterator it = _images.find(key);

	// image not found
	if(it == _images.end())
	{
		img = new Image(file, trans_color, graphic_mode);
		GraphicResource * res = new GraphicResource(img);
		_images.insert(make_pair(key, res));
	}
	else
	{
		(*it).second->AddInstance();
		img = dynamic_cast <Image *>((*it).second->GetSurface());
	}

	return img;
}

// create a movable surface
graphic::MovableSurface * graphic::GraphicTracker::GetMovableSurface(Uint16 w, Uint16 h, bool graphic_mode,
																									Uint32 flags, bool trans, Uint16 bpp)
{
	MovableSurface * msurf = new MovableSurface(w, h, graphic_mode, flags, trans, bpp);
	
	_mov_surfaces.push_back(msurf);

	return msurf;
}

// create a new image from a file name, by default no alpha
graphic::MovableImage * graphic::GraphicTracker::GetMovableImage(std::string file, bool graphic_mode)
{
	MovableImage * mimg;

	string key;
	key = CODE_IMG + file;

	// look for the image
	ImageMap::iterator it = _mov_images.find(key);

	// image not found
	if(it == _mov_images.end())
	{
		mimg = new MovableImage(file, graphic_mode);
		GraphicResource * res = new GraphicResource(mimg);
		_mov_images.insert(make_pair(key, res));
	}
	else
	{
		(*it).second->AddInstance();
		mimg = dynamic_cast <MovableImage *>((*it).second->GetSurface());
	}

	return mimg;
}

// create a new image from a file name setting a color as transparent
graphic::MovableImage * graphic::GraphicTracker::GetMovableImage(std::string file, SDL_Color trans_color, bool graphic_mode)
{
	MovableImage * mimg;

	string key;
	key = CODE_IMG + file;

	// look for the image
	ImageMap::iterator it = _mov_images.find(key);

	// image not found
	if(it == _mov_images.end())
	{
		mimg = new MovableImage(file, trans_color, graphic_mode);
		GraphicResource * res = new GraphicResource(mimg);
		_mov_images.insert(make_pair(key, res));
	}
	else
	{
		(*it).second->AddInstance();
		mimg = dynamic_cast <MovableImage *>((*it).second->GetSurface());
	}

	return mimg;
}

void graphic::GraphicTracker::DeleteResource(Surface * s)
{

}

// delete all the stored graphic resources
void graphic::GraphicTracker::DeleteAllResources()
{
	int num_res = _surfaces.size();
	int i;

	// delete all the Surfaces
	for(i = 0; i < num_res; i++)
		delete _surfaces[i];

	_surfaces.clear();

	// delete all the Images
	for(ImageMap::iterator it = _images.begin(); it != _images.end(); it++)
		delete (*it).second;

	_images.clear();

	// delete all the MovableSurfaces
	num_res = _mov_surfaces.size();

	for(i = 0; i < num_res; i++)
		delete _mov_surfaces[i];

	_mov_surfaces.clear();

	// delete all the MovableImages
	for(MovableImageMap::iterator it = _mov_images.begin(); it != _mov_images.end(); it++)
		delete (*it).second;

	_mov_images.clear();
}


#ifdef WITH_OPENGL
// change the graphic mode at runtime (SDL/GL)
void graphic::GraphicTracker::ChangeGraphicMode(bool g_mode)
{
	_graphic_mode = g_mode;

	int num_res = _surfaces.size();

	// change the graphic mode for all the Surfaces
	for(int i = 0; i < num_res; i++)
		_surfaces[i]->ChangeGraphicMode(g_mode);

	// change the graphic mode for all the Images
	for(ImageMap::iterator it = _images.begin(); it != _images.end(); it++)
		(dynamic_cast <Image *>((*it).second->GetSurface()))->ChangeGraphicMode(g_mode);

	// change the graphic mode for all the MovableSurfaces
	num_res = _mov_surfaces.size();

	for(int i = 0; i < num_res; i++)
		_mov_surfaces[i]->ChangeGraphicMode(g_mode);

	// change the graphic mode for all the MovableImages
	for(MovableImageMap::iterator it = _mov_images.begin(); it != _mov_images.end(); it++)
		(dynamic_cast <MovableImage *>((*it).second->GetSurface()))->ChangeGraphicMode(g_mode);
}

// update all the textures (this method is required by fucking win)
void graphic::GraphicTracker::UpdateAllTextures()
{
	int num_res = _surfaces.size();

	// update the texture for all the Surfaces
	for(int i = 0; i < num_res; i++)
		_surfaces[i]->UpdateTex();

	// update the texture for all the Images
	for(ImageMap::iterator it = _images.begin(); it != _images.end(); it++)
		(dynamic_cast <Image *>((*it).second->GetSurface()))->UpdateTex();

	// update the texture for all the MovableSurfaces
	num_res = _mov_surfaces.size();

	for(int i = 0; i < num_res; i++)
		_mov_surfaces[i]->UpdateTex();

	// update the texture for all the MovableImages
	for(MovableImageMap::iterator it = _mov_images.begin(); it != _mov_images.end(); it++)
		(dynamic_cast <MovableImage *>((*it).second->GetSurface()))->UpdateTex();
}
#endif

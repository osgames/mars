#ifndef CLASS_SCREEN
#define CLASS_SCREEN

#ifdef WITH_OPENGL
#include <SDL_opengl.h>
#endif
#include <SDL.h>
#include <iostream>
#include <sstream>
#include <string>

#include "../Exception.h"
#ifdef WITH_LIBPNG
#include "PngFunctions.h"
#endif
#define SDL_GRAPHIC 0
#define GL_GRAPHIC 1

#define GAMMA_INC 0.1

extern std::string screens_dir;

namespace graphic
{

/// This is the screen (or window) where everything is blitted, it uses SDL
/// to get an available surface
class Screen
{
	private:
		/// screen width
		int _width;
		/// screen height
		int _height;

		/// screen surface
		SDL_Surface * _surface;

		/// screen flags
		Uint32 _flags;

		/// number of screenshots taken
		int _num_screens;

		/// init the graphic contest
		void InitScreen();

		/// gamma value, it's the same for R-G-B fields
		float _gamma;

	public:
		/// empty constructor
		Screen(Uint16 w, Uint16 h, Uint32 flags = SDL_SWSURFACE);
		/// destroyer -> video surface is freed by SDL_Quit
		~Screen() { };

		/// set screen flags
		void SetFlags(Uint32 flags);
		/// get setted screen flags
		Uint32 GetFlags() { return _flags; };

		/// set screen resolution
		void SetResolution(Uint16 w, Uint16 h);

		/// go in fullscreen mode
		void GoFullScreen();
		/// go in windowed mode
		void GoWindowed();

		/// get the Bits Per Pixel format (8 - 16 - 24 - 32)
		Uint8  GetBitPP() { return _surface->format->BitsPerPixel; };
		/// get sdl_surface
		SDL_Surface * GetSurface() { return _surface; };

		/// update the entire screen
		void Update();
		/// update part of the screen
		void UpdateRect(SDL_Rect & rect);

		/// fill the surface with a color
		void Fill(SDL_Color & color);

		/// clear the screen
		void Clear();

		/// return a value that rapresents the current graphic mode
		int GetGraphicMode()
		{
			#ifdef WITH_OPENGL
			if(_flags & SDL_OPENGL)
				return GL_GRAPHIC;
			else
			#endif
				return SDL_GRAPHIC;
		};

		/// return screen width
		int GetW() { return _width; };
		/// return screen height
		int GetH() { return _height; };

		#ifdef WITH_OPENGL
		// chage the graphic contest
		void ChangeGraphicMode(bool graphic = SDL_GRAPHIC);

		// switch the graphic contest
		void SwitchGraphicMode()
		{
			if(_flags & SDL_OPENGL)
				ChangeGraphicMode(SDL_GRAPHIC);
			else
				ChangeGraphicMode(GL_GRAPHIC);
		}
		#endif

		/// take a screenshot and save the PNG file in data/screens/
		void TakeScreenshot();

		/// set the gamma value to an upper value
		void GammaUp()
		{
			_gamma += GAMMA_INC;
			SDL_SetGamma(_gamma, _gamma, _gamma);
		};
		/// set the gamma value to a lower value
		void GammaDown()
		{
			_gamma -= GAMMA_INC;
			SDL_SetGamma(_gamma, _gamma, _gamma);
		};
};

/// external pointer to the screen
extern Screen * screen;

}

#endif

#ifndef CLASS_MOVABLE_SURFACE
#define CLASS_MOVABLE_SURFACE

#include "Blitter.h"
#include "Surface.h"

namespace graphic
{

/// A surface usable for animations
class MovableSurface : public virtual Surface
{
	protected:
	 /// backup surface, this surface is used to store the background of the movable
	 /// surface in order to restore it when needed
	 Surface * _bk_surface;

	public:
	 /// empty constructor
	 MovableSurface() { };
	 /// constructor
	 MovableSurface(Uint16 w, Uint16 h, bool graphic_mode = SDL_GRAPHIC, Uint32 flags = SDL_SWSURFACE,
							bool trans = false, Uint16 bpp = screen->GetBitPP()) :
							Surface(w, h, graphic_mode, flags, trans, bpp)
	{
		// SDL SURFACE - required backup surface
		if(graphic_mode == SDL_GRAPHIC)
			_bk_surface = new Surface(w, h, graphic_mode, flags, trans, bpp);
	}
	 /// destructor
	 virtual ~MovableSurface()
	{
		// SDL SURFACE
		if(_graphic_mode == SDL_GRAPHIC && _bk_surface)
			delete _bk_surface;
	};

	 /// set INITIAL position of the upper left IntPoint of the surface (x,y)
	 /// passing a IntPoint
	 void SetInitialPosition(IntPoint position) { SetInitialPosition(position.x, position.y); };
	 /// set INITIAL position of the upper left IntPoint of the surface (x,y)
	 /// passing x and y values
	 void SetInitialPosition(Sint16 x, Sint16 y);

	 /// set the position of the upper left IntPoint of the surface (x,y) passing a IntPoint
	 void SetPosition(IntPoint position)
	 {
		_rectangle->x = position.x;
		_rectangle->y = position.y;
	 }
	 /// set the position of the upper left IntPoint of the surface (x,y) passing x and y values
	 void SetPosition(Sint16 x, Sint16 y)
	 {
		_rectangle->x = x;
		_rectangle->y = y;
	 }

	 /// move image of mov pixels along X and Y axis
	 void Move(Sint16 mov_x, Sint16 mov_y)
	 {
		// update current surface rectangle
		_rectangle->x += mov_x;
		_rectangle->y += mov_y;
	 }

	 /// move image of mov pixels along X axis
	 void MoveX(Sint16 mov) { _rectangle->x += mov; };
	 /// move image of mov pixels along Y axis
	 void MoveY(Sint16 mov) { _rectangle->y += mov; };

	 /// save background of Image before a blit
	 void SaveBackground(Surface * src_surf = NULL);
	 /// restore a saved background of Image after a blit and update _bk_rectangle
	 void RestoreBackground(Surface * dst_surf = NULL)
	 {
		// SDL SURFACE
		if(_graphic_mode == SDL_GRAPHIC)
			blitter->Blit(_bk_surface, dst_surf);
	 };

	 /// update altered rectangle of screen
	 void UpdateScreen();

	 #ifdef WITH_OPENGL
	 /// change the graphic implementation at runtime (SDL/GL)
	 void ChangeGraphicMode(bool graphic_mode = SDL_GRAPHIC);
	 #endif
};

}
#endif

#ifndef PNG_FUNCTIONS
#define PNG_FUNCTIONS

#include <SDL.h>
#include <png.h>

#include "../Exception.h"

int PngColortypeFromSurface(SDL_Surface * surface);

void PngUserWarn(png_structp ctx, png_const_charp str);

void PngUserError(png_structp ctx, png_const_charp str);

int PngSaveSurface(const char * filename, SDL_Surface * surf);

#endif

#ifndef GRAPHIC_RESOURCE
#define GRAPHIC_RESOURCE

#include "Surface.h"

#define CODE_IMG "0+"
#define CODE_CK_IMG "1+"

namespace graphic
{
/// a container for a resource managed by the GraphicTracker
class GraphicResource
{
	private:
	 /// this counter keep track of the number of occurrences for
	 /// this resource
	 int _counter;

	 /// pointer to the concrete resource
	 Surface * _res;

	public:

	 /// constructor
	 GraphicResource(Surface * res)
	 {
		_counter = 1;
		_res = res;
	 };

	 /// destroyer
	 ~GraphicResource() { delete _res; };

	 /// the tracker requests another instance of the resource => increment the counter
	 void AddInstance() { _counter++; };
	 /// the tracker requests to delete an instance of the resource => decrement the counter
	 void DelInstance()
	 {
		if(_counter)
			_counter--;
	 }

	 /// this method queries the counter and return true if there are no instances 
	 /// of the resource in use
	 bool IsEmpty() { return (_counter == 0); };

	 /// return a pointer to the stored surface
	 Surface * GetSurface() { return _res; };
};

}

#endif

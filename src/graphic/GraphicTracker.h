#ifndef CLASS_GRAPHIC_TRACKER
#define CLASS_GRAPHIC_TRACKER

#include <map>
#include <string>
#include <typeinfo>
#include <vector>

#include "GraphicResource.h"
#include "Image.h"
#include "MovableImage.h"
#include "MovableSurface.h"
#include "Surface.h"

#include "../Exception.h"

typedef std::map<std::string, graphic::GraphicResource * > ImageMap;
typedef std::map<std::string, graphic::GraphicResource * > MovableImageMap;

namespace graphic
{

/// a resource tracker for Graphic objects
class GraphicTracker
{
	private:
		/// current graphic mode (SDL or GL)
		bool _graphic_mode;

		/// Surfaces container
		std::vector < Surface * > _surfaces;
		/// Images container
		ImageMap _images;
		/// MovableSurfaces container
		std::vector < MovableSurface * > _mov_surfaces;
		/// MovableImages container
		MovableImageMap _mov_images;

	public:
		/// empty constructor
		GraphicTracker(bool g_mode = SDL_GRAPHIC) { _graphic_mode = g_mode; };
		/// destroyer
		~GraphicTracker() { DeleteAllResources(); };

		/// a new surface
		Surface * GetSurface(Uint16 w, Uint16 h, bool graphic_mode = SDL_GRAPHIC, 
							Uint32 flags = SDL_SWSURFACE, bool trans = false, Uint16 bpp = screen->GetBitPP());

		/// create a surface from a SDL_Surface
		Surface * GetSurface(SDL_Surface * surface, bool graphic_mode = SDL_GRAPHIC);

		/// create a new image from a file name, by default no alpha
		Image * GetImage(std::string file, bool graphic_mode = SDL_GRAPHIC);
		/// create a new image from a file name setting a color as transparent
		Image * GetImage(std::string file, SDL_Color trans_color, bool graphic_mode = SDL_GRAPHIC);

		/// create a movable surface
		MovableSurface * GetMovableSurface(Uint16 w, Uint16 h, bool graphic_mode = SDL_GRAPHIC,
											Uint32 flags = SDL_SWSURFACE, bool trans = false,
											Uint16 bpp = screen->GetBitPP());

		/// create a new image from a file name, by default no alpha
		MovableImage * GetMovableImage(std::string file, bool graphic_mode = SDL_GRAPHIC);

		/// create a new image from a file name setting a color as transparent
		MovableImage * GetMovableImage(std::string file, SDL_Color trans_color, bool graphic_mode = SDL_GRAPHIC);

		/// delete the passed graphic resource
		void DeleteResource(Surface * s);

		/// delete all the stored graphic resources
		void DeleteAllResources();

		#ifdef WITH_OPENGL
		/// change the graphic mode at runtime (SDL/GL)
		void ChangeGraphicMode(bool g_mode = SDL_GRAPHIC);

		// switch the graphic contest
		void SwitchGraphicMode()
		{
			if(_graphic_mode == GL_GRAPHIC)
				ChangeGraphicMode(SDL_GRAPHIC);
			else
				ChangeGraphicMode(GL_GRAPHIC);
		}

		/// update all the textures (this method is required by fucking win)
		void UpdateAllTextures();
		#endif
};

/// external pointer to the GraphicTracker
extern GraphicTracker * gtracker;

}
#endif

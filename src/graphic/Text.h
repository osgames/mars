#ifndef CLASS_TEXT
#define CLASS_TEXT

#include <sstream>
#include <string>

#include <SDL_ttf.h>

#include "../Exception.h"
#include "Blitter.h"
#include "GraphicTracker.h"
#include "Surface.h"

#define NUM_CHARS 223
#define FIRST_CHAR 32
#define LAST_CHAR 255

#define TEXT_LEFT 	0
#define TEXT_CENTER	1
#define TEXT_RIGHT 	2

namespace graphic
{

/// this class manage a text loading all the characters needed to write
class Text
{
	private:
	 /// surfaces of all the characters
	 Surface * _surfaces[NUM_CHARS];
	 /// opacity value of the text, the value range is defined between
	 /// SDL_ALPHA_TRANSPARENT and SDL_ALPHA_OPAQUE
	 Uint8 _opacity;

	public:
	 /// constructor
	 Text(std::string font, SDL_Color color, int size, bool graphic_mode = SDL_GRAPHIC);
	 /// destroyer
	 ~Text() { };

	 /// write a text starting from (x, y) on a Surface
	 void WriteText(std::string text, Sint16 x, Sint16 y, int pos = TEXT_LEFT, Surface * dest = NULL);
	 /// write a text starting from start IntPoint on a Surface
	 void WriteText(std::string text, IntPoint & start, int pos = TEXT_LEFT, Surface * dest = NULL)
	 {
		WriteText(text, start.x, start.y, pos, dest);
	 };

	 /// write a number starting from (x, y) on a Surface
	 void WriteNumber(int num, Sint16 x, Sint16 y, int pos = TEXT_LEFT, Surface * dest = NULL)
	 {
		 std::ostringstream os1;
		 os1 << num;
		 WriteText(os1.str(), x, y, pos, dest);
	 };
	 /// write a number starting from start IntPoint on a Surface
	 void WriteNumber(int num, IntPoint & start, int pos = TEXT_LEFT, Surface * dest = NULL)
	 {
		WriteNumber(num, start.x, start.y, pos, dest);
	 };

	 /// return the height of a charachter (it's the same for all the chars, so it's also the height 
	 /// of a text line
	 Uint16 GetCharH() { return _surfaces[0]->GetH(); };

	 /// return the width of a text
	 Uint16 GetTextW(std::string txt);
	 /// return the width of a number
	 Uint16 GetNumberW(int num)
	 {
		 std::ostringstream os1;
		 os1 << num;
		 return GetTextW(os1.str());
	 };

	 /// set the opacity of the text
	 void SetOpacity(Uint8 opacity)
	 {
		 _opacity = opacity;

		 for(int i = 0; i < NUM_CHARS; i++)
			_surfaces[i]->SetOpacity(opacity);
	 }
	 /// get the opacity value
	 Uint8 GetOpacity() { return _opacity; };
};

}
#endif

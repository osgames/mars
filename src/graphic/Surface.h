#ifndef CLASS_SURFACE
#define CLASS_SURFACE

#ifdef WITH_OPENGL
#include <SDL_opengl.h>
#endif
#include <SDL.h>
#include <cmath>

#include "Screen.h"

#include "../DataTypes.h"
#include "../Exception.h"
#include "guichan/color.hpp"

// value used by SetOpacity SDL code
#define MAX_REST 5

#define ALPHA_BPP 32
#define NO_ALPHA_BPP 24

namespace graphic
{

/// A graphic surface
class Surface
{
	protected:
	 /// rectangle that represent the surface (x,y - w,h)
	 SDL_Rect * _rectangle;
	 /// surface data stored in a SDL struct
	 SDL_Surface * _surface;
	 /// rectangle of the virtual surface
	 SDL_Rect * _virtual;

	 /// graphic mode of the Surface
	 bool _graphic_mode;

	 /// flag setted for transparent surfaces
	 bool _trans;

	 /// opacity value of the surface, the value range is defined between
	 /// SDL_ALPHA_TRANSPARENT and SDL_ALPHA_OPAQUE
	 Uint8 _opacity;

#ifdef WITH_OPENGL
	 // GL DATA
	 /// Texture identificator
	 GLuint _tex_num;
	 /// Real texture dimension (in case padding is required)
	 int _wreal, _hreal;
	 /// this flag is true if the texture needs an update
	 bool _need_tex_update;

	 /// Check for max texture size exceeding
	 bool CheckTexSize(SDL_Surface * surface);
	 /// Make a texture from a surface
	 bool Surf2Tex(SDL_Surface * surface);
#endif

	public:
	 /// empty constructor
	 Surface();
	 /// a new surface
	 Surface(Uint16 w, Uint16 h, bool graphic_mode = SDL_GRAPHIC, Uint32 flags = SDL_SWSURFACE,
						bool trans = false, Uint16 bpp = screen->GetBitPP());

	 /// create a surface from a SDL_Surface
	 Surface(SDL_Surface * surface, bool graphic_mode = SDL_GRAPHIC);

	 /// destroyer
	 virtual ~Surface()
	{ 
		delete _rectangle;
		if(_virtual)
			delete _virtual;
		SDL_FreeSurface(_surface);
	};

	 /// set position of the upper left IntPoint of the surface (x,y)
	 /// passing a IntPoint
	 void SetPosition(IntPoint & position)
	 {
		 _rectangle->x = position.x;
		 _rectangle->y = position.y;
	 };
	 /// set position of the upper left IntPoint of the surface (x,y)
	 /// passing x and y values
	 void SetPosition(Sint16 x, Sint16 y)
	 {
		 _rectangle->x = x;
		 _rectangle->y = y;
	 };
	 
	 /// True if surface consist of a multi image surface, uses virtual
	 bool IsVirtual() { return _virtual; };
	 
	 /// set X value of the upper left point of the surface
	 void SetX(Sint16 x) { _rectangle->x = x; };
	 /// set Y value of the upper left point of the surface
	 void SetY(Sint16 y) { _rectangle->y = y; };

	 /// get Surface X position (top left corner)
	 Sint16 GetX() { return _rectangle->x; };
	 /// get Surface Y position (top left corner)
	 Sint16 GetY() { return _rectangle->y; };
	 /// get Surface width
	 Uint16 GetW() { if(_virtual) return _virtual->w; return _rectangle->w; };
	 /// get Surface height
	 Uint16 GetH() { if(_virtual) return _virtual->h; return _rectangle->h; };
	 /// get Virtual X position (top left corner)
	 Sint16 GetVirtualX() { return _virtual->x; };
	 /// get Virtual Y position (top left corner)
	 Sint16 GetVirtualY() { return _virtual->y; };
	 /// get Virtual number of frames
	 Sint16 GetVirtualFrames() { return _surface->w / _rectangle->w; };

	 /// Texture update from backup surface
	 void UpdateTex();

	 /// fill the surface with a color
	 void Fill(SDL_Color & color);
	 /// fill part of the surface with a color
	 void Fill(SDL_Color & color, SDL_Rect & rect);

	 /// set the opacity of the surface
	 void SetOpacity(Uint8 opacity);
	 /// get the opacity value
	 Uint8 GetOpacity() { return _opacity; };

	 /// get data from the pixel at (x,y) - NEED Lock/Unlock
	 Uint32 GetPixel(Sint16 x, Sint16 y);
	 /// get color from the pixel at (x,y) - NEED Lock/Unlock
	 gcn::Color GetPixelColor(Sint16 x, Sint16 y);
	 /// set data of the pixel at (x,y) - NEED Lock/Unlock
	 void SetPixel(Sint16 x, Sint16 y, Uint32 data);
	 /// set color of the pixel at (x,y) - NEED Lock/Unlock
	 void SetPixelColor(Sint16 x, Sint16 y, gcn::Color color);

	 /// return Alpha value of a pixel at (x,y) - NEED Lock/Unlock
	 Uint8 GetAlpha(Sint16 x, Sint16 y);

	 /// return true if the point p is inside the surface
	 bool IsInside(IntPoint & p) { return IsInside(p.x, p.y); };
	 /// return true if the point (x,y) is inside the surface
	 bool IsInside(Sint16 x, Sint16 y)
	 {
		 if(x >= _rectangle->x && x <= (_rectangle->x + _rectangle->w) &&
			y >= _rectangle->y && y <= (_rectangle->y + _rectangle->h))
			 return true;
		 else
			 return false;
	 };

	 /// lock the surface
	 void Lock()
	 {
		 if(SDL_MUSTLOCK(_surface))
			 SDL_LockSurface(_surface);
	 };
	 /// relase the previous lock
	 void Unlock()
	 {
		 if(SDL_MUSTLOCK(_surface))
			 SDL_UnlockSurface(_surface);
	 };

#ifdef WITH_OPENGL
	 /// return true if the texture needs an update
	 bool NeedTexUpdate() { return _need_tex_update; };
	 /// set tex update flag to true
	 void RequestTexUpdate() { _need_tex_update = true; };

	 /// change the graphic mode at runtime (SDL/GL)
	 virtual void ChangeGraphicMode(bool graphic_mode = SDL_GRAPHIC);
#endif

	 /// Blitter is a friend of Surface so to access to _imp
	 friend class Blitter;
};

}

#endif

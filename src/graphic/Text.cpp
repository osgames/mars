#include "Text.h"

// empty constructor
graphic::Text::Text(std::string font, SDL_Color color, int size, bool graphic_mode)
{
	TTF_Font * ttf_font;
	SDL_Surface * sdl_surf;
	Surface * surf;
	char c[] = " ";

	// loading the font
	ttf_font = TTF_OpenFont(font.c_str(), size);
	// an error occurred loading the font
	if(ttf_font == NULL)
		throw Exception("Text", TTF_GetError());

	for(int i = 0; i < NUM_CHARS ; i++)
	{
		// rendering the font on the SDL_Surface
		sdl_surf = TTF_RenderText_Blended(ttf_font, c, color);
		// an error is occurred rendering the character surface
		if(sdl_surf == NULL)
			throw Exception("Text", TTF_GetError());

		surf = gtracker->GetSurface(sdl_surf, graphic_mode);
		_surfaces[i] = surf;

		c[0]++;
	}

	// close the font
	TTF_CloseFont(ttf_font);
}

// write a text starting from (x, y) on a Surface
void graphic::Text::WriteText(std::string text, Sint16 x, Sint16 y, int pos, Surface * dest)
{
	Sint16 text_x = x;
	Sint16 text_y = y;

	int len = text.length();
	int ind;
	int align_offset = 0;

	if(pos == TEXT_CENTER)
		align_offset = GetTextW(text) / 2;
	else if(pos == TEXT_RIGHT)
		align_offset = GetTextW(text);

	for(int i = 0; i < len; i++)
	{
		if(text[i] == '#' || text[i] == '\n')
		{
			text_x = x;
			text_y += _surfaces[0]->GetH();
		}
		else
		{
			ind = (text[i] & 0xff) - ' ';

			if(ind >=  0 && ind < NUM_CHARS)
			{
				_surfaces[ind]->SetPosition(text_x - align_offset, text_y);

				blitter->Blit(_surfaces[ind], dest);

				text_x += _surfaces[ind]->GetW();
			}
		}
	}
}

// return the width of a text
Uint16 graphic::Text::GetTextW(std::string txt)
{
	Uint16 max_len = 0;
	Uint16 len = 0;

	int str_len = txt.length();
	int char_ind;

	// read all the chars
	for(int i = 0; i < str_len; i++)
	{
		// end of the line, check for its length
		if(txt[i] == '\n')
		{
			if(len > max_len)
				max_len = len;

			len = 0;
		}
		else
		{
			char_ind = (txt[i] & 0xff) - ' ';

			// add the width of the current char
			if(char_ind >=  0 && char_ind < NUM_CHARS)
				len +=_surfaces[char_ind]->GetW();
		}
	}

	if(len > max_len)
		max_len = len;

	return max_len;
}


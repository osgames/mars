#include "MovableSurface.h"

// set INITIAL position of the upper left IntPoint of the surface (x,y) passing x and y values
void graphic::MovableSurface::SetInitialPosition(Sint16 x, Sint16 y)
{
	// setting rectangle position
	_rectangle->x = x;
	_rectangle->y = y;

	// SDL SURFACE - setting backup rectangle position
	if(_graphic_mode == SDL_GRAPHIC)
		_bk_surface->SetPosition(x, y);
}

// save background of Image before a blit
void graphic::MovableSurface::SaveBackground(Surface * src_surf)
{
	//cout << "graphic::MovableSurface::SaveBackground(Surface * src_surf)" << endl;
	// SDL SURFACE - setting backup rectangle position
	if(_graphic_mode == SDL_GRAPHIC)
	{
		// using a temporary rectangle to preserve data integrity
		SDL_Rect temp_r;

		temp_r.x = _rectangle->x;
		temp_r.y = _rectangle->y;
		temp_r.w = _rectangle->w;
		temp_r.h = _rectangle->h;

		// blit from screen
		if(src_surf == NULL)
			blitter->SaveScreen(temp_r, _bk_surface);
		// blit from a src_surface to _bk_surface
		else
			blitter->Blit(src_surf, temp_r, _bk_surface);
	}
}

// update altered rectangle of screen
void graphic::MovableSurface::UpdateScreen()
{
	// SDL SURFACE - setting backup rectangle position
	if(_graphic_mode == SDL_GRAPHIC)
	{
		// backup and update rectangles
		SDL_Rect bk_r, update_r;

		bk_r.x = _bk_surface->GetX();
		bk_r.y = _bk_surface->GetY();
		bk_r.w = _bk_surface->GetW();
		bk_r.h = _bk_surface->GetH();

		// get the minimum X,Y
		update_r.x = (_rectangle->x < bk_r.x) ? _rectangle->x : bk_r.x;
		update_r.y = (_rectangle->y < bk_r.y) ? _rectangle->y : bk_r.y;

		// calculate the movement along X axis
		if(_rectangle->x == bk_r.x)
			update_r.w = _rectangle->w;
		else
			update_r.w = abs(_rectangle->x - bk_r.x) + _rectangle->w;

		// calculate the movement along Y axis
		if(_rectangle->y == bk_r.y)
			update_r.h = _rectangle->h;
		else
			update_r.h = abs(_rectangle->y - bk_r.y) + _rectangle->h;

		// refresh screen
		screen->UpdateRect(update_r);

		// update bk_surface position
		_bk_surface->SetPosition(_rectangle->x, _rectangle->y);
	}
}

#ifdef WITH_OPENGL
// change the graphic implementation at runtime (SDL/GL)
void graphic::MovableSurface::ChangeGraphicMode(bool graphic_mode)
{
		graphic::Surface::ChangeGraphicMode(graphic_mode);

		// SDL SURFACE - required backup surface
		if(graphic_mode == SDL_GRAPHIC)
		{
			_bk_surface = new Surface(_rectangle->w, _rectangle->h, _graphic_mode, _surface->flags,
													_trans, _surface->format->BitsPerPixel);

			_bk_surface->SetPosition(_rectangle->x, _rectangle->y);
		}
		// GL SURFACE - delete backup surface
		else
			delete _bk_surface;
}
#endif


#ifndef CLASS_MOVABLE_IMAGE
#define CLASS_MOVABLE_IMAGE

#include "Blitter.h"
#include "Image.h"
#include "MovableSurface.h"

namespace graphic
{

/// A surface loaded from an image file
class MovableImage :  public MovableSurface, public Image
{
	public:
	 /// base constructor, Create a new image from a file name, by default no alpha
	 MovableImage(std::string file, bool graphic_mode = SDL_GRAPHIC) : Image(file, graphic_mode)
	 {
		if(graphic_mode == SDL_GRAPHIC)
			_bk_surface = new Surface(_rectangle->w, _rectangle->h, graphic_mode, _surface->flags, 
													_trans, _surface->format->BitsPerPixel);
	 };
	 /// create a new image from a file name setting a color as transparent
	 MovableImage(std::string file, SDL_Color trans_color, bool graphic_mode = SDL_GRAPHIC) :
		Image(file, trans_color,graphic_mode )
	 {
		if(graphic_mode == SDL_GRAPHIC)
		 	_bk_surface = new Surface(_rectangle->w, _rectangle->h, graphic_mode, _surface->flags, 
													_trans, _surface->format->BitsPerPixel);
	 };
	 /// destroyer
	 ~MovableImage() { };

	 /// change the graphic implementation at runtime (SDL/GL)
	 /// this method is not really required, cause everything should be managed by the
	 /// MovableSurface class, but this declaration is safer
	 #ifdef WITH_OPENGL
	 void ChangeGraphicMode(bool graphic_mode = SDL_GRAPHIC)
	 {
		graphic::MovableSurface::ChangeGraphicMode(graphic_mode);
	 }
	 #endif
};

}
#endif

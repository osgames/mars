#ifndef CLASS_PATH_NODE
#define CLASS_PATH_NODE

#include "DataTypes.h"

/// a node that make a path
class PathNode
{
	private:
	 /// cell indexes of the node
	 CellInd _indexes;
	 /// direction that an element has to follow passing this node
	 int _direction;
	 /// cost needed to pass through the node
	 int _cost;

	public:
	 /// base constructor, inits default attributes
	 PathNode(int row, int col)
	 {
		 _indexes.row = row;
		 _indexes.col = col;
		 _cost = 0;
		 _direction = 0;
	 }
	 /// constructor, inits attributes
	 PathNode(int row, int col, int cost, int direction)
	 {
		 _indexes.row = row;
		 _indexes.col = col;
		 _cost = cost;
		 _direction = direction;
	 }
	 /// Destroyer
	 virtual ~PathNode() { };

	 /// set cost value
	 void SetCost(int cost) { _cost = cost; };
	 /// set the row
	 void SetRow(int row) { _indexes.row = row; };
	 /// set the col
	 void SetCol(int col) { _indexes.col = col; };

	 /// set the direction that an element has to follow passing this node
	 void SetDirection(int direction) { _direction = direction; };

	 /// return the direction that an element has to follow passing this node
	 int GetDirection() { return _direction; };

	 /// return cost value
	 int GetCost() { return _cost; };

	 /// return cell row
	 int GetRow() { return _indexes.row; };
	 /// return cell col
	 int GetCol() { return _indexes.col; };
};
#endif

#include "IsoMouse.h"

// constructor
IsoMouse::IsoMouse(std::vector< graphic::MovableImage * > pointers, IsoTilesMap * tm, Sint16 x, Sint16 y) : Mouse(pointers, x, y)
{
	// flag that is true when the current cell change
	_changed_cell = false;

	// information about tiles map
	_square_side = tm->GetTileH() * SQRT2;
	_num_rows = tm->GetNumRows();
	_num_cols = tm->GetNumCols();
	_origin = new IntPoint;
	_origin->x = tm->GetOriginX();
	_origin->y = tm->GetOriginY();

	// orthogonal position of the mouse cursor
	_ortho_pos = new FloatPoint;
	_ortho_pos->x = 0;
	_ortho_pos->y = 0;

	// indexes of the current cell pointed by the mouse
	_cur_cell = new CellInd;
	_cur_cell->row = -1;
	_cur_cell->col = -1;
	_prev_cell = new CellInd;
	_prev_cell->row = -1;
	_prev_cell->col = -1;

	SetIndexes();
}

// destroyer
IsoMouse::~IsoMouse()
{
	delete _origin;
	delete _cur_cell;
	delete _prev_cell;
	delete _ortho_pos;
}

void IsoMouse::SetIndexes()
{
	FloatPoint * scr;

	scr = iso2scr(*_abs_position, *_origin);

	// store ortho position
	_ortho_pos->x = scr->x;
	_ortho_pos->y = scr->y;

	// store previous cell
	_prev_cell->row = _cur_cell->row;
	_prev_cell->col = _cur_cell->col;

	// row and col of the IntPoint
	_cur_cell->row = (int)(scr->y / _square_side);
	_cur_cell->col = (int)(scr->x / _square_side);

	// negative adjustement
	if(scr->y < 0)
		_cur_cell->row--;
	if(scr->x < 0)
		_cur_cell->col--;

	// check if the cell is changed
	if(_prev_cell->row != _cur_cell->row || _prev_cell->col != _cur_cell->col)
		_changed_cell = true;
	else
		_changed_cell = false;

	delete scr;
}

// return the current cell indexes
CellInd * IsoMouse::GetCell()
{
	CellInd * cell = new CellInd;

	cell->row = _cur_cell->row;
	cell->col = _cur_cell->col;

	return cell;
}

// return the previous cell indexes
CellInd * IsoMouse::GetPrevCell()
{
	CellInd * cell = new CellInd;

	cell->row = _prev_cell->row;
	cell->col = _prev_cell->col;

	return cell;
}

// return true if the mouse is inside the isometric map
bool IsoMouse::IsInsideMap()
{
	if(_cur_cell->row >= 0 && _cur_cell->row < _num_rows &&
	   _cur_cell->col >= 0 && _cur_cell->col < _num_cols )
		return true;
	else
		return false;
}

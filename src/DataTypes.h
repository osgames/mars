#ifndef DATA_TYPES
#define DATA_TYPES

#include <SDL.h>

/// a point (Sint16)
typedef struct IntPoint
{
	Sint16 x;
	Sint16 y;
} IntPoint;

/// a point (float)
typedef struct FloatPoint
{
	float x;
	float y;
} FloatPoint;

/// indexes of a cell [row, col]
typedef struct CellIndexes
{
	int row;
	int col;
} CellInd;

/// 2D Dimension (width, height)
typedef struct Dimension
{
	Uint16 w;
	Uint16 h;
} Dimension;

/// temporary options struct
typedef struct Options
{
	Dimension ris;
	int choice;
} Options;

#endif

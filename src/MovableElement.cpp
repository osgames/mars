#include "MovableElement.h"

// move the element to the [dest_x, dest_y] point
// return true when the destination is reached
bool MovableElement::MoveTo(Sint16 dest_x, Sint16 dest_y, int mov)
{
	// current distance by the destination
	int x_diff = dest_x - _position.x;
	int y_diff = dest_y - _position.y;

	//Slow down the move speed when traveling short distances
	if( (_direction == NORTH | _direction == EAST | _direction == SOUTH | _direction == WEST))
		mov -= mov / 3;
	
	int mov_x = mov;
	// mov_x is odd
	if(mov_x % 2)
	{
		// mov_x == 1
		if(mov_x == 1)
			mov_x++;
		// mov_x > 2
		else
			mov_x--;
	}

	int mov_y = mov / 2;
	
	// destination reached
	if(x_diff == 0 && y_diff == 0)
		return true;

	if(x_diff < 0)
	{
		if(x_diff <= -mov_x)
			_position.x -= mov_x;
		else
			_position.x += x_diff;
	}
	else
	{
		if(x_diff >= mov_x)
			_position.x += mov_x;
		else
			_position.x += x_diff;
	}

	if(y_diff < 0)
	{
		if(y_diff <= -mov_y)
			_position.y -= mov_y;
		else
			_position.y += y_diff;
	}
	else
	{
		if(y_diff >= mov_y)
			_position.y += mov_y;
		else
			_position.y += y_diff;
	}

	return false;
}


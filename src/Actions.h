#ifndef ACTIONS_H_
#define ACTIONS_H_

/// These are the actions used to communicate between the gui and engine.
/// The guis xml file should use these names as eventId for buttons.

	#define ACT_DONOTHING		""
	#define ACT_QUIT    		"quit"
	#define ACT_EXIT    		"exit" 
	#define ACT_GAMMAUP   		"gammaup" 
	#define ACT_GAMMADOWN  		"gammadown"
	#define ACT_FULLSCREEN 		"fullscreen"
	#define ACT_WINDOWED   		"windowed"
	#define ACT_800x600   		"800x600"
	#define ACT_1024x768   		"1024x768"
	#define ACT_1280x960   		"1280x960"
	#define ACT_1024x640   		"1024x640"
	#define ACT_1280x800   		"1280x800"
	#define ACT_1680x1050  		"1680x1050"
	#define ACT_GRIDSWITCH 		"gridswitch" 
	#define ACT_VFLIP   		"Vflip" 
	#define ACT_GLSWSWITCH 		"glswswitch"
	#define ACT_GLRENDER		"openglrender"
	#define ACT_SDLRENDER		"sdlrender"
	#define ACT_SCREENSHOT 		"screenshot"
	#define ACT_ENDMOVE			"endmove"
	#define ACT_ENDTURN			"endturn"
	#define ACT_FORCEENDTURN	"forceendturn"
	#define ACT_SELECTMODE  	"Deselect"
	#define ACT_MOVEMODE    	"Move"
	#define ACT_ROTATEMODE  	"Rotate"
	#define ACT_ATTACKMODE  	"Attack"
	#define ACT_SELECTCANNON	"selectcannon"
	#define ACT_SELECTROCKET	"selectrocket"
	#define ACT_SELECTQUICK		"selectquick"
	#define ACT_SELECTNORMAL	"selectnormal"
	#define ACT_SELECTAIMED		"selectaimed"
	#define ACT_DESELECT		"deselect"
	#define ACT_HIDEACTION		"hideaction"
	#define ACT_ATTACK			"attack"
	#define ACT_MOVE			"move"
	#define ACT_DEPLOYSELECT	"deployselect"
	#define ACT_DEPLOYELEMENT	"deployelement"


#endif /*ACTIONS_H_*/

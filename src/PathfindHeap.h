#ifndef CLASS_PATHFIND_HEAP
#define CLASS_PATHFIND_HEAP

#include <algorithm>
#include <vector>

#include "PathfindNode.h"

/// binary heap used by the pathfind (head = min(F))
class PathfindHeap
{
	private:
	 /// the heap
	 std::vector < PathfindNode * > _heap;

	public:
	 /// constructor
	 PathfindHeap() { };
	 /// destroyer
	 ~PathfindHeap() { };

	 /// add a node to the heap
	 void AddNode(PathfindNode * node);
	 /// return the head element and remove it from the heap
	 PathfindNode * GetHead();
	 /// return the element identified by passed id
	 PathfindNode * SearchNode(int id);

	 /// return the current number of nodes
	 int GetNumElements() { return _heap.size(); };

	 /// return true if the heap is empty
	 bool IsEmpty() { return _heap.empty(); };

	 /// destroy and removes all the elements from the heap
	 void Clear();

	 /// rebuild the heap restoring its properties
	 void RestoreHeap() { make_heap(_heap.begin(), _heap.end(), GreaterPathfindNode()); };
};

#endif

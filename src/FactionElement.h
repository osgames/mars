#ifndef CLASS_FACTION_ELEMENT
#define CLASS_FACTION_ELEMENT

#include <vector>

#include "Element.h"

// objects id, 
#define F_NO_FACTION	1

/* FACTIONS */
#define F_MERCENARIES	2
#define F_NGG			3
#define F_SWA			4
#define F_TG			5
#define F_MARTIANS		6
#define F_ALIENS		7


/// Element that could belong to a faction
class FactionElement : public virtual Element
{
	private:
		/// id of the faction
		int _faction;
		/// icon image of the element
		graphic::Image * _icon;

	public:
		/// Constructor that specify a faction
		FactionElement(int r, int c, std::vector < graphic::Image * > images, int faction = F_NO_FACTION)
		{
			// set faction
			_faction = faction;

			// set the icon for the element
			_icon = images[(images.size() - 1)];
		};

		/// Destroyer
		virtual ~FactionElement() { };

		/// return the faction id
		int GetFaction() { return _faction; };
		/// set the faction id
		void SetFaction(int faction) { _faction = faction; };

		/// return the icon image
		graphic::Image * GetIcon() { return _icon; };
};

#endif

#include "PlayerContainer.h"

using namespace std;
using namespace graphic;
using namespace xml;


// Places containers units in a group at row,col
void PlayerContainer::PlaceUnitGroup(int row, int col)
{
	int x = row;
	
	for( std::vector<Element *>::iterator it = _elements.begin(); it != _elements.end(); it++ )
	{
		if((*it)->GetType() == UNIT)
		{
			// Move unit until it is at destination
			_om->MoveObject((*it), x, col);
			(*it)->SetPosition(_om->GetObjX((*it)), _om->GetObjY((*it)));
			(*it)->SetDirection(DirectionTowardsCenter(*it));
			x++;
		}
	}
}

void PlayerContainer::RestoreStat(string stat)
{
	for(std::vector<Element *>::iterator it = _elements.begin(); it != _elements.end(); it++)
		(*it)->RestoreCurStat(stat);
}

// Calculates the visibilty for the objectmap with containers Elements
void PlayerContainer::ComputeVisibility()
{
	// reset TilesMap visibility
	_om->ResetMapVisibility();

	for(std::vector<Element *>::iterator it = _elements.begin(); it != _elements.end(); it++)
	{
		//TODO Should first check what elements have vision, 
		// these operations should not be hard coded but set from scripts or xml files
		if((*it)->GetType() == UNIT)
			_om->ComputeObjVisibility((*it), (*it)->GetCurStat("vision"), (*it)->GetDirection());
		else if((*it)->GetType() == BUILDING)
			_om->ComputeObjVisibility((*it), (*it)->GetCurStat("vision"));
	}	
}

/*---- Private ----*/

// TODO create this method
int PlayerContainer::DirectionTowardsCenter(Element * sel_unit)
{
	return SOUTH;
}


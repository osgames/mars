#include "InputManager.h"

void InputManager::Run()
{
	KeyEvent k;
	MouseEvent m;
	WindowEvent w;

	while(SDL_PollEvent(&_event))
	{
		// notify the raw event to all the listeners
		for(int i = 0; i < _num_rls ; i++)
		{
			if(_rls[i]->IsListening())
				_rls[i]->RawEvent(_event);
		}		
		
		switch(_event.type)
		{
			// mouse moved
			case SDL_MOUSEMOTION:
				m.SetPosition(_event.motion.x, _event.motion.y);
				m.SetRelMotion(_event.motion.xrel, _event.motion.yrel);
				m.SetButton(_button);

				// notify the event to all the listeners
				for(int i = 0; i < _num_mls ; i++)
				{
					if(_mls[i]->IsListening())
						_mls[i]->MouseMoved(m);
				}
				break;

			// mouse button pressed
			case SDL_MOUSEBUTTONDOWN:
				m.SetPosition(_event.button.x, _event.button.y);
				m.SetRelMotion(0, 0);
				m.SetButton(_event.button.button);

				// the button is pressed -> update the status
				_button |= _event.button.button;

				// notify the event to all the listeners
				for(int i = 0; i < _num_mls ; i++)
				{
					if(_mls[i]->IsListening())
						_mls[i]->MousePressed(m);
				}
				break;

			// mouse button released
			case SDL_MOUSEBUTTONUP:
				m.SetPosition(_event.button.x, _event.button.y);
				m.SetRelMotion(0, 0);
				m.SetButton(_event.button.button);

				// the button is released -> update the status
				_button &= ~_event.button.button;

				// notify the event to all the listeners
				for(int i = 0; i < _num_mls ; i++)
				{
					if(_mls[i]->IsListening())
						_mls[i]->MouseReleased(m);
				}
				break;

			// key pressed
			case SDL_KEYDOWN :
				// set the key
				k.SetKey(_event.key.keysym.sym);

				// notify the event to all the listeners
				for(int i = 0; i < _num_kls ; i++)
				{
					if(_kls[i]->IsListening())
						_kls[i]->KeyPressed(k);
				}
				break;

			// key released
			case SDL_KEYUP :
				// set the key
				k.SetKey(_event.key.keysym.sym);

				// notify the event to all the listeners
				for(int i = 0; i < _num_kls ; i++)
				{
					if(_kls[i]->IsListening())
						_kls[i]->KeyReleased(k);
				}
				break;

			// quit button of the window pressed
			case SDL_QUIT:
				// notify the event to all the listeners
				for(int i = 0; i < _num_wls ; i++)
				{
					if(_wls[i]->IsListening())
					_wls[i]->QuitAsked();
				}
				break;

			// focus events
			case SDL_ACTIVEEVENT:
				w.SetType(_event.active.state);

				if(_event.active.gain == 0)
				{
					// notify the event to all the listeners
					for(int i = 0; i < _num_wls ; i++)
					{
						if(_wls[i]->IsListening())
							_wls[i]->FocusLost(w);
					}
				}
				else
				{
					// notify the event to all the listeners
					for(int i = 0; i < _num_wls ; i++)
					{
						if(_wls[i]->IsListening())
							_wls[i]->FocusGained(w);
					}
				}
				break;
				
			// Resize window event
			case SDL_VIDEORESIZE:
				w.SetSize(_event.resize.h,_event.resize.w);
				// notify the event to all the listeners
				for(int i = 0; i < _num_wls ; i++)
				{
					if(_wls[i]->IsListening())
						_wls[i]->WindowResize(w);
				}

			default:
				break;
		}
	}
}

#ifndef CLASS_INPUT_MANAGER
#define CLASS_INPUT_MANAGER

#include <vector>

#include "SDL.h"
#include "KeyListener.h"
#include "KeyEvent.h"
#include "MouseListener.h"
#include "MouseEvent.h"
#include "WindowListener.h"
#include "WindowEvent.h"
#include "RawListener.h"

/// Input manager, execute the event loop and generates events
class InputManager
{
	private:
		SDL_Event _event;

		/// this vector stores a pointer to all the key listeners
		std::vector < KeyListener * > _kls;
		/// number of key listeners
		int _num_kls;

		/// this vector stores a pointer to all the mouse listeners
		std::vector < MouseListener * > _mls;
		/// number of mouse listeners
		int _num_mls;

		/// this vector stores a pointer to all the window listeners
		std::vector < WindowListener * > _wls;
		/// number of window listeners
		int _num_wls;
		
		/// this vector stores a pointer to all the raw listeners
		std::vector < RawListener * > _rls;
		/// number of Raw listeners
		int _num_rls;

		/// store the mouse buttons status in order to fix the problem with the status 
		/// during the mouse motion
		Uint8 _button;

	public:
		/// constructor
		InputManager()
		{
			_num_kls = _num_mls = _num_wls = _num_rls = 0;
			_button = 0;
		};
		/// destructor
		~InputManager() { };

		/// run PollEvent and distribute events to registered observers
		void Run();

		/// add a key listener as observer
		void AddKeyListener(KeyListener * k)
		{
			_num_kls++;
			_kls.push_back(k);
		};
		/// add a mouse listener as observer
		void AddMouseListener(MouseListener * m)
		{
			_num_mls++;
			_mls.push_back(m);
		};
		/// add a window listener as observer
		void AddWindowListener(WindowListener * w)
		{
			_num_wls++;
			_wls.push_back(w);
		};
		/// add a raw listener as observer
		void AddRawListener(RawListener * r)
		{
			_num_rls++;
			_rls.push_back(r);
		};

		// remove all the listeners from the queues
		void ClearListeners()
		{
			_kls.clear();
			_mls.clear();
			_wls.clear();
			_rls.clear();

			_num_kls = _num_mls = _num_wls = _num_rls = 0;
			_button = 0;
		}

		/// remove the key listener passed as pointer if it's registered as observer
		bool RemoveKeyListener(KeyListener * k);

		/// remove the mouse listener passed as pointer if it's registered as observer
		bool RemoveMouseListener(MouseListener * m);

		/// remove the window listener passed as pointer if it's registered as observer
		bool RemoveWindowListener(WindowListener * w);
		
		/// remove the window listener passed as pointer if it's registered as observer
		bool RemoveRawListener(RawListener * r);
};

extern InputManager * im;

#endif

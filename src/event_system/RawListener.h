#ifndef CLASS_RAW_LISTENER
#define CLASS_RAW_LISTENER

#include <SDL.h>

/// this is the RawListener interface, every class interesteds in window events
/// has to implement this interface
class RawListener
{
	private:
		bool _listening;

	public:
		RawListener() { _listening = true; };
		virtual ~RawListener() { };

		virtual void RawEvent(SDL_Event &e) { };

		void StartListening() { _listening = true; };
		void StopListening() { _listening = false; };

		bool IsListening() { return _listening; };
};

#endif

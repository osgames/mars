#ifndef CLASS_KEY_LISTENER
#define CLASS_KEY_LISTENER

#include "KeyEvent.h"

/// this is the KeyListener interface, every class interesteds in key events
/// has to implement this interface
class KeyListener
{
	private:
		bool _listening;

	public:
		KeyListener() { _listening = true; };
		virtual ~KeyListener() { };

		virtual void KeyPressed(KeyEvent & e) { };
		virtual void KeyReleased(KeyEvent & e) { };

		void StartListening() { _listening = true; };
		void StopListening() { _listening = false; };

		bool IsListening() { return _listening; };
};

#endif

#ifndef CLASS_WINDOW_LISTENER
#define CLASS_WINDOW_LISTENER

#include "WindowEvent.h"

/// this is the WindoListener interface, every class interesteds in window events
/// has to implement this interface
class WindowListener
{
	private:
		bool _listening;

	public:
		WindowListener() { _listening = true; };
		virtual ~WindowListener() { };

		virtual void QuitAsked() { };

		virtual void FocusLost(WindowEvent & e) { }
		virtual void FocusGained(WindowEvent & e) { }
		
		virtual void WindowResize(WindowEvent & e) { }

		void StartListening() { _listening = true; };
		void StopListening() { _listening = false; };

		bool IsListening() { return _listening; };
};

#endif

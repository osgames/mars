#ifndef CLASS_MOUSE_LISTENER
#define CLASS_MOUSE_LISTENER

#include "MouseEvent.h"

/// this is the MouseListener interface, every class interesteds in mouse events
/// has to implement this interface
class MouseListener
{
	private:
		bool _listening;

	public:
		MouseListener() { _listening = true; };
		virtual ~MouseListener() { };

		virtual void MouseMoved(MouseEvent & e) { };
		virtual void MousePressed(MouseEvent & e) { };
		virtual void MouseReleased(MouseEvent & e) { };

		void StartListening() { _listening = true; };
		void StopListening() { _listening = false; };

		bool IsListening() { return _listening; };
};

#endif

#ifndef CLASS_ATT_HIGHLIGHTER
#define CLASS_TT_HIGHLIGHTER

#include "IsoTilesMap.h"
#include "IsoMouse.h"
#include "Layer.h"
#include "Element.h"
#include "AttackSystem.h"

#include "graphic/Image.h"
#include "graphic/Blitter.h"

#define GREEN 100
#define GREEN_YELLOW 80
#define YELLOW 60
#define ORANGE 40
#define RED 20
#define BLACK 0


/// this is the layer that shows the highlighter for unit attacking
class AttHighlighter : public Layer
{
	private:
		/// images showed as highlighter
		std::vector < graphic::Image * > _images;
		/// a pointer to the selected unit
		Element * _unit;
		/// a pointer to the tiles map
		IsoTilesMap * _map;

		/// next direction to show
		int _direction;
		/// cost of an attack
		int _att_cost;
		
		/// a pointer to isometric mouse
		IsoMouse * _mouse;
		
		AttackSystem * _attacksystem;
		/// Moves input row and col in units direction
		void MoveInDirection(int * row, int * col, int steps=0);
		/// Get color for tile, depending on optimal range value
		int GetColor(int optimal);

	public:
		/// constructor
		AttHighlighter(std::vector < graphic::Image * > images, IsoTilesMap * map);
		/// destroyer
		~AttHighlighter() { };

		/// store a pointer to the selected unit
		void SetSelected(Element * unit) { _unit = unit; };
		/// set the  pointer to the selected unit to NULL
		void ClearSelected() { _unit = NULL; };

		/// set the direction of the rotation to highlight
		void SetDirection(int dir) { _direction = dir; };

		/// set the cost required by the attack
		void SetAttCost(int cost) { _att_cost = cost; };
		
		/// set active isomouse
		void SetMouse(IsoMouse * mouse) { _mouse = mouse; };

		/// blit part of the layer on a surface, if the surface is NULL blit on screen
		void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);
		
		/// blit full weapon range data onto a surface
		void BlitFullRange(SDL_Rect limits_rect, graphic::Surface * dst_surf);
		
		/// blit mouse hover effect
		void BlittMouseHover(SDL_Rect limits_rect, graphic::Surface * dst_surf);

		int GetDirection() { return _direction; };
		
		/// Get cost to perform current active attack
		int GetCost() { return _att_cost; };
		
		/// Set active attacksystem
		void SetAttackSystem(AttackSystem *attsys) { _attacksystem = attsys; };
		/// Get active attacksystem
		AttackSystem * GetAttackSystem() { return _attacksystem; };
};

#endif

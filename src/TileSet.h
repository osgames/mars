#ifndef CLASS_TILE_SET
#define CLASS_TILE_SET

#include <algorithm>
#include <vector>
#include <string>
#include <dirent.h>

#include "graphic/GraphicTracker.h"
#include "graphic/Image.h"

#include "Functions.h"

#define EXT_LEN 3

/// A Tile Set is a set of images that are used for tiles
class TileSet
{
	private:
	 /// this vector stores images of the tile set
	 std::vector < graphic::Image * > _images;
	 /// this vector stores files of the images
	 std::vector < std::string * > _files;
	 /// width of a tile
	 Uint16 _tile_width;
	 /// height of a tile
	 Uint16 _tile_height;

	public:
	 /// this constructor use a vector of strings to create the tileset
	 /// set trans as transparent color if it is passed
	 TileSet(std::vector < std::string > files, SDL_Color * trans = NULL, bool graph = SDL_GRAPHIC);
	 /// this constructor read all the images from a passed directory
	 /// set trans as transparent color if it is passed
	 TileSet(std::string dir, SDL_Color * trans = NULL, bool graph = SDL_GRAPHIC);

	 /// destroyer, delete all images
	 ~TileSet();

	 /// add just an image to the tile set
	 /// set trans as transparent color
	 void AddImg(std::string file, SDL_Color * trans = NULL, bool graph = SDL_GRAPHIC);

	 /// add all images contained in a passed directory
	 /// set trans as transparent color
	 void AddDirImg(std::string dir, SDL_Color * trans = NULL, bool graph = SDL_GRAPHIC);

	 /// remove the index image
	 void RemImg(int index);
	 /// remove an image range from start (included) to end (excluded)
	 void RemImgRange(int start, int end);

	 /// access to the index image
	 graphic::Image * GetImage(int index) { return _images[index]; };

	 /// get total number of images
	 int GetNumImg() { return _images.size(); };

	 /// get width of the tiles
	 Uint16 GetTileW() { return _tile_width; };
	 /// get height of the tiles
	 Uint16 GetTileH() { return _tile_height; };
};

/// binary predicate, used to compare two strings passed by pointer
struct LessStrPointer : public std::binary_function<std::string *, std::string *, bool>
{
	bool operator()(std::string * s1, std::string * s2)
	{
		return (*s1) < (*s2);
	}
};

#endif

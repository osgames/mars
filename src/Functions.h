#ifndef FUNCTIONS
#define FUNCTIONS

#include <functional>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h> 
#include <SDL_mixer.h>

#include "graphic/GraphicTracker.h"
#include "graphic/Image.h"
#include "graphic/Screen.h"
#include "graphic/Surface.h"
#include "graphic/Text.h"

#include "DataTypes.h"
#include "Element.h"
#include "Object.h"

//sin(-45°) & cos(-45°)
#define SIN_MIN_45 -0.707106
#define COS_MIN_45 0.707106

#define TAN_22_5 0.414213
#define TAN_67_5 2.414213

// temporary menu
#define MENU_X_OFFSET 40
#define HELP_X_OFFSET 40
#define HELP_X_OFFSET2 50
#define DIM_FONT_MENU 25
#define DIM_FONT_HELP1 12
#define DIM_FONT_HELP2 10
// 800x600
#define RIS1_W 800
#define RIS1_H 600
// 1024x768
#define RIS2_W 1024
#define RIS2_H 768

extern std::string data_dir;

/// return screen coordinates from iso ones origin represents the origin of
/// the isometric plane
FloatPoint * iso2scr(IntPoint iso, IntPoint origin);

/// return screen coordinates from iso ones origin represents the origin of
/// the isometric plane
FloatPoint * iso2scr(Sint16 ix, Sint16 iy, Sint16 ox, Sint16 oy);

/// return true if 2 images have an overlapping IntPoint, false otherwise
bool ImageCollide(graphic::Image & img1, graphic::Image & img2);

/// return true if 2 objects have an overlapping IntPoint, false otherwise
bool ObjectCollide(Object * ob1, Object * ob2, graphic::Image * img1, graphic::Image * img2);

/// return the relative orientation of an origin cell respect another
int RelDirection(IntPoint * origin, IntPoint * direction);
/// return the relative orientation of an origin cell respect another
int RelDirection(Sint16 ox, Sint16 oy, Sint16 dx, Sint16 dy);

/// return true if the file exists at the given location
bool ImageFileExists(std::string file);

// temporary menu
void menu(Options & op);

#endif

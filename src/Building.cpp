#include "Building.h"

using namespace graphic;

// Full Constructor
Building::Building(int r, int c, std::vector < Image * > images,
				   std::vector <Component *> components, int faction) :
			Element(r, c, components),
			FactionElement(r, c, images, faction)
{
	if(images.size() < NUM_BUILD_VIEWS)
		throw Exception("Building::Building", "a Building misses some images!");

	_type = BUILDING;

	// set all the views pointes to 0
	for(int i = 0; i < NUM_VIEWS; i++)
		_views.push_back(NULL);

	// set north and south views
	_views[NORTH] = images[0];
	_views[SOUTH] = images[1];

	// set default view
	 _img = _views[SOUTH];

	 _direction = SOUTH;
}

// set the image that should be blitted
void Building::SetDirection(int direction)
{
	if(direction != NORTH && direction != SOUTH)
		return;

	_direction = direction;
	_img = _views[direction];
}



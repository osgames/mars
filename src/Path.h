#ifndef CLASS_PATH
#define CLASS_PATH

#include <vector>

#include "DataTypes.h"
#include "PathNode.h"

#define NUM_DIRS 8
#define HNUM_DIRS 4

/// a path from a source node to destination one, it's just a list of nodes
class Path
{
	private:
	 /// the path, a list of Cell Indexes
	 std::vector < PathNode * > _path;
	 /// total cost of the path
	 int _total_cost;

	public:
	 /// Constructor
	 Path() { _total_cost = 0; };
	 /// Destroyer
	 ~Path();

	 /// add a node passing row, col and cost
	 void AddNode(int row, int col, int cost, int dir);
	 /// add a node passing a CellInd and cost
	 void AddNode(CellInd & cell, int cost, int dir) { AddNode(cell.row, cell.col, cost, dir); };

	 /// get a node of the path
	 PathNode * GetNode(int index)
	 {
		if(index < (int)_path.size())
			return _path[index];
		else
			return NULL;
	 };

	 /// return the first node and remove it from the path
	 PathNode * PopNode();

	 /// get the total cost of the path
	 int GetTotalCost() { return _total_cost; };

	 /// get the total lenght of the path
	 int GetPathLen() { return _path.size(); };

	 /// vertical flip of the path
	 void VFlip(int map_rows, int map_cols);
};

#endif

#ifndef CLASS_STATISTICAL_VALUE
#define CLASS_STATISTICAL_VALUE

/* stats */
// Elements Id number
#define STAT_ID 0
// energy / hitpoints
#define STAT_ENERGY 1
// visibility range
#define STAT_VISIBILITY 2
// action points / timeunits
#define STAT_ACTION 3
// Experience point
#define STAT_EXPERIENCE 4
// Cost of moving one tile
#define STAT_MOVEMENT_COST 5

/// A single statistical value of an element
class StatisticalValue
{
	private:
	 /// top value
	 int _max;
	 /// current value
	 int _cur;
	 /// A shared value is added with other same name shared values before returned
	 bool _shared;

	public:
		/// Constuctor
		StatisticalValue(int max, bool shared = true)
		{
			_max = max;
			_cur = max;
			_shared = shared;
		};
		/// Full constructor
		StatisticalValue(int max, int cur, bool shared = true)
		{
			_max = max;
			_cur = cur;
			_shared = shared;
		};
		/// Destroyer
		~StatisticalValue() { };

		/// return the max value
		int GetMax() { return _max; };
		/// set the max value
		void SetMax(int val) { _max = val; };
		/// add a value to the max
		void AddMax(int val) { _max += val; };
		/// subtract a value to the max
		void SubMax(int val)
		{
			if((_max - val) > 0)
				_max -= val;
			else
				_max = 0;
		};

		/// return the current value
		int GetCur() { return _cur; };
		/// set the current value
		void SetCur(int val)
		{
			if(val <= _max && val >= 0)
				_cur = val;
		};
		/// add a value to the current
		void AddCur(int val)
		{
			if(_cur + val <= _max)
				_cur += val;
			else
				_cur = _max;
		};
		/// subtract a value to the current
		void SubCur(int val)
		{
			if((_cur - val) > 0)
				_cur -= val;
			else
				_cur = 0;
		};

		/// restore the current value to the max value
		void RestoreCur() { _cur = _max; };
		
		/// True if value is set to shared
		bool IsShared() { return _shared;};
		/// Toggles shared true/false
		void ToggleShared() { _shared = !_shared;};
};

#endif

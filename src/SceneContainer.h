#ifndef CLASS_SCENE_CONTAINER
#define CLASS_SCENE_CONTAINER

#include <vector>
#include <utility>

#include "graphic/Image.h"
#include "xml/tinyxml/tinyxml.h"
#include "xml/XmlLoader.h"

#include "SceneElement.h"
#include "IsoObjectsMap.h"
#include "Functions.h"

/// store all the elements present in the scene
class SceneContainer
{
	protected:
		/// Links to this containers faction elements
		std::vector<Element *> _elements;
		/// Links to hidden elements, these are not in the game but loaded into memory
		std::vector<Element *> _hidden_elements;
		/// pointer to the IsoObjectsMap used
		IsoObjectsMap * _om;
		/// PlayerContainers start ID number, no validation that the id is unique is performed
		int _id;
		/// XmlLoader
		xml::XmlLoader * _xml;

		/// Delete Element
		virtual void Delete(std::vector<Element *>::iterator it);
		/// Delete hidden Element
		void DeleteHidden(std::vector<Element *>::iterator it);
		/// Finds the Element id in storage
		std::vector<Element *>::iterator LocateElement(int id);

	public:
		/// base constructor
		SceneContainer(int id);
		/// destructor
		virtual ~SceneContainer() { ClearElements();};

		/// store a pointer to the objects map used
		void BindObjectsMap(IsoObjectsMap * om)
		{ 
			_om = om;
			if(om != NULL)
				FillObjectsMap(); 
		};
		/// fill the objects map with elements
		void FillObjectsMap();

		/// Loads elements from xml file
		bool CreateElements(const char *xmlpath);
		/// Loads elements from xml file
		bool CreateElements(std::string xmlpath) { return CreateElements(xmlpath.c_str()); };
		/// destroy all the loaded elements
		void ClearElements();

		/// Delete Element at row,col
		void DeleteElement(int row, int col);
		/// Delete Element with ID
		void DeleteElement(int id);
		/// Delete selected element
		void DeleteElement(Element * element);

		/// Return element in row,col
		Element * GetElement(int row, int col);
		/// Return element with id
		Element * GetElement(int id);
		
		/// Return all container elements
		std::vector<Element *> GetAllElements() { return _elements; };

		/// Return element in row,col and remove from own list
		Element * GiveElement(int row, int col);
		/// Return element with id and remove from own list
		Element * GiveElement(int id);

		/// Adds a already existing Element to the container
		void AddExistingElement(Element * element);

		/// Get total number of ingame elements
		int GetNumElements() { return _elements.size(); };
		/// Get number of elements of TYPE
		int GetNumElements(int type);
		/// Get number of hidden not ingame elements
		int GetNumHiddenElements() { return _hidden_elements.size(); };
		
		/// Moves an element from Hidden to SceneContainer and adds it to IsoObjectMap
		bool UnHideElementAt(int id, int row = -1, int col = -1);
		/// Return id of hidden element nr I
		Element * GetHiddenElement(unsigned int i)
		{ 
			if(i >= _hidden_elements.size()) 
				return NULL; 
			return _hidden_elements[i]; 
		};
		
		/// return container id
		int GetContainerId() { return (int)round(_id/MAX_ELEMENTS); };
};

#endif

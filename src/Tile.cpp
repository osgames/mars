#include "Tile.h"

// tile constructor, just load the image and set default values
Tile::Tile(graphic::Image * img)
{
	_img = img;

	_walkable = true;

	_visible.reset();
	_visible.set(NOT_VISIBLE);
}

// ask to the cell if it is visible or not
int Tile::IsVisible()
{
	if(_visible.test(VISIBLE))
		return VISIBLE;
	else if(_visible.test(PARTIALLY_VISIBLE))
		return PARTIALLY_VISIBLE;
	else
		return NOT_VISIBLE;
}

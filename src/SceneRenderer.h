#ifndef CLASS_SCENE_RENDERER
#define CLASS_SCENE_RENDERER

#include <cmath>
#include <bitset>
#include <vector>

#include "graphic/Surface.h"
#include "graphic/Blitter.h"

#include "Layer.h"

// types of rendering
#define NORMAL_RENDERING 0
#define BUFFERED_RENDERING 1

// directions
#define ARW_UP 0
#define ARW_DOWN 1
#define ARW_LEFT 2
#define ARW_RIGHT 3

#define SCROLLING_H_LIMIT 15
#define SCROLLING_V_LIMIT 15

// scrolling data
#define SCROLL_SPEED 0.40f

/// this class manage the rendering of the current game scene
class SceneRenderer
{
	private:
		/// current graphic mode (SDL or GL)
		bool _graphic_mode;

		/// current view rect
		SDL_Rect * _view;

		/// upper view limit
		Sint16 _u_limit;
		/// bottom view limit
		Sint16 _b_limit;
		/// left view limit
		Sint16 _l_limit;
		/// right view limit
		Sint16 _r_limit;

		/// vector of layers to blit
		std::vector< Layer * > _layers;

		/// buffer surface;
		graphic::Surface * _buffer;
		/// flag activated if SceneRenderer use a buffer surface
		bool _use_buffer;

		/// flag that is true if the scene need to be blitted
		bool _need_blit;
		/// flag that is true if the scene need to be blitted using the buffer
		bool _need_buf_blit;
		/// flag that is true if the buffer need to be refreshed
		bool _need_buf_refresh;
		/// background color of the scene
		SDL_Color _fill_color;

		/// this flag is true during the scene scrolling
		bool _scrolling;
		/// scrolling speed
		float _scroll_speed;

		/// refresh the buffer
		void RefreshBuffer();

		/// blit the current scene on the dest surface
		void DirectBlitScene(graphic::Surface * dest = NULL);
		/// blit the current scene, using the buffer, on the dest surface
		void BufferedBlitScene(graphic::Surface * dest = NULL);

	public:
		/// constructor
		SceneRenderer(Sint16 x, Sint16 y, Uint16 w, Uint16 h, bool g_mode = SDL_GRAPHIC);
		/// destroyer
		~SceneRenderer()
		{
			if(_use_buffer)
				delete _buffer;
			delete _view;
		};

		/// blit the current scene on the dest surface
		void BlitScene(graphic::Surface * dest = NULL);

		/// add a layer to the blit vector
		void AddLayer(Layer * layer);
		/// move a layer up in the blit vector
		void MoveLayerUp(int id);
		/// move a layer down in the blit vector
		void MoveLayerDown(int id);

		/// set view rectangle
		void SetView(SDL_Rect & view)
		{
		 	_view->x = view.x;
		 	_view->y = view.y;
		 	_view->w = view.w;
		 	_view->h = view.h;
		};
		/// update view position
		void UpdateViewPosition(Sint16 x, Sint16 y);
		/// update view dimensions
		void UpdateViewDimensions(Uint16 w, Uint16 h);

		/// set view limits
		void SetLimits(Sint16 u_limit, Sint16 b_limit, Sint16 l_limit, Sint16 r_limit)
		{
			 _u_limit = u_limit;
			_b_limit = b_limit;
			_l_limit = l_limit;
			_r_limit = r_limit;
		};

		/// return upper limit
		Sint16 GetULimit() { return _u_limit; };
		/// return bottom limit
		Sint16 GetBLimit() { return _b_limit; };
		/// return left limit
		Sint16 GetLLimit() { return _l_limit; };
		/// return right limit
		Sint16 GetRLimit() { return _r_limit; };

		/// request of blit, is possible set the type of blit NORMAL or BUFFERED
		/// normal is the default
		void RequestBlit(int type = NORMAL_RENDERING)
		{
			if(type == NORMAL_RENDERING)
			 	_need_blit = true;
		 	else
				_need_buf_blit = true;
		};

		/// return true if the scene need to be blitted
		bool NeedBlit() { return (_need_blit || _need_buf_blit || (!_use_buffer)); };

		/// request of refresh the buffer and then request to reblit the scene (buffered blit)
		void RequestBufferRefresh()
		{
			_need_buf_refresh = true;
			_need_buf_blit = true;
		};
		/// return true if the buf need to be refreshed
		bool NeedBufferRefresh() { return _need_buf_refresh; };

		/// return the X value of the view rect
		Sint16 GetViewX() { return _view->x; };
		/// return the Y value of the view rect
		Sint16 GetViewY() { return _view->y; };
		/// return the view width
		Sint16 GetViewW() { return _view->w; };
		/// return the view height
		Sint16 GetViewH() { return _view->h; };

		/// return true if the scene is scrolling
		bool IsScrolling() { return _scrolling; };
		/// manage the scrolling each frame
		void Scrolling(std::bitset<4> arrows, Sint16 x, Sint16 y, Uint32 time_diff);

		/// set scrolling speed
		void SetScrollingSpeed(float speed) { _scroll_speed = speed; };

		/// tell to the SceneRenderer if use a buffer surface or not
		void UseBuffer(bool use = true);

		#ifdef WITH_OPENGL
		/// change the graphic mode at runtime (SDL/GL)
		void ChangeGraphicMode(bool g_mode = SDL_GRAPHIC)
		{
			_graphic_mode = g_mode;

			if(g_mode == SDL_GRAPHIC)
				UseBuffer();
			else
				UseBuffer(false);
		};

		// switch the graphic contest
		void SwitchGraphicMode()
		{
			if(_graphic_mode == GL_GRAPHIC)
				ChangeGraphicMode(SDL_GRAPHIC);
			else
				ChangeGraphicMode(GL_GRAPHIC);
		}

		#endif
};

#endif

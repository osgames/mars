#ifndef CLASS_TILE
#define CLASS_TILE

#include <bitset>

#include "graphic/Image.h"

#include "Object.h"

#define VIEW_MODES 3

#define NOT_VISIBLE 0
#define PARTIALLY_VISIBLE 1
#define VISIBLE 2

/// A tile of isometric map
class Tile: public Object
{
	private:
	 /// flag that says if a cell is walkable or not
	 bool _walkable;
	 /// flag that says if a cell is visible or not
	 std::bitset<VIEW_MODES> _visible;
	 /// pointer to the Image stored into the vector of tiles of the map
	 graphic::Image * _img;

	public:
	 /// constructor, just load the image and set default values
	 Tile(graphic::Image * img);
	 /// destroyer
	 ~Tile() { };

	 /// set the _walkable value
	 void SetWalkable(bool w = true) { _walkable = w; };
	 /// return true if the tile is walkable, not otherwise
	 bool IsWalkable() { return _walkable; };

	 /// set the visibility value
	 void SetVisible(int v)
	 {
		_visible.reset();
		_visible.set(v);
	 };

	 /// return visibility value
	 int IsVisible();

	 /// get image pointer of the tile
	 graphic::Image * GetImage() { return _img; };
};

#endif

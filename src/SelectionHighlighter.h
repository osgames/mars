#ifndef CLASS_SELECTION_HIGHLIGHTER
#define CLASS_SELECTION_HIGHLIGHTER

#include <vector>

#include "graphic/Image.h"

#include "Element.h"
#include "Layer.h"

#define H_SPACE 10
#define V_SPACE 10

#define SEL_NOT_USABLE_ALPHA 98
#define SEL_USABLE_ALPHA 228

#define LOCKED_IMGS_GAP 4

/// layer that shows graphic selection highlighter
class SelectionHighlighter : public Layer
{
	private:
		/// current selected object
		Element * _sel_elem;

		/// images that made the highlighter
		std::vector < graphic::Image * > _images;

		bool _usable;

	public:
		/// constructor
		SelectionHighlighter(std::vector < graphic::Image * > images);
		/// destroyer
		~SelectionHighlighter() { };

		/// store a pointer to the selected object
		void SetSelected(Element * e, bool usable)
		{
			_sel_elem = e;
			_usable = usable;
			Active();
		};
		/// set the  pointer to the selected object to NULL
		void ClearSelected()
		{
			_sel_elem = NULL;
			Active(false);
		};

		/// return true if an object is selected
		bool IsSelected() { return (_sel_elem != NULL); };

		/// blit part of the layer on a surface, if the surface is NULL blit on screen
		void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);
};

#endif

#ifndef CLASS_PLAYER_CONTAINER
#define CLASS_PLAYER_CONTAINER

#include "Unit.h"
#include "Building.h"

#include "SceneContainer.h"

/// store units and buildings of a faction/player
class PlayerContainer : public SceneContainer
{
	private:
	/// Returns the directions towards center from x,y point
	int DirectionTowardsCenter(Element * sel_unit);

	public:
	/// Base constructor
	PlayerContainer (int id) : SceneContainer(id) { };
	~PlayerContainer () { };

	/// Places units in a line from point
	void PlaceUnitGroup(int row, int col);
	
	/// Restore specific stat for all elements
	void RestoreStat(std::string stat);

	/// Computes visibility for all objects in this player container
	void ComputeVisibility();
};

#endif

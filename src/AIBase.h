#ifndef AIBASE_H_
#define AIBASE_H_

#include "Mission.h"

/// Base class for all mission AIs
class AIBase
{
	protected:
		///The mission engine
		Mission * _m;
		///Action cost
		int _act_cost;
		/// Selected unit
		Element * _selected;
		/// Selected units weapon choice
		int _weapon;
		
		/// return first idle unit
		Element * GetFirstIdle() { return _m->_players[_m->_active_player]->GetNextIdleUnit(); };
		/// return next idle unit
		Element * GetNextIdle() { return _m->_players[_m->_active_player]->GetNextIdleUnit(_selected); };
		/// lock element if it belongs to AI
		void LockOwnElement(Element * e) { _m->_players[_m->_active_player]->SetLocked(e); };	
		/*
		 *  return unit that is within wanted weapon range from selected attacker,
		 *  see AttHighlighter.h for ranges, Yellow is default (60%)
		 *  also sets the correct weapon in attacksystem, and calcs attack cost
		 */
		Element * GetDefender(int optimal = YELLOW);
		/// Moves unit toward closes enemy, until within fire range or out of AP
		Element * MoveTowardsClosest();

		
	public:
		///Defualt constructor
		AIBase(Mission *m) 
		{ 
			_m = m;
			_selected = NULL;
		};
							
		/*
		 * Virtual run for AIs
		 * default run locks first free unit and ends move
		 * */
		virtual bool Run() 
		{
			_act_cost = 0;
			// Lock first unit
			LockOwnElement(GetFirstIdle());
					
			if(GetFirstIdle()) // AI want end move, check that there are idle units
				_m->DoAction(ACT_ENDMOVE);
			else // No more idle units force end turn
				_m->DoAction(ACT_FORCEENDTURN);
			return false; 
		};
	
};

#endif /*AIBASE_H_*/

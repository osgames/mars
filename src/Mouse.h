#ifndef CLASS_MOUSE
#define CLASS_MOUSE

#include <vector>

#include "graphic/Blitter.h"
#include "graphic/MovableImage.h"

#include "DataTypes.h"

#define POINTER_BASE_1 0
#define POINTER_ATTACK_1 1
#define POINTER_ATTACK_2 2
#define POINTER_ATTACK_3 3

/// mouse management
class Mouse
{
	private:
		/// vector of mouse pointers
		std::vector < graphic::MovableImage * > _pointers;
		/// pointer to the image currently used 
		graphic::MovableImage * _cur_pointer;
		/// pointer id
		int _pointer_id;

	protected:
		/// absolute position on the map
		IntPoint * _abs_position;
		/// position on the screen
		IntPoint * _screen_position;
		/// origin of the screen view
		IntPoint * _view;

	public:
		/// constructor
		Mouse(std::vector< graphic::MovableImage * > pointers, Sint16 x = 0, Sint16 y = 0);
		/// destroyer
		virtual ~Mouse();

		/// set the current view values
		void virtual SetView(Sint16 view_x, Sint16 view_y);
		/// set the current view values passing a point
		void virtual SetView(IntPoint view) { SetView(view.x, view.y); };

		/// set the position passing just the screen position
		void virtual SetPosition(Sint16 x, Sint16 y);
		/// set the position passing just the point of screen position
		void virtual SetPosition(IntPoint pos) { SetPosition(pos.x, pos.y); };
		/// set the position passing screen position and view position
		void virtual SetPosition(Sint16 x, Sint16 y, Sint16 view_x, Sint16 view_y)
		{
			 SetView(view_x, view_y);
			SetPosition(x, y);
		};
		/// set the position passing screen position as point and view position as point
		void virtual SetPosition(IntPoint pos, IntPoint view) { SetPosition(pos.x, pos.y, view.x, view.y); };

		/// return the X screen position
		Sint16 GetX() { return _screen_position->x; };
		/// return the Y screen position
		Sint16 GetY() { return _screen_position->y; };

		/// return the absolute X position
		Sint16 GetAbsX() { return _abs_position->x; };
		/// return the absolute Y position
		Sint16 GetAbsY() { return _abs_position->y; };

		/// save the screen under the pointer in the buffer and then blit on screen
		void Blit()
		{
			_cur_pointer->SaveBackground();
			graphic::blitter->Blit(_cur_pointer);
		};

		/// restore the background blitting the buffer on screen
		void RestoreBackg() { _cur_pointer->RestoreBackground(); };

		/// restore the background, move and blit
		void UpdateAndBlit();

		/// move and blit without restoring the background
		void MoveAndBlit();

		/// set the pointer image
		void SetPointer(int p);
		/// return the id of the current pointer image
		int GetPointerId() { return _pointer_id; };

		/// update the mouse position after a VFlip of the screen
		void VFlip(int screen_w, int screen_h);
};

#endif

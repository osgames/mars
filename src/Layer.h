#ifndef CLASS_LAYER
#define CLASS_LAYER

#include "graphic/Blitter.h"
#include "graphic/Surface.h"

/// Layer of a Scene
class Layer
{
	private:
	 /// level of the layer, 0 -> bottom, x>0 -> top
	 int _level;
	 /// flag that is true if the layer need to be blitted
	 bool _active;
	 /// flag that is true if the layer is buffered
	 bool _buffered;

	protected:
	 /// width of the layer
	 Uint16 _width;
	 /// height of the layer
	 Uint16 _height;

	public:
	 /// constructor
	 Layer()
		 {
		_active = true;
		_buffered = false;
	 };
	 /// destroyer
	 virtual ~Layer() { };

	 /// blit part of the layer on a surface, if the surface is NULL blit on screen
	 virtual void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL) = 0;

	 /// blit part of the layer on a surface using dest_rect for the position, if the surface is NULL blit on screen
	 virtual void BlitLayer(SDL_Rect limits_rect, SDL_Rect dest_rect, graphic::Surface * dst_surf = NULL) { };

	 /// get level number
	 int GetLevel() { return _level; };
	 /// set level number
	 void SetLevel(int lev) { _level = lev; };
	 /// increment level number

	 void UpLevel() { _level++; };
	 /// decrement level number
	 void DownLevel()
	 {
		if(_level)
		_level--;
	 };

	 /// obtain width of the layer
	 Uint16 GetW() { return _width; };
	 /// obtain height of the layer
	 Uint16 GetH() { return _height; };

	 /// set the need of blit flag
	 void Active(bool a = true) { _active = a; };
	 /// return true if the layer need to be blitted
	 bool IsActive() { return _active; };

	 /// set the buffering flag
	 void UseBuffer(bool b = true) { _buffered = b; };
	 /// return true if the layer need to be buffered
	 bool IsBuffered() { return _buffered; };

	 /// vertical flip of the layer
	 virtual void VFlip() {};
};

#endif

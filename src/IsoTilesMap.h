#ifndef CLASS_ISO_TILES_MAP
#define CLASS_ISO_TILES_MAP

#include <ctime>
#include <cmath>

#include "graphic/Image.h"

#include "Element.h"
#include "Functions.h"
#include "Map.h"
#include "TileSet.h"
#include "Tile.h"

#define SQRT2 1.414213

/// A map of tiles
class IsoTilesMap : public Map
{
	private:
	 /// vector containing all the used images
	 TileSet * _tile_set;
	 /// number of images used by the map
	 int _num_images;
	 /// width of a tile
	 Uint16 _tile_width;
	 /// height of a tile
	 Uint16 _tile_height;
	 /// side of an orthogonal tile
	 float _square_side;

	public:
	 /// base constructor, use a map to choose tiles
	 IsoTilesMap(int num_rows, int num_cols, int *map, TileSet * ts);
	 /// random map generation constructor, no map required
	 IsoTilesMap(int num_rows, int num_cols, TileSet * ts);

	 /// destroyer
	 ~IsoTilesMap() { delete _tile_set; };

	 /// blit part of the layer on a surface, if the surface is NULL blit on screen
	 void BlitLayer(SDL_Rect limits_rect, graphic::Surface * dst_surf = NULL);

	 /// check if a IntPoint is inside the map passing 2 int
	 bool IsInsideMap(Sint16 x, Sint16 y);
	 /// check if a IntPoint is inside the map passing a IntPoint
	 bool IsInsideMap(IntPoint p) { return IsInsideMap(p.x, p.y); };

	 /// Get width of the tiles
	 Uint16 GetTileW() { return _tile_set->GetTileW(); };
	 /// Get height of the tiles
	 Uint16 GetTileH() { return _tile_set->GetTileH(); };

	 /// return X coordinate of the tile located at [r,c]
	 Sint16 GetTileX(int r, int c) { return _objects[(r * _num_cols) + c]->GetX(); };
	 /// return Y coordinate of the tile located at [r,c]
	 Sint16 GetTileY(int r, int c) { return _objects[(r * _num_cols) + c]->GetY(); };

	 /// set walkable flag at [r,c]
	 void SetWalkable(int r, int c, bool w = true) { ((Tile *)_objects[(r * _num_cols) + c])->SetWalkable(w); };
	 /// set visible flag at [r,c]
	 void SetVisibility(int r, int c, int v = VISIBLE) { ((Tile *)_objects[(r * _num_cols) + c])->SetVisible(v); };
	 /// set visible flag for all tiles
	 void SetVisibility(int v = NOT_VISIBLE)
	 {
		for(int i = 0; i < _num_cells; i++)
			((Tile *) _objects[i])->SetVisible(v);
	 };

	 /// get walkable flag from [r,c]
	 bool IsWalkable(int r, int c) { return ((Tile *)_objects[(r * _num_cols) + c])->IsWalkable(); };
	 /// get visible flag from [r,c]
	 int IsVisible(int r, int c) { return ((Tile *)_objects[(r * _num_cols) + c])->IsVisible(); };

	 /// return the cell indexes of the selected point if the selection is inside the
	 /// map, NULL otherwise
	 CellInd * GetCell(Sint16 x, Sint16 y);

	 /// vertical flip of the layer
	 void VFlip();
};

#endif

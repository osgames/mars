#include "XmlMission.h"

using namespace xml;

XmlMission::XmlMission(std::string path)
{
	TiXmlDocument * tmpxmldoc = new TiXmlDocument(path);
	TiXmlElement * firstchild;

	//	Load test
	if ( tmpxmldoc == NULL || !tmpxmldoc->LoadFile())
		throw Exception("XmlMission::XmlMission","Failed to open: " + path + " : " + tmpxmldoc->ErrorDesc());
	
	if (!(firstchild = tmpxmldoc->FirstChildElement("mission")))
		throw Exception("XmlMission::XmlMission","Failed to read mission tag: " + path + " : Root tag should be <mission>" );
	
	// Load mission options
	TiXmlElement * el = firstchild->FirstChildElement();
	for(; el; el = el->NextSiblingElement())
	{
		if(!((std::string)el->Attribute("type")).compare("int"))
			MissionSetting(el->ValueStr(), atoi(el->GetText()));
		else
			MissionSetting(el->ValueStr(), el->GetText());
	}
		
	// Clean up	
	if(tmpxmldoc != NULL)
	{
	 	tmpxmldoc->Clear();
	 	delete tmpxmldoc;
	}
}

void XmlMission::MissionSetting(std::string setting, std::string value)
{
	Stringmap::iterator it = _strings.find(setting);
	// Already exists, replace old
	if(it != _strings.end())
		_strings.erase(setting);
	_strings.insert(make_pair(setting,value));
}

void XmlMission::MissionSetting(std::string setting, int value)
{
	Intmap::iterator it = _ints.find(setting);
	// Already exists, replace old
	if(it != _ints.end())
		_ints.erase(setting);
	_ints.insert(make_pair(setting,value));	
}

std::string XmlMission::GetStringSetting(std::string setting)
{
	Stringmap::iterator it = _strings.find(setting);
	if(it == _strings.end())	
		throw Exception("XmlMission::GetStringSetting","Failed to find setting: " + setting);
	return (*it).second;
}

int XmlMission::GetIntSetting(std::string setting)
{
	Intmap::iterator it = _ints.find(setting);
	if(it == _ints.end())	
		throw Exception("XmlMission::GetIntSetting","Failed to find setting: " + setting);
	return (*it).second;
}

bool XmlMission::HasSetting(std::string setting)
{
	Stringmap::iterator it = _strings.find(setting);
	if(it != _strings.end())
		return true;
	Intmap::iterator it2 = _ints.find(setting);
	if(it2 != _ints.end())
		return true;
		
	return false;	
}

#ifndef XMLBASIC_H_
#define XMLBASIC_H_

#include "XmlElementCache.h"

#include "tinyxml/tinyxml.h"

#include "../graphic/Image.h"
#include "../graphic/GraphicTracker.h"

#include "../Functions.h"

extern std::string data_dir;

namespace xml
{

/// Base non configurable values all Elements can have
class XmlBasic : public XmlElementCache
{
	private:
	 /// Move animation frames
	 int _mf;
	 /// Cannon fire animation frames
	 int _cf;
	 /// Rocket fire animation frames
	 int _rf;
	 /// Return a direction according to tag
	 int IdDirection(std::string direction);
	 /// Return faction
	 int IdFaction(std::string faction);
	 /// Load images into the cache
	 void LoadImages(std::string path);
	
	protected:
	 /// ID's tag and if found sets the vale in XmlElementCache
	 bool IdentifyBasic(TiXmlElement *tag);	
	
	public:
	 XmlBasic() : XmlElementCache() {};
	 ~XmlBasic() {};
	 
};

}
#endif /*XMLBASIC_H_*/

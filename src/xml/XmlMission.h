#ifndef XMLMISSION_H_
#define XMLMISSION_H_

#include <map>

#include "tinyxml/tinyxml.h"
#include "../Exception.h"

namespace xml
{
	typedef std::map<std::string, std::string > Stringmap;
	typedef std::map<std::string, int > Intmap;
	
	/// Loads the Missions XML file
	class XmlMission
	{
		private:
			///String settings
			Stringmap _strings;
			///Int Settings
			Intmap _ints;
			///Load string mission settings
			void MissionSetting(std::string setting, std::string value);
			///Load int mission settings
			void MissionSetting(std::string setting, int value);
			
		public:
			///Load mission settings
			XmlMission(std::string path);
			///Get string setting
			std::string GetStringSetting(std::string setting);
			///Get int setting
			int GetIntSetting(std::string setting);
			///Check if setting exists
			bool HasSetting(std::string setting);
	};
	
}

#endif /*XMLMISSION_H_*/

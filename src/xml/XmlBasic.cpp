#include "XmlBasic.h"

using namespace std;
using namespace graphic;

bool xml::XmlBasic::IdentifyBasic(TiXmlElement *tag)
{
	if(!tag->ValueStr().compare("direction"))
		_direction = IdDirection(tag->GetText());
	else if(!tag->ValueStr().compare("faction"))
		_faction = IdFaction(tag->GetText());
	else if(!tag->ValueStr().compare("imagepath"))
		LoadImages(tag->GetText());
	else if(!tag->ValueStr().compare("rowpos"))
		_rowpos = atoi(tag->GetText());
	else if(!tag->ValueStr().compare("rowsize"))
		_rowsize = atoi(tag->GetText());
	else if(!tag->ValueStr().compare("colpos"))
		_colpos = atoi(tag->GetText());
	else if(!tag->ValueStr().compare("colsize"))
		_colsize = atoi(tag->GetText());
	else if(!tag->ValueStr().compare("id"))
		_id = atoi(tag->GetText());
	else if(!tag->ValueStr().compare("anim"))
	{
	    std::string name = tag->Attribute("name");
	    int fps = 0;
	    tag->QueryIntAttribute("fps", &fps);
	    int frames = 0;
	    tag->QueryIntAttribute("frames", &frames);
        int loops = 1;
	    tag->QueryIntAttribute("loops", &loops);
	    int inttype = ANIM_REPLACE;
	    if(tag->Attribute("type"))
	    {
            std::string strtype = tag->Attribute("type");
            if(!strtype.compare("overlay") || !strtype.compare("OVERLAY"))
                inttype = ANIM_OVERLAY;
	    }
        graphic::Image * img = gtracker->GetImage(data_dir + tag->GetText(), screen->GetGraphicMode());
        if(img)
        {
            img->SetVirtual(NUM_VIEWS, frames);
            _animations.push_back(new Animation(name, img, inttype, fps, loops));
        }
	}
	else if(!tag->ValueStr().compare("hidden"))
	{
		if( atoi(tag->GetText()) == 1 )
			_hidden = true;
		else if( !((string)tag->GetText()).compare("true"))
			_hidden = true;
	}
	else	// No basic found
		return false;
	return true;
}

int xml::XmlBasic::IdDirection(string direction)
{
	if(!direction.compare("north"))
		return NORTH;
	if(!direction.compare("north_east"))
		return NORTH_EAST;
	if(!direction.compare("east"))
		return EAST;
	if(!direction.compare("south_east"))
		return SOUTH_EAST;
	if(!direction.compare("south"))
		return SOUTH;
	if(!direction.compare("south_west"))
		return SOUTH_WEST;
	if(!direction.compare("west"))
		return WEST;
	if(!direction.compare("north_west"))
		return NORTH_WEST;

	return SOUTH;
}

int xml::XmlBasic::IdFaction(string faction)
{
	if(!faction.compare("no_faction"))
		return F_NO_FACTION;
	if(!faction.compare("mercenaries"))
		return F_MERCENARIES;
	if(!faction.compare("ngg"))
		return F_NGG;
	if(!faction.compare("swa"))
		return F_SWA;
	if(!faction.compare("tg"))
		return F_TG;
	if(!faction.compare("martians"))
		return F_MARTIANS;
	if(!faction.compare("aliens"))
		return F_ALIENS;

	return F_NO_FACTION ;
}

void xml::XmlBasic::LoadImages(string path)
{
	#ifdef DEBUG_MODE
		printf("<xml::XmlBasic::LoadImages(string path)>\nInput: %s\n", path.c_str());
	#endif	
	
	Image * img;
	unsigned int counter = 1;
	char image_name[16] = "01.png";
	bool old = false;

	// Check new or old file names
	if(ImageFileExists(data_dir + path + image_name))
		old = true;
	else
		strncpy(image_name, "still.png", sizeof(image_name));

	// Images are deleted in there target element destructor
	try
	{
		if(old)
		{
			// Load images until no new exists, One must exist or Loading fails
			while(ImageFileExists(data_dir + path + image_name))
			{
				img = gtracker->GetImage(data_dir + path + image_name, screen->GetGraphicMode());
				_views.push_back(img);

				counter++;
				sprintf(image_name, "%02i.png", counter);
			}
		}
		else
		{
			if(ImageFileExists((data_dir + path + image_name)))
			{
				img = gtracker->GetImage(data_dir + path + image_name, screen->GetGraphicMode());
				// Set number of views and frame in the view surface
				img->SetVirtual(NUM_VIEWS,1);
				// New format has only one image but Element still stores in vector to support old format
				_views.push_back(img);
			}
			else
				throw Exception("xml::XmlBasic::LoadImages", "Image: "+ data_dir + path + image_name+ " does not exist");
		}
	}
	catch(Exception e)
	{
		e.PrintError();
		exit(-1);
	}
	#ifdef DEBUG_MODE
		printf("</xml::XmlBasic::LoadImages(string path)>\n");
	#endif	
}

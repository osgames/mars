#ifndef XMLCACHE_H_
#define XMLCACHE_H_

#include <vector>
#include <map>

#include "../Component.h"

#include "../Unit.h"
#include "../Building.h"
#include "../SceneElement.h"
#include "../Animation.h"

namespace xml
{

/// Element cache, An XML Element is completely read and cached, then returned when as for
class XmlElementCache
{
	protected:
	 /// Temporary vector storage of Components before sending to Element
	 std::vector <Component *> _components;

	 /*** Base values all Element can have, not configurable ***/
	 /// <imagepath>, vector, later copied to an element
	 std::vector < graphic::Image * > _views;
	 /// <direction>, default SOUTH
	 int _direction;
	 /// <faction>, default F_NO_FACTION
	 int _faction;
	 /// <rowpos>, element row position, default 1
	 int _rowpos;
	 /// <rowsize>, element row size on the map, default 1
	 int _rowsize;
	 /// <colpos>, element colon position, default 1
	 int _colpos;
	 /// <colsize>, element colon size on the map, default 1
	 int _colsize;
	 /// <hidden>, elements that are hidden are not added to the game until unhidden, default false / 0
	 bool _hidden;
	 /// <id>, elements can have a special id instead of a increment numerical, should be a fairly high number (~100)
	 int _id;
     /// Animations
     std::vector <Animation *> _animations;

	 // What kind of Element this is
	 int _type;

	 // Elements
	 Unit * _unit;
	 Building * _build;
	 SceneElement * _scene;
	 Element * _base;

	 void ResetCache()
	 {
		_views.clear();
		_direction = SOUTH;
		_faction = F_NO_FACTION;
		_rowpos = 1;
		_rowsize = 1;
		_colpos = 1;
		_colsize = 1;
		_id = 0;
		_hidden = false;
		_unit = NULL;
		_build = NULL;
		_scene = NULL;
		_base = NULL;
		_type = SCENE_ELEMENT;
		_components.clear();
		_animations.clear();
	 };

	public:
	 XmlElementCache(){ResetCache();};
	 ~XmlElementCache(){ResetCache();};

	 /// Returns the finished loaded element
	 Element * GetElement() { return _base; };
	 /// Return the Col position of current element
	 int GetColPos() { return _colpos; };
	 /// Return the Row position of current element
	 int GetRowPos() { return _rowpos; };
	 /// Return if the element is hidden and should not be added to the game
	 bool IsHidden() { return _hidden; };
	 /// Return id, 0 is default an means no special id use default incremental instead
	 int GetSpecialId() { return _id; };
};

}

#endif /*XMLCACHE_H_*/

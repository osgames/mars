#ifndef XMLDOC_
#define XMLDOC_

#include "tinyxml/tinyxml.h"

#include "../Element.h"
#include "XmlTag.h"

/* types of xml files */
#define SCENARIO 0
#define ELEMENT 1
#define PLAYER 2
#define XMLINIT 3

extern std::string data_dir;

namespace xml
{

typedef std::map<std::string, XmlTag * > XmlTagmap;

/// Loads the XML file
class XmlDoc
{
	protected:
	 // Map with any tags that can be loaded by this XmlDoc
	 XmlTagmap _element_tags;
	 // The loaded Xml data
	 TiXmlDocument * _xmldoc;
	 // Sub XML file
	 TiXmlDocument * _subxmldoc;
	 // First tag of the document
	 TiXmlElement * _filetag;
	 // Currently used tag of the document
	 TiXmlElement * _tag;
	 // Base tag of old document
	 TiXmlElement * _basetag;
	 // What type
	 int _filetype;
	 // Opens a XML document from a path
	 void XmlSetDoc(std::string xmlpath);
	 // Uses an already open XML doc
	 void XmlSetDoc(TiXmlDocument * xmldoc);
	 /// Opens and reads subfiles that main XML file refers to
	 void SubFile(std::string xmlpath);
	 /// Return back to the original file after reading a subfile
	 void UnSubFile();
		
	private:
	 /// Loads the XML tags that will be used to load & save XML files
	 void XmlElementInit();
	 /// Reads what base tags exists (building, unit)
	 void ReadBaseTag(TiXmlElement * basetag, XmlTag * parent);
	 /// Reads what sub tags exists, these are components for base elements (weapons)
	 void ReadSubTag(TiXmlElement * subtag, XmlTag * parent);
		
	public:
	 XmlDoc() {_xmldoc = NULL; _subxmldoc = NULL;};
	 // Path based Constructor
	 XmlDoc(std::string xmlpath) {_xmldoc = NULL; _subxmldoc = NULL; XmlSetDoc(xmlpath);};
	 // Open Xml file Constructor
	 XmlDoc(TiXmlDocument * xmldoc) {_xmldoc = NULL; _subxmldoc = NULL; XmlSetDoc(xmldoc);};
	 ~XmlDoc();
};

}
#endif /*XMLDOC_*/

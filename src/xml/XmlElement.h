#ifndef XMLELEMENT_H_
#define XMLELEMENT_H_

#include "XmlDoc.h"
#include "XmlBasic.h"

namespace xml
{

/// Handles XML files for Elements, loads, caches and returns when ask for
class XmlElement : public virtual XmlDoc, public XmlBasic
{
	private:
	 /// The XmlTag group used for this current XML block
	 XmlTag * _XmlTagGroup;
	 /// temporary xmldoc for loading imported elements
	 TiXmlDocument * tmpxmldoc;
	 /// Finds the element type and sets _XmlTagGroup (configurable tags) to correct type 
	 void IdElementType();
	 /// Finds the value in _xmlTagGroup and returns the XmlTag
	 XmlTag * IdentifyElementXml(std::string ElXml);
		
	protected:
	 /// Populates the cache with values from the xml file
	 void LoadElementCache();
	 // Goto next xml block
	 bool NextElement();

	public:
	 XmlElement() : XmlBasic() {};
	 XmlElement(std::string xmldoc) : XmlBasic() {};
	 XmlElement(TiXmlDocument * xmldoc) : XmlBasic() {};
	 virtual ~XmlElement() {};
};

}
#endif /*XMLELEMENT_H_*/

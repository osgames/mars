#ifndef XMLTAG_H_
#define XMLTAG_H_

#include <string>
#include <map>
#include <utility>

namespace xml
{

#define TAG_NOTYPE 0
#define TAG_BASE 1
#define TAG_INT 2
#define TAG_STRING 3

/// An XmlTag is a representation of a configurable XML value, it can contain subtags
class XmlTag
{
	private:
	 // What type of data it should load
	 int _type;
	 // map with any subtags belonging to this tag
	 std::map<std::string, XmlTag * > _tags;
		
	public:
	 /// Empty constructor
	 XmlTag() {};
	 /// Full constructor
	 XmlTag(int atype) { _type = atype; };
	 ~XmlTag()
	 {
	 	std::map<std::string, XmlTag * >::iterator it;
	 	for(it = _tags.begin(); it != _tags.end();)
	 	{
	 		delete (*it).second;
	 		_tags.erase(it++);
	 	}
	 }
		
	 /// Get what type of data, tag contains
	 int GetType() { return _type; };
	 /// Set what type of data, tag contains
	 void SetType(int atype) { _type = atype; };
	
	 /// Get subtag
	 XmlTag * GetSub(std::string tag) 
	 {
		std::map<std::string, XmlTag * >::iterator it = _tags.find(tag);
		if( it == _tags.end())
			return NULL;
		return (*it).second; 
	 };

	/// Insert subtag
	XmlTag * InsertSubTag(std::string tag, int type)
	{
		XmlTag * aTag = NULL;
		std::pair< std::map<std::string, XmlTag * >::iterator, bool > sucess = std::make_pair(_tags.begin(),
																								true);
		sucess = _tags.insert(std::make_pair(tag, aTag = new XmlTag(type)));
		if(!sucess.second)
			throw Exception("XmlTag::InsertSubTag()","Duplicate XML tag: " + tag);
		return aTag;
	 }
};

}

#endif /*XMLTAG_H_*/

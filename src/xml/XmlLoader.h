#ifndef XMLLOADER_H_
#define XMLLOADER_H_

#include "XmlElement.h"

namespace xml
{

/// Loads data from XML files
class XmlLoader : public virtual XmlDoc, public XmlElement
{
	public:
	// Empty constructor
	XmlLoader() : XmlDoc(), XmlElement() {};
	/// String constructor
	XmlLoader(std::string xmlpath) : XmlDoc(xmlpath), XmlElement() {};
	/// Xml doc loader
	XmlLoader(TiXmlDocument * xmldoc): XmlDoc(xmldoc), XmlElement() {};
	/// Destructor
	virtual ~XmlLoader () {};

	/// Loads a XML file
	void XmlSetDoc(std::string xmlpath);
	/// Uses an already open XML file
	void XmlSetDoc(TiXmlDocument * xmldoc);
	/// Get data from next tag/element in XML file
	bool NextTag();
};

// Extern
extern XmlLoader * XmlHandler;

}
#endif /*XMLLOADER_H_*/

#include "XmlLoader.h"

using namespace std;

void xml::XmlLoader::XmlSetDoc(string xmlpath)
{
	XmlDoc::XmlSetDoc(xmlpath);
	// Are we loading Element data?	
	if(_filetype == PLAYER )
		LoadElementCache();
}

void xml::XmlLoader::XmlSetDoc(TiXmlDocument * xmldoc)
{
	XmlDoc::XmlSetDoc(xmldoc);
	// Are we loading Element data?
	if(_filetype ==  PLAYER)
		LoadElementCache();
}


/// Get data from next tag/element in XML file
bool xml::XmlLoader::NextTag()
{
	// Are there any more tag in this XML file
	if( XmlElement::NextElement())
		return true;
	// If basetag exists then jump back to old file
	else if(_basetag && _subxmldoc)
	{
		delete _subxmldoc;

		_tag = _basetag;
		_basetag = NULL;
		_subxmldoc = NULL;

		return XmlElement::NextElement();
	}
	return false;
}


/* ------- Private -------*/




#ifndef COMPONENT_H_
#define COMPONENT_H_

#include "Values.h"

class Component : public Values
{
	private:
		/// The state of the value, active or not
		bool _active;
		/// What type the component is
		std::string _type;
		
	public:
		/// Base constructor
		Component() {_active = true; };
		/// Stats constructor
		Component(Statsmap stats) {_stats = stats; _active = true; };
		/// Full constructor
		Component(Statsmap stats, Stringmap strings, std::string type) {_stats = stats; _strings = strings; _type = type; _active = true; };
		
		/// Return the components active state
		bool IsActive() { return _active; };
		/// Toggle the components active state false/true
		void ToggleActive() { _active = !_active; };	
		
		/// Get Component type
		std::string GetComponentType() { return _type;};
		/// Compare component type
		int IsComponent(std::string type) { return !_type.compare(type); };	
	
		/// Adds more stats to the Values Statsmap
		void AddStats(Statsmap stats)
		{
			for(Statsmap::iterator it = stats.begin(); it != stats.end(); it++)
				_stats.insert(*it);
		}
		/// Adds more strings to the Values Stringmap
		void AddStrings(Stringmap strings)
		{
			for(Stringmap::iterator it = strings.begin(); it != strings.end(); it++)
				_strings.insert(*it);
		}
		
		/// Destructor delete all allocated resources from Value.h
		~Component()
		{
			// delete all stats from the map
			for(Statsmap::iterator it = _stats.begin(); it != _stats.end(); it++)
				delete (*it).second;
				
	/*		for(Stringmap::iterator it = _strings.begin(); it != _strings.end(); it++)
				delete (*it).second;	
*/
			_stats.clear();
			_strings.clear();
		}
};

#endif /*COMPONENT_H_*/

#ifndef CLASS_MISSION
#define CLASS_MISSION

#include <vector>

#include "graphic/GraphicTracker.h"
#include "graphic/MovableImage.h"
#include "graphic/Screen.h"
#include "graphic/Text.h"

#include "event_system/KeyListener.h"
#include "event_system/MouseListener.h"
#include "event_system/WindowListener.h"

#include <bitset>

#include "Element.h"
#include "IsoMouse.h"
#include "Keyboard.h"
#include "Player.h"
#include "Scene.h"
#include "SceneContainer.h"
#include "SceneRenderer.h"
#include "SoundTracker.h"
#include "AttackSystem.h"
#include "IsoMiniMap.h"
#include "Actions.h"

#include "guichan.hpp"

#define EXIT_AND_CONTINUE false
#define EXIT_AND_QUIT true

extern std::string data_dir;

/// a single mission of the game
class Mission: public KeyListener, public MouseListener, public WindowListener, public gcn::ActionListener
{
	private:
		/// state of the mission
		bool _running;
		bool _paused;
		bool _exit_status;

		/// graphic layers
		Scene * _scene;
		
		/// Graphic render/blitter
		SceneRenderer * _renderer;

		/// Mouse class
		IsoMouse * _mouse;

		/// Keyboard class
		Keyboard * _keyboard;

		/// Key for scrolling
		std::bitset <4> _arrows;

		/// Gui manager, deleted in GuiLayer destructor
		gui::GuiManager * _gui;

		/// Attacksystem, deleted in _scene
		AttackSystem * _attacksystem;

		/// All players in this mission
		std::vector < Player * > _players;

		/// Currelty active player
		int _active_player;

		/// current mission turn
		int _current_turn;

		/// true of unit is usable
		bool _usable;

		/// True if shift is held down
		bool _shift;

		/// True if mouse press was done in gui, unlock at mouse release
		bool _guilock;

		/// A* pathfinder
		Pathfind * _pathfinder;
		/// Path to destination
		Path * _path;
		/// Current node(tile) in the path
		PathNode * _cur_pnode;

		/// Info text, deleted in InfText layer
		graphic::Text * _info_txt;

		/// selected element
		Element * _selected;
		/// defending element
		Element * _defender;

		/// Elapsed time since last Run()
		Uint32 _time_elapsed;
		
		/// Animated elements currently doing animations
		std::vector <AnimatedElement *> _anims;
		
		enum States{
			NOTHING,
			SELECTED,
			MOVEMODE,
			ROTATEMODE,
			ATTACKMODE,
			MOVING,
			DEPLOYING
		};
		
		/// Current game state, TODO Maybe add a real state handler instead of a varible
		States _gamestate;
		
		///TODO: resolution control should not be in Mission but at a higher level
		///Change game resolution
		void NewResolution(int x, int y);

	public:
		/// Init tilemap and mission
		Mission(int rows, int cols, int terrain);
		~Mission()
		{
			delete _scene;
			delete _renderer;
			delete _attacksystem;
			delete _mouse;
			delete _keyboard;
			delete _pathfinder;
		};
		/// True if engine is running
		bool IsRunning() { return _running; };
		/// Start mission engine
		void Start()
		{
			_running = true;
			_gui->SetPlayer(_players[_active_player]);
			_players[_active_player]->ComputeVisibility();
			_renderer->UpdateViewPosition(_players[_active_player]->GetViewX(), _players[_active_player]->GetViewY());
			// Update turn info to the gui
			char buf[128];
			snprintf(buf, 128, "Turn: %d    Idle Units: %d", _current_turn, _players[_active_player]->GetIdleUnits());
			_gui->SetText("turninfo", buf);
			_gui->UpdateMiniMap();
		};
		/// Stop mission engine
		void Stop() { _running = false; };
		/// Return exit status
		bool GetExitStatus() { return _exit_status; };
		/// Should be called every frame
		bool Run(Uint32 time_elapsed);
		/// Draws mission engine graphics
		bool Draw();
		/// Adds a player to the mission
		void AddPlayer(Player * p);
		/// Adds sceneelements to the mission
		void AddElements(SceneContainer * sc) { _scene->AddElements(sc); };
		/// Sets player as AI
		void SetAI(Player * p);

		///Start a new move within the same turn
		void NewMove();
		///Start new turn and reset all action points
		void NewTurn();

		/// Checks victory and loss conditions and ends game if fulfilled
		void CheckMissionConditions();

		// -- KEYBOARD EVENTS --
		void KeyPressed(KeyEvent & e);
		void KeyReleased(KeyEvent & e);

		// -- MOUSE EVENTS --
		void MouseMoved(MouseEvent & e);
		void MousePressed(MouseEvent & e);
		void MouseReleased(MouseEvent & e);

		// -- WINDOW EVENTS --
		void QuitAsked()
		{
			_running = false;
			_exit_status = EXIT_AND_QUIT;
		};
		void FocusLost(WindowEvent & e);
		void FocusGained(WindowEvent & e);
		void WindowResize(WindowEvent & e);

		///Return the gui used during the mission, needed to add the gui to the InputManager
		gui::GuiManager * GetGui() { return _gui;};
		/// Loads gui from xml file, no path loads default gui
		void LoadGui(std::string path = "") { _gui->ParseGui(path); _gui->InitMiniMap(_renderer); };
		///Perform actions from the gui
		void action(const gcn::ActionEvent& actionEvent);

		/// Perform an action in the mission
		void DoAction(std::string action);

		/// The AIBase is friend class to have access to mission data
		friend class AIBase;
};

#endif

#include "AnimatedElement.h"

using namespace std;
using namespace graphic;

AnimatedElement::~AnimatedElement()
{
    for( std::vector<Animation *>::iterator it = _animations.begin(); it != _animations.end(); it++ )
        delete *it;
}

void AnimatedElement::AddAnimation(Animation * anim)
{
	_animations.push_back(anim);
}


void AnimatedElement::AddAnimations(std::vector <Animation *> anims)
{
	_animations = anims;
}

void AnimatedElement::StartAnimation(string name)
{
	#ifdef DEBUG_MODE
		printf( "<AnimatedElement::StartAnimation(string name)>\nInput: %s\n", name.c_str());
	#endif
	
    _curanim = _animations.end();
    for( std::vector<Animation *>::iterator it = _animations.begin(); it != _animations.end(); it++ )
        if( (*it)->IsCalled(name))
            _curanim = it;

	if(_curanim == _animations.end())
	{
		_noanim = true;
		return;
	}
	_noanim = false;
	_img = (*_curanim)->GetAnim();
	// Set animation to first frame
	_img->SetVirtualFrame(0);
	_img->SetVirtualView(_direction);
	_minmov = 0;
	_lastposmov.x = -1;
	_lastposmov.y = -1;
	_loops = -1;

	#ifdef DEBUG_MODE
		printf( "</AnimatedElement::StartAnimation(string name)>\n");
	#endif
}

void AnimatedElement::StopAnimation()
{
	if(!_noanim)
	{
		_curanim = _animations.end();
		_img = _views[NORTH];
		SetDirection(_direction);
		_noanim = true;
		//_img->SetVirtualView(_direction);
	}
}

void AnimatedElement::CalcMinMovBeforeFrame(int x_dest, int y_dest)
{
	#ifdef DEBUG_MODE
		printf("<AnimatedElement::CalcMinMovBeforeFrame(int x_dest, int y_dest)>\nInput: %d %d\n", x_dest, y_dest);
	#endif
	 // Moving animation need a minimal distant per frame
	if(!_noanim && _curanim != _animations.end() && x_dest != -1 && y_dest != -1)
	{
		tmp = 1;
		int x = abs(x_dest - _position.x);
		int y = abs(y_dest - _position.y);
		// Largest diff is used to calculate min distant
		if(x > y)
		{
			_minmov = x / _img->GetVirtualFrames();
			_lastposmov.x = _position.x;
			NextFrame();
		}
		else
		{
			_minmov = y / _img->GetVirtualFrames();
			_lastposmov.y = _position.y;
			NextFrame();
		}
		#ifdef DEBUG_MODE
			printf("frame %d\n", tmp);
			printf("</AnimatedElement::CalcMinMovBeforeFrame(int x_dest, int y_dest)>\n");
		#endif
	}
}

void AnimatedElement::TickWalkAnim()
{
	#ifdef DEBUG_MODE
		printf("<AnimatedElement::TickWalkAnim()>\n");
	#endif	
	if(_noanim)
		return;
	/*
	 * Modulus (%) can not be used because movement is not int, modules target can be rounded off and frame missed
	 * Instead minimal distant since last frame is used
	 */

	if(_lastposmov.x != -1 && abs(_position.x -_lastposmov.x) >= _minmov)
	{
		_lastposmov.x = _position.x;
		NextFrame();
		tmp++;
		#ifdef DEBUG_MODE
			printf("frame %d\n", tmp);
		#endif

	}
	else if(_lastposmov.y != -1 && abs(_position.y - _lastposmov.y) >= _minmov)
	{
		_lastposmov.y = _position.y;
		NextFrame();
		tmp++;
		#ifdef DEBUG_MODE
			printf("frame %d\n", tmp);
		#endif
	}
	#ifdef DEBUG_MODE
		printf("</AnimatedElement::TickWalkAnim()>\n");
	#endif	
}

bool AnimatedElement::NextFrame()
{
	if(!_noanim && _curanim != _animations.end())
	{
	    // Set next frame
		if( (*_curanim)->NextFrame() == 1)
		{
		    _loops++;
		    if((*_curanim)->GetLoops())
		    {
                if(_loops == (*_curanim)->GetLoops())
                    return false; //done with all loops for this animation
            }
		}

		return true;
	}

	return false;
}

